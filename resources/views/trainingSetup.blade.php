@extends('layouts.master')
@section('title', 'Training Setup')
@section('style')
    <style>
        .calendar-table
        {

        }

    </style>
@endsection
@section('content')

    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>Training Setup</h3>
                </div>


            </div>
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Add Training<!--<small>Input different form elements</small>--></h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                            @if(Session::has('message'))

                                <div id="successMessage" class="alert alert-success alert-dismissible fade in" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                                aria-hidden="true">×</span>
                                    </button>
                                    <strong>{{ Session::get('message') }}</strong>
                                </div>
                            @endif
                        </div>
                        {{--tab view--}}
                        <div class="bs-example">
                            <div class="tabbable">
                                <ul class="nav nav-tabs">
                                    <li class="active"> <input type="button" data-toggle="tab" href="#tab1" data-loading-text="Loading..." class="btn btn-info" value="Course"></li>
                                    <li> <input type="button" data-toggle="tab" href="#tab2" data-loading-text="Loading..." class="btn btn-info" value="  Training Session  "></li>
                                    <li> <input type="button" data-toggle="tab" href="#tab3" data-loading-text="Loading..." class="btn btn-info" value="  Employee Training Session    "></li>

                                </ul>
                                <div class="tab-content">
                                    <div id="tab1" class="tab-pane active fade in">
                                        {{--fade view for add skill--}}
                                        <button onclick="myFunction()" data-backdrop="ture" id="hello" class="btn btn-primary btn-md" data-type="Skills"  data-token="{{ csrf_token() }}"  data-toggle="modal" data-target="#myModalHorizontalkk"><span class="glyphicon glyphicon-plus">
                                                    Add Course
                                                </span></button>


                                        <div class="x_content">
                                            <div class="table-responsive">
                                                <table id="datatable-buttons" class="example Course table table-striped table-bordered table-hover display">
                                                    <thead>
                                                    <tr class="alert-info">
                                                        <th>Code</th>
                                                        <th>Name</th>
                                                        <th>Coordinator</th>
                                                        <th>Trainer</th>
                                                        <th>Payment Type</th>
                                                        <th>Cost</th>
                                                        <th>Status</th>
                                                        <th>Action</th>
                                                    </tr>

                                                    </thead>


                                                    <tbody>
                                                    @foreach($course as $sk)
                                                        <tr class='item{{$sk->id}}' id="row_id{{$sk->id}}">
                                                            <td>{{$sk->code}}</td>
                                                            <td>{{$sk->name}}</td>
                                                            <td>{{$sk->coordinator}}</td>
                                                            <td>{{$sk->trainer}}</td>
                                                            <td>{{$sk->payment_type}}</td>
                                                            <td>{{$sk->cost}}</td>
                                                            <td>{{$sk->status}}</td>


                                                            <td align="center">
                                                                <button class="btn btn-primary btn-sm edit-modal-course" data-id="{{$sk->id}}" data-code="{{$sk->code}}" data-name="{{$sk->name}}" data-coordinator="{{$sk->coordinator}}" data-trainer="{{$sk->trainer}}" data-payment_type="{{$sk->payment_type}}" data-cost="{{$sk->cost}}" data-status="{{$sk->status}}" ><span class="glyphicon glyphicon-edit"></span></button>

                                                                <button class="btn confirm btn-danger btn-sm delete_course"  data-id="{{$sk->id}}" data-code="{{$sk->code}}" data-name="{{$sk->name}}" data-coordinator="{{$sk->coordinator}}" data-trainer="{{$sk->trainer}}" data-payment_type="{{$sk->payment_type}}" data-cost="{{$sk->cost}}" data-status="{{$sk->status}}" ><span class="glyphicon glyphicon-trash"></span></button>
                                                            </td>
                                                        </tr>
                                                    @endforeach

                                                    </tbody>
                                                </table>


                                            {{--Modal--}}


                                            <!-- /.modal -->

                                            </div>
                                        </div>

                                        <meta name="_token" content="{!! csrf_token() !!}"/>


                                        {{--end table view--}}
                                    </div>
                                    <div id="tab2" class="tab-pane fade">
                                        <button onclick="myTraining()"  class=" btn btn-primary btn-md" data-toggle="modal"  data-token="{{ csrf_token() }}"><span class="glyphicon glyphicon-plus">
                                                    Training Session
                                                </span></button>


                                        <div class="x_content">
                                            <div class="table-responsive">
                                                <table id="datatable-buttons" class="table-class Training_Session table table-striped table-bordered table-hover display">
                                                    <thead>
                                                    <tr class="alert-info">
                                                        <th>Name</th>
                                                        <th>Course</th>
                                                        <th>Details</th>
                                                        <th>Schedule Time</th>
                                                        <th>Duration</th>
                                                        <th>Certificate Require</th>
                                                        <th>Action</th>
                                                    </tr>
                                                    </thead>


                                                    <tbody>

                                                    @foreach($training as $train)
                                                        <tr class='item{{$train->id}}' id="row_id{{$train->id}}">
                                                            <td>{{$train->name}}</td>
                                                            <td>{{$train->course}}</td>
                                                            <td>{{$train->details}}</td>
                                                            <td>{{$train->sheduled_time}}</td>
                                                            <td>

                                                           <?php
                                                            $x = array();
                                                                $duration = $train->duration;
                                                            $x = json_decode($duration,true);
                                                            echo $x[0]." - ". $x[1];

                                                            ?>
                                                            </td>
                                                            <td>{{$train->training_certificate}}</td>


                                                            <td align="center">
                                                                <button class="btn btn-primary btn-sm edit-modal-training"   data-id="{{$train->id}}" data-training_name="{{$train->name}}" data-course="{{$train->course}}" data-details="{{$train->details}}" data-sheduled_time="{{$train->sheduled_time}}" data-duration="{{$train->duration}}" data-training_certificate="{{$train->training_certificate}}"><span class="glyphicon glyphicon-edit"></span></button>

                                                                <button class="btn confirm btn-danger btn-sm delete_training"  data-id="{{$train->id}}" data-training_name="{{$train->name}}" data-course="{{$train->course}}" data-details="{{$train->details}}" data-sheduled_time="{{$train->sheduled_time}}" data-duration="{{$train->duration}}" data-training_certificate="{{$train->training_certificate}}" ><span class="glyphicon glyphicon-trash"></span></button>
                                                            </td>
                                                        </tr>
                                                    @endforeach


                                                    </tbody>
                                                </table>


                                            </div>
                                        </div>

                                        <meta name="_token" content="{!! csrf_token() !!}"/>



                                    </div>
                                    <div id="tab3" class="tab-pane fade">
                                        <button onclick="myEmployee()" class="btn btn-primary btn-md" data-toggle="modal"  data-token="{{ csrf_token() }}"  data-type="Certification" data-target=""><span class="glyphicon glyphicon-plus">
                                                    Add Training Session
                                                </span></button>


                                        {{--table--}}
                                        <div class="x_content">
                                            <div class="table-responsive">
                                                <table id="datatable-buttons" class="table-class EmployeeTraining table table-striped table-bordered table-hover display">
                                                    <thead>
                                                    <tr class="alert-info">
                                                        <th>Employee Name</th>
                                                        <th>Training Session</th>
                                                        <th>Status</th>

                                                        <th>Action</th>
                                                    </tr>
                                                    </thead>


                                                    <tbody>

                                                  @foreach($empTrain as $emptr)

                                                        <tr class='item{{$emptr->id}}' id="row_id{{$emptr->id}}">
                                                            <td>{{$emptr->emp_Training->first_name}} {{$emptr->emp_Training->middle_name}} {{$emptr->emp_Training->last_name}}</td>
                                                            <td>{{$emptr->training_session}}</td>
                                                            <td>{{$emptr->status}}</td>

                                                            <td align="center">
                                                                <button class="btn btn-primary btn-sm edit-modal-empTrain"   data-id="{{$emptr->id}}" data-employee_id="{{$emptr->employee_id}}" data-training_session="{{$emptr->training_session}}" data-status="{{$emptr->status}}"  data-last_name="{{$emptr->emp_Training->first_name}}" data-middle_name="{{$emptr->emp_Training->middle_name}}" data-last_name="{{$emptr->emp_Training->last_name}}" ><span class="glyphicon glyphicon-edit"></span></button>
                                                                <button class="btn confirm btn-danger btn-sm delete_empTrain"  data-id="{{$emptr->id}}" data-training_session="{{$emptr->training_session}}" data-status="{{$emptr->status}}" ><span class="glyphicon glyphicon-trash"></span></button>
                                                            </td>
                                                        </tr>
                                                    @endforeach

                                                    </tbody>
                                                </table>


                                            </div>
                                        </div>





                                        <meta name="_token" content="{!! csrf_token() !!}"/>


                                        {{--end table--}}
                                    </div>
                                </div>
                            </div>
                        </div>



                    </div>
                </div>
            </div>
        </div>
    </div>



    {{--delete modal--}}

    <div class="modal fade" id="myModal" aria-labelledby="myModalLabel" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header alert-danger">
                    <button type="button" class="close" data-dismiss="modal"
                            aria-label="Close"><span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title"><span class="glyphicon glyphicon-trash"></span>
                        Confirmation Message</h4>
                </div>
                <div class="modal-body">
                    <p>Are you sure that you want to permanently delete the selected
                        element?</p>
                    <input type="hidden" id="d_id">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">No
                    </button>
                    <button type="button" class="btn btn-danger delete_course_btn delete_train_btn"
                            data-dismiss="modal" value=""
                            data-token="{{ csrf_token() }}" data-dismiss="modal">Delete
                    </button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    {{--end delete modal--}}


    {{--add form modal --}}
    <div class="modal fade open_modal" id="myModalHorizontal" tabindex="-1" role="dialog"
         aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header">
                    <button type="button" class="close closeBtn"
                            data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                        <span class="sr-only">Close</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">
                        Add Course Form
                    </h4>
                </div>

                <!-- Modal Body -->
                <div class="modal-body">
                    <div class="modal-body">

                        <form id="form" class="form-horizontal" role="form" data-parsley-validate>

                            <div class="form-group">

                                <label  class="col-sm-2 control-label">Course Code</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control required"
                                           id="code" name="code" placeholder="Enter Course Code" required>
                                </div>
                            </div>

                            <div class="form-group">

                                <label  class="col-sm-2 control-label">Course Name</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control required"
                                           id="name" name="name" placeholder="Enter Course Name">
                                </div>
                            </div>


                            <div class="form-group">

                                <label  class="col-sm-2 control-label">Coordinator Name</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control"
                                           id="coordinator" name="coordinator" placeholder="Enter Coordinator Name">
                                </div>
                            </div>



                            <div class="form-group">

                                <label  class="col-sm-2 control-label">Trainer Name</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control required"
                                           id="trainer" name="trainer" placeholder="Enter Trainer Name">
                                </div>
                            </div>

                            <div class="form-group">


                                <label class="col-sm-2 control-label" for="filter">Payment Type
                                <select class="form-control" name="payment_type" id="payment_type" >
                                    <option value="Sponsored By Company">Sponsored By Company</option>
                                    <option value="Self Sponsored">Self Sponsored</option>

                                </select>
                                </label>
                            </div>

                            <div class="form-group">

                                <label  class="col-sm-2 control-label">Cost</label>
                                <div class="col-sm-10">
                                    <input type="number" class="form-control required"
                                           id="cost" name="cost">
                                </div>
                            </div>
                            <div class="form-group">

                                <label  class="col-sm-2 control-label">Status</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control required"
                                           id="status" name="status">
                                </div>
                            </div>



                            <div class="form-group">

                                <div class="col-sm-10">
                                    <input type="hidden" class="form-control"
                                           id="id" name="id" placeholder="Enter Certification"/>
                                </div>
                            </div>



                            <div class="form-group">
                                <div class="col-sm-2 col-sm-10">
                                    <button type="button" id="save-btn-course"   data-token="{{ csrf_token() }}"  class="btn btn-primary">Add</button>
                                </div>
                            </div>

                            <div style="display:none" class="modal-footer" id="footer">
                                <button type="button" class="btn btn-default closeBtn"
                                        data-dismiss="modal">
                                    Close
                                </button>
                                <button type="button" class="btn btn-primary edit" id="edit-course">
                                    Save changes
                                </button>
                            </div>

                        </form>
                    </div>

                    <meta name="_token" content="{!! csrf_token() !!}" />
                </div>

                <!-- Modal Footer -->

            </div>

        </div>
    </div>

    
    {{--training Modal--}}

    <div class="modal fade open_modal" id="trainingModal" tabindex="-1" role="dialog"
         aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header">
                    <button type="button" class="close closeC"
                            data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                        <span class="sr-only">Close</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">
                        Add Training Form
                    </h4>
                </div>

                <!-- Modal Body -->
                <div class="modal-body">
                    <div class="modal-body">

                        <form id="form" class="form-horizontal" role="form">

                            <div class="form-group">

                                <label  class="col-sm-2 control-label">Name</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control required"
                                           id="training_name" name="name" placeholder="Enter Training Name">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Course Name</label>
                                <div class="col-md-6 col-sm-10 col-xs-12">
                                    <select class="select2_single form-control required" tabindex="-1" id="course" name="course">
                                        <option value="">Select Course Name</option>


                                        @foreach($course as $course)
                                            <option value="{{$course->name}}">{{$course->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>



                            <div class="form-group">
                                <label class="col-sm-2 control-label">Description</label>
                                <div class="col-sm-10">
                                    <textarea class="form-control" rows="5" id="details" name="details" required></textarea>

                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label">Duration</label>
                                <div class="col-sm-10">
                                    <input type="text"  name="duration" id="duration" value="01/01/2015 - 01/31/2015">
                                    <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>

                                    <script type="text/javascript">
                                        $(function() {
                                            $('input[name="duration"]').daterangepicker();
                                        });
                                    </script>

                                </div>
                            </div>

                            <div class="container">
                                <div class="row">
                                    <div class='col-sm-6'>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Schedule</label>
                                            <div class='input-group date' id='datetimepicker3'>
                                                <input type='text' name="sheduled_time" id="sheduled_time" class="form-control" />
                                                <span class="input-group-addon">
                        <span class="glyphicon glyphicon-time"></span>
                    </span>
                                            </div>
                                        </div>
                                    </div>
                                    <script type="text/javascript">
                                        $(function () {
                                            $('#datetimepicker3').datetimepicker({
                                                format: 'LT'
                                            });
                                        });
                                    </script>
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="filter">Training Certificate Require
                                    <select class="form-control" name="training_certificate" id="training_certificate" >
                                        <option value="Yes">Yes</option>
                                        <option value="No">No</option>

                                    </select>
                                </label>
                            </div>


                            <div class="form-group">

                                <div class="col-sm-10">
                                    <input type="hidden" class="form-control"
                                           id="id" name="id" placeholder="Enter Certification"/>
                                </div>
                            </div>




                            <div class="form-group">
                                <div class="col-sm-2 col-sm-10">
                                    <button type="button" id="save-btn-training"   data-token="{{ csrf_token() }}"  class="btn btn-primary">Add</button>
                                </div>
                            </div>

                            <div style="display:none" class="modal-footer" id="footer-ts">
                                <button type="button" class="btn btn-default closeC"
                                        data-dismiss="modal">
                                    Close
                                </button>
                                <button type="button" class="btn btn-primary edit" id="edit-training">
                                    Save changes
                                </button>
                            </div>

                        </form>
                    </div>

                    <meta name="_token" content="{!! csrf_token() !!}" />
                </div>

                <!-- Modal Footer -->

            </div>

        </div>
    </div>
    
    {{--training modal end--}}


    {{--empolyee training session modal--}}

    <div class="modal fade open_modal" id="employeeTrainingModal" tabindex="-1" role="dialog"
         aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header">
                    <button type="button" class="close closeT"
                            data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                        <span class="sr-only">Close</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">
                        Add Employee Training Form
                    </h4>
                </div>

                <!-- Modal Body -->
                <div class="modal-body">
                    <div class="modal-body">

                        <form id="form" class="form-horizontal" role="form">


                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Employee Name</label>
                                <div class="col-md-6 col-sm-10 col-xs-12">
                                    <select class="select2_single form-control required" tabindex="-1" id="employee_id" name="employee_id">
                                        <option value="">Select Employee</option>

                                        @foreach($employee as $emp)
                                            <option value="{{$emp->id}}">{{$emp->first_name}} {{$emp->middle_name}} {{$emp->last_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Training Session</label>
                                <div class="col-md-6 col-sm-10 col-xs-12">
                                    <select class="select2_single form-control required" tabindex="-1" id="training_session" name="training_session">
                                        <option value="">Select Training</option>


                                        @foreach($training as $train)
                                            <option value="{{$train->name}}">{{$train->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Training Session</label>
                                <div class="col-md-6 col-sm-10 col-xs-12">
                                    <select class="select2_single form-control loan" tabindex="-1" id="estatus" name="estatus">
                                        <option label="Option Group">Select Status</option>
                                             <option value="Active">Active</option>
                                             <option value="Deactive">Deactive</option>
                                    </select>
                                </div>
                            </div>


                            <div class="form-group">

                                <div class="col-sm-10">
                                    <input type="hidden" class="form-control"
                                           id="id" name="id" placeholder="Enter Certification"/>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-2 col-sm-10">
                                    <button type="button" id="save-btn-employee"   data-token="{{ csrf_token() }}"  class="btn btn-primary">Add</button>
                                </div>
                            </div>

                            <div style="display:none" class="modal-footer" id="footer-emp">
                                <button type="button" class="btn btn-default closeT"
                                        data-dismiss="modal">
                                    Close
                                </button>
                                <button type="button" class="btn btn-primary edit" id="edit-empTraining">
                                    Save changes
                                </button>
                            </div>

                        </form>
                    </div>

                    <meta name="_token" content="{!! csrf_token() !!}" />
                </div>

                <!-- Modal Footer -->

            </div>

        </div>
    </div>

@endsection
@section('script')

    <script>
        // validate a field on "blur" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':
        $('form')
            .on('blur', 'input[required], input.optional, select.required', validator.checkField)
            .on('change', 'select.required', validator.checkField)
            .on('keypress', 'input[required][pattern]', validator.keypress);

        $('.multi.required').on('keyup blur', 'input', function() {
            validator.checkField.apply($(this).siblings().last()[0]);
        });


        $('form').submit(function(e) {
            e.preventDefault();
            var submit = true;

            // evaluate the form using generic validaing
            if (!validator.checkAll($(this))) {
                submit = false;
            }

            if (submit)
                this.submit();

            return false;
        });

    </script>

    <script>
        $(document).ready(function(){
            $(".nav-tabs a").click(function(){
                $(this).button('loading').delay(500).queue(function(){
                    $(this).button('reset');
                    $(this).dequeue();
                });
            });
        });

    </script>


    <script>

        function  myFunction() {

            $("#myModalHorizontal").modal('show');

            //$('#footer').hide();
        }

    </script>

    <script>
        $('.closeBtn').click(function(){

            $('#name').val('');
            $('#code').val('');
            $('#coordinator').val('');
            $('#trainer').val('');
            $('#payment_type').val('');
            $('#cost').val('');
            $('#status').val('');
            $('#footer').hide('');
            $('#save-btn-course').show();


            $('input[type="text"],select.required').each(function () {
                if ($.trim($(this).val()) == '') {

                    $(this).css({
                        "border": "",
                        "background": ""
                    });
                }

            });



        });
    </script>

    <script>
        $("#save-btn-course").click(function() {
            //  alert("hello");
            $.ajaxSetup ({
                // Disable caching of AJAX responses
                cache: false
            });
            $.ajax({
                type: 'get',
                url: 'addCourse',
                data: {
                    '_token': $('input[name=_token]').val(),
                    'code': $('#code').val(),
                    'name' : $('#name').val(),
                    'coordinator': $('#coordinator').val(),
                    'trainer' : $('#trainer').val(),
                    'payment_type' : $('#payment_type').val(),
                    'cost' : $('#cost').val(),
                    'status' : $('#status').val()

                },
                success: function(data) {
                    console.log(data);

                    if ((data.errors)){
                        $('.error').removeClass('hidden');
                        $('.error').text(data.errors.name);
                    }
                    else {
                        $('.error').addClass('hidden');
                        $('.Course').append("<tr class='item" + data.id + "'><td>" + data.code + "</td><td>" + data.name + "</td><td>" + data.coordinator + "</td><td>" + data.trainer + "</td><td>" + data.payment_type + "</td><td>" + data.cost + "</td><td>" + data.status + "</td><td align='center'><button class='edit-modal-course btn btn-info' data-id='" + data.id + "' data-code='" + data.code+ "' data-name='" + data.name + "'  data-coordinator='" + data.coordinator+ "'data-trainer='" + data.trainer+ "'data-payment_type='" + data.payment_type+ "'data-cost='" + data.cost+ "'data-status='" + data.status+ "' ><span class='glyphicon glyphicon-edit'></span></button> <button class='delete-modal btn btn-danger delete_course' data-id='" + data.id + "' data-code='" + data.code+ "' data-name='" + data.name + "'  data-coordinator='" + data.coordinator+ "'data-trainer='" + data.trainer+ "'data-payment_type='" + data.payment_type+ "'data-cost='" + data.cost+ "'data-status='" + data.status+ "' ><span class='glyphicon glyphicon-trash'></span></button></td></tr>");
                        $("#myModalHorizontal").modal('hide');

                    }
                },
            });

        });

    </script>

    <script>

        $(document).on('click', '.edit-modal-course', function() {

            $('#id').val($(this).data('id'));
            $('#name').val($(this).data('name'));
            $('#code').val($(this).data('code'));
            $('#coordinator').val($(this).data('coordinator'));
            $('#trainer').val($(this).data('trainer'));
            $('#payment_type').val($(this).data('payment_type'));
            $('#cost').val($(this).data('cost'));
            $('#status').val($(this).data('status'));

            $('#myModalHorizontal').modal('show');
            $('#save-btn-course').hide();
            $('#footer').css('display','block');
            $('#name').css({
                "border": "",
                "background": ""
            });

            $('#edit-course').click(function(){


                $.ajax({
                    type: 'get',
                    url: './editCourse',
                    data: {
                        '_token': $('input[name=_token]').val(),
                        'id': $("#id").val(),
                        'code': $('#code').val(),
                        'name' : $('#name').val(),
                        'coordinator': $('#coordinator').val(),
                        'trainer' : $('#trainer').val(),
                        'payment_type' : $('#payment_type').val(),
                        'cost' : $('#cost').val(),
                        'status' : $('#status').val()
                    },
                    success: function(data) {
                        $('.item' + data.id).replaceWith("<tr class='item" + data.id + "'><td>" + data.code + "</td><td>" + data.name+ "</td><td>" + data.coordinator+ "</td><td>" + data.trainer+ "</td><td>" + data.payment_type+ "</td><td>" + data.cost+ "</td><td>" + data.status+ "</td><td align='center'><button class='edit-modal-course btn btn-info' data-id='" + data.id + "' data-code='" + data.code+ "' data-name='" + data.name+ "'  data-coordiantor='" + data.coordinator+ "' data-tariner='" + data.trainer+ "'data-payment_type='" + data.payment_type+ "'data-cost='" + data.cost+ "' data-status='" + data.status+ "' ><span class='glyphicon glyphicon-edit'></span></button> <button class='delete-modal btn btn-danger delete_course'data-id='" + data.id + "' data-name='" + data.name + "' data-description='" + data.description + "'  data-type='" + data.type+ "'><span class='glyphicon glyphicon-trash'></span></button></td></tr>");
                        $('#myModalHorizontal').modal('hide');
                        $('#id').val($(this).data(''));
                        $('#code').val($(this).data(''));
                        $('#name').val($(this).data(''));
                        $('#coordinator').val($(this).data(''));
                        $('#trainer').val($(this).data(''));
                        $('#payment_type').val($(this).data(''));
                        $('#cost').val($(this).data(''));
                        $('#status').val($(this).data(''));

                        $('#save-btn-course').show();
                        $('#footer').css('display','none');
                      //  $("#myModalHorizontal").modal('hide');

                    }
                });

            });




        });
    </script>
    <script>
        $(document).on("click", ".delete_course", function () {
            var did = $(this).data('id');
            $("#d_id").val(did);
            $('#myModal').modal('show');
        });

        $('.delete_course_btn').click(function () {

            var _id = $("#d_id").val();
            var id = _id;

            var url = "deleteCourse";

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            })
            $.ajax({
                type: 'DELETE',
                url: url + '/' + id,
                success: function (data) {
                    console.log(id);
                    $(".item" + id).remove();
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });

        })
    </script>

    <script>
        $("#save-btn-course").click(function (e) {
            var isValid = true;
            $('input[type="text"].required').each(function () {
                if ($.trim($(this).val()) == '') {
                    isValid = false; /* Required class style */
                    $(this).css({
                        "border": "1px solid red",
                        "background": "#FFCECE"
                    });
                }
                else { /* Required class style removed */
                    $(this).css({
                        "border": "",
                        "background": ""
                    });
                }
            });
            if (isValid == false)
                e.preventDefault();
            else{

            }

        });
    </script>



{{--taring session --}}
    
    <script>
    
    function myTraining(){
        $('#trainingModal').modal('show');

        
    }
    </script>

    <script>
        $("#save-btn-training").click(function() {
            //  alert("hello");
            $.ajaxSetup ({
                // Disable caching of AJAX responses
                cache: false
            });
            $.ajax({
                type: 'get',
                url: 'addTraining',
                data: {
                    '_token': $('input[name=_token]').val(),
                    'name' : $('#training_name').val(),
                    'course': $('#course').val(),
                    'details' : $('#details').val(),
                    'duration' : $('#duration').val(),
                    'sheduled_time' : $('#sheduled_time').val(),
                    'training_certificate' : $('#training_certificate').val()

                },
                success: function(data) {
                    console.log(data);

                    if ((data.errors)){
                        $('.error').removeClass('hidden');
                        $('.error').text(data.errors.name);
                    }
                    else {

                        obj = JSON.parse(data.duration);

                        $('.error').addClass('hidden');
                        $('.Training_Session').append("<tr class='item" + data.id + "'><td>" + data.name + "</td><td>" + data.course + "</td><td>" + data.details + "</td><td>" + data.sheduled_time + "</td><td>" + obj[0]+' - '+obj[1] + "</td><td>" + data.training_certificate + "</td><td align='center'><button class='edit-modal-training btn btn-info' data-id='" + data.id + "' data-name='" + data.name+ "' data-course='" + data.course+ "'  data-details='" + data.details+ "'data-sheduled_time='" + data.sheduled_time+ "'data-duration='" + obj + "'data-training_certificate='" + data.training_certificate+ "'><span class='glyphicon glyphicon-edit'></span></button> <button class='delete-modal btn btn-danger delete_course' data-id='" + data.id + "' data-name='" + data.name+ "' data-details='" + data.details+ "'  data-duration='" + data.duration+ "'data-sheduled_time='" + data.sheduled_time+ "'data-training_certificate='" + data.training_certificate+ "'><span class='glyphicon glyphicon-trash'></span></button></td></tr>");
                        $("#trainingModal").modal('hide');

                    }
                },
            });

        });

    </script>
    <script>

        $(document).on('click', '.edit-modal-training', function() {



            $('#id').val($(this).data('id'));
            $('#training_name').val($(this).data('training_name'));
            $('#course').val($(this).data('course'));
            $('#details').val($(this).data('details'));
            $('#duration').val($(this).data('duration'));
            $('#sheduled_time').val($(this).data('sheduled_time'));
            $('#training_certificate').val($(this).data('training_certificate'));

            $('#trainingModal').modal('show');
            $('#save-btn-training').hide();
            $('#footer-ts').css('display','block');
            $('#name').css({
                "border": "",
                "background": ""
            });

            $('#edit-training').click(function(){


                $.ajax({
                    type: 'get',
                    url: './editTraining',
                    data: {
                        '_token': $('input[name=_token]').val(),
                        'id': $("#id").val(),
                        'name': $('#training_name').val(),
                        'course': $('#course').val(),
                        'details' : $('#details').val(),
                        'duration' : $('#duration').val(),
                        'sheduled_time' : $('#sheduled_time').val(),
                        'training_certificate' : $('#training_certificate').val()

                    },
                    success: function(data) {
                        obj = JSON.parse(data.duration);
                        $('.item' + data.id).replaceWith("<tr class='item" + data.id + "'><td>" + data.name + "</td><td>" + data.course+ "</td><td>" + data.details+ "</td><td>" + data.sheduled_time+ "</td><td>" +  obj[0]+ ' - '+obj[1] + "</td><td>" + data.training_certificate+ "</td><td align='center'><button class='edit-modal-course btn btn-info' data-id='" + data.id + "' data-name='" + data.name+ "' data-course='" + data.course+ "'  data-details='" + data.details+ "' data-sheduled_time='" + data.sheduled_time+ "'data-duration='" + data.duration+ "'data-training_certificate='" + data.training_certificate+ "' ><span class='glyphicon glyphicon-edit'></span></button> <button class='delete-modal btn btn-danger delete_course'data-id='" + data.id + "' data-name='" + data.name+ "' data-course='" + data.course+ "'  data-details='" + data.details+ "' data-sheduled_time='" + data.sheduled_time+ "'data-duration='" + data.duration+ "'data-training_certificate='" + data.training_certificate+ "'><span class='glyphicon glyphicon-trash'></span></button></td></tr>");
                        $('#trainingModal').modal('hide');
                        $('#id').val($(this).data(''));

                        $('#name').val($(this).data(''));
                        $('#course').val($(this).data(''));
                        $('#details').val($(this).data(''));
                        $('#duration').val($(this).data(''));
                        $('#sheduled_time').val($(this).data(''));
                        $('#training_certificate').val($(this).data(''));

                        $('#save-btn-training').show();
                        $('#footer-ts').css('display','none');
                        //  $("#myModalHorizontal").modal('hide');

                    }
                });

            });
        });
    </script>
    <script>
        $('.closeC').click(function(){

            /*training modal clear*/


            $('#training_name').val('');
            $('#course').val('');
            $('#details').val('');
            $('#duration').val('');
            $('#sheduled_time').val('');
            $('#training_certificate').val('');
            $('#footer-ts').hide('');
            $('#save-btn-training').show();



            $('input[type="text"],select.required').each(function () {
                if ($.trim($(this).val()) == '') {

                    $(this).css({
                        "border": "",
                        "background": ""
                    });
                }

            });

        });
    </script>

    <script>
        $(document).on("click", ".delete_training", function () {
            var did = $(this).data('id');
            $("#d_id").val(did);
            $('#myModal').modal('show');
        });

        $('.delete_train_btn').click(function () {

            var _id = $("#d_id").val();
            var id = _id;

            var url = "deleteTraining";

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            })
            $.ajax({
                type: 'DELETE',
                url: url + '/' + id,
                success: function (data) {
                    console.log(id);
                    $(".item" + id).remove();
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });

        })
    </script>

    <script>
        $("#save-btn-training").click(function (e) {
            var isValid = true;
            $('input.required').each(function () {
                if ($.trim($(this).val()) == '') {
                    isValid = false; /* Required class style */
                    $(this).css({
                        "border": "1px solid red",
                        "background": "#FFCECE"
                    });
                }
                else { /* Required class style removed */
                    $(this).css({
                        "border": "",
                        "background": ""
                    });
                }
            });
            if (isValid == false)
                e.preventDefault();
            else{

            }





        });
    </script>


{{--end training session--}}


    {{--start employee training session--}}
<script>
  function myEmployee() {

      $('#employeeTrainingModal').modal('show');

   }
    </script>

    <script>
        $('.closeT').click(function(){

            $('#employee_id').val('');
            $('#training_session').val('');

            $('#status').val('');
            $('#footer-emp').hide('');
            $('#save-btn-employee').show();


            $('select.required').each(function () {
                if ($.trim($(this).val()) == '') {

                    $(this).css({
                        "border": "",
                        "background": ""
                    });
                }

            });

        });
    </script>
    <script>
        $("#save-btn-employee").click(function() {

            $.ajaxSetup ({
                // Disable caching of AJAX responses
                cache: false
            });
            $.ajax({
                type: 'get',
                url: 'addEmployeeTraining',
                data: {
                    '_token': $('input[name=_token]').val(),
                    'employee_id': $('#employee_id').val(),
                    'training_session' : $('#training_session').val(),
                    'status': $('#estatus').val()


                },
                success: function(data) {
                    console.log(data);

                    if ((data.errors)){
                        $('.error').removeClass('hidden');
                        $('.error').text(data.errors.name);
                    }
                    else {
                        $('.error').addClass('hidden');

                        $('.EmployeeTraining').append("<tr class='item" + data[0].id + "'><td>" + data[0].emp__training.first_name + ' '+data[0].emp__training.middle_name+' '+data[0].emp__training.last_name+ "</td><td>" + data[0].training_session + "</td><td>" + data[0].status +  "</td><td align='center'><button class='edit-modal-empTrain btn btn-info' data-id='" + data[0].id + "' data-employee_id='" + data[0].employee_id+ "' data-training_session='" + data[0].training_session + "'  data-status='" + data[0].status+ "'data-first_name='" + data[0].emp__training.first_name + "'data-middle_name='" + data[0].emp__training.middle_name + "'data-last_name='" + data[0].emp__training.last_name + "'><span class='glyphicon glyphicon-edit'></span></button> <button class='delete-modal btn btn-danger delete_empTrain' data-id='" + data[0].id + "' data-employee_id='" +  data[0].employee_id+ "' data-name='" + data.name + "'  data-coordinator='" + data.coordinator+ "'data-trainer='" + data.trainer+ "'data-payment_type='" + data.payment_type+ "'data-cost='" + data.cost+ "'data-status='" + data.status+ "' ><span class='glyphicon glyphicon-trash'></span></button></td></tr>");
                        $("#employeeTrainingModal").modal('hide');

                    }
                },
            });

        });

    </script>

    <script>
        $(document).on("click", ".delete_empTrain", function () {
            var did = $(this).data('id');
            $("#d_id").val(did);
            $('#myModal').modal('show');
        });

        $('.delete_train_btn').click(function () {

            var _id = $("#d_id").val();
            var id = _id;

            var url = "delete_empTrain";

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            })
            $.ajax({
                type: 'DELETE',
                url: url + '/' + id,
                success: function (data) {
                    console.log(id);
                    $(".item" + id).remove();
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });

        })
    </script>


    <script>

        $(document).on('click', '.edit-modal-empTrain', function() {



            $('#id').val($(this).data('id'));
            $('#employee_id').val($(this).data('employee_id'));
            $('#status').val($(this).data('status'));
            $('#training_session').val($(this).data('training_session'));

            $('#employeeTrainingModal').modal('show');
            $('#save-btn-employee').hide();
            $('#footer-emp').css('display','block');
            $('#name').css({
                "border": "",
                "background": ""
            });

           $('#edit-empTraining').click(function(){


                $.ajax({
                    type: 'get',
                    url: './editEmpTraining',
                    data: {
                        '_token': $('input[name=_token]').val(),
                        'id': $("#id").val(),
                        'employee_id': $('#employee_id').val(),
                        'training_session' : $('#training_session').val(),
                        'status': $('#estatus').val()

                    },
                    success: function(data) {


                        $('.item' + data[0].id).replaceWith("<tr class='item" + data[0].id + "'><td>" + data[0].emp__training.first_name + ' '+data[0].emp__training.middle_name+' '+data[0].emp__training.last_name + "</td><td>" + data[0].training_session+ "</td><td>" +  data[0].status + "</td><td align='center'><button class='edit-modal-empTrain btn btn-info' data-id='" + data[0].id + "' data-employee_id='" + data[0].employee_id+ "' data-training_session='" + data[0].training_session + "'  data-status='" + data[0].status+ "'data-first_name='" + data[0].emp__training.first_name + "'data-middle_name='" + data[0].emp__training.middle_name + "'data-last_name='" + data[0].emp__training.last_name + "' ><span class='glyphicon glyphicon-edit'></span></button> <button class='delete-modal btn btn-danger delete_course' data-id='" + data[0].id + "' data-employee_id='" +  data[0].employee_id+ "' ><span class='glyphicon glyphicon-trash'></span></button></td></tr>");
                        $('#employeeTrainingModal').modal('hide');
                        $('#id').val($(this).data(''));
                        $('#employee_id').val('');
                        $('#training_session').val('');

                        $('#status').val('');
                        $('#footer-emp').hide('');
                        $('#save-btn-employee').show();


                    }
                });

            });
        });
    </script>

    <script>
        $("#save-btn-employee").click(function (e) {
            var isValid = true;
            $('select.required').each(function () {
                if ($.trim($(this).val()) == '') {
                    isValid = false; /* Required class style */
                    $(this).css({
                        "border": "1px solid red",
                        "background": "#FFCECE"
                    });
                }
                else { /* Required class style removed */
                    $(this).css({
                        "border": "",
                        "background": ""
                    });
                }
            });
            if (isValid == false)
                e.preventDefault();
            else{

            }


        });
    </script>




@endsection