<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Mail;
use Crypt;
use View;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;




class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */


    public function authenticate(Request $request)
    {
        $email = $request->input('email');
        $pass = $request->input('password');
        $remember = $request->has('remember') ? true : false;

        if (Auth::attempt(['email' => $email, 'password' => $pass,'flag'=> 1],$remember)) {
            // Authentication passed...


            return redirect()->intended('dashboard');

        } else{

            return redirect('login')->with('status', 'Please check your credentials or contact your administrator!');
        }

    }

    public function resetForm(){

        return view('auth.resetForm');

    }
    public function forgetPassword(){

        $email = $_GET['email'];
        $count = User::where('email', '=', $email)->first();

        if (count($count )== 0)
        {
            echo $a = "not_found";
        }
        else{

            $api_key = $count->api_token;
            $email_key = $count->email;
            $encrypted_api = Crypt::encrypt($api_key);
            $encrypted_email = Crypt::encrypt($email_key);
            $base = "http://localhost:8080/hrm/public/resetPassword";
            $url = $base."/".$encrypted_api."/".$encrypted_email;
            return $url;

        }

    }


    public function resetPassword($key,$m){

        $email = Crypt::decrypt($m);
        $api_key = Crypt::decrypt($key);
        $matchThese = ['email' => $email, 'api_token' => $api_key];
        $results = User::where($matchThese)->get()->first();

        if(count($results)==0){
            return view('auth.errorForm');
        }
        else{
            return view('auth.resetPassword',['email' => $email,'api'=>$api_key]);
        }
    }
    public function recoverPasswordEmail(Request $req) {

        $randNo = rand(1111,9999999);
        $api_token = $req->input('api').$randNo;
        $pass =  bcrypt($req->input('password'));
        $email = $req->input('email');

        $new_user_data=array('api_token'=>$api_token,'password'=>$pass);
        $user = User::where ("email",$email)->update($new_user_data);;

        return response()->json($user);
    }

}
