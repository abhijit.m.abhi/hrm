<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmployeeSalarySetup extends Model
{
    //
    public function emp_salary()
    {

        // return $this->belongsTo('App\EmployeePersonalDetails');
        return $this->belongsTo('App\EmployeePersonalDetail', 'employee_id');
    }
    public function emp_jobDetails()
    {

        // return $this->belongsTo('App\EmployeePersonalDetails');
        return $this->belongsTo('App\EmployeeJobDetail', 'job_id');
    }




}
