<?php

namespace App\Ex_class;


use Illuminate\Support\Facades\Auth;

/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 3/7/2017
 * Time: 3:57 PM
 */
class FindRole
{

    public function findMyRole($id)
    {
        if(Auth::User()->type=="User")
        {
            return $uid = Auth::User()->id;
        }
        elseif(Auth::User()->type=="Admin")
        {
            return $uid =  $id;
        }
    }

    public function sum_the_time($time1, $time2) {
        $times = array($time1, $time2);
        $seconds = 0;
        foreach ($times as $time)
        {
            list($hour,$minute,$second) = explode(':', $time);
            $seconds += $hour*3600;
            $seconds += $minute*60;
            $seconds += $second;
        }
        $hours = floor($seconds/3600);
        $seconds -= $hours*3600;
        $minutes  = floor($seconds/60);
        $seconds -= $minutes*60;
        // return "{$hours}:{$minutes}:{$seconds}";
        return sprintf('%02d:%02d:%02d', $hours, $minutes, $seconds);
    }
}