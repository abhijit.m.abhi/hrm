<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Installment extends Model
{
    //
    public function emp_installment(){

        return $this->belongsTo('App\EmployeeLoan','id');
    }

}
