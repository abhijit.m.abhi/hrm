-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 07, 2017 at 01:24 PM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 7.0.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hrm`
--

-- --------------------------------------------------------

--
-- Table structure for table `attendances`
--

CREATE TABLE `attendances` (
  `id` int(10) UNSIGNED NOT NULL,
  `employee_id` int(10) UNSIGNED NOT NULL,
  `date` date NOT NULL,
  `check_in` time DEFAULT NULL,
  `check_out` time DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `attendances`
--

INSERT INTO `attendances` (`id`, `employee_id`, `date`, `check_in`, `check_out`, `created_at`, `updated_at`) VALUES
(42, 1, '2017-04-07', '16:20:23', '16:20:26', '2017-04-07 10:20:23', '2017-04-07 10:20:26'),
(43, 1, '2017-04-07', '16:20:28', '16:20:30', '2017-04-07 10:20:28', '2017-04-07 10:20:30'),
(44, 1, '2017-04-07', '16:20:31', '16:36:26', '2017-04-07 10:20:31', '2017-04-07 10:36:26');

-- --------------------------------------------------------

--
-- Table structure for table `company_structures`
--

CREATE TABLE `company_structures` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `details` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `department_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `office_start` time DEFAULT NULL,
  `office_end` time DEFAULT NULL,
  `last_entry_time` time DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `company_structures`
--

INSERT INTO `company_structures` (`id`, `name`, `details`, `address`, `department_type`, `country`, `ip_address`, `office_start`, `office_end`, `last_entry_time`, `created_at`, `updated_at`) VALUES
(1, 'Kris Group', 'Odit sapiente ex odit.', '37691 Auer Lock\nWest Joannieside, AK 53269-7913', 'Head Office', 'Bosnia and Herzegovina', '60.65.76.143', '09:00:00', '17:00:00', '09:30:00', '2017-04-07 07:38:05', '2017-04-07 07:38:05'),
(2, 'Kuphal-Larkin', 'Ex in.', '5023 Holly Lodge Apt. 158\nHodkiewiczside, SC 83141', 'Head Office', 'Kazakhstan', '30.118.108.139', '09:00:00', '17:00:00', '09:30:00', '2017-04-07 07:38:05', '2017-04-07 07:38:05'),
(3, 'Schroeder, Becker and Oberbrunner', 'Architecto itaque deserunt.', '47328 Philip Row\nRippinstad, GA 04973-7811', 'Unit', 'Saint Barthelemy', '122.56.196.249', '09:00:00', '17:00:00', '09:30:00', '2017-04-07 07:38:05', '2017-04-07 07:38:05'),
(4, 'Kuphal, Langosh and Gottlieb', 'Est voluptatem explicabo et.', '6013 Rice Pine\nTomasberg, WA 01656', 'Head Office', 'Mongolia', '173.74.247.196', '09:00:00', '17:00:00', '09:30:00', '2017-04-07 07:38:05', '2017-04-07 07:38:05'),
(5, 'Green LLC', 'Amet perferendis.', '221 Monroe Square\nEast Demetriusmouth, WY 97107', 'Department', 'Zimbabwe', '169.20.166.178', '09:00:00', '17:00:00', '09:30:00', '2017-04-07 07:38:05', '2017-04-07 07:38:05'),
(6, 'Sawayn, Jenkins and Rempel', 'Molestiae aut a rerum.', '526 Vern Viaduct\nColefurt, DC 87137-8439', 'Department', 'Bermuda', '14.181.55.251', '09:00:00', '17:00:00', '09:30:00', '2017-04-07 07:38:05', '2017-04-07 07:38:05'),
(7, 'Towne PLC', 'Quo velit est veritatis.', '3331 Kerluke Station\nLake Reinhold, WY 53258', 'Unit', 'French Polynesia', '45.248.112.85', '09:00:00', '17:00:00', '09:30:00', '2017-04-07 07:38:05', '2017-04-07 07:38:05'),
(8, 'White and Sons', 'Vero numquam perferendis dolores.', '3203 Gilberto Meadows Apt. 285\nMitchellmouth, WA 55491-0976', 'Regional Office', 'Croatia', '65.159.143.153', '09:00:00', '17:00:00', '09:30:00', '2017-04-07 07:38:05', '2017-04-07 07:38:05'),
(9, 'Stark LLC', 'Id odio ut tempore.', '2840 Shields Common\r\nLillyfort, IN 07682', 'Company', 'Bosnia and Herzegovina', '127.0.0.1', '09:00:00', '17:00:00', '09:30:00', '2017-04-07 07:38:05', '2017-04-07 10:20:21'),
(10, 'Dietrich, Rogahn and Swift', 'Inventore dignissimos voluptas.', '673 Monte Mission\nSouth Glenna, NM 84224-6066', 'Regional Office', 'Taiwan', '180.192.82.76', '09:00:00', '17:00:00', '09:30:00', '2017-04-07 07:38:05', '2017-04-07 07:38:05'),
(11, 'ABC', 'AIUH', 'HU', 'Head Office', 'India', '127.0.0.1', '09:30:00', '17:00:00', NULL, '2017-04-07 09:41:43', '2017-04-07 09:43:49');

-- --------------------------------------------------------

--
-- Table structure for table `courses`
--

CREATE TABLE `courses` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `coordinator` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `trainer` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payment_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cost` double(15,2) NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `employee_attachments`
--

CREATE TABLE `employee_attachments` (
  `id` int(10) UNSIGNED NOT NULL,
  `employee_id` int(10) UNSIGNED NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `location` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `employee_attachments`
--

INSERT INTO `employee_attachments` (`id`, `employee_id`, `type`, `title`, `file_name`, `location`, `created_at`, `updated_at`) VALUES
(1, 1, 'attachment', 'Photo', '101-89038.jpg', 'uploads/101-89038.jpg', '2017-04-07 07:45:37', '2017-04-07 07:45:37');

-- --------------------------------------------------------

--
-- Table structure for table `employee_emergency_contacts`
--

CREATE TABLE `employee_emergency_contacts` (
  `id` int(10) UNSIGNED NOT NULL,
  `employee_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `relation` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `employee_emergency_contacts`
--

INSERT INTO `employee_emergency_contacts` (`id`, `employee_id`, `name`, `relation`, `address`, `mobile`, `created_at`, `updated_at`) VALUES
(1, 1, 'dgdg', 'dgdg', 'dgdgd', '4646', '2017-04-07 07:45:37', '2017-04-07 07:45:37');

-- --------------------------------------------------------

--
-- Table structure for table `employee_experiences`
--

CREATE TABLE `employee_experiences` (
  `id` int(10) UNSIGNED NOT NULL,
  `employee_id` int(10) UNSIGNED NOT NULL,
  `organization_name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact_no` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `duration` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `employee_experiences`
--

INSERT INTO `employee_experiences` (`id`, `employee_id`, `organization_name`, `address`, `contact_no`, `duration`, `created_at`, `updated_at`) VALUES
(1, 1, 'twtw', 'twtwtw', '645467', '1', '2017-04-07 07:45:37', '2017-04-07 07:45:37'),
(2, 1, 'rewteytey', 'ryrutui', '0124567678', '4', '2017-04-07 07:45:37', '2017-04-07 07:45:37');

-- --------------------------------------------------------

--
-- Table structure for table `employee_job_details`
--

CREATE TABLE `employee_job_details` (
  `id` int(10) UNSIGNED NOT NULL,
  `employee_id` int(10) UNSIGNED NOT NULL,
  `employment_status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `job_title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `joined_date` date NOT NULL,
  `confirmation_date` date DEFAULT NULL,
  `termination_date` date DEFAULT NULL,
  `department` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `employee_job_details`
--

INSERT INTO `employee_job_details` (`id`, `employee_id`, `employment_status`, `job_title`, `joined_date`, `confirmation_date`, `termination_date`, `department`, `created_at`, `updated_at`) VALUES
(1, 1, 'Conditional', 'Criminal Investigator', '2017-01-01', '2017-01-01', NULL, 9, '2017-04-07 07:45:37', '2017-04-07 07:45:37');

-- --------------------------------------------------------

--
-- Table structure for table `employee_loans`
--

CREATE TABLE `employee_loans` (
  `id` int(10) UNSIGNED NOT NULL,
  `employee_id` int(10) UNSIGNED NOT NULL,
  `loan_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `start_date` date NOT NULL,
  `last_installment_date` date NOT NULL,
  `loan_period` int(11) NOT NULL,
  `loan_amount` double NOT NULL,
  `monthly_installment` double NOT NULL,
  `paid_amount` double NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `details` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `employee_personal_details`
--

CREATE TABLE `employee_personal_details` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `employee_code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `middle_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nationality` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_of_birth` date NOT NULL,
  `birth_certificate_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `passport_no` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `marital_status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `postal_or_zip_code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `work_phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `employee_personal_details`
--

INSERT INTO `employee_personal_details` (`id`, `user_id`, `employee_code`, `first_name`, `middle_name`, `last_name`, `nationality`, `nid`, `date_of_birth`, `birth_certificate_id`, `passport_no`, `gender`, `marital_status`, `address`, `city`, `postal_or_zip_code`, `country`, `phone`, `work_phone`, `email`, `created_at`, `updated_at`) VALUES
(1, 2, '101', 'Abhijit Mondal', NULL, 'Abhi', 'Bangladeshi', '123456789', '1994-11-10', '123456', '123456', 'Male', 'Single', 'Mohammadpur', 'Dhaka', '1207', 'Bangladesh', '01719000870', '012245', 'abhi.inc@live.com', '2017-04-07 07:45:36', '2017-04-07 07:45:36');

-- --------------------------------------------------------

--
-- Table structure for table `employee_qualifications`
--

CREATE TABLE `employee_qualifications` (
  `id` int(10) UNSIGNED NOT NULL,
  `employee_id` int(10) UNSIGNED NOT NULL,
  `skill` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `qualification` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `certification` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `employee_qualifications`
--

INSERT INTO `employee_qualifications` (`id`, `employee_id`, `skill`, `qualification`, `certification`, `created_at`, `updated_at`) VALUES
(1, 1, '["LemonChiffon","Lavender"]', '["Salmon","Bisque"]', '["OldLace","DarkRed","Red"]', '2017-04-07 07:45:37', '2017-04-07 07:45:37');

-- --------------------------------------------------------

--
-- Table structure for table `employee_salaries`
--

CREATE TABLE `employee_salaries` (
  `id` int(10) UNSIGNED NOT NULL,
  `salary_type` double NOT NULL,
  `salary_grade` double NOT NULL,
  `basic_salary` double NOT NULL,
  `overtime` double NOT NULL,
  `allowances` double NOT NULL,
  `deductions` double NOT NULL,
  `net_salary` double NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `employee_salary_setups`
--

CREATE TABLE `employee_salary_setups` (
  `id` int(10) UNSIGNED NOT NULL,
  `employee_id` int(10) UNSIGNED NOT NULL,
  `salary_grade` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `salary_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `daily_rate` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `basic_salary` double DEFAULT NULL,
  `hourly_rate` double DEFAULT NULL,
  `overtime` double DEFAULT NULL,
  `allowances` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deductions` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `net_salary` double DEFAULT NULL,
  `flag` int(11) NOT NULL DEFAULT '0',
  `job_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `employee_traning_sessions`
--

CREATE TABLE `employee_traning_sessions` (
  `id` int(10) UNSIGNED NOT NULL,
  `employee_id` int(10) UNSIGNED NOT NULL,
  `training_session` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `installments`
--

CREATE TABLE `installments` (
  `id` int(10) UNSIGNED NOT NULL,
  `loan_id` int(10) UNSIGNED NOT NULL,
  `count_installment` int(11) NOT NULL,
  `amount` double NOT NULL,
  `date` date NOT NULL,
  `saved_by` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `job_details_setups`
--

CREATE TABLE `job_details_setups` (
  `id` int(10) UNSIGNED NOT NULL,
  `job_code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `job_title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `specification` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `min_salary` double(15,2) NOT NULL,
  `max_salary` double(15,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `job_details_setups`
--

INSERT INTO `job_details_setups` (`id`, `job_code`, `job_title`, `description`, `specification`, `min_salary`, `max_salary`, `created_at`, `updated_at`) VALUES
(1, '22572', 'Makeup Artists', 'Recusandae et ipsa voluptatem.', NULL, 29279.00, 93890.00, '2017-04-07 07:38:04', '2017-04-07 07:38:04'),
(2, '55201', 'Business Development Manager', 'Placeat deleniti.', NULL, 26760.00, 97729.00, '2017-04-07 07:38:04', '2017-04-07 07:38:04'),
(3, '89905', 'Insulation Worker', 'Ab beatae maxime modi.', NULL, 29857.00, 94945.00, '2017-04-07 07:38:04', '2017-04-07 07:38:04'),
(4, '85877', 'Motion Picture Projectionist', 'Cumque velit ad quis.', NULL, 28544.00, 98469.00, '2017-04-07 07:38:04', '2017-04-07 07:38:04'),
(5, '84817', 'Stone Sawyer', 'Sunt hic labore porro.', NULL, 26034.00, 92859.00, '2017-04-07 07:38:04', '2017-04-07 07:38:04'),
(6, '62342', 'Court Reporter', 'Facere magnam ad.', NULL, 27897.00, 91807.00, '2017-04-07 07:38:04', '2017-04-07 07:38:04'),
(7, '98090', 'Streetcar Operator', 'Cumque sit quia.', NULL, 26971.00, 94963.00, '2017-04-07 07:38:04', '2017-04-07 07:38:04'),
(8, '80107', 'Septic Tank Servicer', 'Inventore ut aliquid deleniti.', NULL, 28441.00, 97032.00, '2017-04-07 07:38:05', '2017-04-07 07:38:05'),
(9, '21744', 'Government', 'Vitae ut vel autem.', NULL, 27895.00, 94255.00, '2017-04-07 07:38:05', '2017-04-07 07:38:05'),
(10, '95737', 'Criminal Investigator', 'Molestiae sit a et.', NULL, 29736.00, 85431.00, '2017-04-07 07:38:05', '2017-04-07 07:38:05');

-- --------------------------------------------------------

--
-- Table structure for table `late_attendances`
--

CREATE TABLE `late_attendances` (
  `id` int(10) UNSIGNED NOT NULL,
  `attendance_id` int(10) UNSIGNED NOT NULL,
  `date` date DEFAULT NULL,
  `entry_time` time DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `late_attendances`
--

INSERT INTO `late_attendances` (`id`, `attendance_id`, `date`, `entry_time`, `created_at`, `updated_at`) VALUES
(4, 42, '2017-04-07', '16:20:23', '2017-04-07 10:20:23', '2017-04-07 10:20:23');

-- --------------------------------------------------------

--
-- Table structure for table `loan_types`
--

CREATE TABLE `loan_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `details` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(915, '2014_10_12_000000_create_users_table', 1),
(916, '2014_10_12_100000_create_password_resets_table', 1),
(917, '2017_02_06_120649_create_company_structures_table', 1),
(918, '2017_02_06_121822_create_job_details_setups_table', 1),
(919, '2017_02_06_125400_create_employee_personal_details_table', 1),
(920, '2017_02_06_131311_create_employee_job_details_table', 1),
(921, '2017_02_06_145845_create_employee_qualifications_table', 1),
(922, '2017_02_06_151446_create_employee_emergency_contacts_table', 1),
(923, '2017_02_06_151946_create_employee_experiences_table', 1),
(924, '2017_02_06_152458_create_employee_attachments_table', 1),
(925, '2017_02_07_103817_create_qualification_setups_table', 1),
(926, '2017_02_17_114054_create_courses_table', 1),
(927, '2017_02_17_120309_create_tranning_sessions_table', 1),
(928, '2017_02_17_121435_create_employee_traning_sessions_table', 1),
(929, '2017_03_06_121529_create_loan_types_table', 1),
(930, '2017_03_06_121619_create_employee_loans_table', 1),
(931, '2017_03_09_130815_create_salary_setups_table', 1),
(932, '2017_03_09_172408_create_installments_table', 1),
(933, '2017_03_14_155338_create_attendances_table', 1),
(934, '2017_03_27_114732_create_employee_salary_setups_table', 1),
(935, '2017_04_07_133502_create_late_attendances_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `qualification_setups`
--

CREATE TABLE `qualification_setups` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `qualification_setups`
--

INSERT INTO `qualification_setups` (`id`, `name`, `description`, `type`, `created_at`, `updated_at`) VALUES
(1, 'Lime', 'Ab repudiandae ducimus.', 'Certification', '2017-04-07 07:38:05', '2017-04-07 07:38:05'),
(2, 'Salmon', 'Doloribus quis rerum consequatur.', 'Education', '2017-04-07 07:38:05', '2017-04-07 07:38:05'),
(3, 'OldLace', 'Velit laudantium.', 'Certification', '2017-04-07 07:38:05', '2017-04-07 07:38:05'),
(4, 'LemonChiffon', 'Doloribus quisquam minima nemo.', 'Skills', '2017-04-07 07:38:05', '2017-04-07 07:38:05'),
(5, 'DarkRed', 'Mollitia consequatur ipsa et.', 'Certification', '2017-04-07 07:38:05', '2017-04-07 07:38:05'),
(6, 'OrangeRed', 'Delectus ea consequuntur sed.', 'Certification', '2017-04-07 07:38:05', '2017-04-07 07:38:05'),
(7, 'Bisque', 'Autem consequatur.', 'Education', '2017-04-07 07:38:05', '2017-04-07 07:38:05'),
(8, 'Red', 'Voluptatibus delectus fugiat ut.', 'Certification', '2017-04-07 07:38:05', '2017-04-07 07:38:05'),
(9, 'Lavender', 'Soluta tempore pariatur.', 'Skills', '2017-04-07 07:38:05', '2017-04-07 07:38:05'),
(10, 'HoneyDew', 'Totam doloribus et fugit.', 'Certification', '2017-04-07 07:38:05', '2017-04-07 07:38:05');

-- --------------------------------------------------------

--
-- Table structure for table `salary_setups`
--

CREATE TABLE `salary_setups` (
  `id` int(10) UNSIGNED NOT NULL,
  `salary_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `salary_grade` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `daily_rate` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `basic_salary` double DEFAULT NULL,
  `hourly_rate` double DEFAULT NULL,
  `overtime` double DEFAULT NULL,
  `allowances` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deductions` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `net_salary` double DEFAULT NULL,
  `flag` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `salary_setups`
--

INSERT INTO `salary_setups` (`id`, `salary_type`, `salary_grade`, `daily_rate`, `basic_salary`, `hourly_rate`, `overtime`, `allowances`, `deductions`, `net_salary`, `flag`, `created_at`, `updated_at`) VALUES
(1, 'Daily', 'A', '20000000', NULL, NULL, NULL, '[{"medical":null},{"houserent":null}]', '[{"provident_fund":null},{"tax_deduction":null}]', 0, 0, '2017-04-07 09:53:30', '2017-04-07 09:53:30');

-- --------------------------------------------------------

--
-- Table structure for table `tranning_sessions`
--

CREATE TABLE `tranning_sessions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `course` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `details` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sheduled_time` datetime NOT NULL,
  `duration` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `training_certificate` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `flag` int(11) NOT NULL,
  `fname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'user',
  `api_token` varchar(160) COLLATE utf8mb4_unicode_ci NOT NULL,
  `saved_by` int(11) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `flag`, `fname`, `lname`, `email`, `password`, `address`, `photo`, `type`, `api_token`, `saved_by`, `remember_token`, `created_at`, `updated_at`) VALUES
(2, 1, 'Abhijit Mondal', 'Abhi', 'abhi.inc@live.com', '$2y$10$gwuiNvCc.vOwiENTqoKx.OyO5Df2pHGCy8K3IEReWwPtWGHmAdOnu', 'Mohammadpur', 'abhi.inc@live.com79565.jpg', 'Admin', 'HemTZVaYcpXx7SzLowEU2nmL0l9P5Vd2WUaA5KLBIyKDN7Pzs43exjDa9hyc', 1, 'IhmrupbvBWh8WtJ6ezbokdvLKq3xGkPD4GE9czi0F6zyAPIrybYn6nmtJQbS', '2017-04-07 07:45:36', '2017-04-07 07:46:19');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `attendances`
--
ALTER TABLE `attendances`
  ADD PRIMARY KEY (`id`),
  ADD KEY `attendances_employee_id_foreign` (`employee_id`);

--
-- Indexes for table `company_structures`
--
ALTER TABLE `company_structures`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `courses`
--
ALTER TABLE `courses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employee_attachments`
--
ALTER TABLE `employee_attachments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `employee_attachments_employee_id_foreign` (`employee_id`);

--
-- Indexes for table `employee_emergency_contacts`
--
ALTER TABLE `employee_emergency_contacts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `employee_emergency_contacts_employee_id_foreign` (`employee_id`);

--
-- Indexes for table `employee_experiences`
--
ALTER TABLE `employee_experiences`
  ADD PRIMARY KEY (`id`),
  ADD KEY `employee_experiences_employee_id_foreign` (`employee_id`);

--
-- Indexes for table `employee_job_details`
--
ALTER TABLE `employee_job_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `employee_job_details_employee_id_foreign` (`employee_id`),
  ADD KEY `employee_job_details_department_foreign` (`department`);

--
-- Indexes for table `employee_loans`
--
ALTER TABLE `employee_loans`
  ADD PRIMARY KEY (`id`),
  ADD KEY `employee_loans_employee_id_foreign` (`employee_id`);

--
-- Indexes for table `employee_personal_details`
--
ALTER TABLE `employee_personal_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `employee_personal_details_user_id_foreign` (`user_id`);

--
-- Indexes for table `employee_qualifications`
--
ALTER TABLE `employee_qualifications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `employee_qualifications_employee_id_foreign` (`employee_id`);

--
-- Indexes for table `employee_salaries`
--
ALTER TABLE `employee_salaries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employee_salary_setups`
--
ALTER TABLE `employee_salary_setups`
  ADD PRIMARY KEY (`id`),
  ADD KEY `employee_salary_setups_employee_id_foreign` (`employee_id`),
  ADD KEY `employee_salary_setups_job_id_foreign` (`job_id`);

--
-- Indexes for table `employee_traning_sessions`
--
ALTER TABLE `employee_traning_sessions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `employee_traning_sessions_employee_id_foreign` (`employee_id`);

--
-- Indexes for table `installments`
--
ALTER TABLE `installments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `installments_loan_id_foreign` (`loan_id`);

--
-- Indexes for table `job_details_setups`
--
ALTER TABLE `job_details_setups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `late_attendances`
--
ALTER TABLE `late_attendances`
  ADD PRIMARY KEY (`id`),
  ADD KEY `late_attendances_attendance_id_foreign` (`attendance_id`);

--
-- Indexes for table `loan_types`
--
ALTER TABLE `loan_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `qualification_setups`
--
ALTER TABLE `qualification_setups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `salary_setups`
--
ALTER TABLE `salary_setups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tranning_sessions`
--
ALTER TABLE `tranning_sessions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD UNIQUE KEY `users_api_token_unique` (`api_token`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `attendances`
--
ALTER TABLE `attendances`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;
--
-- AUTO_INCREMENT for table `company_structures`
--
ALTER TABLE `company_structures`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `courses`
--
ALTER TABLE `courses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `employee_attachments`
--
ALTER TABLE `employee_attachments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `employee_emergency_contacts`
--
ALTER TABLE `employee_emergency_contacts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `employee_experiences`
--
ALTER TABLE `employee_experiences`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `employee_job_details`
--
ALTER TABLE `employee_job_details`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `employee_loans`
--
ALTER TABLE `employee_loans`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `employee_personal_details`
--
ALTER TABLE `employee_personal_details`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `employee_qualifications`
--
ALTER TABLE `employee_qualifications`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `employee_salaries`
--
ALTER TABLE `employee_salaries`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `employee_salary_setups`
--
ALTER TABLE `employee_salary_setups`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `employee_traning_sessions`
--
ALTER TABLE `employee_traning_sessions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `installments`
--
ALTER TABLE `installments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `job_details_setups`
--
ALTER TABLE `job_details_setups`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `late_attendances`
--
ALTER TABLE `late_attendances`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `loan_types`
--
ALTER TABLE `loan_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=936;
--
-- AUTO_INCREMENT for table `qualification_setups`
--
ALTER TABLE `qualification_setups`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `salary_setups`
--
ALTER TABLE `salary_setups`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tranning_sessions`
--
ALTER TABLE `tranning_sessions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `attendances`
--
ALTER TABLE `attendances`
  ADD CONSTRAINT `attendances_employee_id_foreign` FOREIGN KEY (`employee_id`) REFERENCES `employee_personal_details` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `employee_attachments`
--
ALTER TABLE `employee_attachments`
  ADD CONSTRAINT `employee_attachments_employee_id_foreign` FOREIGN KEY (`employee_id`) REFERENCES `employee_personal_details` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `employee_emergency_contacts`
--
ALTER TABLE `employee_emergency_contacts`
  ADD CONSTRAINT `employee_emergency_contacts_employee_id_foreign` FOREIGN KEY (`employee_id`) REFERENCES `employee_personal_details` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `employee_experiences`
--
ALTER TABLE `employee_experiences`
  ADD CONSTRAINT `employee_experiences_employee_id_foreign` FOREIGN KEY (`employee_id`) REFERENCES `employee_personal_details` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `employee_job_details`
--
ALTER TABLE `employee_job_details`
  ADD CONSTRAINT `employee_job_details_department_foreign` FOREIGN KEY (`department`) REFERENCES `company_structures` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `employee_job_details_employee_id_foreign` FOREIGN KEY (`employee_id`) REFERENCES `employee_personal_details` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `employee_loans`
--
ALTER TABLE `employee_loans`
  ADD CONSTRAINT `employee_loans_employee_id_foreign` FOREIGN KEY (`employee_id`) REFERENCES `employee_personal_details` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `employee_personal_details`
--
ALTER TABLE `employee_personal_details`
  ADD CONSTRAINT `employee_personal_details_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `employee_qualifications`
--
ALTER TABLE `employee_qualifications`
  ADD CONSTRAINT `employee_qualifications_employee_id_foreign` FOREIGN KEY (`employee_id`) REFERENCES `employee_personal_details` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `employee_salary_setups`
--
ALTER TABLE `employee_salary_setups`
  ADD CONSTRAINT `employee_salary_setups_employee_id_foreign` FOREIGN KEY (`employee_id`) REFERENCES `employee_personal_details` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `employee_salary_setups_job_id_foreign` FOREIGN KEY (`job_id`) REFERENCES `job_details_setups` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `employee_traning_sessions`
--
ALTER TABLE `employee_traning_sessions`
  ADD CONSTRAINT `employee_traning_sessions_employee_id_foreign` FOREIGN KEY (`employee_id`) REFERENCES `employee_personal_details` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `installments`
--
ALTER TABLE `installments`
  ADD CONSTRAINT `installments_loan_id_foreign` FOREIGN KEY (`loan_id`) REFERENCES `employee_loans` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `late_attendances`
--
ALTER TABLE `late_attendances`
  ADD CONSTRAINT `late_attendances_attendance_id_foreign` FOREIGN KEY (`attendance_id`) REFERENCES `attendances` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
