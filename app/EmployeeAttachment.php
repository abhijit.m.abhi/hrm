<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmployeeAttachment extends Model
{
    protected $fillable = [
        'title[]',
        'file[]',
    ];
}
