@extends('layouts.master')
@section('title', 'Employee Resign List')
@section('content')

    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>View Employee Resign List</h3>
                </div>


            </div>
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Employee Resign List<!--<small>Input different form elements</small>--></h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">


                            <div class="table-responsive">
                                <table id="Hourly"  class="empSalary table table-striped table-bordered table-hover display" >
                                    <thead>
                                    <tr class="alert-info">
                                        <th>Name</th>
                                        <th>Date</th>
                                        <th>Reason</th>


                                        <th align="center">Action</th>
                                    </tr>
                                    </thead>

                                    <tbody>
                                    @foreach($resignList as $resign_info)

                                        <tr id="EmpSalary{{$resign_info->id}}">
                                            <td>{{$resign_info->emp_resign->first_name}} {{$resign_info->emp_resign->last_name}}</td>
                                            <td>{{$resign_info->date}}</td>

                                            <td>{{$resign_info->comments}}</td>



                                            <td align="center">
                                                <div class="btn-group c_button">
                                                    <button type="button" data-value="{{$resign_info->id}}"
                                                            class="btn btn-primary t_button{{$resign_info->id}}">
                                                            <?php

                                                                $user_id =$resign_info->id;
                                                                if($resign_info->flag == 0)
                                                                    {
                                                                        echo "Pending";
                                                                    }
                                                                else if($resign_info->flag == 2)
                                                                {
                                                                    echo "Accepted";
                                                                }
                                                                else if($resign_info->flag == 3)
                                                                {
                                                                    echo "Rejected";
                                                                }


                                                            ?>
                                                    </button>
                                                    <button type="button" class="btn btn-primary dropdown-toggle"
                                                            data-toggle="dropdown" aria-haspopup="true"
                                                            aria-expanded="false">
                                                        <span class="caret"></span>
                                                        <span class="sr-only">Toggle Dropdown</span>
                                                    </button>
                                                    <ul class="dropdown-menu change_d">
                                                        <li><a href="#" class="ad"
                                                               data-value="Accepted~{{$resign_info->id}}">Accepted</a></li>
                                                        <li><a href="#" class="ad"
                                                               data-value="Rejected~{{$resign_info->id}}">Rejected</a></li>
                                                    </ul>
                                                </div>
                                            </td>
                                        </tr>


                                    @endforeach
                                    <meta name="_token" content="{!! csrf_token() !!}" />

                                    </tbody>
                                </table>
                            </div>





                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


<script>
    jQuery(".ad").click(function (e) {
        e.preventDefault();


        var resign_decision = $(this).data('value');
        var fields = resign_decision.split(/~/);
        var decision = fields[0];
        var id_value = fields[1];

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        });

        $.ajax({
            type: 'get',
            url: 'resign_decision',
            data: {
                '_token': $('input[name=_token]').val(),
                'id': id_value,
                'decision':decision


            },
            success: function(data) {
                console.log(data);

                var flag = data.flag;
                if(flag == 2){
                    $(".t_button" + id_value).html('Accepted');
                }
                else if(flag == 3){
                    $(".t_button" + id_value).html('Rejected');
                }
               // $(".t_button" + userId).html(data);

            },
        });


    })

</script>





@endsection