@extends('layouts.master')
@section('title', 'Add User')
@section('content')

    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>Employee Salary</h3>
                </div>


            </div>
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>View Employee Salary<!--<small>Input different form elements</small>--></h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                            @if(Session::has('message'))

                                <div id="successMessage" class="alert alert-success alert-dismissible fade in" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                                aria-hidden="true">×</span>
                                    </button>
                                    <strong>{{ Session::get('message') }}</strong>
                                </div>
                            @endif
                        </div>
                        <div class="x_content">
                            <br/>
                            <form id="form" data-parsley-validate method="POST" class="form-horizontal" role="form">
                                {{ csrf_field() }}
                                <div class="form-body">

                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Select Salary Type : </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <select class="select2_single  form-control" tabindex="-1" id="salary_type" name="salary_type">

                                                <option value="">Select Option</option>
                                                <option value="Monthly">Monthly</option>
                                                <option value="Daily">Daily</option>
                                                <option value="Hourly">Hourly</option>

                                            </select>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="demo" id="demo" style="display: none" >

                                        <div class="table-responsive">
                                            <table id="Monthly"  class="tableSalary table table-striped table-bordered table-hover display" >
                                                <thead>
                                                <tr class="alert-info">
                                                    <th>Salary Grade</th>
                                                    <th>Basic Salary</th>
                                                    <th>Over Time</th>
                                                    <th>Allowances</th>
                                                    <th>Deductions</th>
                                                    <th>Net Salary</th>
                                                    <th>Action</th>
                                                </tr>
                                                </thead>
                                                <tfoot>
                                                <tr>
                                                    <th>Total:</th>
                                                    <th></th>
                                                    <th></th>
                                                    <th></th>
                                                    <th></th>
                                                    <th></th>
                                                    <th></th>

                                                </tr>
                                                </tfoot>

                                                <tbody>
                                                @foreach($monthly as $monthSalary)


                                                    <tr id="monthly{{$monthSalary->id}}">
                                                        <td>{{$monthSalary->salary_grade}}</td>
                                                        <td>{{$monthSalary->basic_salary}}</td>
                                                        <td>{{$monthSalary->overtime}}</td>
                                                        <td>
                                                            <?php
                                                            $totalAllowances = 0;
                                                            $json = json_decode($monthSalary->allowances,true);

                                                            foreach ($json as $key => $value) {
                                                                foreach ($value as $k=>$val)
                                                                {
                                                                    // echo $k ."=".$val."</br>";
                                                                    $totalAllowances= $totalAllowances+$val;
                                                                }
                                                            }
                                                            ?>
                                                                {{$totalAllowances}}


                                                        </td>


                                                        <td>
                                                            <?php
                                                            $totalDeductions = 0;

                                                            $deduct_json = json_decode($monthSalary->deductions,true);
                                                            foreach ($deduct_json as $key => $value) {
                                                                foreach ($value as $k=>$val)
                                                                {
                                                                    //echo $k ."=".$val."</br>";
                                                                    $totalDeductions= $totalDeductions+$val;

                                                                }

                                                            }?>

                                                                {{$totalDeductions}}


                                                        </td>
                                                        <td>{{$monthSalary->net_salary}}</td>




                                                        <td>

                                                            <a href="{{route('set_salary.edit', $monthSalary->id)}}" class="btn btn-primary"><span class="glyphicon glyphicon-edit"></span></a>



                                                            <button class="btn confirm btn-danger delete_p"  data-toggle="modal" data-id="{{$monthSalary->id}}" ><span class="glyphicon glyphicon-trash"></span></button>

                                                        </td>
                                                    </tr>


                                                @endforeach
                                                <meta name="_token" content="{!! csrf_token() !!}" />

                                                </tbody>
                                            </table>
                                        </div>



                                        <div class="ln_solid"></div>

                                    </div>

                                    <div class="demo" id="hourly" style="display: none" >

                                        <div class="table-responsive">
                                            <table id="Hourly"  class="dailyt table table-striped table-bordered table-hover display" >
                                                <thead>
                                                <tr class="alert-info">
                                                    <th>Hourly Grade</th>
                                                    <th>Hourly Rate</th>
                                                    <th>Action</th>
                                                </tr>
                                                </thead>
                                                <tfoot>
                                                <tr>

                                                    <th>Total:</th>
                                                    <th></th>
                                                    <th></th>


                                                </tr>
                                                </tfoot>

                                                <tbody>
                                                @foreach($hourly as $hourlySalary)

                                                    <tr id="hourly{{$hourlySalary->id}}">
                                                        <td>{{$hourlySalary->salary_grade}}</td>
                                                        <td>{{$hourlySalary->hourly_rate}}</td>

                                                        <td align="center">

                                                            <a href="{{route('set_salary.edit', $hourlySalary->id)}}" class="btn btn-primary"><span class="glyphicon glyphicon-edit"></span></a>



                                                            <button class="btn confirm btn-danger delete_hour"  data-toggle="modal" data-id="{{$hourlySalary->id}}"  ><span class="glyphicon glyphicon-trash"></span></button>

                                                        </td>
                                                    </tr>


                                                @endforeach
                                                <meta name="_token" content="{!! csrf_token() !!}" />

                                                </tbody>
                                            </table>
                                        </div>



                                        <div class="ln_solid"></div>

                                    </div>

                                    <div class="demo" id="daily" style="display: none" >

                                        <div class="table-responsive">
                                            <table id="Daily"  class="dailyt table table-striped table-bordered table-hover " >
                                                <thead>
                                                <tr class="alert-info">
                                                    <th>Daily Grade</th>
                                                    <th>Daily Rate</th>
                                                    <th>Action</th>
                                                </tr>
                                                </thead>
                                                <tfoot>
                                                <tr>

                                                    <th>Total:</th>
                                                    <th></th>
                                                    <th></th>


                                                </tr>
                                                </tfoot>

                                                <tbody>
                                                @foreach($daily as $dailySalary)

                                                    <tr id="daily{{$dailySalary->id}}">
                                                        <td>{{$dailySalary->salary_grade}}</td>
                                                        <td>{{$dailySalary->daily_rate}}</td>

                                                        <td align="center">

                                                            <a href="{{route('set_salary.edit', $dailySalary->id)}}" class="btn btn-primary"><span class="glyphicon glyphicon-edit"></span></a>

                                                            <button class="btn confirm btn-danger delete_daily"  data-toggle="modal" data-id="{{$dailySalary->id}}"  ><span class="glyphicon glyphicon-trash"></span></button>

                                                        </td>
                                                    </tr>


                                                @endforeach
                                                <meta name="_token" content="{!! csrf_token() !!}" />

                                                </tbody>
                                            </table>
                                        </div>



                                        <div class="ln_solid"></div>

                                    </div>





                                    <meta name="_token" content="{!! csrf_token() !!}" />
                                </div>
                            </form>


                            <div class="modal fade" tabindex="-1" id="myModal" aria-labelledby="myModalLabel" role="dialog">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            <h3 class="modal-title ">Delete Confirmation </h3>
                                        </div>
                                        <div class="modal-body ">
                                            <p><b>Do you want to delete?&hellip;</b></p>
                                            <input type="hidden" id="d_id">
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default warning" data-dismiss="modal">Close</button>
                                            <button type="button " class="btn btn-primary delete_product btn-danger" data-dismiss="modal" value="" data-token="{{ csrf_token() }}"data-dismiss="modal">Delete</button>
                                        </div>
                                    </div><!-- /.modal-content -->
                                </div><!-- /.modal-dialog -->
                            </div>


                            <div class="modal fade" tabindex="-1" id="dailyModal" aria-labelledby="myModalLabel" role="dialog">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            <h3 class="modal-title ">Delete Confirmation </h3>
                                        </div>
                                        <div class="modal-body ">
                                            <p><b>Do you want to delete?&hellip;</b></p>
                                            <input type="hidden" id="daily_id">
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default warning" data-dismiss="modal">Close</button>
                                            <button type="button " class="btn btn-primary delete_product btn-danger" data-dismiss="modal" value="" data-token="{{ csrf_token() }}"data-dismiss="modal">Delete</button>
                                        </div>
                                    </div><!-- /.modal-content -->
                                </div><!-- /.modal-dialog -->
                            </div>

                            <div class="modal fade" tabindex="-1" id="hourModal" aria-labelledby="myModalLabel" role="dialog">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            <h3 class="modal-title ">Delete Confirmation </h3>
                                        </div>
                                        <div class="modal-body ">
                                            <p><b>Do you want to delete?&hellip;</b></p>
                                            <input type="hidden" id="h_id">
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default warning" data-dismiss="modal">Close</button>
                                            <button type="button " class="btn btn-primary delete_product btn-danger" data-dismiss="modal" value="" data-token="{{ csrf_token() }}"data-dismiss="modal">Delete</button>
                                        </div>
                                    </div><!-- /.modal-content -->
                                </div><!-- /.modal-dialog -->
                            </div>




                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>

    </script>



    <script>
        $(document).ready(function() {
            $(".select2_single").select2({
                placeholder: "Select a state",
                allowClear: true
            });

        });
    </script>

    <script>
        $(function () {
            $("#salary_type").on('change',function (e) {


                var selectedText = $(this).find("option:selected").text();
                var selectedValue = $(this).val();
                if(selectedValue === 'Monthly')
                {
                    $("#daily").css("display","none");
                    $("#hourly").css("display","none");
                    $("#demo").css("display","block");
                }
                else if (selectedValue === 'Hourly'){

                    $("#demo").css("display","none");
                    $("#daily").css("display","none");
                    $("#hourly").css("display","block");
                }
                else{
                    $("#daily").css("display","block");
                    $("#hourly").css("display","none");
                    $("#demo").css("display","none");

                }
            })

        });
    </script>


    <script>

        $("#save-btn-employee").click(function (e) {
            var isValid = true;
            $('select.required').each(function () {
                if ($.trim($(this).val()) == '') {
                    isValid = false; /* Required class style */
                    $(this).css({
                        "border": "1px solid red",
                        "background": "#FFCECE"
                    });
                }
                else { /* Required class style removed */
                    $(this).css({
                        "border": "",
                        "background": ""
                    });
                }
            });
            if (isValid == false)
                e.preventDefault();
            else{

            }


        })
    </script>

    <script type="text/javascript">
        // $('.confirm').confirm(
        $(document).on("click", ".delete_p", function() {
            var did = $(this).data('id');
            $("#d_id").val(did);
            $('#myModal').modal('show');
        });
        var url = "deleteSalary";

        $('.delete_product').click(function(){
            var _id =  $("#d_id").val();



            var id = _id;

            $.ajaxSetup({
                headers : {
                    'X-CSRF-TOKEN' : $('meta[name="_token"]').attr('content')
                }

            })
            $.ajax({
                type : 'GET',
                url : url + '/' + id,
                success: function(data){
                    console.log(id);

                    $("#monthly" + id).remove();
                },
                error:function(data){
                    console.log('Error:',data);
                }

            });
        });

    </script>

    <script type="text/javascript">
        // $('.confirm').confirm(
        $(document).on("click", ".delete_hour", function() {
            var did = $(this).data('id');
            //alert(did);
            $("#h_id").val(did);
            $('#hourModal').modal('show');
        });
        var url = "deleteSalary";

        $('.delete_product').click(function(){
            var _id =  $("#h_id").val();

            var id = _id;

            $.ajaxSetup({
                headers : {
                    'X-CSRF-TOKEN' : $('meta[name="_token"]').attr('content')
                }

            })
            $.ajax({
                type : 'GET',
                url : url + '/' + id,
                success: function(data){
                    console.log(id);

                    $("#hourly" + id).remove();
                },
                error:function(data){
                    console.log('Error:',data);
                }

            });
        });

    </script>

    <script type="text/javascript">
        // $('.confirm').confirm(
        $(document).on("click", ".delete_daily", function() {
            var did = $(this).data('id');
            $("#daily_id").val(did);
            $('#dailyModal').modal('show');
        });
        var url = "deleteSalary";

        $('.delete_product').click(function(){
            var _id =  $("#daily_id").val();


            var id = _id;

            $.ajaxSetup({
                headers : {
                    'X-CSRF-TOKEN' : $('meta[name="_token"]').attr('content')
                }

            })
            $.ajax({
                type : 'GET',
                url : url + '/' + id,
                success: function(data){
                    console.log(id);

                    $("#daily" + id).remove();
                },
                error:function(data){
                    console.log('Error:',data);
                }

            });
        });

    </script>




@endsection