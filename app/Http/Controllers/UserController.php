<?php

namespace App\Http\Controllers;

use App\Http\Requests\AddUserRequest;
use App\Http\Requests\EditUserRequest;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use DB;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $allUserList = User::where('flag', '<>', 0)->get();
        return view('userList', ['allUserList' => $allUserList]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('addUser');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(AddUserRequest $request)
    {
        $user = new User();
        $destinationPath = 'images/users'; // upload path
        $extension = $request->file('photo')->getClientOriginalExtension(); // getting image extension
        $fileName = $request->input('fname') . rand(10000, 99999) . '.' . $extension; // renameing image
        $user->fname = $request->input('fname');
        $user->lname = $request->input('lname');
        $user->email = $request->input('email');
        $user->address = $request->input('address');
        $user->type = $request->input('type');
        $user->flag = $request->input('status');
        $user->photo = $fileName;
        $user->password = bcrypt($request->input('password'));
        $user->api_token = str_random(60);
        $user->saved_by = Auth::user()->id;
        if ($user->save()) {
            //$request->session()->flash('status', 'success');
            Session::flash('message', 'User Created Successfully!');
            $request->file('photo')->move($destinationPath, $fileName); // uploading file to given path
        };
        return redirect('/user');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $userData = User::findOrFail($id);
        return view('editUser', ['user' => $userData]);

       
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(EditUserRequest $request, User $user)
    {
        $user->fname = $request->input('fname');
        $user->lname = $request->input('lname');
        $user->email = $request->input('email');
        $user->address = $request->input('address');
        $user->type = $request->input('type');
        $user->flag = $request->input('status');
        $file_icon = $request->file('photo');
        $fileCountIcon = count($file_icon);
        $destinationPath = 'images/users'; // upload path
        if ($fileCountIcon != 0) {
            if ($user->photo == "") {
                $extension = $request->file('photo')->getClientOriginalExtension(); // getting image extension
                $fileName = $request->input('fname') . rand(10000, 99999) . '.' . $extension; // renameing image
                $file_icon->move($destinationPath, $fileName);
                $user->photo = $fileName;
            } else {
                $file_icon->move($destinationPath, $user->photo);
            }
        }

        if ($request->has('password')) {
            $user->password = bcrypt($request->input('password'));
        }
        $user->api_token = str_random(60);

        //return $user->id;
        if ($user->update()) {
            //$request->session()->flash('status', 'success');
            Session::flash('message', 'Information Updated Successfully !');
            //Session::flash('alert-class', 'alert-success');
            return redirect('/user');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::where('id', $id)->update(['flag' => 0]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * Load Profile of Current User
     */

    public function profile()
    {
        $user_id = Auth::user()->id;
        $users = User::findOrFail($user_id);

        return view('profile', ['user' => $users]);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * Delete User from UserList
     */

    function delete_user($id)
    {
        $user = User::find($id);
        if ($user->delete()) {
            return redirect('/user');
        }
    }



}
