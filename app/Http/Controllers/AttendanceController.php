<?php

namespace App\Http\Controllers;

use App\Attendance;
use App\EmployeeJobDetail;
use App\EmployeePersonalDetail;
use App\Ex_class\FindRole;
use App\LateAttendance;
use App\User;
use Carbon\Carbon;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Symfony\Component\Finder\Comparator\DateComparator;

class AttendanceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $allUserList = User::where('flag', '<>', 0)->get();
        return view('attendanceControl', ['allUserList' => $allUserList]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        echo "Hello";
        exit;
        return view('monthlyAttendance');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('monthlyAttendance');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //$emp = User::find($id);

        $emp = User::with(array(
            "emp_official_info" => function ($query) {
                $query->with(array('emp_attendance' => function ($query) {
                    $query->orderBy('date', 'desc');
                }));
            }
        ))->find($id);


        $attendance = $emp->emp_official_info->emp_attendance;

        // return $attendance;

        return view('attendanceList', ['attendance' => $attendance]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function giveAttendance(Request $request)
    {

        // current logged in user
        $userId = Auth::user()->id;


        // current date and time
        $today = Carbon::now();
        $date = $today->toDateString();
        $entry = $today->toTimeString();

        // find the Employee ID using logged in User ID
        $find_emp_id = EmployeePersonalDetail::select('id')->where('user_id', $userId)->firstOrFail();

        // find PC IP address
        $ip = $request->ip();

        // find department IP address
        $dept_ip = User::with(array(
            "emp_official_info" => function ($query) {
                $query->with(array("emp_job_details" => function ($query) {
                    $query->with(array("dep_info"));
                }));
            }
        ))->find($userId);


        $this_dept_ip = $dept_ip->emp_official_info->emp_job_details->dep_info->ip_address;

        // find where is the user now (match the IP address)
        if ($ip == $this_dept_ip) {
            // find last Check_Out
            $find_check_out = Attendance::select('check_out')->where('employee_id', $find_emp_id->id)
                ->where('date', $date)
                ->orderBy('id', 'desc')->first();

            // find Attendance where to update Check_Out
            $select_user = Attendance::where('employee_id', $find_emp_id->id)->orderBy('id', 'desc')->first();
            //return $find_check_out;

            // check if this user has any attendance info for today
            if ($find_check_out != "") {

                // check if Check_Out is empty
                if ($find_check_out->check_out == "") {
                    $select_user->check_out = $entry;
                    $select_user->update();
                    echo "check_out";

                } else {

                    // create new Checked_In
                    $attendance = new Attendance();

                    $attendance->date = $date;
                    $attendance->check_in = $entry;

                    $emp_personal_detail = EmployeePersonalDetail::where('user_id', $userId)->firstOrFail();
                    $emp_personal_detail->emp_attendance()->save($attendance);

                    echo "check_in";
                }
            } else {


                // create new Checked_In
                $attendance = new Attendance();

                $attendance->date = $date;
                $attendance->check_in = $entry;

                $emp_personal_detail = EmployeePersonalDetail::where('user_id', $userId)->firstOrFail();
                $emp_personal_detail->emp_attendance()->save($attendance);

                //////////////////////////////////////////////////

                $employeeDeptInfo = User::with(array(
                    "emp_official_info" => function ($query) {
                        $query->with(array("emp_job_details" => function($query) {
                            $query->with(array("dep_info"));
                        }));
                    }
                ))->find($userId);

                $last_entry_time = $employeeDeptInfo->emp_official_info->emp_job_details->dep_info->last_entry_time;

                // find First check_in time
                $f_checkIn = Attendance::select('id')->where('date', $date)->first();

                $lateAttendance = new LateAttendance();
                $lateAttendance->date = $date;
                $lateAttendance->entry_time = $entry;

                if($attendance->check_in > $last_entry_time)
                {

                    $f_checkIn->late_attendance()->save($lateAttendance);

                }

                echo "check_in";
            }
        } else {

            echo "Not_Same_IP";
        }


    }

    // Manual Attendance
    public function manualAttendance($id, $date, $check_in, $check_out)
    {
        $attendance = new Attendance();

        $attendance->date = $date;
        $attendance->check_in = $check_in;
        $attendance->check_out = $check_out;

        $emp = User::find($id);

        //return  $emp->emp_official_info;

        $emp->emp_official_info->emp_attendance()->save($attendance);

        echo "Success";

    }

    public function editAttendance($id, $date, $c_in = null, $c_out = null)
    {
        $this_user = Attendance::find($id);

        $this_user->check_in = $c_in;
        $this_user->check_out = $c_out;


        $this_user->save();

        echo "Success";

    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @show monthly Attendance
     */
    public function monthlyData($uid)
    {

        $i = new FindRole();
        $id = $i->findMyRole($uid);


        // current month & year
        $dt = Carbon::now();
        $date = $dt->format('F Y');

        // userId
        $userId = $id;

        $now = Carbon::now();
        $currentYear = $now->year;
        $currentMonth = $now->month;

        $emp_official_details = User::with('emp_official_info')->find($id);

        $employee_id = $emp_official_details->emp_official_info->id;

        // get all present date of the current month
        $presentDate = Attendance::select('date')->where('employee_id', $employee_id)
            ->whereYear('date', $currentYear)->whereMonth('date', $currentMonth)
            ->orderBy('date')->groupBy('date')->get();

        // variable declaration
        $thisDayAttend = array();
        $exactDayTotal = array();
        $dateStore = array();
        $first_checkIn = array();
        $last_checkOut = array();
        $early_entry = array();
        $late_entry = array();
        $early_leave = array();
        $over_time = array();
        $day_total_time = '';
        $monthly_total_time = '';

        // Employee Office Entry and Leave Time from Department

        $employeeDeptInfo = User::with(array(
            "emp_official_info" => function ($query) {
                $query->with(array("emp_job_details" => function($query) {
                    $query->with(array("dep_info"));
                }));
            }
        ))->find($uid);

        // Office Start and End Time
        $office_start_time = $employeeDeptInfo->emp_official_info->emp_job_details->dep_info->office_start;
        $office_end_time = $employeeDeptInfo->emp_official_info->emp_job_details->dep_info->office_end;

        foreach ($presentDate as $pDate) {

            // get all check_in $ check_out of this date
            $thisDay = Attendance::where('date', $pDate->date)->get();

            // find First check_in time
            $f_checkIn = Attendance::select('check_in')->where('date', $pDate->date)->first();

            // store First check_in into an array
            $first_checkIn[] = $f_checkIn->check_in;

            // Calculation of Early Entry & Late Entry
            $office_s_time = new DateTime($office_start_time);
            $entry_time = new DateTime($f_checkIn->check_in);

            $e_time = $f_checkIn->check_in;

            $timeDiff = date_diff($entry_time, $office_s_time);
            $entry_time_difference = $timeDiff->format("%H:%I:%S");


            if ($office_start_time < $e_time) {
                $early_entry[] = '00:00:00';
                $late_entry[] = $entry_time_difference;
            } elseif ($office_start_time > $e_time) {
                $early_entry[] = $entry_time_difference;
                $late_entry[] = '00:00:00';
            } else {
                $early_entry[] = '00:00:00';
                $late_entry[] = '00:00:00';
            }

            // Find Last check_out time
            $l_checkOut = Attendance::select('check_out')->where('date', $pDate->date)->orderBy('check_out', 'desc')->first();

            // store First check_out into an array
            $last_checkOut[] = $l_checkOut->check_out;

            // Calculation of Early Leave & OverTime
            $office_e_time = new DateTime($office_end_time);
            $departure_time = new DateTime($l_checkOut->check_out);

            $d_time = $l_checkOut->check_out;

            $departure_timeDiff = date_diff($departure_time, $office_e_time);
            $departure_time_difference = $departure_timeDiff->format("%H:%I:%S");


            if ($office_end_time < $d_time) {
                $early_leave[] = '00:00:00';
                $over_time[] = $departure_time_difference;
            } elseif ($office_end_time > $d_time) {
                $early_leave[] = $departure_time_difference;
                $over_time[] = '00:00:00';
            } else {
                $early_leave[] = '00:00:00';
                $over_time[] = '00:00:00';
            }

            // store this date into an array
            $dateStore[] = $thisDay[0]->date;

            foreach ($thisDay as $attInfo) {
                $dteStart = new DateTime($attInfo->check_in);
                $dteEnd = new DateTime($attInfo->check_out);

                $dteDiff = $dteStart->diff($dteEnd);

                $thisDayAttend[] = $dteDiff->format("%H:%I:%S");

            }

            // Day Total Office Time
            $start_time = '00:00:00';
            foreach ($thisDayAttend as $tAttend) {
                $end_time = $tAttend;
                $thisRole = new FindRole();
                $day_total_time = $thisRole->sum_the_time($start_time, $end_time);
                $start_time = $day_total_time;
            }

            $exactDayTotal[] = $day_total_time;

            unset($thisDayAttend);


        }

//        $monthDay = array();
//        foreach ($dateStore as $d_store)
//        {
//            $string = $d_store;
//            $timestamp = strtotime($string);
//            $monthDay[] = date("d", $timestamp);
//        }
//
//        //return $monthDay;
//
//        $first_array = array_combine($monthDay, $early_entry);
//
//        $day_calendar = array();
//        $early_entry_graph_input = array();
//        $i=1;
//        while($i <= 31)
//        {
//            if($i<10)
//            {
//                $day_calendar['0'.$i] = '00:00:00' ;
//            }
//            else
//            {
//                $day_calendar[$i] = '00:00:00' ;
//            }
//
//            $i++;
//        }
//
//        $collection2 = collect($first_array);
//
//        $union = $collection2
//            ->union($day_calendar);
//
//        $tttt = $union->all();
//        ksort($tttt);
//
//        foreach ($tttt as $key => $val) {
//
//            $early_entry_graph_input[] = $val;
//        }
//
//        $early_json = json_encode($early_entry_graph_input);

        //return $early_json;


        // Monthly Total Office Time
        $start_time = '00:00:00';
        foreach ($exactDayTotal as $exDayTotal) {
            $end_time = $exDayTotal;
            $thisRole = new FindRole();
            $monthly_total_time = $thisRole->sum_the_time($start_time, $end_time);
            $start_time = $monthly_total_time;
        }

        $rowCount = count($dateStore);

        // Store all array into a Single array
        $collection = collect([$dateStore, $first_checkIn, $early_entry, $late_entry, $last_checkOut, $early_leave, $over_time, $exactDayTotal]);

        return view('monthlyAttendance', ['collection' => $collection, 'rowCount' => $rowCount, 'userId' => $userId,
            'date' => $date, 'monthly_total_time' => $monthly_total_time]);

    }


    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @Specific Month Attendance
     */

    public function specificMonth(Request $request)
    {
        // UserId
        $userId = $request->id;

        // This Date
        $date = $request->datePicker;

        // Date Conversion
        $pickedDate = $request->datePicker;
        $pickedDateString = strtotime($pickedDate);

        $formattedDate = date('m-Y', $pickedDateString);

        $dateExplode = explode('-', $formattedDate);
        $thisMonth = $dateExplode[0];
        $thisYear = $dateExplode[1];


        $emp_official_details = User::with('emp_official_info')->find($request->id);


        $employee_id = $emp_official_details->emp_official_info->id;

        // Get all Present Date of the specific month
        $presentDate = Attendance::select('date')->where('employee_id', $employee_id)
            ->whereYear('date', $thisYear)->whereMonth('date', $thisMonth)
            ->orderBy('date')->groupBy('date')->get();

        // Array Initialize
        $thisDayAttend = array();
        $exactDayTotal = array();
        $dateStore = array();
        $first_checkIn = array();
        $last_checkOut = array();
        $early_entry = array();
        $late_entry = array();
        $early_leave = array();
        $over_time = array();
        $day_total_time = '';
        $monthly_total_time = '';


        // Employee Office Entry and Leave Time from Department

        $employeeDeptInfo = User::with(array(
            "emp_official_info" => function ($query) {
                $query->with(array("emp_job_details" => function($query) {
                    $query->with(array("dep_info"));
                }));
            }
        ))->find($userId);

        // Office Start and End Time
        $office_start_time = $employeeDeptInfo->emp_official_info->emp_job_details->dep_info->office_start;
        $office_end_time = $employeeDeptInfo->emp_official_info->emp_job_details->dep_info->office_end;


        foreach ($presentDate as $pDate) {

            $thisDay = Attendance::where('date', $pDate->date)->get();

            // First Check In of this day
            $f_checkIn = Attendance::select('check_in')->where('date', $pDate->date)->first();

            // First check_in store into an array
            $first_checkIn[] = $f_checkIn->check_in;

            // Early Entry & Late Entry Calculation
            $office_s_time = new DateTime($office_start_time);
            $entry_time = new DateTime($f_checkIn->check_in);

            $e_time = $f_checkIn->check_in;

            $timeDiff = date_diff($entry_time, $office_s_time);
            $entry_time_difference = $timeDiff->format("%H:%I:%S");


            if ($office_start_time < $e_time) {
                $early_entry[] = '00:00:00';
                $late_entry[] = $entry_time_difference;
            } elseif ($office_start_time > $e_time) {
                $early_entry[] = $entry_time_difference;
                $late_entry[] = '00:00:00';
            } else {
                $early_entry[] = '00:00:00';
                $late_entry[] = '00:00:00';
            }

            // Last check_out of this day
            $l_checkOut = Attendance::select('check_out')->where('date', $pDate->date)->orderBy('check_out', 'desc')->first();

            // Last check_out store into an array
            $last_checkOut[] = $l_checkOut->check_out;

            // Early Leave & Overtime calculation
            $office_e_time = new DateTime($office_end_time);
            $departure_time = new DateTime($l_checkOut->check_out);

            $d_time = $l_checkOut->check_out;

            $departure_timeDiff = date_diff($departure_time, $office_e_time);
            $departure_time_difference = $departure_timeDiff->format("%H:%I:%S");

            if ($office_end_time < $d_time) {
                $early_leave[] = '00:00:00';
                $over_time[] = $departure_time_difference;
            } elseif ($office_end_time > $d_time) {
                $early_leave[] = $departure_time_difference;
                $over_time[] = '00:00:00';
            } else {
                $early_leave[] = '00:00:00';
                $over_time[] = '00:00:00';
            }

            // Date store in a array
            $dateStore[] = $thisDay[0]->date;

            foreach ($thisDay as $attInfo) {
                $dteStart = new DateTime($attInfo->check_in);
                $dteEnd = new DateTime($attInfo->check_out);

                $dteDiff = $dteStart->diff($dteEnd);

                $thisDayAttend[] = $dteDiff->format("%H:%I:%S");

            }

            // Total Office Time of this Day
            $start_time = '00:00:00';
            foreach ($thisDayAttend as $tAttend) {
                $end_time = $tAttend;
                $thisRole = new FindRole();
                $day_total_time = $thisRole->sum_the_time($start_time, $end_time);
                $start_time = $day_total_time;
            }

            $exactDayTotal[] = $day_total_time;

            unset($thisDayAttend);
        }

        // Total Monthly Office Time
        $start_time = '00:00:00';
        foreach ($exactDayTotal as $exDayTotal) {
            $end_time = $exDayTotal;
            $thisRole = new FindRole();
            $monthly_total_time = $thisRole->sum_the_time($start_time, $end_time);
            $start_time = $monthly_total_time;
        }

        $rowCount = count($dateStore);

        // All array stored into an single array
        $collection = collect([$dateStore, $first_checkIn, $early_entry, $late_entry, $last_checkOut, $early_leave, $over_time, $exactDayTotal]);

        return view('monthlyAttendance', ['collection' => $collection, 'rowCount' => $rowCount, 'userId' => $userId, 'date' => $date, 'monthly_total_time' => $monthly_total_time]);

    }



    public static function AttendanceGraphData($id=3, $currentYear = 2017, $currentMonth=04 )
    {
//        // current month & year
//        $dt = Carbon::now();
//        $date = $dt->format('F Y');
//
//        // userId
//        $userId = $id;
//
//        $now = Carbon::now();
//        $currentYear = $now->year;
//        $currentMonth = $now->month;


        $emp_official_details = User::with('emp_official_info')->find($id);


        $employee_id = $emp_official_details->emp_official_info->id;

        // get all present date of the current month
        $presentDate = Attendance::select('date')->where('employee_id', $employee_id)
            ->whereYear('date', $currentYear)->whereMonth('date', $currentMonth)
            ->orderBy('date')->groupBy('date')->get();


        // variable declaration
        $thisDayAttend = array();
        $exactDayTotal = array();
        $dateStore = array();
        $first_checkIn = array();
        $last_checkOut = array();
        $early_entry = array();
        $late_entry = array();
        $early_leave = array();
        $over_time = array();
        $day_total_time = '';
        $monthly_total_time = '';

        // Initialize Office Start and End Time
        $office_start_time = '09:00:00';
        $office_end_time = '17:00:00';


        foreach ($presentDate as $pDate) {

            // get all check_in $ check_out of this date
            $thisDay = Attendance::where('date', $pDate->date)->get();

            // find First check_in time
            $f_checkIn = Attendance::select('check_in')->where('date', $pDate->date)->first();

            // store First check_in into an array
            $first_checkIn[] = $f_checkIn->check_in;

            // Calculation of Early Entry & Late Entry
            $office_s_time = new DateTime($office_start_time);
            $entry_time = new DateTime($f_checkIn->check_in);

            $e_time = $f_checkIn->check_in;

            $timeDiff = date_diff($entry_time, $office_s_time);
            $entry_time_difference = $timeDiff->format("%H:%I:%S");


            if ($office_start_time < $e_time) {
                $early_entry[] = '00:00:00';
                $late_entry[] = $entry_time_difference;
            } elseif ($office_start_time > $e_time) {
                $early_entry[] = $entry_time_difference;
                $late_entry[] = '00:00:00';
            } else {
                $early_entry[] = '00:00:00';
                $late_entry[] = '00:00:00';
            }

            // Find Last check_out time
            $l_checkOut = Attendance::select('check_out')->where('date', $pDate->date)->orderBy('check_out', 'desc')->first();

            // store First check_out into an array
            $last_checkOut[] = $l_checkOut->check_out;

            // Calculation of Early Leave & OverTime
            $office_e_time = new DateTime($office_end_time);
            $departure_time = new DateTime($l_checkOut->check_out);

            $d_time = $l_checkOut->check_out;

            $departure_timeDiff = date_diff($departure_time, $office_e_time);
            $departure_time_difference = $departure_timeDiff->format("%H:%I:%S");


            if ($office_end_time < $d_time) {
                $early_leave[] = '00:00:00';
                $over_time[] = $departure_time_difference;
            } elseif ($office_end_time > $d_time) {
                $early_leave[] = $departure_time_difference;
                $over_time[] = '00:00:00';
            } else {
                $early_leave[] = '00:00:00';
                $over_time[] = '00:00:00';
            }

            // store this date into an array
            $dateStore[] = $thisDay[0]->date;

            foreach ($thisDay as $attInfo) {
                $dteStart = new DateTime($attInfo->check_in);
                $dteEnd = new DateTime($attInfo->check_out);

                $dteDiff = $dteStart->diff($dteEnd);

                $thisDayAttend[] = $dteDiff->format("%H:%I:%S");

            }

            // Day Total Office Time
            $start_time = '00:00:00';
            foreach ($thisDayAttend as $tAttend) {
                $end_time = $tAttend;
                $thisRole = new FindRole();
                $day_total_time = $thisRole->sum_the_time($start_time, $end_time);
                $start_time = $day_total_time;
            }

            $exactDayTotal[] = $day_total_time;

            unset($thisDayAttend);


        }

        $monthDay = array();
        foreach ($dateStore as $d_store)
        {
            $string = $d_store;
            $timestamp = strtotime($string);
            $monthDay[] = date("d", $timestamp);
        }

        // Early Entry Graph Inputs
        $date_early_attend_combined = array_combine($monthDay, $early_entry);

        $day_calendar = array();
        $early_entry_graph_input = array();
        $i=1;
        while($i <= 31)
        {
            if($i<10)
            {
                $day_calendar['0'.$i] = '00:00:00' ;
            }
            else
            {
                $day_calendar[$i] = '00:00:00' ;
            }

            $i++;
        }

        $collection = collect($date_early_attend_combined);

        $union = $collection
            ->union($day_calendar);
        $tttt = $union->all();
        ksort($tttt);

        foreach ($tttt as $key => $val) {
            $early_entry_graph_input[] = $val;
        }

        $early_json = json_encode($early_entry_graph_input);

        // Late Entry Graph Inputs

        $date_late_attend_combined = array_combine($monthDay, $late_entry);

        $day_calendar = array();
        $late_entry_graph_input = array();
        $i=1;
        while($i <= 31)
        {
            if($i<10)
            {
                $day_calendar['0'.$i] = '00:00:00' ;
            }
            else
            {
                $day_calendar[$i] = '00:00:00' ;
            }

            $i++;
        }

        $collection = collect($date_late_attend_combined);

        $union = $collection
            ->union($day_calendar);
        $tttt = $union->all();
        ksort($tttt);

        foreach ($tttt as $key => $val) {
            $late_entry_graph_input[] = $val;
        }

        return $late_json = json_encode($late_entry_graph_input);

        // Early Leave Graph Inputs

        $date_leave_attend_combined = array_combine($monthDay, $early_leave);

        $day_calendar = array();
        $early_entry_graph_input = array();
        $i=1;
        while($i <= 31)
        {
            if($i<10)
            {
                $day_calendar['0'.$i] = '00:00:00' ;
            }
            else
            {
                $day_calendar[$i] = '00:00:00' ;
            }

            $i++;
        }

        $collection = collect($date_leave_attend_combined);

        $union = $collection
            ->union($day_calendar);
        $tttt = $union->all();
        ksort($tttt);

        foreach ($tttt as $key => $val) {
            $early_entry_graph_input[] = $val;
        }

        $leave_json = json_encode($early_entry_graph_input);

        // OverTimeGraph Inputs

        $date_overtime_attend_combined = array_combine($monthDay, $over_time);

        $day_calendar = array();
        $early_entry_graph_input = array();
        $i=1;
        while($i <= 31)
        {
            if($i<10)
            {
                $day_calendar['0'.$i] = '00:00:00' ;
            }
            else
            {
                $day_calendar[$i] = '00:00:00' ;
            }

            $i++;
        }

        $collection = collect($date_overtime_attend_combined);

        $union = $collection
            ->union($day_calendar);
        $tttt = $union->all();
        ksort($tttt);

        foreach ($tttt as $key => $val) {
            $early_entry_graph_input[] = $val;
        }

        $overtime_json = json_encode($early_entry_graph_input);





//        // Monthly Total Office Time
//        $start_time = '00:00:00';
//        foreach ($exactDayTotal as $exDayTotal) {
//            $end_time = $exDayTotal;
//            $thisRole = new FindRole();
//            $monthly_total_time = $thisRole->sum_the_time($start_time, $end_time);
//            $start_time = $monthly_total_time;
//        }
//
//        $rowCount = count($dateStore);
//
//        // Store all array into a Single array
//        $collection = collect([$dateStore, $first_checkIn, $early_entry, $late_entry, $last_checkOut, $early_leave, $over_time, $exactDayTotal]);
//
//        return view('monthlyAttendance', ['collection' => $collection, 'rowCount' => $rowCount, 'userId' => $userId,
//            'date' => $date, 'monthly_total_time' => $monthly_total_time]);
    }


}
