@extends('layouts.master')
@section('title', 'Set Salary')
@section('content')

    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>Employee Salary</h3>
                </div>


            </div>
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Set Employee Salary<!--<small>Input different form elements</small>--></h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <br/>
                            <form id="form" action="{{route('set_salary.store')}}" method="POST" class="form-horizontal" role="form" files="true" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div class="form-body">

                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Salary Type</label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <select class="select2_single form-control salary " tabindex="-1" id="salary_type" name="salary_type" onchange="set_salary();return false;">
                                                <option value="">Select Option</option>
                                                <option value="Monthly">Monthly</option>
                                                <option value="Daily">Daily</option>
                                                <option value="Hourly">Hourly</option>

                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Salary Grade<span class="tp" style="color: red;">*</span></label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="text" class="form-control col-md-7 col-xs-12 valid hour_valid daily_valid"
                                                   placeholder="Enter Salary Grade" id="salary_grade" name="salary_grade" value="{{old('salary_grade')}}" >
                                            @if ($errors->has('salary_grade'))
                                                <span class="help-block">
                                                <strong style="font-weight: bold;">{{ $errors->first('salary_grade') }}</strong>
                                            </span>
                                            @endif
                                            <span class="chk" id="chk" style="display: none;">
                                                <strong style="color:red">Grade already used</strong>
                                                    <div col-md-6 col-sm-6 col-xs-12 id="search-result">

                                                    </div>
                                            </span>
                                        </div>
                                    </div>

                                    <div id="monthly" style="display: none;margin-top: 46px;">

                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Basic Salary<span class="tp" style="color: red;">*</span></label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="number" class="form-control col-md-7 col-xs-12 valid"
                                                       placeholder="Enter Basic Salary" id="basic_salary" name="basic_salary"  value="" >
                                                @if ($errors->has('basic_salary'))
                                                    <span class="help-block">
                                                <strong>{{ $errors->first('basic_salary') }}</strong>
                                            </span>
                                                @endif
                                            </div>
                                        </div>



                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Overtime Rate(Per Hour)</label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="number" class="form-control col-md-7 col-xs-12"
                                                       placeholder="Enter Overtime rate" id="over_time" name="over_time" value="" >
                                                @if ($errors->has('over_time'))
                                                    <span class="help-block">
                                                <strong>{{ $errors->first('over_time') }}</strong>
                                            </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="ln_solid"></div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-6 panel-heading" id="allowance"><b><p style="font-size: medium">Allowances</p></b><hr style="width: 100%; height: 1px; background-color: green; color: green;" />
                                                    <div style="opacity: 0.7;">
                                                        <div class="form-group" style="margin-top: 55px;">
                                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Medical Expanse</label>
                                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                                <input type="hidden" id="title" name="title[]" placeholder="Enter Title" value="medical" class="form-control has-feedback-left col-md-7 col-xs-12">
                                                                <input type="number" class="form-control col-md-7 col-xs-12"
                                                                       placeholder="Enter Medical expanse" id="medical_expanse" name="amount[]" value=""  >
                                                                @if ($errors->has('over_time'))
                                                                    <span class="help-block">
                                                                <strong>{{ $errors->first('medical_expanse') }}</strong>
                                                                </span>
                                                                @endif
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">House Rent</label>
                                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                                <input type="hidden" id="title" name="title[]" placeholder="Enter Title" value="houserent" class="form-control has-feedback-left col-md-7 col-xs-12">
                                                                <input type="number" class="form-control col-md-7 col-xs-12"
                                                                       placeholder="Enter House Rent" name="amount[]" id="house_rent" value=""  >

                                                                @if ($errors->has('house_rent'))
                                                                    <span class="help-block">
                                                                <strong>{{ $errors->first('house_rent') }}</strong>
                                                                </span>
                                                                @endif
                                                            </div>

                                                        </div>
                                                        <div id="allow">
                                                        </div>

                                                        <div class="form-group">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3 col-sm-3 col-xs-12"></label>
                                                                <div class="col-md-3 col-sm-3 col-xs-12">
                                                                    <button type="button" class="btn btn-primary" id="add_more_allow">
                                                                        Add <span class="glyphicon glyphicon-plus"></span>
                                                                    </button>
                                                                </div>

                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                                <div class="col-md-6 panel-heading" id="deductions"><b><p style="font-size: medium">Deductions</p></b><hr style="width: 100%; height: 1px; background-color: green; color: green;" />
                                                    <div style="opacity: 0.7;">
                                                        <div class="form-group" style="margin-top: 55px;">
                                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Provident Fund</label>
                                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                                <input type="hidden" id="tit" name="title_deduction[]"  placeholder="Enter Title" value="provident_fund" class="form-control has-feedback-left col-md-7 col-xs-12">
                                                                <input type="number" class="form-control col-md-7 col-xs-12"
                                                                       placeholder="Enter Provident Fund" name="amount_deduction[]" id="provident_fund" >
                                                                @if ($errors->has('provident_fund'))
                                                                    <span class="help-block">
                                                                <strong>{{ $errors->first('provident_fund') }}</strong>
                                                                </span>
                                                                @endif
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Tax Deduction</label>
                                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                                <input type="hidden" id="tit" name="title_deduction[]"  placeholder="Enter Title" value="tax_deduction" class="form-control has-feedback-left col-md-7 col-xs-12">
                                                                <input type="number" class="form-control col-md-7 col-xs-12"
                                                                       placeholder="Enter Tax Deduction" id="tax_deduction" name="amount_deduction[]" value="" >

                                                                @if ($errors->has('tax_deduction'))
                                                                    <span class="help-block">
                                                                <strong>{{ $errors->first('tax_deduction') }}</strong>
                                                                </span>
                                                                @endif
                                                            </div>

                                                        </div>

                                                        <div id="deduct">
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="control-label col-md-3 col-sm-3 col-xs-12"></label>
                                                            <div class="col-md-3 col-sm-3 col-xs-12">
                                                                <button type="button" class="btn btn-primary" id="add_more_deduction">
                                                                    Add <span class="glyphicon glyphicon-plus"></span>
                                                                </button>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12"></label>
                                            <div class="col-md-3 col-sm-3 col-xs-12">
                                                <button type="button" class="btn btn-primary center-block" id="total_salary">
                                                    Salary Calculate <i class="fa fa-cc-paypal"></i>
                                                </button>
                                            </div>

                                        </div>

                                        <div class="form-group" id="calculate" style="display: none;">
                                            <div class="panel panel-default">
                                                <div class="panel-heading"><p style="text-align: center;font-size: medium;font-weight: bold;">Total Salary Details</p></div>
                                                <div class="panel-body">

                                                    <div class="form-group">
                                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Gross Salary</label>
                                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                                            <input type="text" class="form-control col-md-7 col-xs-12"
                                                                   name="gross_salary" id="gross_salary" value="0.0" readonly>
                                                            @if ($errors->has('gross_salary'))
                                                                <span class="help-block">
                                                              <strong>{{ $errors->first('gross_salary') }}</strong>
                                                            </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Total Deduction</label>
                                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                                            <input type="text" class="form-control col-md-7 col-xs-12"
                                                                   name="total_deduction" id="total_deduction" value="0.0" readonly>
                                                            @if ($errors->has('total_deduction'))
                                                                <span class="help-block">
                                                              <strong>{{ $errors->first('total_deduction') }}</strong>
                                                            </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Net Salary</label>
                                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                                            <input type="text" class="form-control col-md-7 col-xs-12"
                                                                   name="net_salary" id="net_salary" value="0.0" readonly>
                                                            @if ($errors->has('net_salary'))
                                                                <span class="help-block">
                                                              <strong>{{ $errors->first('net_salary') }}</strong>
                                                            </span>
                                                            @endif
                                                        </div>
                                                    </div>


                                                </div>

                                            </div>
                                        </div>

                                    </div>
                                    <div id="hourly" style="display: none;margin-top: 46px;">


                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Hourly Rate</label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="text" class="form-control col-md-7 col-xs-12 hour_valid"
                                                       placeholder="Enter Hourly Rate" name="hourly_rate" id="hourly_rate" value="{{old('hourly_rate')}}" >
                                                @if ($errors->has('hourly_rate'))
                                                    <span class="help-block">
                                                <strong>{{ $errors->first('hourly_rate') }}</strong>
                                            </span>
                                                @endif
                                            </div>
                                        </div>

                                    </div>
                                    <div id="daily" style="display: none;margin-top: 46px;">


                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Daily Rate</label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="text" class="form-control col-md-7 col-xs-12 daily_valid"
                                                       placeholder="Enter Daily Rate" id="daily_rate" name="daily_rate" value="{{old('daily_rate')}}" >
                                                @if ($errors->has('daily_rate'))
                                                    <span class="help-block">
                                                <strong>{{ $errors->first('daily_rate') }}</strong>
                                            </span>
                                                @endif
                                            </div>
                                        </div>

                                    </div>


                                    <div class="form-group">
                                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3" id="add-option" style="display: none;">
                                            <button  type="submit" class="btn btn-primary">Cancel</button>
                                            <button type="submit" class="btn btn-success" id="sub">Submit</button>
                                        </div>
                                    </div>

                                </div>
                            </form>



                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(document).on('click', '#total_salary', function () {

            $('#calculate').show();
            $('#add-option').show();



            var value = 0.0;
            var salary = $('#basic_salary').val();
            var overtime = $('#over_time').val();
            var houseRent = $('#house_rent').val();
            var medicalExpense = $('#medical_expanse').val();
            var providentFund = $('#provident_fund').val();
            var title_name = $('#title').val();
            var tax = $('#tax_deduction').val();



            var sal ;
            var over_time;
            var house_Rent;
            var medical_Expense;
            var provident_Fund;
            var taxAmount;



            if(salary == ""){
                sal = parseFloat("0.0");
            }else {
                sal = parseFloat(salary);
            }


            if(overtime == ""){
                over_time = parseFloat("0.0");
            }else {
                over_time = parseFloat(overtime);
            }


            /*other allowances total calculation*/
            var other_allowances = parseFloat("0.0");
            var names=document.getElementsByName('amount[]');

            for(key=0; key < names.length; key++)  {

                other_allowances = other_allowances+ parseFloat(names[key].value);
            }



            /*other tax total calculation*/

            var other_deductions = parseFloat("0.0");
            var deduct=document.getElementsByName('amount_deduction[]');

            for(key=0; key < deduct.length; key++)  {

                other_deductions = other_deductions+ parseFloat(deduct[key].value);
            }



            var grossSalary = parseFloat(sal)+parseFloat(other_allowances);


            var gSalary = 0.0;
            if(grossSalary == ""){
                $('#gross_salary').val(parseFloat(gSalary));
            }
            else{
                $('#gross_salary').val(parseFloat(grossSalary));
            }


            var totalDeductions = other_deductions;
            $('#total_deduction').val(totalDeductions);

            var netSalary =  parseFloat(grossSalary) - parseFloat(totalDeductions);
            $('#net_salary').val(netSalary);



        });
    </script>
    <script>

        function set_salary() {

            var category = $('#salary_type').val();
            if(category=='Monthly')
            {
                $('#daily').hide();
                $('#hourly').hide();
                $('#monthly').show();
                $('#add-option').hide();

            }
            else if(category=='Hourly')
            {
                $('#daily').hide();
                $('#monthly').hide();
                $('#hourly').show();
                $('#add-option').show();
            }
            else if(category=='Daily'){
                $('#daily').show();
                $('#hourly').hide();
                $('#monthly').hide();
                $('#add-option').show();
            }
//---------------------
        }

    </script>

    <script>
        $(document).on('click', '#add_more_allow', function () {
            $('#allow').append('<div><br/><hr/><div class="form-group"> ' +
                '<label class="control-label col-md-3 col-sm-3 col-xs-12" for="title">Title <span class="" style="color: red;">*</span> ' +
                '</label> ' +
                '<div class="col-md-6 col-sm-6 col-xs-12"> ' +
                '<input type="text" id="title" name="title[]" placeholder="Enter Title" class="form-control has-feedback-left col-md-7 col-xs-12"> ' +
                '<span class="fa fa-suitcase form-control-feedback left" aria-hidden="true"></span> ' +
                '</div> ' +
                '</div> ' +

                '<div class="form-group"> ' +
                '<label class="control-label col-md-3 col-sm-3 col-xs-12" for="amount">Amount <span class="" style="color: red;">*</span></label> ' +
                '<div class="col-md-6 col-sm-6 col-xs-12"> ' +
                '<input type="number" id="amount" name="amount[]" placeholder="Enter Amount"  class="form-control has-feedback-left col-md-7 col-xs-12" > ' +
                '<span class="fa fa-mobile-phone form-control-feedback left" aria-hidden="true"></span> ' +
                '</div></div><div class="col-md-9"></div><div class="col-md-3"> <button type="button"  class="btn btn-sm btn-danger btn-circle removeExp"><i class="glyphicon glyphicon-remove"></i></button> </div>');
        })
        $(document).on('click', '.removeExp', function () {
            $(this).parent().parent().remove();
        });
    </script>
    <script>
        $(document).on('click', '#add_more_deduction', function () {
            $('#deduct').append('<div><br/><hr/><div class="form-group"> ' +
                '<label class="control-label col-md-3 col-sm-3 col-xs-12" for="title_deduction">Title <span class="" style="color: red;">*</span> ' +
                '</label> ' +
                '<div class="col-md-6 col-sm-6 col-xs-12"> ' +
                '<input type="text" id="title_deduction" name="title_deduction[]" placeholder="Enter Title"  class="form-control has-feedback-left col-md-7 col-xs-12"> ' +
                '<span class="fa fa-suitcase form-control-feedback left" aria-hidden="true"></span> ' +
                '</div> ' +
                '</div> ' +

                '<div class="form-group"> ' +
                '<label class="control-label col-md-3 col-sm-3 col-xs-12" for="amount_deduction">Amount <span class="" style="color: red;">*</span></label> ' +
                '<div class="col-md-6 col-sm-6 col-xs-12"> ' +
                '<input type="number" id="amount_deduction" name="amount_deduction[]" placeholder="Enter Amount"  class="form-control has-feedback-left col-md-7 col-xs-12" > ' +
                '<span class="fa fa-mobile-phone form-control-feedback left" aria-hidden="true"></span> ' +
                '</div></div><div class="col-md-9"></div><div class="col-md-3"> <button type="button"  class="btn btn-sm btn-danger btn-circle removeExpt"><i class="glyphicon glyphicon-remove"></i></button> </div>');
        })
        $(document).on('click', '.removeExpt', function () {
            $(this).parent().parent().remove();
        });
    </script>

    <script>
        $(document).ready(function() {
            $(".select2_single").select2({
                placeholder: "Select a state",
                allowClear: true
            });

        });
    </script>

    <script type="text/javascript">

        $('#basic_salary').on('focus', function() {

            var salaryGrade = $('#salary_grade').val();

            $.ajaxSetup({
                headers : {
                    'X-CSRF-TOKEN' : $('meta[name="_token"]').attr('content')
                }

            })
            $.ajax({
                type : 'GET',
                url : './checkSalaryGrade',
                data: {'grade': salaryGrade},
                success: function(data){
                    if(data == "not")
                    {

                        $("#chk").css("display", "block");

                        $('#sub').prop('disabled', true);
                    }

                    else
                    {

                        $("#chk").css("display", "none");
                        $('#sub').prop('disabled', false);
                    }

                },
                error:function(data){
                    console.log('Error:',data);
                }

            });

//
//
        });

        //});
    </script>

    <script type="text/javascript">


        $(document).ready(function() {
            $('select').on('change', function() {


                $('input[type=text],input[type=number]').focus(function() {

                    $(this).css({
                        "border": "",
                        "background": ""
                    });

                });

                $('#salary_grade').css({
                    "border": "",
                    "background": ""
                });

                var salary = $(this).val();

                if(salary=='Monthly')
                {
                    $(document).on('click', '#total_salary', function () {


                        var isValid = true;
                        $('.valid').each(function () {
                            if ($.trim($(this).val()) == '') {
                                isValid = false; /!* Required class style *!/
                                $(this).css({
                                    "border": "1px solid red",
                                    "background": "#FFCECE"
                                });
                            }
                            else { /!* Required class style removed *!/
                                $(this).css({
                                    "border": "",
                                    "background": ""
                                });
                            }
                        });
                        if (isValid == false)
                            $('#sub').prop('disabled', true);
                        else{

                        }

                    })
                }
                else if(salary=='Hourly')
                {


                    $("#salary_grade").val('');
                    $("#chk").css("display", "none");
                    $('#sub').prop('disabled', false);
                    $(document).on('click', '#sub', function (e) {



                        var isValid = true;
                        $('.hour_valid').each(function () {
                            if ($.trim($(this).val()) == '') {

                                isValid = false; /!* Required class style *!/
                                $(this).css({
                                    "border": "1px solid red",
                                    "background": "#FFCECE"
                                });
                            }
                            else { /!* Required class style removed *!/
                                $(this).css({
                                    "border": "",
                                    "background": ""
                                });
                            }
                        });
                        if (isValid == false)
                            e.preventDefault();
                        else{

                        }

                    })



                }
                else if(salary=='Daily'){

                    $("#salary_grade").val('');
                    $("#chk").css("display", "none");
                    $('#sub').prop('disabled', false);
                    $(document).on('click', '#sub', function (e) {



                        var isValid = true;
                        $('.daily_valid').each(function () {
                            if ($.trim($(this).val()) == '') {

                                isValid = false; /!* Required class style *!/
                                $(this).css({
                                    "border": "1px solid red",
                                    "background": "#FFCECE"
                                });
                            }
                            else { /!* Required class style removed *!/
                                $(this).css({
                                    "border": "",
                                    "background": ""
                                });
                            }
                        });
                        if (isValid == false)
                            e.preventDefault();
                        else{

                        }

                    });

                }
            })
        })





    </script>

    <script type="text/javascript">

        $('#hourly_rate').on('focus', function() {

            var salaryGrade = $('#salary_grade').val();

            $.ajaxSetup({
                headers : {
                    'X-CSRF-TOKEN' : $('meta[name="_token"]').attr('content')
                }

            })
            $.ajax({
                type : 'GET',
                url : './checkSalaryGrade',
                data: {'grade': salaryGrade},
                success: function(data){
                    if(data == "not")
                    {

                        $("#chk").css("display", "block");

                        $('#sub').prop('disabled', true);
                    }

                    else
                    {

                        $("#chk").css("display", "none");
                        $('#sub').prop('disabled', false);
                    }

                },
                error:function(data){
                    console.log('Error:',data);
                }

            });

//
//
        });

        //});
    </script>

    <script type="text/javascript">

        $('#daily_rate').on('focus', function() {

            var salaryGrade = $('#salary_grade').val();

            $.ajaxSetup({
                headers : {
                    'X-CSRF-TOKEN' : $('meta[name="_token"]').attr('content')
                }

            })
            $.ajax({
                type : 'GET',
                url : './checkSalaryGrade',
                data: {'grade': salaryGrade},
                success: function(data){
                    if(data == "not")
                    {

                        $("#chk").css("display", "block");

                        $('#sub').prop('disabled', true);
                    }

                    else
                    {

                        $("#chk").css("display", "none");
                        $('#sub').prop('disabled', false);
                    }

                },
                error:function(data){
                    console.log('Error:',data);
                }

            });

//
//
        });

        //});
    </script>
@endsection