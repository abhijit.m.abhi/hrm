<?php

namespace App\Http\Controllers;

use App\CompanyStructure;
use App\Qualifications;
use App\QualificationSetup;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class QualificationsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {


    }

    public function asd()
    {

        $skill = QualificationSetup::where('type','=','Skills')->get();
        $edu = QualificationSetup::where('type','=','Education')->get();
        $cer = QualificationSetup::where('type','=','Certification')->get();
        return view('addQualifications', ['skill' => $skill,'edu' => $edu,'cer' => $cer]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
       // return view('addQualifications');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    // edit data function
    public function editItem(Request $req) {


        $qualification = QualificationSetup::find ($req->id);
        $qualification->name = $req->name;
        $qualification->description = $req->description;
        $qualification->type = $req->type;
        $qualification->update();


        return response()->json($qualification);
    }

    // add data into database
    public function addItem(Request $req) {

        $qualification = new QualificationSetup();
        $qualification->name = $req->name;
        $qualification->description = $req->description;
        $qualification->type = $req->type;
        $qualification->save();
            return response()->json($qualification);

    }

}
