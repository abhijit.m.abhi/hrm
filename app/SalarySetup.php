<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SalarySetup extends Model
{
    //
    protected $table = 'salary_setups';
    protected $fillable = [
        'salary_type',
        'salary_grade',
        'basic_salary',
        'overtime',
        'allowances',
        'deductions',
        'net_salary',
        'flag',
    ];
}
