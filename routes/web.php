<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\CompanyStructure;
use App\EmployeePersonalDetail;
use App\JobDetailsSetup;
use App\User;
use App\Holiday;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Response;


Route::get('login', 'Auth\LoginController@showLoginForm');
Route::post('login', 'Auth\LoginController@authenticate');
Route::get('logout', 'Auth\LoginController@logout');
Route::get('resetForm', 'Auth\LoginController@resetForm');
Route::get('check/{token?}', 'Auth\LoginController@forgetPassword');
Route::get('resetPassword/{key?}/{m?}', 'Auth\LoginController@resetPassword');
Route::post('/recoverPasswordEmail', 'Auth\LoginController@recoverPasswordEmail');


Route::group(['middleware' => ['auth', 'cuser']], function () {


    Route::get('/', function () {
        return view('dashboard');
    });

    Route::get('/dashboard', function () {
        return view('dashboard');
    });


    /*
     * User Route
     */

    Route::resource('user', 'UserController');

    Route::delete('/user/{id?}', function ($id) {

        $user = User::where('id', $id)->update(['flag' => 0]);
        return response::json($user);
    });

    // User Status Change

    Route::get('/type_change/{id?}', function ($id) {

        $flag = USER::find($id)->flag;

        if ($flag == 1) {
            User::where('id', $id)->update(['flag' => 2]);
            echo "Inactivated";

        } else if ($flag == 2) {
            User::where('id', $id)->update(['flag' => 1]);
            echo "Activated";
        }

    });

    // User Type Change

    Route::get('/t_change/{id?}/{type?}', function ($id, $type) {

        if ($type == "Admin") {
            User::where('id', $id)->update(['type' => 'Admin']);
            echo "Admin";
        } elseif ($type == "User") {
            User::where('id', $id)->update(['type' => 'User']);
            echo "User";
        }

    });


    /*
     * Job Details Setup
     */

    Route::resource('jobDetails', 'JobDetailsSetupController');
    Route::delete('/jobDetails/{id?}', function ($id) {
        $income = JobDetailsSetup::destroy($id);
        return response::json($income);
    });

    /*
     * Employee Setup
     */

    Route::resource('employeeSetup', 'employeeSetupController');
    Route::delete('/employee/{id?}', function ($id) {

        $employee = EmployeePersonalDetail::where('id', $id)->update(['flag' => 0]);

        return response::json($employee);
    });

    Route::get('email_check/{mail_check?}', function () {
        $mail = $_GET['mail_check'];
        //  $check = Loan::matchAccount($account_no);
        $c = User::where('email', $mail)->get()->count();
        if ($c == 0) {
            echo $a = "ok";
            //return response::json($a);

        } else {

            echo $a = "not";
        }

    });

    // Employee profile



    Route::get('empProfile/{id}', 'employeeSetupController@empProfile');





    Route::resource('company_structure', 'CompanyStructureController');

    Route::delete('/structure/{id?}', function ($id) {

        $structureList = CompanyStructure::destroy($id);
        return response::json($structureList);
    });

    Route::get('qualification', 'QualificationsController@asd');

    Route::get('/editItem', 'QualificationsController@editItem');
    Route::get('/addItem', 'QualificationsController@addItem');


    Route::delete('/delete/{id?}', function ($id) {

        $qualification = \App\QualificationSetup::destroy($id);
        return response::json($qualification);
    });


    Route::get('training_setup', 'TrainingController@view');

    Route::get('/addCourse', 'TrainingController@addCourse');

    Route::get('/editCourse', 'TrainingController@editCourse');


    Route::delete('/deleteCourse/{id?}', function ($id) {

        $course = \App\Course::destroy($id);
        return response::json($course);
    });

    Route::get('/addTraining', 'TrainingController@addTraining');

    Route::get('/editTraining', 'TrainingController@editTraining');

    Route::delete('/deleteTraining/{id?}', function ($id) {

        $training = \App\TranningSession::destroy($id);
        return response::json($training);
    });

    Route::get('/addEmployeeTraining', 'TrainingController@addEmployeeTraining');

    Route::delete('/delete_empTrain/{id?}', function ($id) {

        $emp_train = \App\EmployeeTraningSession::destroy($id);
        return response::json($emp_train);
    });

    Route::get('/editEmpTraining', 'TrainingController@edit_emp_Training');


    /*Loan Setup*/
    Route::get('loan_setup', 'LoanController@view');
    Route::get('/addLoan', 'LoanController@addLoan');
    Route::get('/editLoan', 'LoanController@editLoan');
    Route::delete('/deleteLoan/{id?}', function ($id) {

        $loan = \App\LoanType::destroy($id);
        return response::json($loan);
    });


    Route::get('/addEmployeeLoan', 'LoanController@addEmployeeLoan');
    Route::get('/editEmployeeLoan', 'LoanController@edit_emp_Loan');
    Route::delete('/delete_EmployeeLoan/{id?}', function ($id) {
        $emp_train = \App\EmployeeLoan::destroy($id);
        return response::json($emp_train);
    });

    Route::get('employee_salary', 'employeeSetupController@employee_salary');
    Route::get('salary_setup', 'PayrollController@view');
    Route::resource('set_salary', 'PayrollController');

    Route::get('view_salary_setup', 'PayrollController@view_setup');

    // Loan Status Change

    Route::get('/status_change/{id?}', function ($id) {
        $status = \App\EmployeeLoan::find($id)->status;
        if ($status == "Active") {
            \App\EmployeeLoan::where('id', $id)->update(['status' => "Deactive"]);
        } else if ($status == "Deactive") {
            \App\EmployeeLoan::where('id', $id)->update(['status' => "Active"]);
        }
    });

//    Installment
    Route::resource('installment', 'InstallmentController');
    Route::get('/employee-loan/{id?}', function ($id) {
        $employee_loans = \App\EmployeeLoan::where('employee_id', $id)->get();
        return response::json($employee_loans);
    });

    Route::get('/loan-installment/{id?}', function ($id) {
        $installments = \App\Installment::where('loan_id', $id)->get();
        return response::json($installments);
    });
    Route::get('/addInstallment', 'InstallmentController@add_installment');
    Route::delete('/delete_LoanInstallment/{id?}', function ($id) {
        $del_installment = \App\Installment::destroy($id);
        return response::json($del_installment);
    });


    /**
     * Attendance
     */

    Route::resource('attendance', 'AttendanceController');
    Route::any('attendance/monthlyView/{id}', 'AttendanceController@show');
    Route::get('attendance/monthly/{id}', 'AttendanceController@monthlyData');
    Route::post('attendance/specific_month', 'AttendanceController@specificMonth');
    Route::get('attendance/specific_month_user/{id}', 'AttendanceController@monthlyData');
    Route::get('/giv_atten', 'AttendanceController@giveAttendance');
    Route::get('/manual_atten/{id}/{date}/{check_in}/{check_out}', 'AttendanceController@manualAttendance');
    Route::get('/edit_atten/{id}/{date}/{c_in?}/{c_out?}', 'AttendanceController@editAttendance');

    Route::get('attendance_graph_data' , 'AttendanceController@AttendanceGraphData');

    Route::get('checkSalaryGrade/{salaryGrade?}', function () {
        $salary_grade = $_GET['grade'];

        $count = \App\SalarySetup::where('salary_grade', '=', $salary_grade)->where('flag', '=', 0)->get()->count();
        if ($count == 0) {
            echo $a = "ok";

        } else {

            echo $a = "not";
        }

    });

    Route::get('/deleteSalary/{id?}', function ($id) {

        $delete = \App\SalarySetup::find($id);
        $delete->flag = 1;
        $delete->update();
        return response::json($delete);
    });





    /*Employee salary setup*/

    Route::get('emp_salary_setup', 'EmployeeSalarySetupController@view');
    Route::get('salaryGradeInfo', 'EmployeeSalarySetupController@salaryGradeInfo');
    Route::get('salaryGrade', 'EmployeeSalarySetupController@salaryGrade');

    Route::resource('set_employee_salary', 'EmployeeSalarySetupController');

    Route::get('emp_view_salary_setup', 'EmployeeSalarySetupController@view_salary');

    Route::get('/deleteEmployeeSalary/{id?}',function($id){

        $delete = \App\EmployeeSalarySetup::find($id);
        $delete->flag = 1;
        $delete->update();
        return response::json($delete);
    });
    Route::get('view_details/{id}', 'EmployeeSalarySetupController@view_details');

    Route::get('checkEmpId/{employee_id?}', function () {
        $emp_id = $_GET['employee_id'];

        $count = \App\EmployeeSalarySetup::where('employee_id', '=', $emp_id)->where('flag', '=', 0)->get()->count();
        if ($count == 0) {
            echo $a = "ok";

        } else {

            echo $a = "not";
        }

    });


    /*Employee Loan setup*/
    Route::get('employeeLoan/{id}', 'LoanController@empLoan');


    Route::get('/applyEmployeeLoan', 'LoanController@applyEmployeeLoan');
    Route::get('/editApplyEmployeeLoan', 'LoanController@editApplyEmployeeLoan');

    Route::get('/showInstallment', 'LoanController@showInstallment');


    /*Resign Setting*/



    Route::get('resignApply/{id}', 'ResignController@view');

    Route::get('/apply_for_resign', 'ResignController@applyResign');

    Route::get('/editResign', 'ResignController@editResign');

    Route::get('/deleteEmployeeResign/{id?}',function($id){

        $delete = \App\Resign::find($id);
        $delete->flag = 1;
        $delete->update();
        return response::json($delete);
    });

    Route::get('/resignList', 'ResignController@ResignList');


    Route::get('/resign_decision', 'ResignController@Resign_decision');

    Route::get('/reApply', 'ResignController@reApply');


    /*Bill Apply*/
    Route::get('billApply/{id}', 'BillController@view');
    Route::resource('bill','BillController');

    Route::get('view_employee_bill/{id}', 'BillController@viewBillList');

    Route::get('deleteBill', 'BillController@deleteBill');

    Route::get('billInfo', 'BillController@billInfo');
    Route::get('/bill_decision', 'BillController@bill_decision');


    //    ****** Start Leave Settings *****
    //    ||-- Weekly Holiday Settings --||
    Route::resource('weekly-holiday', 'LeaveController');
    Route::get('/add_weekly_holiday', 'LeaveController@addWeeklyHoliday');
    Route::get('/edit_weekly_holiday', 'LeaveController@editWeeklyHoliday');
    Route::get('/delete_weekly_holiday', 'LeaveController@deleteWeeklyHoliday');

    //    ||-- Public Holiday Settings --||
    Route::get('public-holiday', 'LeaveController@view');
    Route::get('/add_public_holiday', 'LeaveController@addPublicHoliday');
    Route::get('/edit_public_holiday', 'LeaveController@editPublicHoliday');
    Route::delete('/delete_public_holiday/{id?}', function ($id) {
        $p_holiday = \App\Holiday::destroy($id);
        return response::json($p_holiday);
    });

    //    ||-- Employee Leave Settings --||
    Route::get('employee-leave', 'LeaveController@viewEmpLeave');
    Route::get('/add-employee-leave', 'LeaveController@addEmployeeLeave');
    Route::get('/edit-employee-leave', 'LeaveController@editEmployeeLeave');
    Route::delete('/delete-employee-leave/{id?}', function ($id) {
        $emp_leave = \App\EmployeeLeave::destroy($id);
        return response::json($emp_leave);
    });

    //    ****** End Leave Settings *****
    Route::get('billApply', 'BillController@view');
    Route::resource('bill','BillController');

    Route::get('view_employee_bill', 'BillController@viewBillList');

    Route::get('deleteBill', 'BillController@deleteBill');
    Route::get('adminDeleteBill', 'BillController@deleteBill');

    Route::get('billInfo', 'BillController@billInfo');
    Route::get('/bill_decision', 'BillController@bill_decision');
    Route::get('/addBillInfo', 'BillController@addBillInfo');
    Route::post('/employeeBill', 'BillController@employeeBill');
    Route::get('/reportView', 'BillController@reportView');

    Route::get('/billReportInfo', 'BillController@billReportInfo');

});



