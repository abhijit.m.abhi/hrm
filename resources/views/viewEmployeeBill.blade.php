@extends('layouts.master')
@section('title', 'Employee Bill')
@section('content')

    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>Employee Bill</h3>
                </div>


            </div>
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>View Employee Bill<!--<small>Input different form elements</small>--></h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">

                            <div class="table-responsive">
                                <table id="Hourly"  class="empSalary table table-striped table-bordered table-hover display" >
                                    <thead>
                                    <tr class="alert-info">
                                        <th>Bill Name</th>
                                        <th>Date</th>
                                        <th>Total</th>
                                        <th>Status</th>
                                        <th>Attachment</th>

                                        <th>Action</th>
                                    </tr>
                                    </thead>

                                    <tbody>
                                    @foreach($viewList as $list)


                                        <tr id="monthly{{$list->id}}">
                                            <td>{{$list->title}}</td>
                                            <td>{{$list->date}}</td>
                                            <td>{{$list->total_bill}}</td>
                                            <td style="color: red;">
                                                <?php
                                                if($list->flag==0)
                                                    {
                                                        echo "Pending";
                                                    }

                                                    else if($list->flag==2)
                                                    {
                                                        echo "Rejected";
                                                    }
                                                    else if($list->flag==1)
                                                    {
                                                        echo "Accepted";
                                                    }
                                                    else{

                                                    }

                                                ?>

                                            <td>

                                                <a href="{{asset('/files/'.$list->attachment)}}">
                                                    {{$list->attachment}}</a>
                                            </td>

                                            <?php
                                            if($list->flag==0 || $list->flag==2 )
                                            {
                                            ?>

                                            <td>

                                                <a href="{{route('bill.edit', $list->id)}}" class="btn btn-primary"><span class="glyphicon glyphicon-edit"></span></a>

                                                <button class="btn confirm btn-danger delete_p"  data-toggle="modal" data-id="{{$list->id}}" data-attachment="{{$list->attachment}}"><span class="glyphicon glyphicon-trash"></span></button>

                                            </td>

                                            <?php
                                            }
                                            else if($list->flag==1){


                                            ?>
                                            <td style="color: red;"> Decided </td>

                                            <?php } ?>

                                        </tr>


                                    @endforeach
                                    <meta name="_token" content="{!! csrf_token() !!}" />

                                    </tbody>

                                </table>
                            </div>

                            <div class="modal fade" tabindex="-1" id="myModal" aria-labelledby="myModalLabel" role="dialog">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            <h3 class="modal-title ">Delete Confirmation </h3>
                                        </div>
                                        <div class="modal-body ">
                                            <p><b>Do you want to delete?&hellip;</b></p>
                                            <input type="hidden" id="d_id">
                                            <input type="hidden" id="attach_file">
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default warning" data-dismiss="modal">Close</button>
                                            <button type="button " class="btn btn-primary delete_confirm btn-danger" data-dismiss="modal" value="" data-token="{{ csrf_token() }}"data-dismiss="modal">Delete</button>
                                        </div>
                                    </div><!-- /.modal-content -->
                                </div><!-- /.modal-dialog -->
                            </div>




                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <script type="text/javascript">

        $(document).on("click", ".delete_p", function() {
            var did = $(this).data('id');
            var attachment = $(this).data('attachment');

            $("#d_id").val(did);
            $("#attach_file").val(attachment);
            $('#myModal').modal('show');
        });
        var url = "deleteBill";

        $('.delete_confirm').click(function(){
            var _id =  $("#d_id").val();
            var id = _id;
            var file = $("#attach_file").val();

            $.ajaxSetup({
                headers : {
                    'X-CSRF-TOKEN' : $('meta[name="_token"]').attr('content')
                }

            })
            $.ajax({
                type : 'GET',
                url : url,
                data: {
                    '_token': $('input[name=_token]').val(),
                    'id': id,
                    'file': file,

                },

                success: function(data){
                    console.log(id);

                    $("#monthly" + id).remove();
                },
                error:function(data){
                    console.log('Error:',data);
                }

            });
        });

    </script>


@endsection