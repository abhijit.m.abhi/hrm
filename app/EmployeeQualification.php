<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmployeeQualification extends Model
{
    protected $fillable = [
        'skill',
        'qualification',
        'certification',
    ];
}
