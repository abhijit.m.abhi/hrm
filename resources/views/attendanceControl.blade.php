@extends('layouts.master')
@section('title', 'Attendance Control')
@section('content')

    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    {{--<h3>User <!--<small>Some examples to get you started</small>--></h3>--}}
                </div>


            </div>

            <div class="clearfix"></div>

            <div class="row">

                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Attendance Control<!--<small>Users</small>--></h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        @if(Session::has('message'))

                            <div id="successMessage" class="alert alert-success alert-dismissible fade in" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                            aria-hidden="true">×</span>
                                </button>
                                <strong>{{ Session::get('message') }}</strong>
                            </div>
                        @endif
                        <div class="x_content">
                            <div class="table-responsive">
                                <table id="datatable-buttons"
                                       class="table table-striped table-bordered table-hover display">
                                    <thead>
                                    <tr class="alert-info">
                                        <th>First Name</th>
                                        <th>Last Name</th>
                                        <th>E-Mail</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>

                                    <tbody>
                                    @foreach($allUserList as $userList)

                                        <tr id="user_id{{$userList->id}}">
                                            <td>{{$userList->fname}}</td>
                                            <td>{{$userList->lname}}</td>
                                            <td>{{$userList->email}}</td>

                                            <td align="center">
                                                <a href="{{route('attendance.edit', $userList->id)}}"
                                                   class="btn btn-primary btn-sm"><span
                                                            class="glyphicon glyphicon-eye-open"></span></a>

                                                <button class="btn confirm btn-info btn-sm manual_atten"
                                                        data-toggle="modal" data-id="{{$userList->id}}"><span
                                                            class="glyphicon glyphicon-cloud-upload"></span></button>

                                                <a href="{{ url('attendance/monthly/'.$userList->id) }}"
                                                   class="btn btn-success btn-sm"><span
                                                            class="fa fa-calendar-o"></span></a>
                                            </td>
                                        </tr>

                                        {{--Modal--}}
                                        <div class="modal fade" id="myModal" aria-labelledby="myModalLabel"
                                             role="dialog">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header alert-success">
                                                        <button type="button" class="close" data-dismiss="modal"
                                                                aria-label="Close"><span
                                                                    aria-hidden="true">&times;</span>
                                                        </button>
                                                        <h4 class="modal-title"><span
                                                                    class="glyphicon glyphicon-cloud-upload"></span>
                                                            Manual Attendance</h4>
                                                    </div>
                                                    <div class="modal-body col-md-12">
                                                        <div class="col-md-4">
                                                            <input type="text" name="date" class="form-control"
                                                                   value="{{date('Y-m-d')}}" id="d_date"
                                                                   placeholder="Select Date" readonly>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <input type='text' name="check_in"
                                                                   class="form-control datetimepicker3" id="d_check_in"
                                                                   placeholder="Check In"/>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <input type='text' name="check_out"
                                                                   class="form-control datetimepicker3" id="d_check_out"
                                                                   placeholder="Check Out"/>
                                                        </div>
                                                        <input type="hidden" id="d_id">

                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-warning"
                                                                data-dismiss="modal">No
                                                        </button>
                                                        <button type="button" class="btn btn-primary delete_product"
                                                                data-dismiss="modal" value="{{$userList->id}}"
                                                                data-token="{{ csrf_token() }}" data-dismiss="modal">
                                                            Confirm
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    @endforeach

                                    </tbody>
                                </table>


                            </div>

                        </div>

                        <meta name="_token" content="{!! csrf_token() !!}"/>

                    </div>
                </div>

            </div>
        </div>
    </div>


@endsection
@section('script')

    {{--Flash Message--}}
    <script>
        setTimeout(function () {
            $("#successMessage").fadeOut('slow');
        }, 3000);

    </script>

    {{--DatePicker--}}
    <script>

        $(".date_pick").datepicker({
            format: "dd MM yyyy",
            autoclose: true
        });

    </script>

    {{--TimePicker--}}
    <script type="text/javascript">

        $('.datetimepicker3').datetimepicker({
            format: 'HH:mm:ss'
        });

    </script>

    {{--Manual Attendance--}}
    <script type="text/javascript">

        $(document).on("click", ".manual_atten", function () {
            var did = $(this).data('id');
            $("#d_id").val(did);

            $("#d_check_in").val("");
            $("#d_check_in").val("");
            $("#d_check_out").val("");

            $('#myModal').modal('show');
        });

        var url = {!! json_encode(url('/manual_atten')) !!};

        $('.delete_product').click(function () {
            var id = $("#d_id").val();
            var date = $("#d_date").val();
            var check_in = $("#d_check_in").val();
            var check_out = $("#d_check_out").val();
            //alert("id: " + id + " Date: " + date + " CheckIn: " + check_in + " CheckOUt " + check_out);

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            })
            $.ajax({
                type: 'GET',
                url: url + '/' + id + '/' + date + '/' + check_in + '/' + check_out,
                success: function (data) {
                    console.log(data);
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        });

    </script>


    {{--DataTable--}}
    <script type="text/javascript">

        var handleDataTableButtons = function () {
            if ($("#datatable-buttons").length) {
                $("#datatable-buttons").DataTable({
                    dom: "Bfrtip",
                    buttons: [
                        {
                            extend: "copy",
                            className: "btn-sm"
                        },
                        {
                            extend: "csv",
                            className: "btn-sm"
                        },
                        {
                            extend: "excel",
                            className: "btn-sm"
                        },
                        {
                            extend: "pdfHtml5",
                            className: "btn-sm"
                        },
                        {
                            extend: "print",
                            className: "btn-sm"
                        },
                        {
                            extend: "colvis",
                            className: "btn-sm"
                        }
                    ],
                    responsive: false
                });
            }
        };

        TableManageButtons = function () {
            "use strict";
            return {
                init: function () {
                    handleDataTableButtons();
                }
            };
        }();

        $('#datatable').dataTable();

        $('#datatable-keytable').DataTable({
            keys: true
        });

        $('#datatable-responsive').DataTable();

        $('#datatable-scroller').DataTable({
            ajax: "js/datatables/json/scroller-demo.json",
            deferRender: true,
            scrollY: 380,
            scrollCollapse: true,
            scroller: true
        });

        $('#datatable-fixed-header').DataTable({
            fixedHeader: true
        });

        var $datatable = $('#datatable-checkbox');

        $datatable.dataTable({
            'order': [[1, 'asc']],
            'columnDefs': [
                {orderable: false, targets: [0]}
            ]
        });
        $datatable.on('draw.dt', function () {
            $('input').iCheck({
                checkboxClass: 'icheckbox_flat-green'
            });
        });

        TableManageButtons.init();

    </script>


@endsection