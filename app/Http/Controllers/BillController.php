<?php

namespace App\Http\Controllers;

use App\Bill;
use App\EmployeePersonalDetail;
use App\EmployeeSalarySetup;
use App\User;
use Carbon\Carbon;
use Illuminate\Contracts\Session\Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\View\View;

class BillController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function view()
    {
        //


        $user_id = Auth::user()->id;
        return view('applyBill',['var'=> $user_id]);
    }

    public function viewBillList()
    {
        $id = Auth::user()->id;


        $employeeData = User::with(array(
            "emp_official_info" => function ($query) {
                $query->with(array("emp_job_details", "emp_qualifications", "emp_emergency_contact", "emp_experience", "emp_attachment"));
            }
        ))->find($id);

        $viewBillList = Bill::where('employee_id','=',$employeeData->emp_official_info->id)->get();
       // return $viewBillList;
        return view('viewEmployeeBill',['viewList' => $viewBillList]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $time = Carbon::now();

        $employee_id =Auth::User()->id;

        $employeeData = User::with(array(
            "emp_official_info" => function ($query) {
                $query->with(array("emp_job_details", "emp_qualifications", "emp_emergency_contact", "emp_experience", "emp_attachment"));
            }
        ))->find($employee_id);

        $destinationPath = 'files';

        $extension = $request->file('attachment')->getClientOriginalExtension(); // getting image extension
        $fileName = $request->input('title') . rand(10000, 99999) . '.' . $extension;



        $bill = new Bill();

        $bill->title = $request->input('title').'('.$time->toTimeString().')';
        $bill->attachment = $fileName;
        $bill->total_bill = $request->input('total_bill');
        $bill->employee_id = $employeeData->emp_official_info->id;
        $bill->flag = 0;
        $newDate = $request->input('date');;
        $saveDate = date("Y-m-d",strtotime($newDate));
        $bill->date = $saveDate;


        if ( $bill->save()) {


            $request->file('attachment')->move($destinationPath, $fileName);
        };


        return redirect('/billApply');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $editBill = Bill::findOrFail($id);
        return view('editBill', ['editSal' => $editBill]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {


        $time = Carbon::now();
        $editBill = Bill::findOrFail($id);






        $editBill->title = $request->input('title').'('.$time->toTimeString().')';

        $editBill->total_bill = $request->input('total_bill');
        //$editBill->employee_id = $employeeData->emp_official_info->id;
        $editBill->flag = 0;
        $newDate = $request->input('date');
        $saveDate = date("Y-m-d",strtotime($newDate));
        $editBill->date = $saveDate;

        if($request->file('attachment')!=''){
            $previousFile = $editBill->attachment;
            $destinationPath = 'files';

            $extension = $request->file('attachment')->getClientOriginalExtension(); // getting image extension
            $fileName = $request->input('title') . '.' . $extension;

            $editBill->attachment = $fileName;

            if($editBill->update())
            {
              File::delete('files/' .$previousFile);
                $request->file('attachment')->move($destinationPath, $fileName);

            }

           // $editBill->update();

            return redirect('/view_employee_bill');

        }
        else{

            $editBill->update();
          //  $request->file('attachment')->move($destinationPath, $fileName);
            // Session::flash('message', 'Information Updated Successfully !');
            return redirect('/view_employee_bill');

        }




    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function deleteBill(Request $request){

        $id = $request->id;

       // return $id;
        $file = $request->file;
        $delete_bill = Bill::findOrFail($id);
        $delete_bill->delete();
        File::delete('files/' .$file);

        return response()->json($delete_bill);

    }

    public function billInfo(){

        $billInfo = Bill::with('emp_bill')->get();


        return view('viewBillList',['bill' => $billInfo]);
    }


    public function bill_decision(Request $request){

        $Bill_id = $_GET['id'];
        $decision = $_GET['decision'];
        $title = $_GET['title_name'];
        $cost = $_GET['total_bill'];
        $emp_id = $_GET['emp_id'];

        //return $Bill_id;


        if($decision == 'Accepted'){

            $var = 1;
        }
        else if($decision == 'Rejected'){

            $var = 2;
        }


        $bill = Bill::where('id' , $Bill_id)->first();;

        $bill -> flag = $var;
        $bill->save();



        $salary_add = EmployeeSalarySetup::where('employee_id','=',$emp_id)->first();
        $salary_id = $salary_add->id;
        $previous_allowance = $salary_add->allowances;

        $allowance = json_decode($previous_allowance);



        $allow = array($title => $cost);

        $file_data_array[] = $allow;
        $allowance_merge = array_merge($allowance,$file_data_array);

        $total_allowance = json_encode($allowance_merge);
       // return $allowance_merge;


        if($var == 1){

            $salary_add = EmployeeSalarySetup::find($salary_id);

            $salary_add->allowances=$total_allowance;
            $salary_add->save();
        }

        else if($var ==2){

            $arr = json_decode($total_allowance, true);
            foreach($arr as $k1=>$q) {
                foreach($q as $k2=>$r) {

                    if($k2 == $title) {
                        unset($arr[$k1]);
                    }
                }
            }
            $nn = json_encode($arr);

            $salary_add = EmployeeSalarySetup::find($salary_id);

            $salary_add->allowances=$nn;
            $salary_add->save();


        }


        return response()->json($bill);
    }


    public function addBillInfo(Request $request){

        $employee_details = EmployeePersonalDetail::all();
        //return $employee_details;

        return view('adminAddBill',['emp_details' => $employee_details]);
    }

    public function employeeBill(Request $request)
    {
        $time = Carbon::now();

        $employee_id =$request->input('employee_id');


        $destinationPath = 'files';

        $extension = $request->file('attachment')->getClientOriginalExtension(); // getting image extension
        $fileName = $request->input('title') . rand(10000, 99999) . '.' . $extension;



        $bill = new Bill();

        $bill->title = $request->input('title').'-'.$time->toTimeString();
        $bill->attachment = $fileName;
        $bill->total_bill = $request->input('total_bill');
        $bill->employee_id = $employee_id;
        $bill->flag = 1;
        $newDate = $request->input('date');;
        $saveDate = date("Y-m-d",strtotime($newDate));
        $bill->date = $saveDate;


        if ( $bill->save()) {


            $request->file('attachment')->move($destinationPath, $fileName);
        };


        return redirect('/addBillInfo');


    }

    public function billReportInfo(Request $request){

        $employee_details = EmployeePersonalDetail::all();
        return view('viewReport',['emp_details' => $employee_details]);
    }

    public function reportView(Request $request)
    {

        $id = $request->employee_id;
        $datePick = $request->input('daterange');
        $dateSeparator = explode(" - ", $datePick);

        $s = strtotime($dateSeparator[0]);
        $e = strtotime($dateSeparator[1]);

        $start_date = date('Y-m-d', $s);
        $end_date = date('Y-m-d', $e);



        $report = Bill::whereBetween('date', [$start_date, $end_date])->where('employee_id','=',$id)->where('flag','=',1)->get();

        return response()->json($report);
    }

}
