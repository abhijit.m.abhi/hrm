<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobDetailsSetup extends Model
{
    protected $fillable = [
        'job_code',
        'job_title',
        'description',
        'specification',
        'min_salary',
        'max_salary',
    ];
}
