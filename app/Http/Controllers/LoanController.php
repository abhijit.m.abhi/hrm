<?php

namespace App\Http\Controllers;

use App\Ex_class\FindRole;
use App\Installment;
use App\User;
use Illuminate\Http\Request;
use App\EmployeePersonalDetail;
use App\LoanType;
use App\EmployeeLoan;
use App\EmployeeTraningSession;
use Illuminate\Support\Facades\Input;


class LoanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    public function view(){

        $loans = LoanType::all();
        $employeeLoans = EmployeeLoan::all();
        $employeeLoanSession = EmployeeLoan::with("emp_loan")->get();
        $employee = EmployeePersonalDetail::all();



        return view('loanSetup', ['loans' => $loans,'training' => $employeeLoans,'employee' => $employee,'empLoans' => $employeeLoanSession]);

    }
    public function addloan(Request $req){

        $addLoan = new LoanType();
        $addLoan->name = $req->name;
        $addLoan->details = $req->description;
        $addLoan->save();
        return response()->json($addLoan);

    }

    public function editLoan(Request $req){
        $editLoan = LoanType::find ($req->id);
        $editLoan->name = $req->name;
        $editLoan->details = $req->description;
        $editLoan->update();
        return response()->json($editLoan);
    }

    public function addEmployeeLoan(Request $req){

        $addEmployeeLoan = new EmployeeLoan();
        $addEmployeeLoan->employee_id = $req->employee_id;
        $addEmployeeLoan->loan_type = $req->loan_type;

        $l_start_date = $req->loan_start_date;
        $addEmployeeLoan->start_date = date("Y-m-d", strtotime($l_start_date));

        $l_installment_date = $req->loan_start_date;
        $addEmployeeLoan->last_installment_date = date("Y-m-d", strtotime($l_installment_date));

        $addEmployeeLoan->loan_period = $req->loan_period;
        $addEmployeeLoan->loan_amount = $req->loan_amount;
        $addEmployeeLoan->monthly_installment = $req->monthly_installment;
        $addEmployeeLoan->paid_amount = $req->paid_amount;
        $addEmployeeLoan->status = $req->status;
        $addEmployeeLoan->details = $req->details;
        $addEmployeeLoan->save();

        $id= $addEmployeeLoan->id;
        $employeeLoanSession = EmployeeLoan::with("emp_loan")->where('id', '=' ,$id)->get();
        return response()->json($employeeLoanSession);

    }

    public function edit_emp_Loan(Request $req){

        $editEmployeeLoan = EmployeeLoan::find ($req->id);
//        $editEmployeeLoan->employee_id = $req->employee_id;
        $editEmployeeLoan->loan_type = $req->loan_type;

        $l_start_date = $req->loan_start_date;
        $editEmployeeLoan->start_date = date("Y-m-d", strtotime($l_start_date));

        $l_installment_date = $req->loan_start_date;
        $editEmployeeLoan->last_installment_date = date("Y-m-d", strtotime($l_installment_date));

        $editEmployeeLoan->loan_period = $req->loan_period;
        $editEmployeeLoan->loan_amount = $req->loan_amount;
        $editEmployeeLoan->monthly_installment = $req->monthly_installment;
        $editEmployeeLoan->paid_amount = $req->paid_amount;
        $editEmployeeLoan->status = $req->status;
        $editEmployeeLoan->details = $req->details;

        $editEmployeeLoan ->save();
        $id= $req->id;
        $editEmployeeLoanSession = EmployeeLoan::with("emp_loan")->where('id', '=' ,$id)->get();
        return response()->json($editEmployeeLoanSession );

    }




    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function empLoan($id){

        $i = new FindRole();
        $user_id = $i->findMyRole($id);

        $loanType = LoanType::all();
        $employeeData = User::with(array(
            "emp_official_info" => function ($query) {
                $query->with(array("emp_job_details" => function($query) {
                    $query->with(array("dep_info"));
                },"emp_qualifications", "emp_emergency_contact", "emp_experience", "emp_attachment"));
            }
        ))->find($user_id);
        $emp_id = $employeeData->emp_official_info->id;

        $loanData = EmployeeLoan::with("emp_loan")->where('employee_id', '=' ,$emp_id)->get();


        //return $loanData;


        return view('employeeLoan', ['loanType' => $loanType,'userData' => $employeeData,'loanData' => $loanData]);
    }

    public function applyEmployeeLoan(Request $req)
    {

        $addEmployeeLoan = new EmployeeLoan();
        $addEmployeeLoan->employee_id = $req->employee_id;
        $addEmployeeLoan->loan_type = $req->loan_type;


        $addEmployeeLoan->loan_amount = $req->loan_amount;
        $addEmployeeLoan->status = 'Deactive';
        $addEmployeeLoan->details = $req->details;
        $addEmployeeLoan->save();


        return response()->json($addEmployeeLoan);

    }


    public function editApplyEmployeeLoan(Request $req){


        $editApplyEmployeeLoan = EmployeeLoan::find ($req->id);

        $editApplyEmployeeLoan ->loan_type = $req->loan_type;


        $editApplyEmployeeLoan ->loan_amount = $req->loan_amount;

        $editApplyEmployeeLoan ->details = $req->details;

        $editApplyEmployeeLoan  ->save();


        return response()->json($editApplyEmployeeLoan);

    }

    public function showInstallment(Request $req)
    {

        $id = $_GET['id'];

        $showData = Installment::where('loan_id','=',$id)->get();


        return response()->json($showData);

    }



}
