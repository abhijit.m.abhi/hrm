<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        factory(App\User::class, 1)->create();
        factory(App\JobDetailsSetup::class, 10)->create();
        factory(App\CompanyStructure::class, 10)->create();
        factory(App\QualificationSetup::class, 10)->create();
    }
}
