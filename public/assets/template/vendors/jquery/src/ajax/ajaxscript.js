$(document).ready(function (){

    var url = "http://localhost:8080/pos_admin/public/loan";

    $('.delete-product').click(function(){
        console.log("ok");
        var id = $(this).val();
        $.ajaxSetup({
            headers : {
                'X-CSRF-TOKEN' : $('meta[name="_token"]').attr('content')
            }

        })
        $.ajax({
            type : 'DELETE',
            url : url + '/' + id,
            success: function(data){
                console.log("ok");

                $("#tbl_loan" + id).remove();
            },
            error:function(data){
                console.log('Error:',data);
            }

        });
    });

});