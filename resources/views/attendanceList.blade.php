@extends('layouts.master')
@section('title', 'Attendance Information')
@section('content')

    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    {{--<h3>User <!--<small>Some examples to get you started</small>--></h3>--}}
                </div>


            </div>

            <div class="clearfix"></div>

            <div class="row">

                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Attendance Information<!--<small>Users</small>--></h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        @if(Session::has('message'))

                            <div id="successMessage" class="alert alert-success alert-dismissible fade in" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                            aria-hidden="true">×</span>
                                </button>
                                <strong>{{ Session::get('message') }}</strong>
                            </div>
                        @endif
                        <div class="x_content">
                            <div class="table-responsive">
                                <table id="datatable-buttons"
                                       class="table table-striped table-bordered table-hover display">
                                    <thead>
                                    <tr class="alert-info">
                                        <th>Date</th>
                                        <th>Check In</th>
                                        <th>Check Out</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($attendance as $attend)

                                        <tr id="user_id{{$attend->id}}">
                                            <td>{{$attend->date}}</td>
                                            <td id="ch_in{{$attend->id}}">{{$attend->check_in}}</td>
                                            <td id="ch_out{{$attend->id}}">{{$attend->check_out}}</td>
                                            <td align="center">

                                                <button class="btn confirm btn-info btn-sm manual_atten"
                                                        data-toggle="modal" data-id="{{$attend->id}}" data-date="{{$attend->date}}" data-check_in="{{$attend->check_in}}" data-check_out="{{$attend->check_out}}" ><span
                                                            class="glyphicon glyphicon-edit"></span></button>
                                            </td>

                                        </tr>

                                        {{--Modal--}}
                                        <div class="modal fade" id="myModal" aria-labelledby="myModalLabel"
                                             role="dialog">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header alert-success">
                                                        <button type="button" class="close" data-dismiss="modal"
                                                                aria-label="Close"><span
                                                                    aria-hidden="true">&times;</span>
                                                        </button>
                                                        <h4 class="modal-title"><span
                                                                    class="glyphicon glyphicon-cloud-upload"></span>
                                                            Edit Attendance</h4>
                                                    </div>
                                                    <div class="modal-body col-md-12">

                                                        <div class="col-md-4">
                                                            <input type="text" name="date" class="form-control"
                                                                    id="d_date" placeholder="Select Date" readonly>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <input type='text' name="check_in" class="form-control datetimepicker3" id="d_check_in"
                                                                   placeholder="Check In"/>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <input type='text' name="check_out"
                                                                   class="form-control datetimepicker3" id="d_check_out"
                                                                   placeholder="Check Out"/>
                                                        </div>
                                                        <input type="hidden" id="d_id">

                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-warning"
                                                                data-dismiss="modal">No
                                                        </button>
                                                        <button type="button" class="btn btn-primary edit_atten"
                                                                data-dismiss="modal" value="{{$attend->id}}"
                                                                data-token="{{ csrf_token() }}" data-dismiss="modal">
                                                            Confirm
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    @endforeach

                                    </tbody>
                                </table>


                            </div>

                        </div>

                        <meta name="_token" content="{!! csrf_token() !!}"/>

                    </div>
                </div>

            </div>
        </div>
    </div>


@endsection
@section('script')

    {{--Flash Message--}}
    <script>
        setTimeout(function () {
            $("#successMessage").fadeOut('slow');
        }, 3000);

    </script>

    {{--TimePicker--}}
    <script type="text/javascript">

        $('.datetimepicker3').datetimepicker({
            format: 'HH:mm:ss'
        });

    </script>


    {{--Edit Attendance--}}
    <script type="text/javascript">

        $(document).on("click", ".manual_atten", function () {

            var did = $(this).data('id');
            var ddate = $(this).data('date');
            var dcheck_in = $(this).data('check_in');
            var dcheck_out = $(this).data('check_out');

            $("#d_id").val(did);
            $('#d_date').val(ddate);
            $("#d_check_in").val(dcheck_in);
            $("#d_check_out").val(dcheck_out);
            $('#myModal').modal('show');


        });

        var url = {!! json_encode(url('/edit_atten')) !!};



        $('.edit_atten').click(function () {
            var id = $("#d_id").val();
            var date = $("#d_date").val();
            var c_in = $("#d_check_in").val();
            var c_out = $("#d_check_out").val();

            //alert("id: " + id + " Date: " + date + " CheckIn: " + check_in + " CheckOUt " + check_out);

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            });
            $.ajax({
                type: 'GET',
                url: url + '/' + id + '/' + date + '/' + c_in + '/' + c_out,
                success: function (data) {

                    if(data == "Success")
                    {
                        $('#ch_in'+ id).html(c_in);
                        $('#ch_out'+ id).html(c_out);
                    }

                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        });

    </script>


    {{--DataTable--}}
    <script type="text/javascript">

        var handleDataTableButtons = function () {
            if ($("#datatable-buttons").length) {
                $("#datatable-buttons").DataTable({
                    dom: "Bfrtip",
                    buttons: [
                        {
                            extend: "copy",
                            className: "btn-sm"
                        },
                        {
                            extend: "csv",
                            className: "btn-sm"
                        },
                        {
                            extend: "excel",
                            className: "btn-sm"
                        },
                        {
                            extend: "pdfHtml5",
                            className: "btn-sm"
                        },
                        {
                            extend: "print",
                            className: "btn-sm"
                        },
                        {
                            extend: "colvis",
                            className: "btn-sm"
                        }
                    ],
                    responsive: false,
                    order: [[0, "desc"]]
                    //bSort: false

                });
            }
        };

        TableManageButtons = function () {
            "use strict";
            return {
                init: function () {
                    handleDataTableButtons();
                }
            };
        }();

        $('#datatable').dataTable();

        $('#datatable-keytable').DataTable({
            keys: true
        });

        $('#datatable-responsive').DataTable();

        $('#datatable-scroller').DataTable({
            ajax: "js/datatables/json/scroller-demo.json",
            deferRender: true,
            scrollY: 380,
            scrollCollapse: true,
            scroller: true
        });

        $('#datatable-fixed-header').DataTable({
            fixedHeader: true
        });

        var $datatable = $('#datatable-checkbox');

        $datatable.dataTable({
            'order': [[1, 'asc']],
            'columnDefs': [
                {orderable: false, targets: [0]}
            ]
        });
        $datatable.on('draw.dt', function () {
            $('input').iCheck({
                checkboxClass: 'icheckbox_flat-green'
            });
        });

        TableManageButtons.init();

    </script>


@endsection