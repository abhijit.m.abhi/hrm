<?php

namespace App\Http\Controllers;

use App\EmployeePersonalDetail;
use App\Resign;
use App\User;
use Illuminate\Http\Request;

class ResignController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function view()
    {

        $url = url()->current();
        $parts = explode("/", $url);
        $var = end($parts);

        $employeeData = User::with(array(
            "emp_official_info" => function ($query) {
                $query->with(array("emp_job_details", "emp_qualifications", "emp_emergency_contact", "emp_experience", "emp_attachment"));
            }
        ))->find($var);

        $resign = Resign::where('employee_id',$employeeData->emp_official_info->id)->where('flag',0)->orwhere('flag',3)->first();

//return $resign;


        return view('applyResign',['resign' => $resign, 'empData' =>$employeeData]);

    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function applyResign(Request $req)
    {
        //


        $apply_resign = new Resign();
        $apply_resign->employee_id = $req->employee_id;
        $apply_resign->comments = $req->comments;
        $apply_resign->resign_type = 'Resigned';
        $apply_resign->flag = 0;


        $newDate = $req->date;
        $saveDate = date("Y-m-d",strtotime($newDate));


        $apply_resign->date = $saveDate;
        $apply_resign->save();


        return response()->json($apply_resign);

    }


    public function editResign(Request $req)
    {

        //return $req->all();

        $resign_edit =Resign::find ($req->id);
        $resign_edit->employee_id = $req->employee_id;
        $resign_edit->comments = $req->comments;
        $resign_edit->resign_type = 'Resigned';
        $resign_edit->flag = 0;


        $newDate = $req->date;
        $saveDate = date("Y-m-d",strtotime($newDate));


        $resign_edit->date = $saveDate;
        $resign_edit->update();


        return response()->json($resign_edit);

    }

    public function ResignList(Request $req)
    {


        //$details =


        $view_resign = Resign::with('emp_resign')->where('flag', '!=' ,1)->where('resign_type', '=' ,'Resigned')->get();

      //  return $view_resign;


        //return $view_resign;

        return view('viewResignList',['resignList' => $view_resign]);
    }




    public function Resign_decision(Request $req)
    {

        $id = $_GET['id'];
        $decision = $_GET['decision'];

        $user = Resign::where('id' , '=',$id)->first();
        $employee = $user->employee_id;
        $emp_details = EmployeePersonalDetail::where('id' , '=',$employee)->first();
        $user_id = $emp_details->user_id;

        if($decision=='Accepted'){
            $var = 2;
            $user = User::find($user_id);
            $user-> flag = 2;
            $user->update();


        }
        else if($decision == 'Rejected')
        {
            $var = 3;
        }

        $resign = Resign::find($id);
        $resign -> flag = $var;
        $resign->update();
        return response()->json($resign);

    }

    public function reApply(Request $request){

        $id = $_GET['id'];

        $re_apply = Resign::find($id);
        $re_apply->flag = 1;
        $re_apply->update();
        return response()->json($re_apply);



    }



}
