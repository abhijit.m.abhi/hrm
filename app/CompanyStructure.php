<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyStructure extends Model
{
    //
    protected $table = 'company_structures';
    protected $fillable = [
        'name',
        'details',
        'address',
        'department_type',
        'country',
        'ip_address',
        'office_start_time',
        'last_entry_time',
        'office_end_time'
    ];


}
