@extends('layouts.master')
@section('title', 'Add Job Details')
@section('content')

    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    {{--<h3>Job Details Setup</h3>--}}
                </div>


            </div>
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Add Job Details<!--<small>Input different form elements</small>--></h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>

                        @if(Session::has('message'))

                            <div id="successMessage" class="alert alert-success alert-dismissible fade in" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                            aria-hidden="true">×</span>
                                </button>
                                <strong>{{ Session::get('message') }}</strong>
                            </div>
                        @endif

                        <div class="x_content">
                            <br/>

                            <form id="form" data-parsley-validate action="{{url('jobDetails')}}" method="POST"
                                  class="form-horizontal" role="form"
                                  files="true" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div class="item form-body">
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Job Code <span
                                                    class="required" style="color: red;">*</span></label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="text" class="form-control col-md-7 col-xs-12"
                                                   placeholder="Job Code" name="jobCode" value="{{old('jobCode')}}"
                                                   required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Job Title <span
                                                    class="required" style="color: red;">*</span></label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="text" class="form-control col-md-7 col-xs-12"
                                                   placeholder="Job Title" name="jobTitle" value="{{old('jobTitle')}}"
                                                   required>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Description <span
                                                    class="required" style="color: red;">*</span> </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <textarea rows="3" cols="20" class="form-control col-md-7 col-xs-12"
                                                      placeholder="Description" name="description"
                                                      value="{{old('description')}}" required></textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Specification</label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="text" name="specification"
                                                   class="form-control col-md-7 col-xs-12"
                                                   placeholder="Specification" value="{{old('specification')}}">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Minimun Salary <span
                                                    class="required" style="color: red;">*</span> </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="number" class="form-control col-md-7 col-xs-12"
                                                   placeholder="Minimun Salary" name="minSal" value="{{old('minSal')}}"
                                                   required>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Maximum Salary <span
                                                    class="required" style="color: red;">*</span> </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="number" class="form-control col-md-7 col-xs-12"
                                                   placeholder="Maximum Salary" name="maxSal" value="{{old('maxSal')}}"
                                                   required>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12"></label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <span class="text-muted">
                                                <em>
                                                    <span style="color: red;">*</span>
                                                     Indicates required field
                                                </em>
                                            </span>
                                        </div>
                                    </div>


                                    <div class="ln_solid"></div>
                                    <div class="form-group">
                                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                            <a type="cancel" href="{{ url('dashboard') }}"
                                               class="btn btn-danger">Cancel</a>
                                            <button id="send" type="submit" class="btn btn-primary">Add</button>
                                        </div>
                                    </div>

                                </div>
                            </form>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection

@section('script')

    {{-- Start Seccuess Message Script--}}
    <script>

        setTimeout(function () {
            $("#successMessage").fadeOut('slow');
        }, 3000);

    </script>
    {{-- End Seccuess Message Script--}}

    {{--Start Form Valiation Script--}}
    <script>
        // validate a field on "blur" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':
        $('form')
            .on('blur', 'input[required], input.optional, select.required', validator.checkField)
            .on('change', 'select.required', validator.checkField)
            .on('keypress', 'input[required][pattern]', validator.keypress);

        $('.multi.required').on('keyup blur', 'input', function () {
            validator.checkField.apply($(this).siblings().last()[0]);
        });

        $('form').submit(function (e) {
            e.preventDefault();
            var submit = true;

            // evaluate the form using generic validaing
            if (!validator.checkAll($(this))) {
                submit = false;
            }

            if (submit)
                this.submit();

            return false;
        });

    </script>
    {{--End Form Valiation Script--}}

@endsection