<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bill extends Model
{
    //
    public function emp_bill()
    {

        return $this->belongsTo('App\EmployeePersonalDetail','employee_id');
    }
}
