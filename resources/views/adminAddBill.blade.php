@extends('layouts.master')
@section('title', 'Employee Bill')
@section('content')

    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>Add Employee Bill</h3>
                </div>


            </div>
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Manually Add Employee Bill<!--<small>Input different form elements</small>--></h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">


                            <form action="{{url('employeeBill')}}" id="form" data-parsley-validate method="POST" class="form-horizontal" role="form"
                                  files="true" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div class="form-body">

                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Employee Name</label>
                                        <div class="col-md-6 col-sm-6 col-xs-12  ">
                                            <select class="select2_single form-control employee_id boldoption" tabindex="-1" id="employee_id" name="employee_id">
                                                <option value="">Select Option</option>


                                                @foreach($emp_details as $employee)

                                                    <option value="{{$employee->id}}">{{$employee->first_name}} {{$employee->middle_name}} {{$employee->last_name}}</option>
                                                @endforeach

                                            </select>
                                            <span class="error_name" id="error_name" style="display: none;"><strong style="color:red">Select Employee Name</strong></span>

                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Bill Title</label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <textarea class="form-control col-md-7 col-xs-12 title"
                                                      name="title" id="title" rows="4" cols="3"  required></textarea>
                                            @if ($errors->has('title'))
                                                <span class="help-block">
                                                <strong>{{ $errors->first('title') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Total Cost</label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="text" class="form-control col-md-7 col-xs-12 total_bill" name="total_bill" id="total_bill" value="{{old('total_bill')}}" required>
                                            @if ($errors->has('total_bill'))
                                                <span class="help-block">
                                                <strong>{{ $errors->first('total_bill') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Attachment</label>
                                        <div class="fileinput fileinput-new col-md-6 col-sm-6 col-xs-12" data-provides="fileinput">
                                            <input type="file" class="" id="attachment" name="attachment" value="{{old('attachment')}}"/>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Date</label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <div class='input-group date' id='datetimepicker1'>
                                                <input type='text' name="date" id="date" class="form-control date" value="{{old('date')}}"/>
                                                <span class="input-group-addon">
                                                     <span class="glyphicon glyphicon-calendar"></span>
                                                          </span>
                                            </div>
                                        </div>
                                        <script type="text/javascript">
                                            $(function () {
                                                $('#datetimepicker1').datepicker();
                                            });
                                        </script>
                                    </div>


                                    <div class="form-group">

                                        <div class="col-sm-10">
                                            <input type="hidden" class="form-control id"
                                                   id="id" name="id" />
                                        </div>
                                    </div>



                                    <div class="form-group">
                                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">

                                            <button type="button" id="save-btn" data-token="{{ csrf_token() }}" class="btn btn-primary">Apply</button>
                                        </div>
                                    </div>


                                </div>
                            </form>





                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


   <script>
        $(document).ready(function() {
            $(".select2_single").select2({
                placeholder: "Select a state",
                allowClear: true
            });
        });
    </script>

    <script>
        $("#save-btn").click(function (e) {



            $('textarea.title,input.date,input.total_bill,option.select2_single.selected').each(function () {
                if ($.trim($(this).val()) == '') {
                    isValid = false; /* Required class style */
                    $(this).css({
                        "border": "1px solid red",
                        "background": "#FFCECE"
                    });
                    //isGoodImage(file);
                }
                else { /* Required class style removed */
                    $(this).css({
                        "border": "",
                        "background": ""
                    });
                    isValid = true;
                }
            });

            if($('#employee_id').val()==''){
                isValid = false; /* Required class style */
                $("#error_name").css("display", "block");
            }
            else { /* Required class style removed */
                $("#error_name").css("display", "none");
                isValid = true;
            }



            if (isValid == false)
                e.preventDefault();
            else{

                $('#form').submit();

            }



        })


    </script>




@endsection