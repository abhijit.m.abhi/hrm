<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Attendance extends Model
{
    //use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'date',
        'check_in',
        'check_out',
    ];

    public  function late_attendance()
    {
        return $this->hasOne('App\LateAttendance', 'attendance_id');
    }
}
