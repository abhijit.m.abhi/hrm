<?php

namespace App\Http\Controllers;

use App\EmployeePersonalDetail;
use App\Installment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class InstallmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $employees = EmployeePersonalDetail::all();
        return view('addInstallment', ['employees' => $employees]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    public function add_installment(Request $req){

        $addInstallment = new Installment();
        $addInstallment->loan_id = $req->id;
        $checkCount = Installment::where('loan_id', '=', $req->id)->orderBy('count_installment', 'desc')->first();
        if($checkCount==NULL){
            $addInstallment->count_installment = 1;
        }
        else{
            $addInstallment->count_installment = ($checkCount->count_installment)+1;
        }
        $addInstallment->date = date("Y-m-d", strtotime($req->date));
        $addInstallment->amount = $req->amount;
        $addInstallment->saved_by = Auth::user()->id;
        $addInstallment->save();
        return response()->json($addInstallment);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
