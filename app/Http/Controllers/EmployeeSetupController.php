<?php

namespace App\Http\Controllers;

use App\CompanyStructure;
use App\EmployeeAttachment;
use App\EmployeeEmergencyContact;
use App\EmployeeExperience;
use App\EmployeeJobDetail;
use App\EmployeePersonalDetail;
use App\EmployeeQualification;
use App\Http\Requests\AddEmployeeRequest;
use App\JobDetailsSetup;
use App\QualificationSetup;
use App\User;
use App\Ex_class\FindRole;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;


class EmployeeSetupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $allEmployeeList = User::with(array(
            "emp_official_info" => function ($query) {
                $query->with(array("emp_job_details", "emp_qualifications"));
            }
        ))->where('flag', '<>', 0)->get();

        //return $allEmployeeList;

        return view('employeeList', ['allEmployeeList' => $allEmployeeList]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $jobTitleList = JobDetailsSetup::latest()->pluck('job_title');
        $companyDeptList = CompanyStructure::latest()->select('department_type', 'id')->get();

        //return $companyDeptList;

        $skillList = QualificationSetup::where('type', 'Skills')->latest()->pluck('name');
        $qualificationList = QualificationSetup::where('type', 'Education')->latest()->pluck('name');
        $certificationList = QualificationSetup::where('type', 'Certification')->latest()->pluck('name');

        return view('addEmployee', ['jobTitleList' => $jobTitleList, 'companyDeptList' => $companyDeptList, 'skillList' => $skillList, 'qualificationList' => $qualificationList, 'certificationList' => $certificationList]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(AddEmployeeRequest $request)
    {
        /*
         * Create New User
         */

        $user = new User();
        $destinationPath = 'images/users'; // upload path
        $extension = $request->file('photo')->getClientOriginalExtension(); // getting image extension
        $fileName = $request->input('email') . rand(10000, 99999) . '.' . $extension; // renameing image
        $user->fname = $request->input('firstName');
        $user->lname = $request->input('lastName');
        $user->email = $request->input('email');
        $user->address = $request->input('address');
        $user->type = 'User';
        $user->flag = 2;
        $user->photo = $fileName;
        $rand_pass = str_random(8);
        $user->password = Hash::make($rand_pass);
        $user->api_token = str_random(60);
        $user->saved_by = Auth::user()->id;
        if ($user->save()) {

            Session::flash('password_message', 'Please send this Password to the Newly Created User: ' . $rand_pass);
            $request->file('photo')->move($destinationPath, $fileName); // uploading file to given path
        };

        // Store the Last User Insert Id
        $last_user = User::find($user->id);


        //return $user;

        /*
         * Employee Personal Info
         */
        $emp_personal_details = new EmployeePersonalDetail();

        $emp_personal_details->employee_code = $request->input('empCode');
        $emp_personal_details->first_name = $request->input('firstName');
        $emp_personal_details->middle_name = $request->input('middleName');
        $emp_personal_details->last_name = $request->input('lastName');
        $emp_personal_details->nationality = $request->input('nationality');
        $emp_personal_details->nid = $request->input('nid');

        $dob = $request->input('dateOfBirth');
        $dob_string = strtotime($dob);
        $dob_formatted = date('Y-m-d', $dob_string);

        $emp_personal_details->date_of_birth = $dob_formatted;
        $emp_personal_details->birth_certificate_id = $request->input('birthId');
        $emp_personal_details->passport_no = $request->input('passportNo');
        $emp_personal_details->gender = $request->input('gender');
        $emp_personal_details->marital_status = $request->input('maritalStatus');


        $emp_personal_details->address = $request->input('address');
        $emp_personal_details->city = $request->input('city');
        $emp_personal_details->postal_or_zip_code = $request->input('postalCode');
        $emp_personal_details->country = $request->input('country');
        $emp_personal_details->phone = $request->input('phone');
        $emp_personal_details->work_phone = $request->input('officePhone');
        $emp_personal_details->email = $request->input('email');


        $last_user->emp_official_info()->save($emp_personal_details);


        // Store the Last Employee Insert Id
        $emp_personal_detail = EmployeePersonalDetail::find($emp_personal_details->id);

        /*
         * Employee Job Details
         */

        $emp_job_details = new EmployeeJobDetail();

        $emp_job_details->employment_status = $request->input('employmentStatus');
        $emp_job_details->job_title = $request->input('jobTitle');


        $joinDate = $request->input('joinedDate');
        $joinDate_string = strtotime($joinDate);
        $joinDate_formatted = date('Y-m-d', $joinDate_string);
        $emp_job_details->joined_date = $joinDate_formatted;


        $confirmDate = $request->input('confirmationDate');
        $confirmDate_string = strtotime($confirmDate);
        $confirmDate_formatted = date('Y-m-d', $confirmDate_string);
        $emp_job_details->confirmation_date = $confirmDate_formatted;

        $emp_job_details->department = $request->input('department');

        $emp_personal_detail->emp_job_details()->save($emp_job_details);

        /*
         * Employee Qualifications
         */

        $emp_qualification = new EmployeeQualification();

        $get_skills = $request->input('skill.*.name');
        $skills = json_encode($get_skills);
        $emp_qualification->skill = $skills;

        $get_qualifications = $request->input('qualification.*.name');
        $qualifications = json_encode($get_qualifications);
        $emp_qualification->qualification = $qualifications;

        $get_certifications = $request->input('certification.*.name');
        $certifications = json_encode($get_certifications);
        $emp_qualification->certification = $certifications;


        $emp_personal_detail->emp_qualifications()->save($emp_qualification);


        /*
         * Employee Emergency Contact info
         */

        $emp_emr_contact = new EmployeeEmergencyContact();

        $emp_emr_contact->name = $request->input('emrName');
        $emp_emr_contact->relation = $request->input('relation');
        $emp_emr_contact->address = $request->input('emrAddress');
        $emp_emr_contact->mobile = $request->input('emrPhone');

        $emp_personal_detail->emp_emergency_contact()->save($emp_emr_contact);

        /*
         * Employee Experience info
         */

        for ($i = 0; $i < count($request->organization); $i++) {


            $emp_experience = new EmployeeExperience();

            $emp_experience->organization_name = $request->input('organization')[$i];
            $emp_experience->address = $request->input('orgaAddress')[$i];
            $emp_experience->contact_no = $request->input('contactNo')[$i];
            $emp_experience->duration = $request->input('duration')[$i];

            $emp_personal_detail->emp_experience()->save($emp_experience);


        }


        /*
         * Attachment Upload
         */

        if ($request->hasFile('file')) {
            for ($i = 0; $i < count($request->file('file')); $i++) {
                $destinationPath = 'uploads'; // upload path
                $extension = $request->file('file')[$i]->getClientOriginalExtension(); // getting file extension
                $fileName = $emp_personal_details->employee_code . '-' . rand(10000, 99999) . '.' . $extension; // renaming file
                $attachment = new EmployeeAttachment();
                $attachment->type = 'attachment';
                $attachment->title = $request->input('title')[$i] ? $request->input('title')[$i] : '';
                $attachment->file_name = $fileName;
                $attachment->location = 'uploads/' . $fileName;

                $request->file('file')[$i]->move($destinationPath, $fileName);

                $emp_personal_detail->emp_attachment()->save($attachment);

            }


        }

        Session::flash('message', 'New Employee Added Successfully!');
        return redirect('/employeeSetup/create');

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($uid)
    {
        $i = new FindRole();
        $id = $i->findMyRole($uid);

        $jobTitleList = JobDetailsSetup::latest()->pluck('job_title');
        $companyDeptList = CompanyStructure::latest()->pluck('department_type');
        $skillList = QualificationSetup::where('type', 'Skills')->latest()->pluck('name');
        $qualificationList = QualificationSetup::where('type', 'Education')->latest()->pluck('name');
        $certificationList = QualificationSetup::where('type', 'Certification')->latest()->pluck('name');


        $employeeData = User::with(array(
            "emp_official_info" => function ($query) {
                $query->with(array("emp_job_details", "emp_qualifications", "emp_emergency_contact", "emp_experience", "emp_attachment"));
            }
        ))->find($id);


        return view('editEmployee', ['employee' => $employeeData, 'jobTitleList' => $jobTitleList, 'companyDeptList' => $companyDeptList, 'skillList' => $skillList, 'qualificationList' => $qualificationList, 'certificationList' => $certificationList]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         /*
          * Edit User
          */


        $user = User::find($id);

        $user->fname = $request->input('firstName');
        $user->lname = $request->input('lastName');
        $user->email = $request->input('email');
        $user->address = $request->input('address');
        $user->type = 'User';
        $user->flag = 2;


        $user->api_token = str_random(60);
        $user->saved_by = Auth::user()->id;

        $file_icon = $request->file('photo');
        $fileCountIcon = count($file_icon);
        $destinationPath = 'images/users'; // upload path
        if ($fileCountIcon != 0) {
            if ($user->photo == "") {
                $extension = $request->file('photo')->getClientOriginalExtension(); // getting image extension
                $fileName = $request->input('fname') . rand(10000, 99999) . '.' . $extension; // renameing image
                $file_icon->move($destinationPath, $fileName);
                $user->photo = $fileName;
            } else {
                $file_icon->move($destinationPath, $user->photo);
            }
        }


        if ($request->has('password')) {
            $user->password = bcrypt($request->input('password'));
        }

        if ($user->update()) {

            Session::flash('message', 'Information Updated Successfully !');

        }

        /*
         * Edit Personal Info
         */

        $emp_personal_details = EmployeePersonalDetail::where('user_id', $id)->firstOrFail();


        $emp_personal_details->employee_code = $request->input('empCode');
        $emp_personal_details->first_name = $request->input('firstName');
        $emp_personal_details->middle_name = $request->input('middleName');
        $emp_personal_details->last_name = $request->input('lastName');
        $emp_personal_details->nationality = $request->input('nationality');
        $emp_personal_details->nid = $request->input('nid');


        $dob = $request->input('dateOfBirth');
        $dob_string = strtotime($dob);
        $dob_formatted = date('Y-m-d', $dob_string);

        $emp_personal_details->date_of_birth = $dob_formatted;
        $emp_personal_details->birth_certificate_id = $request->input('birthId');
        $emp_personal_details->passport_no = $request->input('passportNo');
        $emp_personal_details->gender = $request->input('gender');
        $emp_personal_details->marital_status = $request->input('maritalStatus');


        $emp_personal_details->address = $request->input('address');
        $emp_personal_details->city = $request->input('city');
        $emp_personal_details->postal_or_zip_code = $request->input('postalCode');
        $emp_personal_details->country = $request->input('country');
        $emp_personal_details->phone = $request->input('phone');
        $emp_personal_details->work_phone = $request->input('officePhone');
        $emp_personal_details->email = $request->input('email');


        $emp_personal_details->update();


        /*
         * Employee Job Details
         */

        $emp_job_details = EmployeeJobDetail::where('employee_id', $emp_personal_details->id)->firstOrFail();


        $emp_job_details->employment_status = $request->input('employmentStatus');
        $emp_job_details->job_title = $request->input('jobTitle');


        $joinDate = $request->input('joinedDate');
        $joinDate_string = strtotime($joinDate);
        $joinDate_formatted = date('Y-m-d', $joinDate_string);
        $emp_job_details->joined_date = $joinDate_formatted;


        $confirmDate = $request->input('confirmationDate');
        $confirmDate_string = strtotime($confirmDate);
        $confirmDate_formatted = date('Y-m-d', $confirmDate_string);
        $emp_job_details->confirmation_date = $confirmDate_formatted;


        $terminationDate = $request->input('terminationDate');
        $terminationDate_string = strtotime($terminationDate);
        $terminationDate_formatted = date('Y-m-d', $terminationDate_string);
        $emp_job_details->termination_date = $terminationDate_formatted;

        $emp_job_details->department = $request->input('department');

        $emp_job_details->update();


        /*
         * Employee Qualifications
         */

        $emp_qualification = EmployeeQualification::where('employee_id', $emp_personal_details->id)->firstOrFail();


        $get_skills = $request->input('skill.*.name');
        $skills = json_encode($get_skills);
        $emp_qualification->skill = $skills;

        $get_qualifications = $request->input('qualification.*.name');
        $qualifications = json_encode($get_qualifications);
        $emp_qualification->qualification = $qualifications;

        $get_certifications = $request->input('certification.*.name');
        $certifications = json_encode($get_certifications);
        $emp_qualification->certification = $certifications;

        $emp_qualification->update();


        /*
         * Employee Emergency Contact info
         */

        $emp_emr_contact = EmployeeEmergencyContact::where('employee_id', $emp_personal_details->id)->firstOrFail();


        $emp_emr_contact->name = $request->input('emrName');
        $emp_emr_contact->relation = $request->input('relation');
        $emp_emr_contact->address = $request->input('emrAddress');
        $emp_emr_contact->mobile = $request->input('emrPhone');

        $emp_emr_contact->update();


        /*
         * Employee Experience info
         */


        $emp_experience = EmployeeExperience::where('employee_id', $emp_personal_details->id)->get();

        for ($i = 0; $i < count($request->organization); $i++) {

            if ($i < count($emp_experience)) {

                $emp_experience[$i]->organization_name = $request->input('organization')[$i];
                $emp_experience[$i]->address = $request->input('orgaAddress')[$i];
                $emp_experience[$i]->contact_no = $request->input('contactNo')[$i];
                $emp_experience[$i]->duration = $request->input('duration')[$i];
                $emp_experience[$i]->save();
            } else {

                $emp_experience = new EmployeeExperience();

                $emp_experience->organization_name = $request->input('organization')[$i];
                $emp_experience->address = $request->input('orgaAddress')[$i];
                $emp_experience->contact_no = $request->input('contactNo')[$i];
                $emp_experience->duration = $request->input('duration')[$i];
                $emp_experience->employee_id = $emp_personal_details->id;

                $emp_experience->save();
            }

        }

        /*
         * Attachment Upload
         */

        $attachment = EmployeeAttachment::where('employee_id', $emp_personal_details->id)->get();


        for ($i = 0; $i < count($request->title); $i++) {

            if ($i < count($attachment)) {

                if ($request->hasFile('file')[$i]) {

                    $destinationPath = 'uploads'; // upload path
                    $extension = $request->file('file')[$i]->getClientOriginalExtension(); // getting file extension
                    $fileName = $emp_personal_details->employee_code . '-' . rand(10000, 99999) . '.' . $extension; // renaming file
                    $attachment[$i]->type = 'attachment';
                    $attachment[$i]->title = $request->input('title')[$i];
                    $attachment[$i]->file_name = $fileName;
                    $attachment[$i]->location = 'uploads/' . $fileName;
                    $request->file('file')[$i]->move($destinationPath, $fileName);

                    $attachment[$i]->update();

                } else {

                    $attachment[$i]->type = 'attachment';
                    $attachment[$i]->title = $request->input('title')[$i];
                    $attachment[$i]->update();
                }

            } else {

                $destinationPath = 'uploads'; // upload path
                $extension = $request->file('file')[$i]->getClientOriginalExtension(); // getting file extension
                $fileName = $emp_personal_details->employee_code . '-' . rand(10000, 99999) . '.' . $extension; // renaming file
                $attachment = new EmployeeAttachment();
                $attachment->type = 'attachment';
                $attachment->title = $request->input('title')[$i] ? $request->input('title')[$i] : '';
                $attachment->file_name = $fileName;
                $attachment->location = 'uploads/' . $fileName;

                $request->file('file')[$i]->move($destinationPath, $fileName);

                $attachment->employee_id = $emp_personal_details->id;

                $attachment->save();
            }

        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $employee = EmployeePersonalDetail::where('id', $id)->update(['flag' => 0]);
    }


    /**
     * Employee Profile
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */


    public function empProfile($id)
    {
        $i = new FindRole();
        $uid = $i->findMyRole($id);

        $employeeData = User::with(array(
            "emp_official_info" => function ($query) {
                $query->with(array("emp_job_details" => function($query) {
                    $query->with(array("dep_info"));
                },"emp_qualifications", "emp_emergency_contact", "emp_experience", "emp_attachment"));
            }
        ))->find($uid);

        //return $employeeData;



        return view('employeeProfile', ['employee' => $employeeData]);
    }



}
