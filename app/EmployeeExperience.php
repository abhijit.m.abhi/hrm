<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmployeeExperience extends Model
{
    protected $fillable = [
        'organization',
        'orgaAddress',
        'contactNo',
        'duration',
    ];
}
