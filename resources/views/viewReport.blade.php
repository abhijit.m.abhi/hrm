@extends('layouts.master')
@section('title', 'Employee Bill')
@section('content')

    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>Employee Bill</h3>
                </div>


            </div>
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Employee Bill Report<!--<small>Input different form elements</small>--></h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">


                            <form action="" id="form" data-parsley-validate method="POST" class="form-horizontal" role="form"
                                  files="true" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div class="form-body">

                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Employee Name</label>
                                        <div class="col-md-6 col-sm-6 col-xs-12  ">
                                            <select class="select2_single form-control employee_id boldoption" tabindex="-1" id="employee_id" name="employee_id">
                                                <option value="">Select Option</option>

                                                @foreach($emp_details as $employee)

                                                    <option value="{{$employee->id}}">{{$employee->first_name}} {{$employee->middle_name}} {{$employee->last_name}}</option>
                                                @endforeach

                                            </select>
                                            <span class="error_name" id="error_name" style="display: none;"><strong style="color:red">Select Employee Name</strong></span>

                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Date</label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="text" name="daterange" id="daterange" value="01/01/2015 - 01/31/2015" />

                                        </div>
                                    </div>

                                    <div class="form-group">

                                        <div class="col-sm-10">
                                            <input type="hidden" class="form-control id"
                                                   id="id" name="id" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">

                                            <button type="button" id="save-btn" data-token="{{ csrf_token() }}" class="btn btn-primary">View Report</button>
                                        </div>
                                    </div>


                                </div>
                                <div class="table-responsive" id="reportShow" style="display: none;">
                                    <table id="datatable-buttons" class=" table table-striped table-bordered table-hover display">


                                        <thead>
                                        <tr class="alert-info head">
                                            <th>Title</th>
                                            <th>Date</th>
                                            <th>Amount</th>
                                            <th>Attachment</th>
                                        </tr>
                                        </thead>



                                        <tbody class="list">
                                        </tbody>

                                        <tfoot>
                                        <tr class="alert-success">
                                            <th>Total:</th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>

                            </form>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">

        var handleDataTableButtons = function () {
            if ($("#datatable-buttons").length) {
                $("#datatable-buttons").DataTable({
                    dom: "Bfrtip",
                    buttons: [
                        {
                            extend: "copy",
                            className: "btn-sm"
                        },
                        {
                            extend: "csv",
                            className: "btn-sm"
                        },
                        {
                            extend: "excel",
                            className: "btn-sm"
                        },
                        {
                            extend: "pdfHtml5",
                            className: "btn-sm"
                        },
                        {
                            extend: "print",
                            className: "btn-sm"
                        },
                        {
                            extend: "colvis",
                            className: "btn-sm"
                        }
                    ],
                    responsive: false
                });
            }
        };

        TableManageButtons = function () {
            "use strict";
            return {
                init: function () {
                    handleDataTableButtons();
                }
            };
        }();

        $('#datatable').dataTable();

        $('#datatable-keytable').DataTable({
            keys: true
        });

        $('#datatable-responsive').DataTable();

        $('#datatable-scroller').DataTable({
            ajax: "js/datatables/json/scroller-demo.json",
            deferRender: true,
            scrollY: 380,
            scrollCollapse: true,
            scroller: true
        });

        $('#datatable-fixed-header').DataTable({
            fixedHeader: true
        });

        var $datatable = $('#datatable-checkbox');

        $datatable.dataTable({
            'order': [[1, 'asc']],
            'columnDefs': [
                {orderable: false, targets: [0]}
            ]
        });
        $datatable.on('draw.dt', function () {
            $('input').iCheck({
                checkboxClass: 'icheckbox_flat-green'
            });
        });

        TableManageButtons.init();

    </script>

    <script>
        $(document).ready(function() {
            $(".select2_single").select2({
                placeholder: "Select a state",
                allowClear: true
            });
        });
    </script>

    <script>
        $("#save-btn").click(function (e) {



            $('input.daterange,option.select2_single.selected').each(function () {
                if ($.trim($(this).val()) == '') {
                    isValid = false; /* Required class style */
                    $(this).css({
                        "border": "1px solid red",
                        "background": "#FFCECE"
                    });
                    //isGoodImage(file);
                }
                else { /* Required class style removed */
                    $(this).css({
                        "border": "",
                        "background": ""
                    });
                    isValid = true;
                }
            });

            if($('#employee_id').val()==''){
                isValid = false; /* Required class style */
                $("#error_name").css("display", "block");
            }
            else { /* Required class style removed */
                $("#error_name").css("display", "none");
                isValid = true;
            }



            if (isValid == false)
                e.preventDefault();
            else{
                myFunction();

            }



        })


    </script>

    <script type="text/javascript">
        $(function() {
            $('#daterange').daterangepicker();
        });
    </script>

    <script>
       function myFunction() {


           $.ajax({
               type: 'get',
               url: './reportView',
               data: {
                   '_token': $('input[name=_token]').val(),
                   'employee_id': $("#employee_id").val(),
                   'daterange': $('#daterange').val()

               },
               success: function(data) {
                   $('#reportShow').show();

                   //$('.Report').html("<tr class='head'><td>" + 'Title'+ "</td><td>" + Date + "</td><td>" + Amount + "</td><td>" + Attachment + "</td></tr>");
                   $('.list').html('');
                   var length = data.length;
                   for(var i= 0;i<length;i++){
                       console.log(data[i].id);
                       $('.list').append("<tr class='list" + data[i].id + "'><td>" + data[i].title + "</td><td>" + data[i].date + "</td><td>" + data[i].total_bill + "</td><td>" + data[i].attachment + "</td></tr>");
                   }
                   console.log(length);
                   //console.log(data.id);
                   //  $('.Report').html("<tr class='list" + data.id + "'><td>" + data.title + "</td><td>" + data.date + "</td><td>" + data.total_bill + "</td><td>" + data.attachment + "</td><td></tr>");



               }
           })




       }

    </script>




@endsection