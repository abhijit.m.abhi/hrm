@extends('layouts.master')
@section('title', 'Resign Apply')
@section('content')
    <?php
    //print_r($resign);
    if(empty($resign)){
        $display = 1;
    }
    else $display =2;
    //echo $display.'<br>';
   // $act = $resign->flag;
    //echo $act;
           // dd($resign);
$value = 1;



    ?>

    <?php
    $url = url()->current();
    $parts = explode("/", $url);
    $var = end($parts);


    ?>
    <body onload="myFunction()">
    <div class="right_col" role="main">

        <div class="page-title">
            <div class="title_left">
                {{--<h3>Company Structure</h3>--}}
            </div>


        </div>
        <div class="clearfix"></div>


        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Apply For Resign<!--<small>Input different form elements</small>--></h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>

                    <div class="x_content">

                        <br/>

                        <div id="show_ajax_table" style="display: none;">
                            <table id="apply_r" class="table table-striped" >
                                <thead>
                                <tr>
                                    <th>Date</th>
                                    <th>Reason</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr class="append_rw{{$var}}">

                                </tr>


                                </tbody>
                            </table>
                        </div>


                        <?php

                        if(!empty($resign)){

                        ?>

                        <div id="status">
                            <table id="apply_res" class="table table-striped">
                                <thead>
                                <tr>
                                    <th>Date</th>
                                    <th>Reason</th>
                                    <th>Status</th>
                                    <th class="show_action_btn">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr id="append_row">
                                    <td>{{$resign->date}}</td>
                                    <td>{{$resign->comments}}</td>
                                    <td>
                                        <?php
                                        if($resign->flag=='0')
                                        {
                                            echo 'Pending';
                                        }
                                        elseif($resign->flag=='3')
                                        {

                                            echo 'Rejected';
                                            $value = 3;
                                        }
                                        else

                                            echo 'Accepted';

                                        ?>

                                    </td>
                                    <td class="show_action_btn">
                                        <button class="btn btn-primary btn-sm edit-modal-resign"   data-id="{{$resign->id}}" data-date="{{$resign->date}}" data-comments="{{$resign->comments}}"><span class="glyphicon glyphicon-edit"></span></button>
                                        <button class="btn confirm btn-danger btn-sm delete_resign"  data-id="{{$resign->id}}" data-date="{{$resign->date}}" data-comments="{{$resign->comments}}"><span class="glyphicon glyphicon-trash"></span></button>
                                    </td>

                                </tr>


                                </tbody>
                            </table>

                            <div id="re_apply" style="display: none;"><button type="button" data-id="{{$resign->id}}" class="btn btn-primary" id="btn_reApply" name="btn_reApply">Re-Apply</button></div>

                        </div>

                        <?php
                        }
                        else
                        {


                        ?>



                        <div id="showForm">
                            <form  data-parsley-validate method="POST" class="form-horizontal" role="form"
                                   files="true" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div class="form-body">

                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Resign Reason</label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <textarea class="form-control col-md-7 col-xs-12 comments"
                                                      name="comments" id="comments" rows="4" cols="3" required></textarea>
                                            @if ($errors->has('comments'))
                                                <span class="help-block">
                                                <strong>{{ $errors->first('comments') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Date</label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <div class='input-group date' id='datetimepicker1'>
                                                <input type='text' name="date" id="date" class="form-control date" />
                                                <span class="input-group-addon">
                                                     <span class="glyphicon glyphicon-calendar"></span>
                                                          </span>
                                            </div>
                                        </div>
                                        <script type="text/javascript">
                                            $(function () {
                                                $('#datetimepicker1').datepicker();
                                            });
                                        </script>
                                    </div>





                                    <div class="form-group">

                                        <input type="hidden" id="employee_id" name="employee_id" value="{{$empData->emp_official_info->id}}">
                                    </div>

                                    <div class="form-group">

                                        <div class="col-sm-10">
                                            <input type="hidden" class="form-control id"
                                                   id="id" name="id" />
                                        </div>
                                    </div>



                                    <div class="form-group">
                                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">

                                            <button type="button" id="save-btn" data-token="{{ csrf_token() }}" class="btn btn-primary">Apply</button>
                                        </div>
                                    </div>


                                </div>
                            </form>
                        </div>

                        <?php
                        }
                        ?>


                    </div>
                </div>
            </div>
        </div>

        {{--edit modal--}}

        <div class="modal fade open_modal" id="employeeResign" tabindex="-1" role="dialog"
             aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <!-- Modal Header -->
                    <div class="modal-header">
                        <button type="button" class="close closeT"
                                data-dismiss="modal">
                            <span aria-hidden="true">&times;</span>
                            <span class="sr-only">Close</span>
                        </button>
                        <h4 class="modal-title" id="myModalLabel">
                            Edit Resign Form
                        </h4>
                    </div>

                    <!-- Modal Body -->
                    <div class="modal-body">
                        <div class="modal-body">

                            <form id="form" class="form-horizontal" role="form">


                                <div class="form-body">

                                    <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                        <label class="control-label col-md-3 col-sm-6 col-xs-12">Resign Reason</label>
                                        <div class="col-md-9 col-sm-12 col-xs-12">
                                            <textarea class="form-control comments"
                                                      name="comments " id="emp_comments" rows="3" cols="5" required></textarea>
                                            @if ($errors->has('comments'))
                                                <span class="help-block">
                                                <strong>{{ $errors->first('comments') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>


                                    <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                        <label class="control-label col-md-3 col-sm-6 col-xs-12">Date</label>
                                        <div class="col-md-9 col-sm-6 col-xs-12">
                                            <div class='input-group date' id='datetimepicker1'>
                                                <input type='text' name="date" id="emp_date" class="form-control date" />
                                                <span class="input-group-addon">
                                                     <span class="glyphicon glyphicon-calendar"></span>
                                                          </span>
                                            </div>
                                        </div>
                                        <script type="text/javascript">
                                            $(function () {
                                                $('#datetimepicker1').datepicker();
                                            });
                                        </script>
                                    </div>


                                    <?php
                                    $url = url()->current();
                                    $parts = explode("/", $url);
                                    $var = end($parts);

                                    ?>


                                    <div class="form-group">

                                        <input type="hidden" id="employee_user_id" name="employee_id" value="{{$empData->emp_official_info->id}}">
                                    </div>

                                    <div class="form-group">

                                        <div class="col-sm-10">
                                            <input type="hidden" class="form-control id"
                                                   id="emp_id" name="id" />
                                        </div>
                                    </div>



                                    <div  class="modal-footer" id="footer-emp">
                                        <button type="button" class="btn btn-default closeT"
                                                data-dismiss="modal">
                                            Close
                                        </button>
                                        <button type="button" class="btn btn-primary edit"  id="edit-resign">
                                            Save changes
                                        </button>
                                    </div>


                                </div>


                            </form>
                        </div>

                        <meta name="_token" content="{!! csrf_token() !!}" />
                    </div>

                    <!-- Modal Footer -->

                </div>

            </div>
        </div>
        {{--delete modal--}}

        <div class="modal fade" tabindex="-1" id="myModal" aria-labelledby="myModalLabel" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h3 class="modal-title ">Delete Confirmation </h3>
                    </div>
                    <div class="modal-body ">
                        <p><b>Do you want to delete?&hellip;</b></p>
                        <input type="hidden" id="d_id">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default warning" data-dismiss="modal">Close</button>
                        <button type="button " class="btn btn-primary delete_confirm btn-danger" data-dismiss="modal" value="" data-token="{{ csrf_token() }}"data-dismiss="modal">Delete</button>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div>




    </div>


    </body>


@endsection

@section('script')

    <script>

        $('#save-btn').click(function () {

            //  alert("hello");
            $.ajaxSetup ({
                // Disable caching of AJAX responses
                cache: false
            });
            $.ajax({
                type: 'get',
                url: '../apply_for_resign',
                data: {
                    '_token': $('input[name=_token]').val(),
                    'comments': $('#comments').val(),
                    'employee_id': $('#employee_id').val(),
                    'date' : $('#date').val()

                },
                success: function(data) {
                    console.log(data);
                    $('#showForm').hide();



                    $('#show_ajax_table').show();
                    $('#apply_r').append("<tr class='append_rw" + data.employee_id + "'></td><td>" + data.date + "</td><td>" + data.comments+ "</td><td>" + ' Pending '+ "</td><td><button class='edit-modal-resign btn btn-info' data-id='" + data.id + "' data-date='" + data.date+ "' data-comments='" + data.comments + "' ><span class='glyphicon glyphicon-edit'></span></button> <button class='delete-modal btn btn-danger delete_resign' data-id='" + data.id + "' data-date='" + data.date+ "' data-comments='" + data.comments + "'><span class='glyphicon glyphicon-trash'></span></button></td></tr>");
                    window.location.reload();

                },
            });


        });



        /*edit modal*/


        $(document).on('click', '.edit-modal-resign', function() {

            $('#employeeResign').modal('show');
            $('#emp_id').val($(this).data('id'));
            $('#emp_comments').val($(this).data('comments'));
            $('#emp_date').val($(this).data('date'));


            $('.edit').click(function(){


                $.ajax({
                    type: 'get',
                    url: '../editResign',
                    data: {
                        '_token': $('input[name=_token]').val(),
                        'id': $("#emp_id").val(),
                        'comments': $('#emp_comments').val(),
                        'employee_id': $('#employee_user_id').val(),
                        'date' : $('#emp_date').val()

                    },
                    success: function(data) {

                        $('#append_row').replaceWith("<tr id='append_row'><td>" + data.date + "</td><td>" +  data.comments + "</td><td>" +   ' Pending ' + "</td><td><button class='edit-modal-resign btn btn-info' data-id='" + data.id + "' data-date='" + data.date+ "' data-comments='" + data.comments + "' ><span class='glyphicon glyphicon-edit'></span></button> <button class='delete-modal btn btn-danger delete_resign' data-id='" + data.id + "' data-date='" + data.date+ "' data-comments='" + data.comments + "' ><span class='glyphicon glyphicon-trash'></span></button></td></tr>");
                        //$('#append_rw').replaceWith("<tr id='append_rw'><td>" + data.date + "</td><td>" +  data.comments + "</td><td>" +   ' Pending ' + "</td><td><button class='edit-modal-resign btn btn-info' data-id='" + data.id + "' data-date='" + data.date+ "' data-comments='" + data.comments + "' ><span class='glyphicon glyphicon-edit'></span></button> <button class='delete-modal btn btn-danger delete_resign' data-id='" + data.id + "' data-date='" + data.date+ "' data-comments='" + data.comments + "' ><span class='glyphicon glyphicon-trash'></span></button></td></tr>");
                        $('#employeeResign').modal('hide');

                    }
                });

            })

        })




        $(document).on("click", ".delete_resign", function() {
            var did = $(this).data('id');
            $("#d_id").val(did);
            $('#myModal').modal('show');
        });
        var url = "../deleteEmployeeResign";

        $('.delete_confirm').click(function(){
            var _id =  $("#d_id").val();

            var id = _id;

            $.ajaxSetup({
                headers : {
                    'X-CSRF-TOKEN' : $('meta[name="_token"]').attr('content')
                }

            })
            $.ajax({
                type : 'GET',
                url : url + '/' + id,
                success: function(data){
                    console.log(id);

                    $("#append_row").remove();
                },
                error:function(data){
                    console.log('Error:',data);
                }

            });
        });


function myFunction() {

    var reject = '<?php echo $value;?>';



    if(reject == 3)
    {
       $('#re_apply').show();
        $('.show_action_btn').hide();

        $('#btn_reApply').click(function(){


            var user_id = $(this).data('id');

            $.ajax({
                type: 'get',
                url: '../reApply',
                data: {
                    '_token': $('input[name=_token]').val(),
                    'id': user_id


                },
                success: function(data) {


                    window.location.reload();
                   // $('#append_row').replaceWith("<tr id='append_row'><td>" + data.date + "</td><td>" +  data.comments + "</td><td>" +   ' Pending ' + "</td><td><button class='edit-modal-resign btn btn-info' data-id='" + data.id + "' data-date='" + data.date+ "' data-comments='" + data.comments + "' ><span class='glyphicon glyphicon-edit'></span></button> <button class='delete-modal btn btn-danger delete_resign' data-id='" + data.id + "' data-date='" + data.date+ "' data-comments='" + data.comments + "' ><span class='glyphicon glyphicon-trash'></span></button></td></tr>");
                    //$('#append_rw').replaceWith("<tr id='append_rw'><td>" + data.date + "</td><td>" +  data.comments + "</td><td>" +   ' Pending ' + "</td><td><button class='edit-modal-resign btn btn-info' data-id='" + data.id + "' data-date='" + data.date+ "' data-comments='" + data.comments + "' ><span class='glyphicon glyphicon-edit'></span></button> <button class='delete-modal btn btn-danger delete_resign' data-id='" + data.id + "' data-date='" + data.date+ "' data-comments='" + data.comments + "' ><span class='glyphicon glyphicon-trash'></span></button></td></tr>");
                   // $('#employeeResign').modal('hide');

                }
            });

        })


    }


}



    </script>
@endsection
