<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeeLoansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_loans', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('employee_id')->unsigned();
            $table->string('loan_type');
            $table->date('start_date')->nullable();
            $table->date('last_installment_date')->nullable();
            $table->integer('loan_period')->nullable();
            $table->double('loan_amount');
            $table->double('monthly_installment')->nullable();
            $table->double('paid_amount')->nullable();
            $table->string('status')->nullable();
            $table->string('details')->nullable();
            $table->timestamps();

            $table->foreign('employee_id')->references('id')->on('employee_personal_details')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee_loans');
    }
}
