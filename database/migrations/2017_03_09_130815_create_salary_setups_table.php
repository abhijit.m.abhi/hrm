<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalarySetupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('salary_setups', function (Blueprint $table) {
            $table->increments('id');
            $table->string('salary_type');
            $table->string('salary_grade');
            $table->string('daily_rate')->nullable();
            $table->double('basic_salary')->nullable();
            $table->double('hourly_rate')->nullable();
            $table->double('overtime')->nullable();
            $table->string('allowances')->nullable();
            $table->string('deductions')->nullable();
            $table->double('net_salary')->nullable();
            $table->integer('flag')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('salary_setups');
    }
}
