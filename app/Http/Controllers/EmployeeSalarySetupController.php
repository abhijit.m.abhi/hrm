<?php

namespace App\Http\Controllers;

use App\EmployeePersonalDetail;
use App\EmployeeSalarySetup;
use App\SalarySetup;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class EmployeeSalarySetupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    //
    }

    public function view()
    {

        $employee_info = EmployeePersonalDetail::all();
        return view('employeeSalarySetup',['employee_info' => $employee_info]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        $setEmployeeSalary = new EmployeeSalarySetup();

        $title_data = Input::get('title');
        $amount_data = Input::get('amount');


        foreach( $title_data as $key => $n ) {

            $data = array(
                $n => $amount_data[$key]
            );
            $file_data_allowances_array[] = $data;
        }
        $allowance = json_encode($file_data_allowances_array);



        $title_deduction = Input::get('title_deduction');
        $amount_deduction = Input::get('amount_deduction');

        foreach( $title_deduction as $key => $n ) {

            $data = array(
                $n => $amount_deduction[$key]
            );
            $file_data_deductions_array[] = $data;
        }
        $deduction = json_encode($file_data_deductions_array);

        $setEmployeeSalary->employee_id=$request->input('employee_id');

        $setEmployeeSalary->salary_type=$request->input('salary_type');
        $setEmployeeSalary->salary_grade=$request->input('salary_grade');
        $setEmployeeSalary->basic_salary=$request->input('basic_salary');
        $setEmployeeSalary->overtime=$request->input('over_time');
        //$setSalary->hourly_grade=$request->input('hourly_grade');
        $setEmployeeSalary->hourly_rate=$request->input('hourly_rate');
        // $setSalary->daily_grade=$request->input('daily_grade');
        $setEmployeeSalary->daily_rate=$request->input('daily_rate');
        $setEmployeeSalary->flag=0;
        $setEmployeeSalary->allowances=$allowance;
        $setEmployeeSalary->deductions=$deduction;
        $setEmployeeSalary->net_salary=$request->input('net_salary');
        $setEmployeeSalary->job_id=$request->input('job_id');
        /*var_dump($setSalary);
        exit;*/
        if($setEmployeeSalary->save()){
            return redirect('/emp_view_salary_setup');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        //$editSalary = EmployeeSalarySetup::findOrFail($id);
        $employeeSalaryEdit = EmployeeSalarySetup::where('id','=',$id)->with("emp_salary")->where('flag', '=', '0')->first();

        return view('editEmployeeSalary', ['employeeSalaryEdit' => $employeeSalaryEdit]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $setEmployeeSalaryEdit = EmployeeSalarySetup::findOrFail($id);

        $title_data = Input::get('title');
        $amount_data = Input::get('amount');


        foreach( $title_data as $key => $n ) {

            $data = array(
                $n => $amount_data[$key]
            );
            $file_data_allowances_array[] = $data;
        }
        $allowance = json_encode($file_data_allowances_array);



        $title_deduction = Input::get('title_deduction');
        $amount_deduction = Input::get('amount_deduction');

        foreach( $title_deduction as $key => $n ) {

            $data = array(
                $n => $amount_deduction[$key]
            );
            $file_data_deductions_array[] = $data;
        }
        $deduction = json_encode($file_data_deductions_array);

        $setEmployeeSalaryEdit->employee_id=$request->input('employee_id');

        $setEmployeeSalaryEdit->salary_type=$request->input('salary_type');
        $setEmployeeSalaryEdit->salary_grade=$request->input('salary_grade');
        $setEmployeeSalaryEdit->basic_salary=$request->input('basic_salary');
        $setEmployeeSalaryEdit->overtime=$request->input('over_time');
        //$setSalary->hourly_grade=$request->input('hourly_grade');
        $setEmployeeSalaryEdit->hourly_rate=$request->input('hourly_rate');
        // $setSalary->daily_grade=$request->input('daily_grade');
        $setEmployeeSalaryEdit->daily_rate=$request->input('daily_rate');
        $setEmployeeSalaryEdit->flag=0;
        $setEmployeeSalaryEdit->allowances=$allowance;
        $setEmployeeSalaryEdit->deductions=$deduction;
        $setEmployeeSalaryEdit->net_salary=$request->input('net_salary');
        $setEmployeeSalaryEdit->job_id=$request->input('job_id');
        /*var_dump($setSalary);
        exit;*/
        if($setEmployeeSalaryEdit->update()){
            return redirect('/emp_view_salary_setup');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

    }

    public function salaryGradeInfo()
    {
        $salary_type = $_GET['salary_type'];
        $salary_info = SalarySetup::where('salary_type', '=',$salary_type )->where('flag', '=', '0')->get();
        return response()->json($salary_info);
    }

    public function salaryGrade()
    {

        $salary_grade = $_GET['salary_grade'];
        $salary_grade_info = SalarySetup::where('salary_grade', '=',$salary_grade )->where('flag', '=', '0')->get();
        return response()->json($salary_grade_info);
    }

    public function view_salary(){

        $salaryInfo = EmployeeSalarySetup::with("emp_salary")->where('flag', '=', '0')->get();
        return view('viewEmployeeSalary',['salary' => $salaryInfo]);

    }

    public function view_details($id){

        //return $id;

       //$job_details =  EmployeePersonalDetail::where('id','=',$id)->with('emp_job_details')->first();
       //return $job_details;


        $viewInfo = EmployeeSalarySetup::with(array(
            "emp_salary" => function ($query) {
                $query->with(array("emp_job_details", "emp_qualifications", "emp_emergency_contact", "emp_experience", "emp_attachment"));
            }
        ))->find($id);


        //return $viewInfo;


        return view('employeeSalaryViewDetails',['viewInfo' => $viewInfo]);

    }


}
