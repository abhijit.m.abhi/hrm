<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LateAttendance extends Model
{
    //

    protected $fillable = [
        'date',
        'entry_time'
    ];
}
