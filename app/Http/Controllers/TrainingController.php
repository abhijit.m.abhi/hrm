<?php

namespace App\Http\Controllers;

use App\Course;
use App\EmployeePersonalDetail;
use App\EmployeeTraningSession;
use App\EmployeeQualification;
use App\TranningSession;
use DateTime;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Input;

class TrainingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

    }

    public function view(){

        $addCourse = Course::all();
        $Training = TranningSession::all();

       $employeeTrainingSession = EmployeeTraningSession::with("emp_Training")->get();
       $employee = EmployeePersonalDetail::all();



        return view('trainingSetup', ['course' => $addCourse,'training' => $Training,'employee' => $employee,'empTrain' => $employeeTrainingSession]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function addCourse(Request $req){

        $addCourse = new Course();

        $addCourse->code = $req->code;
        $addCourse->name = $req->name;
        $addCourse->coordinator = $req->coordinator;
        $addCourse->trainer = $req->trainer;
        $addCourse->payment_type = $req->payment_type;
        $addCourse->cost = $req->cost;
        $addCourse->status = $req->status;

        $addCourse->save();
        return response()->json($addCourse);

    }

    public function editCourse(Request $req){

        $editCourse = Course::find ($req->id);
        $editCourse->code = $req->code;
        $editCourse->name = $req->name;
        $editCourse->coordinator = $req->coordinator;
        $editCourse->trainer = $req->trainer;
        $editCourse->payment_type = $req->payment_type;
        $editCourse->cost = $req->cost;
        $editCourse->status = $req->status;
        $editCourse->update();


        return response()->json($editCourse);
    }

    public function addTraining(Request $req){

        $training = new TranningSession();

        $training->name = $req->name;
        $training->course = $req->course;
        $training->details = $req->details;

        $time =  date("Y-m-d h:i:s",strtotime($req->sheduled_time));
        $training->sheduled_time =$time;

        $datePick = $req->input('duration');
        $dateSeparator = explode(" - ",$datePick);
        $s = strtotime($dateSeparator[0]);
        $e = strtotime($dateSeparator[1]);
        $start_date = date('Y-m-d', $s);
        $end_date = date('Y-m-d', $e);
        $duration = array("$start_date" , "$end_date");
        $date = array_values($duration);
        $duration_time=json_encode($date);
        $training->duration =$duration_time;


        $training->training_certificate = $req->training_certificate;

        $training->save();
        return response()->json($training);


    }

    public function editTraining(Request $req){

        $editTraining = TranningSession::find ($req->id);
        $editTraining ->name = $req->name;
        $editTraining ->course = $req->course;
        $editTraining ->details = $req->details;
        $time =  date("Y-m-d h:i:s",strtotime($req->sheduled_time));
        $editTraining ->sheduled_time =$time;

        $datePick = $req->input('duration');


        $dateSeparator = explode('-',$datePick);
        $s = strtotime($dateSeparator[0]);
        $e = strtotime($dateSeparator[1]);
        $start_date = date('Y-m-d', $s);
        $end_date = date('Y-m-d', $e);
        $duration = array("$start_date" , "$end_date");
        $date = array_values($duration);
        $duration_time=json_encode($date);
        $editTraining ->duration =$duration_time;

        $editTraining ->training_certificate = $req->training_certificate;

        $editTraining ->update();
        return response()->json($editTraining );

    }

    public function addEmployeeTraining(Request $req){

        $addEmployeeTraining = new EmployeeTraningSession();

        $emp_no = $_GET['employee_id'];

        $addEmployeeTraining->employee_id = $req->employee_id;
//        $emp = EmployeePersonalDetail::where('id', '=' ,$emp_no)->get();


        $addEmployeeTraining->training_session = $req->training_session;
        $addEmployeeTraining->status = $req->status;

        $addEmployeeTraining->save();
        $id= $addEmployeeTraining->id;

        $employeeTrainingSession = EmployeeTraningSession::with("emp_Training")->where('id', '=' ,$id)->get();
        //return $employeeTrainingSession;

        return response()->json($employeeTrainingSession);

    }

    public function edit_emp_Training(Request $req){

        $editEmp = EmployeeTraningSession::find ($req->id);
        $emp_no = $_GET['employee_id'];
        $editEmp->employee_id = $req->employee_id;
        $editEmp->training_session = $req->training_session;
        $editEmp->status = $req->status;


        $editEmp->save();
        $id= $req->id;
        $editEmployeeTrainingSession = EmployeeTraningSession::with("emp_Training")->where('id', '=' ,$id)->get();
        return response()->json($editEmployeeTrainingSession);
    }
}
