<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>HRM | Login</title>

    <!-- CSS -->
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
    <link href="{{ asset('/assets/loginTemplate/assets/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet"
          type="text/css"/>
    <link href="{{ asset('/assets/loginTemplate/assets/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet"
          type="text/css"/>
    <link href="{{ asset('/assets/loginTemplate/assets/css/form-elements.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('/assets/loginTemplate/assets/css/form-elements.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('/assets/loginTemplate/assets/css/style.css') }}" rel="stylesheet" type="text/css"/>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Favicon and touch icons -->

    <style>
        .centered-text {
            text-align:center;

        }
        .parallax {
            /* The image used */
            background-image: url("{{ asset('/assets/loginTemplate/assets/img/backgrounds/1.jpg') }}");

            /* Set a specific height */
            min-height: 800px;

            /* Create the parallax scrolling effect */
            background-attachment: fixed;
            background-position: center;
            background-repeat: no-repeat;
            background-size: cover;
        }
    </style>

</head>

<body>

<!-- Top content -->
<div class="top-content">

    <div class="inner-bg parallax">
        <div class="container">
            <div class="row">
                <div class="col-sm-8 col-sm-offset-2 text">
                    <h1><strong>Human Resource Management</strong></h1>
                    <div class="description">
                        <p>

                        </p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6 col-sm-offset-3 form-box">
                    <div class="form-top">
                        <div class="centered-text">

                            <h3 class="red-text text-center">Reset Password</h3>
                        </div>

                    </div>
                    <div class="form-bottom">
                        <form class="login-form" role="form">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label class="sr-only" for="email">Email</label>
                                <input type="text" name="email" placeholder="Email" class="form-useremail form-control"
                                       id="email">
                                <span class="chk" id="chk" style="display: none;">
                                <strong style="color:red">Email not found</strong>
                            </span>

                            </div>


                                <!-- Button -->
                                <div >
                                    <button id="btn-signup" type="button" class="btn btn-success">Submit</button>
                                </div>
                            <div class="form-group centered-text ">
                                <a style="color:red" href="{{ url('/login') }}">Back to login</a>

                            </div>

                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>

</div>


<!-- Javascript -->
<script src="{{ asset('/assets/loginTemplate/assets/js/jquery-1.11.1.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/loginTemplate/assets/bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/loginTemplate/assets/js/jquery.backstretch.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/loginTemplate/assets/js/scripts.js') }}" type="text/javascript"></script>


<!--[if lt IE 10]>

<![endif]-->

</body>

</html>
<script type="text/javascript">

    $('#btn-signup').on('click', function() {

        var email = $('#email').val();

        $.ajaxSetup({
            headers : {
                'X-CSRF-TOKEN' : $('meta[name="_token"]').attr('content')
            }

        })
        $.ajax({
            type : 'GET',
            url : 'check',
            data: {'email': email},
            success: function(data){
                if(data == "not_found")
                {
                    $("#chk").css("display", "block");
                }

                else
                {
                    $("#chk").css("display", "none");
                }

            },
            error:function(data){
                console.log('Error:',data);
            }

        });

    });

    //});
</script>
