@extends('layouts.master')
@section('title', 'Loan Setup')
@section('style')
    <style>
        .calendar-table
        {

        }
        .select2{
            width: 100% !important;
        }

    </style>
@endsection
@section('content')

    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>Apply Employee Loan Setup</h3>
                </div>


            </div>
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Apply Loan<!--<small>Input different form elements</small>--></h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                            @if(Session::has('message'))

                                <div id="successMessage" class="alert alert-success alert-dismissible fade in" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                                aria-hidden="true">×</span>
                                    </button>
                                    <strong>{{ Session::get('message') }}</strong>
                                </div>
                            @endif
                        </div>
                        {{--tab view--}}
                        <div class="bs-example">
                            <div class="tabbable">
                                <ul class="nav nav-tabs">
                                    <li class="active"> <input type="button" data-toggle="tab" href="#tab1" data-loading-text="Loading..." class="btn btn-info" value="   Loan  "></li>
                                    {{--<li> <input type="button" data-toggle="tab" href="#tab2" data-loading-text="Loading..." class="btn btn-info" value="    View Loan    "></li>--}}

                                </ul>
                                <div class="tab-content">

                                    <div id="tab1" class="tab-pane active fade in">
                                        {{--fade view for add skill--}}
                                        <button onclick="myEmployee();" data-backdrop="ture" id="hello" class="btn btn-primary btn-md" data-type="Skills"  data-token="{{ csrf_token() }}"  data-toggle="modal" data-target="#myModalHorizontalkk"><span class="glyphicon glyphicon-plus">
                                                    Apply Employee Loan
                                                </span></button>

                                        {{--table--}}
                                        <div class="x_content">
                                            <div class="table-responsive">
                                                <table id="datatable-buttons" class="table-class EmployeeLoan table table-striped table-bordered table-hover display">
                                                    <thead>
                                                    <tr class="alert-info">
                                                        <th>Loan Type</th>
                                                        <th>Amount</th>
                                                        <th>Loan Period</th>
                                                        <th>Installment Amount</th>
                                                        <th>Details</th>
                                                        <th>Action</th>
                                                        <th>Action</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @foreach($loanData as $empLoan)
                                                        <tr class='item{{$empLoan->id}}' id="row_id{{$empLoan->id}}">

                                                            <td>{{$empLoan->loan_type}}</td>

                                                            <td>{{$empLoan->loan_amount}}</td>

                                                            <td>{{$empLoan->loan_period}}</td>
                                                            <td>{{$empLoan->monthly_installment}}</td>

                                                            <td>{{$empLoan->details}}</td>

                                                            <td>{{$empLoan->status}}</td>
                                                            <td align="center">

                                                               @if ($empLoan->status == 'Active')
                                                                    <button class="btn btn-primary" onclick="view_installment({{$empLoan->id}});" data-id="{{$empLoan->id}}"><span class="glyphicon glyphicon-eye-open"></span></button>
                                                                   @else
                                                                   <button class="btn btn-primary btn-sm edit-modal-empTrain"   data-id="{{$empLoan->id}}" data-loan_type="{{$empLoan->loan_type}}" data-loan_amount="{{$empLoan->loan_amount}}" data-details="{{$empLoan->details}}" data-employee_id="{{$empLoan->employee_id}}" ><span class="glyphicon glyphicon-edit"></span></button>
                                                                @endif





                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>

                                        <div id="showInstallments" style="display: none;">
                                            <div class="x_content">
                                                <div class="table-responsive">
                                                    <h2 align="center"><b> Installment Information</b></h2>
                                                    <table id="datatable-buttons" class="table-class EmployeeLoanInstallment table table-striped table-bordered table-hover display">
                                                        <thead>
                                                        <tr class="alert-info">
                                                            <th style="text-align: center;" >SL No.</th>
                                                            <th style="text-align: center;">Installment Amount</th>
                                                            <th style="text-align: center;"> Date</th>

                                                        </tr>
                                                        </thead>
                                                        <tbody class="installment_data">

                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>


                                        <meta name="_token" content="{!! csrf_token() !!}"/>
                                        {{--end table--}}
                                    </div>

                                   {{-- <div id="tab2" class="tab-pane fade">





                                        <meta name="_token" content="{!! csrf_token() !!}"/>
                                        --}}{{--end table view--}}{{--
                                    </div>--}}


                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



    {{--delete modal--}}

    <div class="modal fade" id="deleteLoanModal" aria-labelledby="myModalLabel" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header alert-danger">
                    <button type="button" class="close" data-dismiss="modal"
                            aria-label="Close"><span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title"><span class="glyphicon glyphicon-trash"></span>
                        Confirmation Message</h4>
                </div>
                <div class="modal-body">
                    <p>Are you sure that you want to permanently delete the selected
                        element?</p>
                    <input type="text" id="d_id">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">No
                    </button>
                    <button type="button" id="delete_empLoan_btn" class="btn btn-danger delete_empLoan_btn"
                            data-dismiss="modal" value=""
                            data-token="{{ csrf_token() }}" data-dismiss="modal">Delete
                    </button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    {{--end delete modal--}}


    {{--add form modal --}}



    {{--empolyee training session modal--}}

    <div class="modal fade open_modal" id="employeeLoanModal" tabindex="-1" role="dialog"
         aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header">
                    <button type="button" class="close closeT"
                            data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                        <span class="sr-only">Close</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">
                        Apply For Loan Form
                    </h4>
                </div>

                <!-- Modal Body -->
                <div class="modal-body">
                    <div class="modal-body">

                        <form id="form" class="form-horizontal" role="form">


                            <div class="form-group">
                                <label class="control-label col-md-4 col-sm-4 col-xs-12">Employee Name</label>
                                <div class="col-md-6 col-sm-10 col-xs-12">


                                    <input type="text" class="form-control"
                                           id="emp_name" name="emp_name" value="{{$userData->emp_official_info->first_name}} {{$userData->emp_official_info->last_name}}" readonly>
                                    <input type="hidden" class="form-control"
                                           id="employee_id" name="employee_id" value="{{$userData->emp_official_info->id}}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-4 col-sm-4 col-xs-12">Loan Type<span class="tp" style="color: red;">*</span></label>
                                <div class="col-md-6 col-sm-10 col-xs-12">
                                    <select class="loanType form-control required" tabindex="-1" id="loan_type" name="loan_type">
                                        <option value="">Select Loan Type</option>
                                        @foreach($loanType as $loan)
                                            <option value="{{$loan->name}}">{{$loan->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>






                            <div class="form-group">
                                <label class="control-label col-md-4 col-sm-4 col-xs-12">Loan Amount<span class="tp" style="color: red;">*</span></label>
                                <div class="col-md-6 col-sm-10 col-xs-12">
                                    <input type="number" class="form-control required"
                                           id="loan_amount" name="loan_amount" placeholder="Enter Loan Amount">
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="control-label col-md-4 col-sm-4 col-xs-12">Details</label>
                                <div class="col-md-6 col-sm-10 col-xs-12">
                                    <textarea class="form-control" rows="5" id="details" name="details" ></textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-2 col-sm-10">
                                    <button type="button" id="save-btn-employeeLoan"   data-token="{{ csrf_token() }}"  class="btn btn-primary">Add</button>
                                </div>
                            </div>

                            <div class="form-group">

                                <div class="col-sm-10">
                                    <input type="hidden" class="form-control"
                                           id="id" name="id"/>
                                </div>
                            </div>

                            <div style="display:none" class="modal-footer" id="footer-emp">
                                <button type="button" class="btn btn-default closeT"
                                        data-dismiss="modal">
                                    Close
                                </button>
                                <button type="button" class="btn btn-primary edit" id="edit-empLoan">
                                    Update
                                </button>
                            </div>

                        </form>
                    </div>

                    <meta name="_token" content="{!! csrf_token() !!}" />
                </div>

                <!-- Modal Footer -->

            </div>

        </div>
    </div>

@endsection
@section('script')

    <script>
        // validate a field on "blur" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':
        $('form')
            .on('blur', 'input[required], input.optional, select.required', validator.checkField)
            .on('change', 'select.required', validator.checkField)
            .on('keypress', 'input[required][pattern]', validator.keypress);

        $('.multi.required').on('keyup blur', 'input', function() {
            validator.checkField.apply($(this).siblings().last()[0]);
        });


        $('form').submit(function(e) {
            e.preventDefault();
            var submit = true;

            // evaluate the form using generic validaing
            if (!validator.checkAll($(this))) {
                submit = false;
            }

            if (submit)
                this.submit();

            return false;
        });

    </script>


    <script>
        $(document).ready(function(){
            $(".nav-tabs a").click(function(){
                $(this).button('loading').delay(500).queue(function(){
                    $(this).button('reset');
                    $(this).dequeue();
                });
            });


        });
    </script>

    <script type="text/javascript">
        function totalPaidAmount() {
            var period = $("#loan_period").val();
            var installment = $("#monthly_installment").val();

            var amount = (period * installment).toFixed(2);
            $("#paid_amount").val(amount);

        }
    </script>
    <script>
        $(".employeeName").select2();
        $(".loanType").select2();
    </script>


    <script>

        function  myFunction() {
            $("#myModalHorizontal").modal('show');
            $('#loan_amount').focus(function() {

                $(this).css({
                    "border": "",
                    "background": ""
                });

            })
            //$('#footer').hide();
        }
    </script>

    <script>
        $('.closeBtn').click(function(){
            $('#name').val('');
            $('#description').val('');
            $('#footer').hide('');
            $('#save-btn-loan').show();
            $('input[type="text"],select.required').each(function () {
                if ($.trim($(this).val()) == '') {
                    $(this).css({
                        "border": "",
                        "background": ""
                    });
                }
            });
        });
    </script>

    <script>
       function view_installment(id) {
           var employee_id = id;

           $('.installment_data').html("");

           $.ajax({

               type: 'get',
               url: '../showInstallment',
               data: {
                   '_token': $('input[name=_token]').val(),
                   'id':employee_id

               },
               success: function(data) {
                  console.log(data);
                   $('#showInstallments').css('display','block');
                  var i = 0;
                  for(i=0;i<data.length;i++){
                      $('.installment_data').append("<tr class='item'>" +

                          "<td align='center'>"+data[i].count_installment+"</td>" +
                          "<td align='center'>"+data[i].amount+"</td>" +
                          "<td align='center'>"+data[i].date+"</td>" +
//

                          "</tr>");
                  }

               }
           });
       }

    </script>




    <script>
        function myEmployee() {

            $('#employeeLoanModal').modal('show');
            $('#save-btn-employeeLoan').show();

        }
    </script>

    <script>
        $('.closeT').click(function(){

            //$('#employee_id').val('');
            $('#loan_type').val('');

            $('#loan_amount').val('');

            $('#details').val('');
            $('#footer-emp').hide('');
            $('#save-btn-employee').show();


            $('input[type="number"]').each(function () {
                if ($.trim($(this).val()) == '') {

                    $(this).css({
                        "border": "",
                        "background": ""
                    });
                }

            });

        });
    </script>
    <script>
        $("#save-btn-employeeLoan").click(function() {
            //var a = $('#employee_id').val();


            //$('#employee_id').val()


            $.ajaxSetup ({
                // Disable caching of AJAX responses
                cache: false
            });
            $.ajax({
                type: 'get',
                url: '../applyEmployeeLoan',
                data: {
                    '_token': $('input[name=_token]').val(),
                    'employee_id': $('#employee_id').val(),
                    'loan_type': $('#loan_type').val(),
                    'loan_amount' : $('#loan_amount').val(),
                    'details': $('#details').val()



                },
                success: function(data) {
                    console.log(data);


                    if ((data.errors)){
                        $('.error').removeClass('hidden');
                        $('.error').text(data.errors.name);
                    }
                    else {
                        $('.error').addClass('hidden');


                        $('.EmployeeLoan').append("<tr class='item" + data.id + "'><td>" + data.loan_type + "</td><td>" + data.loan_amount+ "</td><td>" +data.loan_period +"</td><td>" + data.monthly_installment+"</td><td>" + data.details+ "</td><td>" + data.status+"</td><td align='center'><button class='edit-modal-empTrain btn btn-info' data-id='" + data.id + "'   data-details='" + data.details+ "' data-loan_amount='" + data.loan_amount+ "'  data-employee_id='" + data.employee_id+ "' ><span class='glyphicon glyphicon-edit'></span></button> </td></tr>");
                        $("#employeeLoanModal").modal('hide');

                    }
                    $('#loan_type').val('');
                    $('#loan_amount').val('');
                    $('#details').val('');
                },
            });



        });

    </script>




    <script>

        $(document).on('click', '.edit-modal-empTrain', function() {

            $('#id').val($(this).data('id'));
            $('#employee_id').val($(this).data('employee_id'));
           // $('#loan_type').val($(this).data('loan_type'));

            $('#loan_amount').val($(this).data('loan_amount'));


            $('#details').val($(this).data('details'));
            $('#save-btn-employeeLoan').hide();
            $('#employeeLoanModal').modal('show');
            //$('#save-btn-employee').hide();
            $('#footer-emp').css('display','block');


            $('#edit-empLoan').click(function(){
                $.ajax({
                    type: 'get',
                    url: '../editApplyEmployeeLoan',
                    data: {
                        '_token': $('input[name=_token]').val(),
                        'id': $("#id").val(),
                        'employee_id': $('#employee_id').val(),
                        'loan_type': $('#loan_type').val(),

                        'loan_amount' : $('#loan_amount').val(),

                        'details': $('#details').val()

                    },
                    success: function(data) {

                        $('.item' + data.id).replaceWith("<tr class='item" + data.id + "'><td>" + data.loan_type + "</td><td>" + data.loan_amount+ "</td><td>" +data.loan_period +"</td><td>" + data.monthly_installment+"</td><td>" + data.details+ "</td><td>" + data.status +  "</td><td align='center'><button class='edit-modal-empTrain btn btn-info' data-id='" + data.id + "'  data-loan_type='" + data.loan_type + "'  data-loan_amount='" + data.loan_amount+"'  data-details='" + data.details+"'   ><span class='glyphicon glyphicon-edit'></span></button> </td></tr>");
                       $('#employeeLoanModal').modal('hide');



                        $('#footer-emp').hide('');
                        $('#save-btn-employee').show();
                    }
                });

            });
        });
    </script>

    <script>
        $("#save-btn-employeeLoan").click(function (e) {
            var isValid = true;
            $('input[type="number"],select.required').each(function () {
                if ($.trim($(this).val()) == '') {
                    isValid = false; /* Required class style */
                    $(this).css({
                        "border": "1px solid red",
                        "background": "#FFCECE"
                    });
                }
                else { /* Required class style removed */
                    $(this).css({
                        "border": "",
                        "background": ""
                    });
                }
            });
            if (isValid == false)
                e.preventDefault();
            else{

            }


        });
    </script>



@endsection