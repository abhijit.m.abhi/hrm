@extends('layouts.master')
@section('title', 'Employee Leaves ')
@section('style')
    <style>
        .calendar-table
        {

        }
        .select2{
            width: 100% !important;
        }

    </style>
@endsection
@section('content')

    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>Employee Leaves</h3>
                </div>


            </div>
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Employee Leaves<!--<small>Input different form elements</small>--></h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                            @if(Session::has('message'))

                                <div id="successMessage" class="alert alert-success alert-dismissible fade in" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                                aria-hidden="true">×</span>
                                    </button>
                                    <strong>{{ Session::get('message') }}</strong>
                                </div>
                            @endif
                        </div>
                        {{--tab view--}}
                        <div class="bs-example">
                            <div class="tabbable">
                                <ul class="nav nav-tabs">
                                    {{--<li class="active"> <input type="button" data-toggle="tab" href="#tab1" data-loading-text="Loading..." class="btn btn-info" value="Loan"></li>--}}
                                    {{--<li> <input type="button" data-toggle="tab" href="#tab2" data-loading-text="Loading..." class="btn btn-info" value="  Employee Loan    "></li>--}}

                                </ul>
                                <div class="tab-content">
                                    <div id="tab1" class="tab-pane active fade in">
                                        {{--fade view for add skill--}}
                                        <button onclick="myFunction()" data-backdrop="ture" id="hello" class="btn btn-primary btn-md" data-type="Skills"  data-token="{{ csrf_token() }}"  data-toggle="modal" data-target="#myModalHorizontalkk"><span class="glyphicon glyphicon-plus">
                                                    Add A Employee Leave
                                                </span></button>


                                        <div class="x_content">
                                            <div class="table-responsive">
                                                <table id="datatable-buttons" class="Holidays table table-striped table-bordered table-hover display">
                                                    <thead>
                                                    <tr class="alert-info">
                                                        <th>Employee Name</th>
                                                        <th>Leave Type</th>
                                                        <th>Leave Day (s)</th>
                                                        <th>Action</th>
                                                    </tr>

                                                    </thead>


                                                    <tbody>
                                                    @foreach($empLeaves as $empLeave)
                                                        <tr class='item{{$empLeave->id}}' id="row_id{{$empLeave->id}}">
                                                            <td>{{$empLeave->emp_leave->first_name}} {{$empLeave->emp_leave->middle_name}} {{$empLeave->emp_leave->last_name}}</td>
                                                            <td>{{$empLeave->leave_type}}</td>
                                                            <td>{{$empLeave->leave_days}}</td>

                                                            <td align="center">
                                                                <button class="btn btn-primary btn-sm edit-modal-public_holiday" data-id="{{$empLeave->id}}" data-emp_name="{{$empLeave->employee_id}}" data-leave_type="{{$empLeave->leave_type}}" data-leave_days="{{$empLeave->leave_days}}" ><span class="glyphicon glyphicon-edit"></span></button>
                                                                <button class="btn confirm btn-danger btn-sm delete_course"  data-id="{{$empLeave->id}}" data-emp_name="{{$empLeave->emp_leave->first_name}} {{$empLeave->emp_leave->middle_name}} {{$empLeave->emp_leave->last_name}}" data-leave_type="{{$empLeave->leave_type}}" data-leave_days="{{$empLeave->leave_days}}" ><span class="glyphicon glyphicon-trash"></span></button>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            {{--Modal--}}
                                            <!-- /.modal -->
                                            </div>
                                        </div>
                                        <meta name="_token" content="{!! csrf_token() !!}"/>
                                        {{--end table view--}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



    {{--delete modal--}}
    <div class="modal fade" id="myModal" aria-labelledby="myModalLabel" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header alert-danger">
                    <button type="button" class="close" data-dismiss="modal"
                            aria-label="Close"><span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title"><span class="glyphicon glyphicon-trash"></span>
                        Confirmation Message</h4>
                </div>
                <div class="modal-body">
                    <p>Are you sure that you want to permanently delete the selected
                        element?</p>
                    <input type="hidden" id="d_id">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">No
                    </button>
                    <button type="button" class="btn btn-danger delete_holiday_btn delete_empLoan_btn"
                            data-dismiss="modal" value=""
                            data-token="{{ csrf_token() }}" data-dismiss="modal">Delete
                    </button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    {{--end delete modal--}}


    {{--add form modal --}}
    <div class="modal fade open_modal" id="myModalHorizontal" tabindex="-1" role="dialog"
         aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header">
                    <button type="button" class="close closeBtn"
                            data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                        <span class="sr-only">Close</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">
                        Add Leave For Employee
                    </h4>
                </div>

                <!-- Modal Body -->
                <div class="modal-body">
                    <div class="modal-body">

                        <form id="form" class="form-horizontal" role="form" data-parsley-validate>
                            <div class="form-group">
                                <label class="control-label col-md-4 col-sm-4 col-xs-12">Employee Name</label>
                                <div class="col-md-6 col-sm-10 col-xs-12">
                                    <select class="loanType form-control required"  id="emp_name" name="emp_name">
                                        <option value="">Select an employee</option>
                                        @foreach($employees as $emp)
                                            <option value="{{$emp->id}}">{{$emp->first_name}} {{$emp->middle_name}} {{$emp->last_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-4 col-sm-4 col-xs-12">Leave Type</label>
                                <div class="col-md-6 col-sm-10 col-xs-12">
                                    <select class="loanType form-control required" tabindex="-1" id="leave_type" name="leave_type">
                                        <option value="">Select leave type</option>
                                        <option value="Medical Leave">Medical Leave</option>
                                        <option value="Paid Leave">Paid Leave</option>
                                        <option value="Others">Others</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label  class="col-sm-2 control-label">Leave Days</label>
                                <div class="col-sm-10">
                                    <input type="number" class="form-control required"
                                           id="leave_days" name="leave_days" placeholder="Enter total leave days" required>
                                </div>
                            </div>


                            <div class="form-group">
                                <div class="col-sm-2 col-sm-10">
                                    <button type="button" id="save-btn-employeeLeave"   data-token="{{ csrf_token() }}"  class="btn btn-primary">Add</button>
                                </div>
                            </div>

                            <div class="form-group">

                                <div class="col-sm-10">
                                    <input type="hidden" class="form-control"
                                           id="id" name="id" placeholder="Enter Certification"/>
                                </div>
                            </div>

                            <div style="display:none" class="modal-footer" id="footer">
                                <button type="button" class="btn btn-default closeBtn"
                                        data-dismiss="modal">
                                    Close
                                </button>
                                <button type="button" class="btn btn-primary edit" id="edit-publicHoliday">
                                    Update
                                </button>
                            </div>

                        </form>
                    </div>

                    <meta name="_token" content="{!! csrf_token() !!}" />
                </div>
                <!-- Modal Footer -->
            </div>
        </div>
    </div>


@endsection
@section('script')

    <script>
        // validate a field on "blur" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':
        $('form')
            .on('blur', 'input[required], input.optional, select.required', validator.checkField)
            .on('change', 'select.required', validator.checkField)
            .on('keypress', 'input[required][pattern]', validator.keypress);

        $('.multi.required').on('keyup blur', 'input', function() {
            validator.checkField.apply($(this).siblings().last()[0]);
        });


        $('form').submit(function(e) {
            e.preventDefault();
            var submit = true;
            // evaluate the form using generic validaing
            if (!validator.checkAll($(this))) {
                submit = false;
            }

            if (submit)
                this.submit();
            return false;
        });
    </script>
    <script type="text/javascript">

        var handleDataTableButtons = function () {
            if ($("#datatable-buttons").length) {
                $("#datatable-buttons").DataTable({
                    dom: "Bfrtip",
                    buttons: [
                        {
                            extend: "copy",
                            className: "btn-sm"
                        },
                        {
                            extend: "csv",
                            className: "btn-sm"
                        },
                        {
                            extend: "excel",
                            className: "btn-sm"
                        },
                        {
                            extend: "pdfHtml5",
                            className: "btn-sm"
                        },
                        {
                            extend: "print",
                            className: "btn-sm"
                        },
                        {
                            extend: "colvis",
                            className: "btn-sm"
                        }
                    ],
                    responsive: false
                });
            }
        };

        TableManageButtons = function () {
            "use strict";
            return {
                init: function () {
                    handleDataTableButtons();
                }
            };
        }();

        $('#datatable').dataTable();

        $('#datatable-keytable').DataTable({
            keys: true
        });

        $('#datatable-responsive').DataTable();

        $('#datatable-scroller').DataTable({
            ajax: "js/datatables/json/scroller-demo.json",
            deferRender: true,
            scrollY: 380,
            scrollCollapse: true,
            scroller: true
        });

        $('#datatable-fixed-header').DataTable({
            fixedHeader: true
        });

        var $datatable = $('#datatable-checkbox');

        $datatable.dataTable({
            'order': [[1, 'asc']],
            'columnDefs': [
                {orderable: false, targets: [0]}
            ]
        });
        $datatable.on('draw.dt', function () {
            $('input').iCheck({
                checkboxClass: 'icheckbox_flat-green'
            });
        });

        TableManageButtons.init();

    </script>

    <script>
        $(document).ready(function(){
            $(".nav-tabs a").click(function(){
                $(this).button('loading').delay(500).queue(function(){
                    $(this).button('reset');
                    $(this).dequeue();
                });
            });
        });
    </script>

    <script>

        function  myFunction() {
            $("#myModalHorizontal").modal('show');
            //$('#footer').hide();
        }
    </script>

    <script>
        $('.closeBtn').click(function(){
            $('#emp_name').val('');
            $('#leave_type').val('');
            $('#leave_days').val('');
            $('#footer').hide('');
            $('#save-btn-employeeLeave').show();
            $('input[type="text"],select.required').each(function () {
                if ($.trim($(this).val()) == '') {
                    $(this).css({
                        "border": "",
                        "background": ""
                    });
                }
            });
        });
    </script>

    <script>
        $("#save-btn-employeeLeave").click(function() {
            //  alert("hello");
            $.ajaxSetup ({
                // Disable caching of AJAX responses
                cache: false
            });
            $.ajax({
                type: 'get',
                url: 'add-employee-leave',
                data: {
                    '_token': $('input[name=_token]').val(),
                    'emp_name' : $('#emp_name').val(),
                    'leave_type': $('#leave_type').val(),
                    'leave_days': $('#leave_days').val()
                },
                success: function(data) {
                    console.log(data);

                    if ((data.errors)){
                        $('.error').removeClass('hidden');
                        $('.error').text(data.errors.name);
                    }
                    else {
                        $('.error').addClass('hidden');
                        $('.Holidays').append("<tr class='item" + data[0].id + "'><td>" + data[0].emp_leave.first_name + ' '+data[0].emp_leave.last_name+ "</td><td>" + data[0].leave_type + "</td><td>" + data[0].leave_days +  "</td><td align='center'><button class='edit-modal-public_holiday btn btn-info' data-id='" + data[0].id +"' data-emp_name='" + data[0].employee_id+ "' data-leave_type='" + data[0].leave_type + "'  data-leave_days='" + data[0].leave_days+ "'data-first_name='" +  data[0].emp_leave.first_name + "'data-middle_name='" + data[0].emp_leave.middle_name + "'data-last_name='" + data[0].emp_leave.last_name + "'> <span class='glyphicon glyphicon-edit'></span></button> <button class='delete-modal btn btn-danger delete_course' data-id='" + data[0].id + "' data-emp_name='" +  data[0].employee_id+ "' data-leave_type='" + data[0].leave_type + "'  data-leave_days='" + data[0].leave_days+ "'><span class='glyphicon glyphicon-trash'></span></button></td></tr>");
                        $("#myModalHorizontal").modal('hide');
                        $('#emp_name').val('');
                        $('#leave_type').val('');
                        $('#leave_days').val('');
                    }
                },
            });

        });

    </script>

    <script>
        $(document).on('click', '.edit-modal-public_holiday', function() {
            $('#id').val($(this).data('id'));
            $('#emp_name').val($(this).data('emp_name'));
            $('#leave_type').val($(this).data('leave_type'));
            $('#leave_days').val($(this).data('leave_days'));
            $('#myModalHorizontal').modal('show');
            $('#save-btn-employeeLeave').hide();
            $('#footer').css('display','block');
            $('#name').css({
                "border": "",
                "background": ""
            });

            $('#edit-publicHoliday').click(function(){

                $.ajax({
                    type: 'get',
                    url: './edit-employee-leave',
                    data: {
                        '_token': $('input[name=_token]').val(),
                        'id': $("#id").val(),
                        'emp_name' : $('#emp_name').val(),
                        'leave_type': $('#leave_type').val(),
                        'leave_days': $('#leave_days').val()
                    },
                    success: function(data) {
                        $('.item' + data[0].id).replaceWith("<tr class='item" + data[0].id + "'><td>" + data[0].emp_leave.first_name + ' '+data[0].emp_leave.last_name+ "</td><td>" + data[0].leave_type + "</td><td>" + data[0].leave_days +  "</td><td align='center'><button class='edit-modal-public_holiday btn btn-info' data-id='" + data[0].id +"' data-emp_name='" + data[0].employee_id+ "' data-leave_type='" + data[0].leave_type + "'  data-leave_days='" + data[0].leave_days+ "'data-first_name='" +  data[0].emp_leave.first_name + "'data-middle_name='" + data[0].emp_leave.middle_name + "'data-last_name='" + data[0].emp_leave.last_name + "'><span class='glyphicon glyphicon-edit'></span></button> <button class='delete-modal btn btn-danger delete_course'data-id='" + data[0].id + "' data-emp_name='" +  data[0].employee_id+ "' data-leave_type='" + data[0].leave_type + "'  data-leave_days='" + data[0].leave_days+ "'><span class='glyphicon glyphicon-trash'></span></button></td></tr>");
                        $('#myModalHorizontal').modal('hide');
                        $('#id').val($(this).data(''));
                        $('#emp_name').val($(this).data(''));
                        $('#leave_type').val($(this).data(''));
                        $('#leave_days').val($(this).data(''));

                        $('#save-btn-employeeLeave').show();
                        $('#footer').css('display','none');
                        //  $("#myModalHorizontal").modal('hide');
                    }
                });
            });
        });
    </script>
    <script>
        $(document).on("click", ".delete_course", function () {
            var did = $(this).data('id');
            $("#d_id").val(did);
            $('#myModal').modal('show');
        });
        $('.delete_holiday_btn').click(function () {
            var _id = $("#d_id").val();
            var id = _id;
            var url = "delete-employee-leave";
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            })
            $.ajax({
                type: 'DELETE',
                url: url + '/' + id,
                success: function (data) {
                    console.log(id);
                    $(".item" + id).remove();
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        })
    </script>

    <script>
        $("#save-btn-employeeLeave").click(function (e) {
            var isValid = true;
            $('input[type="text"].required').each(function () {
                if ($.trim($(this).val()) == '') {
                    isValid = false; /* Required class style */
                    $(this).css({
                        "border": "1px solid red",
                        "background": "#FFCECE"
                    });
                }
                else { /* Required class style removed */
                    $(this).css({
                        "border": "",
                        "background": ""
                    });
                }
            });
            if (isValid == false)
                e.preventDefault();
            else{
            }
        });
    </script>


    <script>
        $(".dateTime").datepicker( {
            format: "dd MM yyyy",
            autoclose: true
        });
    </script>
    <script type="text/javascript">

        $(".newTable").on("click", function () {
            var type = $(this).children('.fl').val();
            var id = $(this).children('.id').val();
            var url = "status_change";

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            });

            $.ajax({
                type: 'GET',
                url: url + '/' + id,
                success: function (data) {
                    console.log(data);

                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        });
    </script>



@endsection