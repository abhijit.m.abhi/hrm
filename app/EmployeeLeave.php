<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmployeeLeave extends Model
{
    public function emp_leave()
    {

        return $this->belongsTo('App\EmployeePersonalDetail', 'employee_id');
    }
}
