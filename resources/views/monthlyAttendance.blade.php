@extends('layouts.master')
@section('title', 'Monthly Attendance')
@section('content')


    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    {{--<h3>User <!--<small>Some examples to get you started</small>--></h3>--}}
                </div>


            </div>

            <div class="clearfix"></div>

            <div class="row">

                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Monthly Attendance<!--<small>Users</small>--></h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        @if(Session::has('message'))

                            <div id="successMessage" class="alert alert-success alert-dismissible fade in" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                            aria-hidden="true">×</span>
                                </button>
                                <strong>{{ Session::get('message') }}</strong>
                            </div>
                        @endif
                        <div class="x_content">
                            <div class="table-responsive">
                                <form method="post" action="{{url('attendance/specific_month')}}"
                                      class="form-horizontal">
                                    {{ csrf_field() }}
                                    <div class="form-group">
                                        <div class="col-md-2 col-sm-2" style="margin-left: -9px !important;">
                                            <input type="text" name="datePicker"
                                                   class="form-control has-feedback-left date_pick" id=""
                                                   value="{{ $date }}"
                                                   aria-describedby="inputSuccess2Status3">
                                            {{--@if ($errors->has('dateOfBirth'))--}}
                                            {{--<span class="help-block"><strong>{{ $errors->first('dateOfBirth') }}</strong></span>--}}
                                            {{--@endif--}}
                                            <span class="fa fa-calendar form-control-feedback left"
                                                  aria-hidden="true"></span>
                                            <input type="hidden" name="id" value="{{$userId}}">
                                        </div>
                                        <div class="col-md-6 col-lg-6">
                                            <button type="submit" class="btn btn-primary">
                                                <span class="glyphicon glyphicon-search"></span> View
                                            </button>
                                        </div>
                                        <div>
                                            <p class="well well-sm pull-right alert-info">
                                                <strong>{!!'Month Total: '.'<span style="color:yellow; font-weight: bolder;  font-size: 120%;">'.$monthly_total_time.'</span>'.' hour'!!}</strong>
                                            </p>
                                        </div>
                                    </div>
                                </form>


                                <table id="datatable-buttons"
                                       class="table table-striped table-bordered table-hover display">
                                    <thead>
                                    <tr class="alert-info">
                                        <th>Date</th>
                                        <th>First Check In</th>
                                        <th>Early Entry</th>
                                        <th>Late Entry</th>
                                        <th>Last Check Out</th>
                                        <th>Early Leave</th>
                                        <th>Overtime</th>
                                        <th>Total Working Hour</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @for($i = 0 ; $i < $rowCount; $i++)
                                        <tr>
                                            @foreach($collection as $collect)
                                                <td>{{$collect[$i]}}</td>
                                            @endforeach
                                        </tr>
                                    @endfor
                                    </tbody>
                                </table>


                            </div>

                        </div>

                        <meta name="_token" content="{!! csrf_token() !!}"/>

                    </div>
                </div>

            </div>
        </div>
    </div>


@endsection
@section('script')

    {{--Flash Message--}}
    <script>
        setTimeout(function () {
            $("#successMessage").fadeOut('slow');
        }, 3000);

    </script>

    {{--TimePicker--}}
    <script type="text/javascript">

        $('.datetimepicker3').datetimepicker({
            format: 'HH:mm:ss'
        });

    </script>


    <script>

        $(".date_pick").datepicker({
            format: "MM yyyy",
            viewMode: "months",
            minViewMode: "months",
            autoclose: true
        });

    </script>


    {{--DataTable--}}
    <script type="text/javascript">

        var handleDataTableButtons = function () {
            if ($("#datatable-buttons").length) {
                $("#datatable-buttons").DataTable({
                    dom: "Bfrtip",
                    buttons: [
                        {
                            extend: "copy",
                            className: "btn-sm"
                        },
                        {
                            extend: "csv",
                            className: "btn-sm"
                        },
                        {
                            extend: "excel",
                            className: "btn-sm"
                        },
                        {
                            extend: "pdfHtml5",
                            className: "btn-sm"
                        },
                        {
                            extend: "print",
                            className: "btn-sm"
                        },
                        {
                            extend: "colvis",
                            className: "btn-sm"
                        }
                    ],
                    responsive: false,
                    'bPaginate': false

                });
            }
        };

        TableManageButtons = function () {
            "use strict";
            return {
                init: function () {
                    handleDataTableButtons();
                }
            };
        }();

        $('#datatable').dataTable();

        $('#datatable-keytable').DataTable({
            keys: true
        });

        $('#datatable-responsive').DataTable();

        $('#datatable-scroller').DataTable({
            ajax: "js/datatables/json/scroller-demo.json",
            deferRender: true,
            scrollY: 380,
            scrollCollapse: true,
            scroller: true
        });

        $('#datatable-fixed-header').DataTable({
            fixedHeader: true
        });

        var $datatable = $('#datatable-checkbox');

        $datatable.dataTable({
            'order': [[1, 'asc']],
            'columnDefs': [
                {orderable: false, targets: [0]}
            ]
        });
        $datatable.on('draw.dt', function () {
            $('input').iCheck({
                checkboxClass: 'icheckbox_flat-green'
            });
        });

        TableManageButtons.init();

    </script>


@endsection