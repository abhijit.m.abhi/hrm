@extends('layouts.master')
@section('title', 'Loan Setup')
@section('style')
    <style>
        .calendar-table
        {

        }
        .select2{
            width: 100% !important;
        }

    </style>
@endsection
@section('content')

    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>Training Setup</h3>
                </div>


            </div>
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Add Loan<!--<small>Input different form elements</small>--></h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                            @if(Session::has('message'))

                                <div id="successMessage" class="alert alert-success alert-dismissible fade in" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                                aria-hidden="true">×</span>
                                    </button>
                                    <strong>{{ Session::get('message') }}</strong>
                                </div>
                            @endif
                        </div>
                        {{--tab view--}}
                        <div class="bs-example">
                            <div class="tabbable">
                                <ul class="nav nav-tabs">
                                    <li class="active"> <input type="button" data-toggle="tab" href="#tab1" data-loading-text="Loading..." class="btn btn-info" value="Loan"></li>
                                    <li> <input type="button" data-toggle="tab" href="#tab2" data-loading-text="Loading..." class="btn btn-info" value="  Employee Loan    "></li>

                                </ul>
                                <div class="tab-content">
                                    <div id="tab1" class="tab-pane active fade in">
                                        {{--fade view for add skill--}}
                                        <button onclick="myFunction()" data-backdrop="ture" id="hello" class="btn btn-primary btn-md" data-type="Skills"  data-token="{{ csrf_token() }}"  data-toggle="modal" data-target="#myModalHorizontalkk"><span class="glyphicon glyphicon-plus">
                                                    Add Loan
                                                </span></button>


                                        <div class="x_content">
                                            <div class="table-responsive">
                                                <table id="datatable-buttons" class="Loans table table-striped table-bordered table-hover display">
                                                    <thead>
                                                    <tr class="alert-info">
                                                        <th>Loan Name</th>
                                                        <th>Details</th>
                                                        <th>Action</th>
                                                    </tr>

                                                    </thead>


                                                    <tbody>
                                                    @foreach($loans as $loan)
                                                        <tr class='item{{$loan->id}}' id="row_id{{$loan->id}}">
                                                            <td>{{$loan->name}}</td>
                                                            <td>{{$loan->details}}</td>


                                                            <td align="center">
                                                                <button class="btn btn-primary btn-sm edit-modal-loan" data-id="{{$loan->id}}" data-name="{{$loan->name}}" data-description="{{$loan->details}}" ><span class="glyphicon glyphicon-edit"></span></button>
                                                                <button class="btn confirm btn-danger btn-sm delete_course"  data-id="{{$loan->id}}" data-name="{{$loan->name}}" data-description="{{$loan->details}}" ><span class="glyphicon glyphicon-trash"></span></button>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            {{--Modal--}}
                                            <!-- /.modal -->
                                            </div>
                                        </div>
                                        <meta name="_token" content="{!! csrf_token() !!}"/>
                                        {{--end table view--}}
                                    </div>

                                    <div id="tab2" class="tab-pane fade">
                                        <button onclick="myEmployee()" class="btn btn-primary btn-md" data-toggle="modal"  data-token="{{ csrf_token() }}"  data-type="Certification" data-target=""><span class="glyphicon glyphicon-plus">
                                                    Add Employee Loan
                                                </span></button>
                                        {{--table--}}
                                        <div class="x_content">
                                            <div class="table-responsive">
                                                <table id="datatable-buttons" class="table-class EmployeeLoan table table-striped table-bordered table-hover display">
                                                    <thead>
                                                    <tr class="alert-info">
                                                        <th>Employee</th>
                                                        <th>Loan Type</th>
                                                        <th>Start Date</th>
                                                        <th>Last Installment Date</th>
                                                        <th>Period</th>
                                                        <th>Amount</th>
                                                        <th>Installment</th>
                                                        <th>Paid</th>
                                                        <th>Status</th>
                                                        <th>Details</th>
                                                        <th>Action</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @foreach($empLoans as $empLoan)
                                                        <tr class='item{{$empLoan->id}}' id="row_id{{$empLoan->id}}">
                                                            <td>{{$empLoan->emp_loan->first_name}} {{$empLoan->emp_loan->middle_name}} {{$empLoan->emp_loan->last_name}}</td>
                                                            <td>{{$empLoan->loan_type}}</td>
                                                            @php $start_date = date("d F Y", strtotime($empLoan->start_date)) @endphp
                                                            <td>{{$start_date}}</td>
                                                            @php $installment_date = date("d F Y", strtotime($empLoan->last_installment_date)) @endphp
                                                            <td>{{$installment_date}}</td>
                                                            <td>{{$empLoan->loan_period}}</td>
                                                            <td>{{$empLoan->loan_amount}}</td>
                                                            <td>{{$empLoan->monthly_installment}}</td>
                                                            <td>{{$empLoan->paid_amount}}</td>
                                                            {{--<td>{{$empLoan->status}}</td>--}}

                                                            <td><span class="newTable">@if($empLoan->status == "Active")
                                                                        <input type="text" class="fl" value="Active"
                                                                               style="display: none">
                                                                        <input type="text" class="id" value="{{$empLoan->id}}"
                                                                               style="display: none">
                                                                        <input class="change_type" type="checkbox" checked
                                                                               data-toggle="toggle" data-onstyle="success"
                                                                               data-offstyle="danger" data-on="Active"
                                                                               data-off="Deactive">
                                                                    @elseif($empLoan->status == "Deactive")
                                                                        <input type="text" class="fl" value="Deactive"
                                                                               style="display: none">
                                                                        <input type="text" class="id" value="{{$empLoan->id}}"
                                                                               style="display: none">
                                                                        <input class="change_type" type="checkbox" data-toggle="toggle"
                                                                               data-onstyle="success" data-offstyle="danger"
                                                                               data-on="Active" data-off="Deactive">
                                                                    </span>
                                                                @endif


                                                            </td>

                                                            <td>{{$empLoan->details}}</td>
                                                            <td align="center">
                                                                <button class="btn btn-primary btn-sm edit-modal-empTrain"   data-id="{{$empLoan->id}}" data-employee_id="{{$empLoan->employee_id}}" data-loan_type="{{$empLoan->loan_type}}" data-loan_start_date="{{date("d F Y", strtotime($empLoan->start_date))}}"  data-last_installment_date="{{date("d F Y", strtotime($empLoan->last_installment_date))}}"data-loan_period="{{$empLoan->loan_period}}"data-loan_amount="{{$empLoan->loan_amount}}"data-monthly_installment="{{$empLoan->monthly_installment}}"data-paid_amount="{{$empLoan->paid_amount}}"data-status="{{$empLoan->status}}"data-details="{{$empLoan->details}}" data-last_name="{{$empLoan->emp_loan->first_name}}" data-middle_name="{{$empLoan->emp_loan->middle_name}}" data-last_name="{{$empLoan->emp_loan->last_name}}" ><span class="glyphicon glyphicon-edit"></span></button>
                                                                <button class="btn confirm btn-danger btn-sm delete_empLoan"  data-id="{{$empLoan->id}}" data-loan_type="{{$empLoan->loan_type}}" data-loan_start_date="{{$empLoan->start_date}}" data-last_installment_date="{{$empLoan->last_installment_date}}"data-loan_period="{{$empLoan->loan_period}}"data-loan_amount="{{$empLoan->loan_amount}}"data-monthly_installment="{{$empLoan->monthly_installment}}"data-paid_amount="{{$empLoan->paid_amount}}"data-status="{{$empLoan->status}}"data-details="{{$empLoan->details}}" ><span class="glyphicon glyphicon-trash"></span></button>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <meta name="_token" content="{!! csrf_token() !!}"/>
                                        {{--end table--}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



    {{--delete modal--}}

    <div class="modal fade" id="myModal" aria-labelledby="myModalLabel" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header alert-danger">
                    <button type="button" class="close" data-dismiss="modal"
                            aria-label="Close"><span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title"><span class="glyphicon glyphicon-trash"></span>
                        Confirmation Message</h4>
                </div>
                <div class="modal-body">
                    <p>Are you sure that you want to permanently delete the selected
                        element?</p>
                    <input type="hidden" id="d_id">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">No
                    </button>
                    <button type="button" class="btn btn-danger delete_course_btn delete_empLoan_btn"
                            data-dismiss="modal" value=""
                            data-token="{{ csrf_token() }}" data-dismiss="modal">Delete
                    </button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    {{--end delete modal--}}


    {{--add form modal --}}
    <div class="modal fade open_modal" id="myModalHorizontal" tabindex="-1" role="dialog"
         aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header">
                    <button type="button" class="close closeBtn"
                            data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                        <span class="sr-only">Close</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">
                        Add Loan Form
                    </h4>
                </div>

                <!-- Modal Body -->
                <div class="modal-body">
                    <div class="modal-body">

                        <form id="form" class="form-horizontal" role="form" data-parsley-validate>
                            <div class="form-group">
                                <label  class="col-sm-2 control-label">Loan Name</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control required"
                                           id="name" name="name" placeholder="Enter Name" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label">Description</label>
                                <div class="col-sm-10">
                                    <textarea class="form-control" rows="5" id="description" name="description" required></textarea>

                                </div>
                            </div>


                            <div class="form-group">
                                <div class="col-sm-2 col-sm-10">
                                    <button type="button" id="save-btn-loan"   data-token="{{ csrf_token() }}"  class="btn btn-primary">Add</button>
                                </div>
                            </div>

                            <div class="form-group">

                                <div class="col-sm-10">
                                    <input type="hidden" class="form-control"
                                           id="id" name="id" placeholder="Enter Certification"/>
                                </div>
                            </div>

                            <div style="display:none" class="modal-footer" id="footer">
                                <button type="button" class="btn btn-default closeBtn"
                                        data-dismiss="modal">
                                    Close
                                </button>
                                <button type="button" class="btn btn-primary edit" id="edit-course">
                                    Update
                                </button>
                            </div>

                        </form>
                    </div>

                    <meta name="_token" content="{!! csrf_token() !!}" />
                </div>
                <!-- Modal Footer -->
            </div>
        </div>
    </div>


    {{--empolyee training session modal--}}

    <div class="modal fade open_modal" id="employeeLoanModal" tabindex="-1" role="dialog"
         aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header">
                    <button type="button" class="close closeT"
                            data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                        <span class="sr-only">Close</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">
                        Add Employee Loan Form
                    </h4>
                </div>

                <!-- Modal Body -->
                <div class="modal-body">
                    <div class="modal-body">

                        <form id="form" class="form-horizontal" role="form">


                            <div class="form-group">
                                <label class="control-label col-md-4 col-sm-4 col-xs-12">Employee Name</label>
                                <div class="col-md-6 col-sm-10 col-xs-12">
                                    <select class="employeeName form-control required" tabindex="-1" id="employee_id" name="employee_id">
                                        <option value="">Select Employee</option>

                                        @foreach($employee as $emp)
                                            <option value="{{$emp->id}}">{{$emp->first_name}} {{$emp->middle_name}} {{$emp->last_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-4 col-sm-4 col-xs-12">Loan Type</label>
                                <div class="col-md-6 col-sm-10 col-xs-12">
                                    <select class="loanType form-control required" tabindex="-1" id="loan_type" name="loan_type">
                                        <option value="">Select Loan Type</option>
                                        @foreach($loans as $loan)
                                            <option value="{{$loan->name}}">{{$loan->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-4 col-sm-4 col-xs-12">Loan Start Date</label>
                                <div class="col-md-6 col-sm-10 col-xs-12">
                                    <input type="text" class="form-control has-feedback-left dateTime" name="loan_start_date" id="loan_start_date" placeholder="Select Month" aria-describedby="inputSuccess2Status2" required>
                                    <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                                    <span id="inputSuccess2Status2" class="sr-only">(success)</span>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-4 col-sm-4 col-xs-12">Last Installment Date</label>
                                <div class="col-md-6 col-sm-10 col-xs-12">
                                    <input type="text" class="form-control has-feedback-left dateTime" name="last_installment_date" id="last_installment_date" placeholder="Select Month" aria-describedby="inputSuccess2Status2" required>
                                    <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                                    <span id="inputSuccess2Status2" class="sr-only">(success)</span>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-4 col-sm-4 col-xs-12">Loan Period</label>
                                <div class="col-md-6 col-sm-10 col-xs-12">
                                    <input type="number" class="form-control required"
                                           onkeyup="totalPaidAmount()" id="loan_period" name="loan_period" placeholder="Enter Loan Period">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-4 col-sm-4 col-xs-12">Loan Amount</label>
                                <div class="col-md-6 col-sm-10 col-xs-12">
                                    <input type="number" class="form-control required"
                                           id="loan_amount" name="loan_amount" placeholder="Enter Loan Amount">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-4 col-sm-4 col-xs-12">Monthly Installment</label>
                                <div class="col-md-6 col-sm-10 col-xs-12">
                                    <input type="number" class="form-control required"
                                           onkeyup="totalPaidAmount()" id="monthly_installment" name="monthly_installment" placeholder="Enter Monthly Installment">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-4 col-sm-4 col-xs-12">Paid Amount</label>
                                <div class="col-md-6 col-sm-10 col-xs-12">
                                    <input type="number" class="form-control required"
                                           id="paid_amount" name="paid_amount" readonly>
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="control-label col-md-4 col-sm-4 col-xs-12">Status</label>
                                <div class="col-md-6 col-sm-10 col-xs-12">
                                    <select class="form-control loan required" tabindex="-1" id="status" name="status">
                                        <option label="Option Group">Select Status</option>
                                        <option value="Active">Active</option>
                                        <option value="Deactive">Deactive</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-4 col-sm-4 col-xs-12">Details</label>
                                <div class="col-md-6 col-sm-10 col-xs-12">
                                    <textarea class="form-control" rows="5" id="details" name="details" required></textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-2 col-sm-10">
                                    <button type="button" id="save-btn-employee"   data-token="{{ csrf_token() }}"  class="btn btn-primary">Add</button>
                                </div>
                            </div>

                            <div class="form-group">

                                <div class="col-sm-10">
                                    <input type="hidden" class="form-control"
                                           id="id" name="id" placeholder="Enter Certification"/>
                                </div>
                            </div>

                            <div style="display:none" class="modal-footer" id="footer-emp">
                                <button type="button" class="btn btn-default closeT"
                                        data-dismiss="modal">
                                    Close
                                </button>
                                <button type="button" class="btn btn-primary edit" id="edit-empTraining">
                                    Update
                                </button>
                            </div>

                        </form>
                    </div>

                    <meta name="_token" content="{!! csrf_token() !!}" />
                </div>

                <!-- Modal Footer -->

            </div>

        </div>
    </div>

@endsection
@section('script')

    <script>
        // validate a field on "blur" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':
        $('form')
            .on('blur', 'input[required], input.optional, select.required', validator.checkField)
            .on('change', 'select.required', validator.checkField)
            .on('keypress', 'input[required][pattern]', validator.keypress);

        $('.multi.required').on('keyup blur', 'input', function() {
            validator.checkField.apply($(this).siblings().last()[0]);
        });


        $('form').submit(function(e) {
            e.preventDefault();
            var submit = true;
            // evaluate the form using generic validaing
            if (!validator.checkAll($(this))) {
                submit = false;
            }

            if (submit)
                this.submit();
            return false;
        });
    </script>
    <script type="text/javascript">

        var handleDataTableButtons = function () {
            if ($("#datatable-buttons").length) {
                $("#datatable-buttons").DataTable({
                    dom: "Bfrtip",
                    buttons: [
                        {
                            extend: "copy",
                            className: "btn-sm"
                        },
                        {
                            extend: "csv",
                            className: "btn-sm"
                        },
                        {
                            extend: "excel",
                            className: "btn-sm"
                        },
                        {
                            extend: "pdfHtml5",
                            className: "btn-sm"
                        },
                        {
                            extend: "print",
                            className: "btn-sm"
                        },
                        {
                            extend: "colvis",
                            className: "btn-sm"
                        }
                    ],
                    responsive: false
                });
            }
        };

        TableManageButtons = function () {
            "use strict";
            return {
                init: function () {
                    handleDataTableButtons();
                }
            };
        }();

        $('#datatable').dataTable();

        $('#datatable-keytable').DataTable({
            keys: true
        });

        $('#datatable-responsive').DataTable();

        $('#datatable-scroller').DataTable({
            ajax: "js/datatables/json/scroller-demo.json",
            deferRender: true,
            scrollY: 380,
            scrollCollapse: true,
            scroller: true
        });

        $('#datatable-fixed-header').DataTable({
            fixedHeader: true
        });

        var $datatable = $('#datatable-checkbox');

        $datatable.dataTable({
            'order': [[1, 'asc']],
            'columnDefs': [
                {orderable: false, targets: [0]}
            ]
        });
        $datatable.on('draw.dt', function () {
            $('input').iCheck({
                checkboxClass: 'icheckbox_flat-green'
            });
        });

        TableManageButtons.init();

    </script>

    <script>
        $(document).ready(function(){
            $(".nav-tabs a").click(function(){
                $(this).button('loading').delay(500).queue(function(){
                    $(this).button('reset');
                    $(this).dequeue();
                });
            });
        });
    </script>

    <script type="text/javascript">
        function totalPaidAmount() {
            var period = $("#loan_period").val();
            var installment = $("#monthly_installment").val();

            var amount = (period * installment).toFixed(2);
            $("#paid_amount").val(amount);

        }
    </script>
    <script>
        $(".employeeName").select2();
        $(".loanType").select2();
    </script>


    <script>

        function  myFunction() {
            $("#myModalHorizontal").modal('show');
            //$('#footer').hide();
        }
    </script>

    <script>
        $('.closeBtn').click(function(){
            $('#name').val('');
            $('#description').val('');
            $('#footer').hide('');
            $('#save-btn-loan').show();
            $('input[type="text"],select.required').each(function () {
                if ($.trim($(this).val()) == '') {
                    $(this).css({
                        "border": "",
                        "background": ""
                    });
                }
            });
        });
    </script>

    <script>
        $("#save-btn-loan").click(function() {
            //  alert("hello");
            $.ajaxSetup ({
                // Disable caching of AJAX responses
                cache: false
            });
            $.ajax({
                type: 'get',
                url: 'addLoan',
                data: {
                    '_token': $('input[name=_token]').val(),
                    'name' : $('#name').val(),
                    'description': $('#description').val()
                },
                success: function(data) {
                    console.log(data);

                    if ((data.errors)){
                        $('.error').removeClass('hidden');
                        $('.error').text(data.errors.name);
                    }
                    else {
                        $('.error').addClass('hidden');
                        $('.Loans').append("<tr class='item" + data.id + "'><td>" + data.name + "</td><td>" + data.details+ "</td><td align='center'><button class='edit-modal-loan btn btn-info' data-id='" + data.id + "' data-name='" + data.name + "'  data-description='" + data.details+ "' ><span class='glyphicon glyphicon-edit'></span></button> <button class='delete-modal btn btn-danger delete_course' data-id='" + data.id + "' data-name='" + data.name + "'  data-description='" + data.description+ "' ><span class='glyphicon glyphicon-trash'></span></button></td></tr>");
                        $("#myModalHorizontal").modal('hide');
                        $('#name').val('');
                        $('#description').val('');

                    }
                },
            });

        });

    </script>

    <script>

        $(document).on('click', '.edit-modal-loan', function() {
            $('#id').val($(this).data('id'));
            $('#name').val($(this).data('name'));
            $('#description').val($(this).data('description'));
            $('#myModalHorizontal').modal('show');
            $('#save-btn-loan').hide();
            $('#footer').css('display','block');
            $('#name').css({
                "border": "",
                "background": ""
            });

            $('#edit-course').click(function(){


                $.ajax({

                    type: 'get',
                    url: './editLoan',
                    data: {
                        '_token': $('input[name=_token]').val(),
                        'id': $("#id").val(),
                        'name' : $('#name').val(),
                        'description': $('#description').val()
                    },
                    success: function(data) {
                        $('.item' + data.id).replaceWith("<tr class='item" + data.id + "'><td>" + data.name+ "</td><td>" + data.details+ "</td><td align='center'><button class='edit-modal-loan btn btn-info' data-id='" + data.id + "' data-name='" + data.name+ "'  data-description='" + data.details+ "' ><span class='glyphicon glyphicon-edit'></span></button> <button class='delete-modal btn btn-danger delete_course'data-id='" + data.id + "' data-name='" + data.name + "' data-description='" + data.description + "' ><span class='glyphicon glyphicon-trash'></span></button></td></tr>");
                        $('#myModalHorizontal').modal('hide');
                        $('#id').val($(this).data(''));
                        $('#name').val($(this).data(''));
                        $('#description').val($(this).data(''));

                        $('#save-btn-loan').show();
                        $('#footer').css('display','none');
                        //  $("#myModalHorizontal").modal('hide');
                    }
                });
            });
        });
    </script>
    <script>
        $(document).on("click", ".delete_course", function () {
            var did = $(this).data('id');
            $("#d_id").val(did);
            $('#myModal').modal('show');
        });
        $('.delete_course_btn').click(function () {
            var _id = $("#d_id").val();
            var id = _id;
            var url = "deleteLoan";
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            })
            $.ajax({
                type: 'DELETE',
                url: url + '/' + id,
                success: function (data) {
                    console.log(id);
                    $(".item" + id).remove();
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        })
    </script>

    <script>
        $("#save-btn-loan").click(function (e) {
            var isValid = true;
            $('input[type="text"].required').each(function () {
                if ($.trim($(this).val()) == '') {
                    isValid = false; /* Required class style */
                    $(this).css({
                        "border": "1px solid red",
                        "background": "#FFCECE"
                    });
                }
                else { /* Required class style removed */
                    $(this).css({
                        "border": "",
                        "background": ""
                    });
                }
            });
            if (isValid == false)
                e.preventDefault();
            else{

            }

        });
    </script>




    {{--start employee training session--}}
    <script>
        function myEmployee() {

            $('#employeeLoanModal').modal('show');

        }
    </script>

    <script>
        $('.closeT').click(function(){

            $('#employee_id').val('');
            $('#loan_type').val('');
            $('#loan_start_date').val('');
            $('#last_installment_date').val('');
            $('#loan_period').val('');
            $('#loan_amount').val('');
            $('#monthly_installment').val('');
            $('#paid_amount').val('');
            $('#status').val('');
            $('#details').val('');
            $('#footer-emp').hide('');
            $('#save-btn-employee').show();


            $('select.required').each(function () {
                if ($.trim($(this).val()) == '') {

                    $(this).css({
                        "border": "",
                        "background": ""
                    });
                }

            });

        });
    </script>
    <script>
        $("#save-btn-employee").click(function() {

            $.ajaxSetup ({
                // Disable caching of AJAX responses
                cache: false
            });
            $.ajax({
                type: 'get',
                url: 'addEmployeeLoan',
                data: {
                    '_token': $('input[name=_token]').val(),
                    'employee_id': $('#employee_id').val(),
                    'loan_type': $('#loan_type').val(),
                    'loan_start_date' : $('#loan_start_date').val(),
                    'last_installment_date' : $('#last_installment_date').val(),
                    'loan_period' : $('#loan_period').val(),
                    'loan_amount' : $('#loan_amount').val(),
                    'monthly_installment' : $('#monthly_installment').val(),
                    'paid_amount' : $('#paid_amount').val(),
                    'status': $('#status').val(),
                    'details': $('#details').val()


                },
                success: function(data) {
                    console.log(data);


                    if ((data.errors)){
                        $('.error').removeClass('hidden');
                        $('.error').text(data.errors.name);
                    }
                    else {
                        $('.error').addClass('hidden');


                        $('.EmployeeLoan').append("<tr class='item" + data[0].id + "'><td>" + data[0].emp_loan.first_name + ' '+data[0].emp_loan.last_name+ "</td><td>" + data[0].loan_type + "</td><td>" + data[0].start_date + "</td><td>" + data[0].last_installment_date +  "</td><td>" + data[0].loan_period +  "</td><td>" + data[0].loan_amount +  "</td><td>" + data[0].monthly_installment +  "</td><td>" + data[0].paid_amount +  "</td><td>" + data[0].status +  "</td><td>" + data[0].details +  "</td><td align='center'><button class='edit-modal-empTrain btn btn-info' data-id='" + data[0].id + "' data-employee_id='" + data[0].employee_id+ "' data-loan_type='" + data[0].loan_type + "'  data-loan_start_date='" + data[0].start_date+"'  data-last_installment_date='" + data[0].last_installment_date+"'  data-loan_period='" + data[0].loan_period+"'  data-loan_amount='" + data[0].loan_amount+"'  data-monthly_installment='" + data[0].monthly_installment+"'  data-paid_amount='" + data[0].paid_amount+"'  data-status='" + data[0].status+"'  data-details='" + data[0].details+ "'data-first_name='" + data[0].emp_loan.first_name + "'data-middle_name='" + data[0].emp_loan.middle_name + "'data-last_name='" + data[0].emp_loan.last_name + "'><span class='glyphicon glyphicon-edit'></span></button> <button class='delete-modal btn btn-danger delete_empLoan' data-id='" + data[0].id + "' data-employee_id='" +  data[0].employee_id+ "' data-loan_type='" + data.loan_type + "'  data-loan_start_date='" + data.loan_start_date+ "'data-last_installment_date='" + data.last_installment_date+ "'data-loan_period='" + data.loan_period+ "'data-loan_amount='" + data.loan_amount+ "'data-monthly_installment='" + data.monthly_installment+"'data-paid_amount='" + data.paid_amount+"'data-status='" + data.status+"'data-details='" + data.details+ "' ><span class='glyphicon glyphicon-trash'></span></button></td></tr>");
                        $("#employeeLoanModal").modal('hide');

                    }
                },
            });

        });

    </script>

    <script>
        $(document).on("click", ".delete_empLoan", function () {
            var did = $(this).data('id');
            $("#d_id").val(did);
            $('#myModal').modal('show');
        });

        $('.delete_empLoan_btn').click(function () {
            var _id = $("#d_id").val();
            var id = _id;
            var url = "delete_EmployeeLoan";
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            })
            $.ajax({
                type: 'DELETE',
                url: url + '/' + id,
                success: function (data) {
                    console.log(id);
                    $(".item" + id).remove();
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });

        })
    </script>


    <script>

        $(document).on('click', '.edit-modal-empTrain', function() {

            $('#id').val($(this).data('id'));
            $('#employee_id').val($(this).data('employee_id'));
            $('#loan_type').val($(this).data('loan_type'));
            $('#loan_start_date').val($(this).data('loan_start_date'));
            $('#last_installment_date').val($(this).data('last_installment_date'));
            $('#loan_period').val($(this).data('loan_period'));
            $('#loan_amount').val($(this).data('loan_amount'));
            $('#monthly_installment').val($(this).data('monthly_installment'));
            $('#paid_amount').val($(this).data('paid_amount'));
            $('#status').val($(this).data('status'));
            $('#details').val($(this).data('details'));

            $('#employeeLoanModal').modal('show');
            $('#save-btn-employee').hide();
            $('#footer-emp').css('display','block');
            $('#name').css({
                "border": "",
                "background": ""
            });

            $('#edit-empTraining').click(function(){
                $.ajax({
                    type: 'get',
                    url: './editEmployeeLoan',
                    data: {
                        '_token': $('input[name=_token]').val(),
                        'id': $("#id").val(),
                        'employee_id': $('#employee_id').val(),
                        'loan_type': $('#loan_type').val(),
                        'loan_start_date' : $('#loan_start_date').val(),
                        'last_installment_date' : $('#last_installment_date').val(),
                        'loan_period' : $('#loan_period').val(),
                        'loan_amount' : $('#loan_amount').val(),
                        'monthly_installment' : $('#monthly_installment').val(),
                        'paid_amount' : $('#paid_amount').val(),
                        'status': $('#status').val(),
                        'details': $('#details').val()

                    },
                    success: function(data) {

                        $('.item' + data[0].id).replaceWith("<tr class='item" + data[0].id + "'><td>" + data[0].emp_loan.first_name + ' '+data[0].emp_loan.last_name+ "</td><td>" + data[0].loan_type + "</td><td>" + data[0].start_date +  "</td><td>" + data[0].last_installment_date +  "</td><td>" + data[0].loan_period +  "</td><td>" + data[0].loan_amount +  "</td><td>" + data[0].monthly_installment +  "</td><td>" + data[0].paid_amount +  "</td><td>" + data[0].status +  "</td><td>" + data[0].details +  "</td><td align='center'><button class='edit-modal-empTrain btn btn-info' data-id='" + data[0].id + "' data-employee_id='" + data[0].employee_id+ "' data-loan_type='" + data[0].loan_type + "'  data-loan_start_date='" + data[0].loan_start_date+"'  data-last_installment_date='" + data[0].last_installment_date+"'  data-loan_period='" + data[0].loan_loan_period+"'  data-loan_amount='" + data[0].loan_amount+"'  data-monthly_installment='" + data[0].monthly_installment+"'  data-paid_amount='" + data[0].paid_amount+"'  data-status='" + data[0].status+"'  data-details='" + data[0].details+ "'data-first_name='" + data[0].emp_loan.first_name + "'data-middle_name='" + data[0].emp_loan.middle_name + "'data-last_name='" + data[0].emp_loan.last_name + "' ><span class='glyphicon glyphicon-edit'></span></button> <button class='delete-modal btn btn-danger delete_course' data-id='" + data[0].id + "' data-employee_id='" +  data[0].employee_id+ "' data-loan_type='" + data.loan_type + "'  data-loan_start_date='" + data.loan_start_date+ "'data-last_installment_date='" + data.last_installment_date+ "'data-loan_period='" + data.loan_period+ "'data-loan_amount='" + data.loan_amount+ "'data-monthly_installment='" + data.monthly_installment+"'data-paid_amount='" + data.paid_amount+"'data-status='" + data.status+"'data-details='" + data.details+ "' ><span class='glyphicon glyphicon-trash'></span></button></td></tr>");
                        $('#employeeLoanModal').modal('hide');
                        $('#id').val($(this).data(''));


                        $('#status').val('');
                        $('#footer-emp').hide('');
                        $('#save-btn-employee').show();
                    }
                });

            });
        });
    </script>

    <script>
        $("#save-btn-employee").click(function (e) {
            var isValid = true;
            $('select.required').each(function () {
                if ($.trim($(this).val()) == '') {
                    isValid = false; /* Required class style */
                    $(this).css({
                        "border": "1px solid red",
                        "background": "#FFCECE"
                    });
                }
                else { /* Required class style removed */
                    $(this).css({
                        "border": "",
                        "background": ""
                    });
                }
            });
            if (isValid == false)
                e.preventDefault();
            else{

            }


        });
    </script>
    <script>
        $(".dateTime").datepicker( {
            format: "dd MM yyyy",
            autoclose: true
        });
    </script>
    <script type="text/javascript">

        $(".newTable").on("click", function () {

            var type = $(this).children('.fl').val();
            var id = $(this).children('.id').val();

            var url = "status_change";

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            });


            $.ajax({
                type: 'GET',
                url: url + '/' + id,
                success: function (data) {
                    console.log(data);

                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });


        });

    </script>



@endsection