@php

    use Illuminate\Support\Facades\Auth;

    $user_id = Auth::user()->id;

    $get_emp_id = \App\EmployeePersonalDetail::select('id')->where('user_id', $user_id)->get();

    $emp_id = $get_emp_id[0]->id;

    $emp_attendance_today = \App\Attendance::select('date')->where('date', date('Y-m-d'))
                                                       ->where('employee_id', $emp_id)->get();


      $find_check_out = \App\Attendance::select('check_out')->where('employee_id', $emp_id)
            ->where('date', date('Y-m-d'))
            ->orderBy('id', 'desc')->first();

    if(count($emp_attendance_today) > 0)
    {
        //echo "Present Today";
        $present = true;
    }
    else
    {
        //echo "Abcent Today";
        $present = false;
    }


/*
    $emp_attendance_info = User::with(array(
    "emp_official_info" => function ($query) {
         $query->with(array("emp_attendance" => function ($query) {
         $query->where('date', '=', date('Y-m-d'));} ));
      }))->where('id', $user_id)->get();


*/

@endphp


        <!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>HRM | @yield('title') </title>
{{--    <link rel="shortcut icon" href="{!! asset('/assets/template/favicon_cart02.ico') !!}">--}}


<!-- Data Table Total Amount -->

    {{--<script src="http://code.jquery.com/jquery-1.12.4.js"></script>--}}
    {{--<script src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>--}}
    <script src="{{ asset('/assets/template/vendors/dataTableJS/jquery-1.12.4.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/assets/template/vendors/dataTableJS/jquery.dataTables.min.js') }}"
            type="text/javascript"></script>

    <!-- Data Table Total Amount JS -->
    <script src="{{ asset('/assets/template/vendors/totalAmount/tamount.js') }}" type="text/javascript"></script>

    {{--date time picker--}}
    <link rel="stylesheet"
          href="//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/a549aa8780dbda16f6cff545aeabc3d71073911e/build/css/bootstrap-datetimepicker.css">



    <!-- Bootstrap -->
    <link href="{{ asset('/assets/template/vendors/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet"
          type="text/css"/>
    <!-- Font Awesome -->
    <link href="{{ asset('/assets/template/vendors/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet"
          type="text/css"/>
    <!-- NProgress -->
    <link href="{{ asset('/assets/template/vendors/nprogress/nprogress.css') }}" rel="stylesheet" type="text/css"/>
    <!-- iCheck -->
    <link href="{{ asset('/assets/template/vendors/iCheck/skins/flat/green.css') }}" rel="stylesheet" type="text/css"/>
    <!-- Datatables -->
    <link href="{{ asset('/assets/template/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css') }}"
          rel="stylesheet" type="text/css"/>
    <link href="{{ asset('/assets/template/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css') }}"
          rel="stylesheet" type="text/css"/>
    <link href="{{ asset('/assets/template/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css') }}"
          rel="stylesheet" type="text/css"/>
    <link href="{{ asset('/assets/template/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css') }}"
          rel="stylesheet" type="text/css"/>
    <link href="{{ asset('/assets/template/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css') }}"
          rel="stylesheet" type="text/css"/>


    <!-- bootstrap-wysiwyg -->
    <link href="{{ asset('/assets/template/vendors/google-code-prettify/bin/prettify.min.css') }}" rel="stylesheet"
          type="text/css"/>
    <!-- Select2 -->
    <link href="{{ asset('/assets/template/vendors/select2/dist/css/select2.min.css') }}" rel="stylesheet"
          type="text/css"/>
    <!-- Switchery -->
    <link href="{{ asset('/assets/template/vendors/switchery/dist/switchery.min.css') }}" rel="stylesheet"
          type="text/css"/>
    <!-- starrr -->
    <link href="{{ asset('/assets/template/vendors/starrr/dist/starrr.css') }}" rel="stylesheet" type="text/css"/>
    <!-- bootstrap-progressbar -->
    <link href="{{ asset('/assets/template/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css') }}"
          rel="stylesheet" type="text/css"/>

    <!-- PNotify -->
    <link href="{{ asset('/assets/template/vendors/pnotify/dist/pnotify.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('/assets/template/vendors/pnotify/dist/pnotify.buttons.css') }}" rel="stylesheet"
          type="text/css"/>
    <link href="{{ asset('/assets/template/vendors/pnotify/dist/pnotify.nonblock.css') }}" rel="stylesheet"
          type="text/css"/>


    <!-- JQVMap -->
    <link href="{{ asset('/assets/template/vendors/jqvmap/dist/jqvmap.min.css') }}" rel="stylesheet" type="text/css"/>
    <!-- bootstrap-daterangepicker -->
    <link href="{{ asset('/assets/template/vendors/bootstrap-daterangepicker/daterangepicker.css') }}" rel="stylesheet"
          type="text/css"/>
    <!-- Custom Theme Style -->
    {{--<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css">--}}
    {{--<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.2.4/css/buttons.dataTables.min.css">--}}

    {{--bootstrap tab view --}}


    {{-- <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css">
     <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap-theme.min.css">
     <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">--}}

<!-- validator -->
    <link href="{{ asset('/assets/template/vendors/validator/validator.js') }}" rel="stylesheet" type="text/css"/>


    <!-- Dropzone.js -->
    <link href="{{ asset('/assets/template/vendors/dropzone/dist/min/dropzone.min.css') }}" rel="stylesheet"
          type="text/css"/>

    <link href="{{ asset('/assets/template/build/css/custom.min.css') }}" rel="stylesheet" type="text/css"/>
    <!-- month view date picker -->
    <link href="http://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css"
          rel="stylesheet">
    <link href="//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/build/css/bootstrap-datetimepicker.css"
          rel="stylesheet">

    <!-- data table css -->

    {{--BootStrap Data Toggle Checkbox --}}

    <link href="{{ asset('/assets/bootstrap-toggle-master/css/bootstrap-toggle.min.css') }}" rel="stylesheet"
          type="text/css"/>
    <script src="{{ asset('/assets/bootstrap-toggle-master/js/bootstrap-toggle.min.js') }}"
            type="text/javascript"></script>


    {{--<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">--}}
    {{--<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>--}}


    <Style>
        th {
            white-space: nowrap;
        }

        input[type=number] {
            -moz-appearance: textfield;
        }

        ::-webkit-inner-spin-button {
            -webkit-appearance: none;
        }

        ::-webkit-outer-spin-button {
            -webkit-appearance: none;
        }

        .nav a {
            outline: none;
        }

        .bs-example {
            margin: 20px;
        }

        .modal-body .form-horizontal .col-sm-2,
        .modal-body .form-horizontal .col-sm-10 {
            width: 100%
        }

        .modal-body .form-horizontal .control-label {
            text-align: left;
        }

        .modal-body .form-horizontal .col-sm-offset-2 {
            margin-left: 15px;
        }

        .ui-pnotify {
            top: 70px;
        }



    </Style>

    @yield('style')
</head>

<body class="nav-md">
<div class="container body">
    <div class="main_container">
        <div class="col-md-3 left_col">
            <div class="left_col scroll-view">
                <div class="navbar nav_title" style="border: 0;">
                    <a href="{{ url('dashboard') }}" class="site_title"><i class="fa fa-paw"></i>
                        <span>HRM</span></a>
                </div>

                <div class="clearfix"></div>

                <!-- menu profile quick info -->
                <div class="profile clearfix">
                    <div class="profile_pic">
                        <!--<img src="images/img.jpg" alt="..." class="img-circle profile_img">-->
                        @if(Auth::user()->saved_by == 0)
                            <img src="{{ asset('/images/users/Admin.png') }} " alt="Photo"
                                 class="img-circle profile_img">
                        @else
                            <img src="{{ asset('/images/users/'.Auth::user()->photo) }} " alt="Photo"
                                 class="img-circle profile_img">
                        @endif
                    </div>
                    <div class="profile_info">
                        <span>Welcome,</span>
                        <h2>{{Auth::user()->fname." ".Auth::user()->lname}}</h2>
                        <span>{{ Auth::user()->type }}</span>
                    </div>
                </div>


                <br/>

                <!-- sidebar menu -->
                <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                    <div class="menu_section">
                        <h3>General</h3>

                        <ul class="nav side-menu">

                            <li><a href="{{ url('dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard </a></li>
                            @if(Auth::user()->type=="Admin" || Auth::user()->type=="Manager" )

                                <li><a href="{{ url('#') }}"><i class="fa fa-cogs"></i> Settings<span
                                                class="fa fa-chevron-down"></span> </a>
                                    <ul class="nav child_menu">
                                        <li><a><i class="fa fa-wrench"></i> Job Details Setup <span
                                                        class="fa fa-chevron-down"></span>
                                            </a>
                                            <ul class="nav child_menu">
                                                <li><a href="{{ url('jobDetails/create') }}">Add Job Details</a></li>
                                                <li><a href="{{ url('/jobDetails') }}">View Job Details</a></li>
                                            </ul>
                                        </li>
                                        <li><a><i class="fa fa-wrench"></i>Company Structure <span
                                                        class="fa fa-chevron-down"></span>
                                            </a>
                                            <ul class="nav child_menu">
                                                <li><a href="{{ url('company_structure/create') }}">Add Company
                                                        Structure</a></li>
                                                <li><a href="{{ url('/company_structure') }}">View Company Structure</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li><a href="{{ url('/qualification') }}"><i class="fa fa-wrench"></i>Qualification
                                                Setup </a>

                                        </li>

                                        <li><a href="{{ url('/training_setup') }}"><i class="fa fa-wrench"></i>Training
                                                Setup</a></li>
                                        <li><a href="{{ url('/loan_setup') }}"><i class="fa fa-wrench"></i>Loan
                                                Setup</a></li>

                                    </ul>

                                </li>

                                <li><a><i class="fa fa-user"></i> User <span class="fa fa-chevron-down"></span> </a>
                                    <ul class="nav child_menu">
                                        <li><a href="{{ url('user/create') }}">Add User</a></li>
                                        <li><a href="{{ url('/user') }}">View User</a></li>
                                    </ul>
                                </li>




                                <li><a><i class="fa fa-wrench"></i> Employee Setup <span
                                                class="fa fa-chevron-down"></span>
                                    </a>
                                    <ul class="nav child_menu">
                                        <li><a href="{{ url('employeeSetup/create') }}">Add Employee</a></li>
                                        <li><a href="{{ url('/employeeSetup') }}">View Employee</a></li>

                                        <li><a><i class="fa fa-wrench"></i> Loan Installment <span
                                                        class="fa fa-chevron-down"></span>
                                            </a>
                                            <ul class="nav child_menu">
                                                <li><a href="{{ url('installment/create') }}">Add Loan Installment</a>
                                                </li>
                                                {{--<li><a href="{{ url('/installment') }}">View Loan Installment</a></li>--}}
                                            </ul>
                                        </li>
                                    </ul>
                                </li>


                                <li><a><i class="fa fa-money"></i> Payroll <span
                                                class="fa fa-chevron-down"></span>
                                    </a>

                                    <ul class="nav child_menu">
                                        <li><a><i class="fa fa-wrench"></i> Salary Setup <span
                                                        class="fa fa-chevron-down"></span>
                                            </a>
                                            <ul class="nav child_menu">

                                                <li><a href="{{ url('/salary_setup') }}">Set Salary Setup</a></li>
                                                <li><a href="{{ url('/view_salary_setup') }}">View Salary Setup</a></li>


                                                {{--<li><a href="{{ url('/installment') }}">View Loan Installment</a></li>--}}
                                            </ul>
                                        </li>




                                        <li><a><i class="fa fa-wrench"></i> Employee Salary Setup <span
                                                        class="fa fa-chevron-down"></span>
                                            </a>
                                            <ul class="nav child_menu">

                                                <li><a href="{{ url('/emp_salary_setup') }}">Set Employee Salary Setup</a></li>
                                                <li><a href="{{ url('/emp_view_salary_setup') }}">View Employee Salary Setup</a></li>

                                                {{--<li><a href="{{ url('/installment') }}">View Loan Installment</a></li>--}}
                                            </ul>
                                        </li>





                                    </ul>
                                </li>



                                <li><a><i class="fa fa-calendar"></i> Attendance <span class="fa fa-chevron-down"></span>
                                    </a>
                                    <ul class="nav child_menu">
                                        <li><a href="{{ url('attendance') }}">Admin Control</a></li>
                                    </ul>
                                </li>

                                <li><a><i class="fa fa-calendar"></i> Leave Settings <span class="fa fa-chevron-down"></span>
                                    </a>
                                    <ul class="nav child_menu">
                                        <li><a href="{{ url('weekly-holiday') }}">Weekly Holiday</a></li>
                                        <li><a href="{{ url('public-holiday') }}">Public Holiday</a></li>
                                        <li><a href="{{ url('employee-leave') }}">Employee Leave</a></li>
                                    </ul>
                                </li>

                                <li><a href="{{ url('resignList')}}"><i class="glyphicon glyphicon-tasks"></i> Resign List</a></li>

                                <li><a href="{{ url('billInfo')}}"><i class="fa fa-money"></i> Bill List</a></li>
                                <li><a href="{{ url('resignList')}}"><i class="glyphicon glyphicon-tasks"></i> Resign List</a></li>


                                <li><a><i class="fa fa-money"></i> Bill <span class="fa fa-chevron-down"></span>
                                    </a>
                                    <ul class="nav child_menu">
                                        <li><a href="{{ url('addBillInfo') }}">Add Bill</a></li>
                                        <li><a href="{{ url('billInfo') }}">Bill List</a></li>
                                        <li><a href="{{ url('billReportInfo') }}">Bill Report</a></li>
                                    </ul>
                                </li>

                            @endif

                            @if(Auth::user()->type=="User" )
                                <li><a href="{{ url('attendance/specific_month_user/'.Auth::user()->id)}}"><i class="fa fa-calendar"></i>Attendance</a></li>

                                <li><a href="{{ url('resignApply/'.Auth::user()->id)}}"><i class="glyphicon glyphicon-tasks"></i> Apply for Resign</a></li>



                                <li><a><i class="fa fa-money"></i> Bill <span
                                                class="fa fa-chevron-down"></span>
                                    </a>
                                    <ul class="nav child_menu">

                                        <li><a href="{{ url('billApply')}}">Apply for Bill</a></li>
                                        <li><a href="{{ url('view_employee_bill/') }}">View Bill</a></li>

                                        {{--<li><a href="{{ url('/installment') }}">View Loan Installment</a></li>--}}
                                    </ul>
                                </li>


                            @endif

                            <li><a href="{{ url('empProfile/'.Auth::user()->id)}}"><i class="fa fa-user"></i>My Profile</a></li>

                            <li><a href="{{ url('employeeLoan/'.Auth::user()->id)}}"><i class="fa fa-plus-circle"></i>Loan</a></li>



                        </ul>
                    </div>

                </div>
                <!-- /sidebar menu -->

                <!-- /menu footer buttons -->
                <div class="sidebar-footer hidden-small">
                    <a data-toggle="tooltip" data-placement="top" title="Settings">
                        <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
                    </a>
                    <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                        <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
                    </a>
                    <a data-toggle="tooltip" data-placement="top" title="Lock">
                        <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
                    </a>
                    <a href="{{URL('logout')}}" data-toggle="tooltip" data-placement="top" title="Logout">
                        <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
                    </a>
                </div>
                <!-- /menu footer buttons -->
            </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
            <div class="nav_menu">
                <nav>
                    <div class="nav toggle">
                        <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                    </div>

                    <ul class="nav navbar-nav navbar-right">
                        <li class="">
                            <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown"
                               aria-expanded="false">
                                {{--<img src="{{ asset('/images/users/'.Auth::user()->photo) }}"--}}
                                {{--alt="">{{Auth::user()->fname." ".Auth::user()->lname}}--}}

                                @if(Auth::user()->saved_by == 0)
                                    <img src="{{ asset('/images/users/Admin.png') }}"
                                         alt="">{{Auth::user()->fname." ".Auth::user()->lname}}
                                @else
                                    <img src="{{ asset('/images/users/'.Auth::user()->photo) }}"
                                         alt="">{{Auth::user()->fname." ".Auth::user()->lname}}
                                @endif

                                <span class=" fa fa-angle-down"></span>
                            </a>
                            <ul class="dropdown-menu dropdown-usermenu pull-right">
                                <li><a href="{{route('user.edit', Auth::user()->id)}}"> Profile</a></li>
                                <li>
                                    <a href="javascript:;">
                                        <span class="badge bg-red pull-right">50%</span>
                                        <span>Settings</span>
                                    </a>
                                </li>
                                <li><a href="javascript:;">Help</a></li>
                                <li><a href="{{URL('logout')}}"><i class="fa fa-sign-out pull-right"></i> Log Out</a>
                                </li>
                            </ul>
                        </li>


                        @if($present == false)

                            <li role="presentation" class="dropdown">
                                <a href="" class="dropdown-toggle info-number give_att" data-toggle="dropdown"
                                   aria-expanded="false" id="">
                                    <button type="button" class="btn btn-warning" id="att_btn">Give Attendance</button>

                                </a>

                            </li>
                        @else

                            @if($find_check_out->check_out != "")

                                <li role="presentation" class="dropdown">
                                    <a href="" class="dropdown-toggle info-number give_att" data-toggle="dropdown"
                                       aria-expanded="false" id="">
                                        <button type="button" class="btn btn-success" id="att_btn">Check In</button>

                                    </a>

                                </li>

                            @else

                                <li role="presentation" class="dropdown">
                                    <a href="" class="dropdown-toggle info-number give_att" data-toggle="dropdown"
                                       aria-expanded="false" id="">
                                        <button type="button" class="btn btn-danger" id="att_btn">Check Out</button>

                                    </a>

                                </li>

                            @endif

                        @endif


                        <meta name="_token" content="{!! csrf_token() !!}"/>

                        <li>

                        </li>

                    </ul>
                </nav>
            </div>
        </div>


        <!-- /top navigation -->

        <!-- page content -->

    @yield('content')

    <!-- /page content -->

        <!-- footer content -->
        <footer>
            <div class="pull-right">
                Powered by <a href="http://synergyforcesolutions.com/" target='_blank'>Synergyforce Solutions</a>
            </div>
            <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
    </div>
</div>

{{--bootstrap tab view script--}}
<script type="text/javascript" src="http://code.jquery.com/jquery.min.js"></script>
<script src="http://netdna.bootstrapcdn.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>


<!-- jQuery -->
<script src="{{ asset('/assets/template/vendors/jquery/dist/jquery.min.js') }}" type="text/javascript"></script>


<script src="{{ asset('/assets/template/vendors/jquery/dist/jquery.confirm.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/template/vendors/jquery/dist/jquery.confirm.min.js') }}" type="text/javascript"></script>

{{--<script src="{{ asset('/assets/template/vendors/jquery/dist/TotalAmount/code.jquery.com/jquery-1.12.4.js') }}" type="text/javascript"></script>--}}
{{--<script src="{{ asset('/assets/template/vendors/jquery/dist/TotalAmount/jquery.dataTables.min.js') }}" type="text/javascript"></script>--}}
{{----}}

<!-- Bootstrap -->
<script src="{{ asset('/assets/template/vendors/bootstrap/dist/js/bootstrap.min.js') }}"
        type="text/javascript"></script>
<!-- FastClick -->
<script src="{{ asset('/assets/template/vendors/fastclick/lib/fastclick.js') }}" type="text/javascript"></script>
<!-- NProgress -->
<script src="{{ asset('/assets/template/vendors/nprogress/nprogress.js') }}" type="text/javascript"></script>

<!-- Chart.js -->
<script src="{{ asset('/assets/template/vendors/Chart.js/dist/Chart.min.js') }}" type="text/javascript"></script>
<!-- gauge.js -->
<script src="{{ asset('/assets/template/vendors/gauge.js/dist/gauge.min.js') }}" type="text/javascript"></script>
<!-- bootstrap-progressbar -->
<script src="{{ asset('/assets/template/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js') }}"
        type="text/javascript"></script>
<!-- iCheck -->

<script src="{{ asset('/assets/template/vendors/iCheck/icheck.min.js') }}" type="text/javascript"></script>


<!-- PNotify -->


<script src="{{ asset('/assets/template/vendors/pnotify/dist/pnotify.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/template/vendors/pnotify/dist/pnotify.buttons.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/template/vendors/pnotify/dist/pnotify.nonblock.js') }}" type="text/javascript"></script>


<!-- Datatables -->
<script src="{{ asset('/assets/template/vendors/datatables.net/js/jquery.dataTables.min.js') }}"
        type="text/javascript"></script>
<script src="{{ asset('/assets/template/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"
        type="text/javascript"></script>
<script src="{{ asset('/assets/template/vendors/datatables.net-buttons/js/dataTables.buttons.min.js') }}"
        type="text/javascript"></script>
<script src="{{ asset('/assets/template/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js') }}"
        type="text/javascript"></script>
<script src="{{ asset('/assets/template/vendors/datatables.net-buttons/js/buttons.flash.min.js') }}"
        type="text/javascript"></script>
<script src="{{ asset('/assets/template/vendors/datatables.net-buttons/js/buttons.html5.min.js') }}"
        type="text/javascript"></script>
<script src="{{ asset('/assets/template/vendors/datatables.net-buttons/js/buttons.print.min.js') }}"
        type="text/javascript"></script>
<script src="{{ asset('/assets/template/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js') }}"
        type="text/javascript"></script>
<script src="{{ asset('/assets/template/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js') }}"
        type="text/javascript"></script>
<script src="{{ asset('/assets/template/vendors/datatables.net-responsive/js/dataTables.responsive.min.js') }}"
        type="text/javascript"></script>
<script src="{{ asset('/assets/template/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js') }}"
        type="text/javascript"></script>
<script src="{{ asset('/assets/template/vendors/datatables.net-scroller/js/datatables.scroller.min.js') }}"
        type="text/javascript"></script>
<script src="{{ asset('/assets/template/vendors/pdfmake/build/pdfmake.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/template/vendors/pdfmake/build/vfs_fonts.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/template/vendors/jszip/dist/jszip.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/template/vendors/jquery/dist/buttons.colVis.min.js') }}" type="text/javascript"></script>


<!-- Skycons -->
<script src="{{ asset('/assets/template/vendors/skycons/skycons.js') }}" type="text/javascript"></script>
<!-- Flot -->
<script src="{{ asset('/assets/template/vendors/Flot/jquery.flot.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/template/vendors/Flot/jquery.flot.pie.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/template/vendors/Flot/jquery.flot.time.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/template/vendors/Flot/jquery.flot.stack.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/template/vendors/Flot/jquery.flot.resize.js') }}" type="text/javascript"></script>
<!-- Flot plugins -->
<script src="{{ asset('/assets/template/vendors/flot.orderbars/js/jquery.flot.orderBars.js') }}"
        type="text/javascript"></script>
<script src="{{ asset('/assets/template/vendors/flot-spline/js/jquery.flot.spline.min.js') }}"
        type="text/javascript"></script>
<script src="{{ asset('/assets/template/vendors/flot.curvedlines/curvedLines.js') }}" type="text/javascript"></script>
<!-- DateJS -->
<script src="{{ asset('/assets/template/vendors/DateJS/build/date.js') }}" type="text/javascript"></script>
<!-- JQVMap -->
<script src="{{ asset('/assets/template/vendors/jqvmap/dist/jquery.vmap.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/template/vendors/jqvmap/dist/maps/jquery.vmap.world.js') }}"
        type="text/javascript"></script>
<script src="{{ asset('/assets/template/vendors/jqvmap/examples/js/jquery.vmap.sampledata.js') }}"
        type="text/javascript"></script>
<!-- Dropzone.js -->
<script src="{{ asset('/assets/template/vendors/dropzone/dist/min/dropzone.min.js') }}"
        type="text/javascript"></script>


<!-- Custom Theme Scripts -->
<script src="{{ asset('/assets/template/build/js/custom.min.js') }}" type="text/javascript"></script>


<!-- jQuery Smart Wizard -->
<script src="{{ asset('/assets/template/vendors/jQuery-Smart-Wizard/js/jquery.smartWizard.js') }}"
        type="text/javascript"></script>
<!-- jQuery Smart Wizard -->
<script>
    $(document).ready(function () {
        $('#wizard').smartWizard();

        $('#wizard_verticle').smartWizard({
            transitionEffect: 'slide'
        });

        $('.buttonNext').addClass('btn btn-success');
        $('.buttonPrevious').addClass('btn btn-primary');
        $('.buttonFinish').addClass('btn btn-default');
    });
</script>
<!-- /jQuery Smart Wizard -->

<!-- bootstrap-daterangepicker -->
<script src="{{ asset('/assets/template/vendors/moment/min/moment.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/template/vendors/bootstrap-daterangepicker/daterangepicker.js') }}"
        type="text/javascript"></script>

<script src="//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/src/js/bootstrap-datetimepicker.js"></script>

<!-- bootstrap-wysiwyg -->
<script src="{{ asset('/assets/template/vendors/bootstrap-wysiwyg/js/bootstrap-wysiwyg.min.js') }}"
        type="text/javascript"></script>
<script src="{{ asset('/assets/template/vendors/jquery.hotkeys/jquery.hotkeys.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/template/vendors/google-code-prettify/src/prettify.js') }}"
        type="text/javascript"></script>


<!-- jQuery Tags Input -->
<script src="{{ asset('/assets/template/vendors/jquery.tagsinput/src/jquery.tagsinput.js') }}"
        type="text/javascript"></script>

<!-- Switchery -->
<script src="{{ asset('/assets/template/vendors/switchery/dist/switchery.min.js') }}" type="text/javascript"></script>

<!-- Select2 -->
<script src="{{ asset('/assets/template/vendors/select2/dist/js/select2.full.min.js') }}"
        type="text/javascript"></script>

<!-- Parsley -->
<script src="{{ asset('/assets/template/vendors/parsleyjs/dist/parsley.min.js') }}" type="text/javascript"></script>

<!-- Autosize -->
<script src="{{ asset('/assets/template/vendors/autosize/dist/autosize.min.js') }}" type="text/javascript"></script>

<!-- jQuery autocomplete -->
<script src="{{ asset('/assets/template/vendors/devbridge-autocomplete/dist/jquery.autocomplete.min.js') }}"
        type="text/javascript"></script>

<!-- starrr -->
<script src="{{ asset('/assets/template/vendors/starrr/dist/starrr.js') }}" type="text/javascript"></script>
{{--<script src="{{ asset('/assets/template/build/js/custom.min.js') }}" type="text/javascript"></script>--}}
{{--Loan Validation--}}


<!-- month view date picker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js"></script>
<script>
    $(document).ready(function () {
        $('.birthday').daterangepicker({
            singleDatePicker: true,
            calender_style: "picker_4"
        }, function (start, end, label) {
            console.log(start.toISOString(), end.toISOString(), label);
        });
    });
</script>

<!-- /bootstrap-daterangepicker -->

<!-- Datatables -->


<!-- Flot -->
<script>
    $(document).ready(function () {
        var data1 = [
            [gd(2012, 1, 1), 17],
            [gd(2012, 1, 2), 74],
            [gd(2012, 1, 3), 6],
            [gd(2012, 1, 4), 39],
            [gd(2012, 1, 5), 20],
            [gd(2012, 1, 6), 85],
            [gd(2012, 1, 7), 7]
        ];

        var data2 = [
            [gd(2012, 1, 1), 82],
            [gd(2012, 1, 2), 23],
            [gd(2012, 1, 3), 66],
            [gd(2012, 1, 4), 9],
            [gd(2012, 1, 5), 119],
            [gd(2012, 1, 6), 6],
            [gd(2012, 1, 7), 9]
        ];
        $("#canvas_dahs").length && $.plot($("#canvas_dahs"), [
            data1, data2
        ], {
            series: {
                lines: {
                    show: false,
                    fill: true
                },
                splines: {
                    show: true,
                    tension: 0.4,
                    lineWidth: 1,
                    fill: 0.4
                },
                points: {
                    radius: 0,
                    show: true
                },
                shadowSize: 2
            },
            grid: {
                verticalLines: true,
                hoverable: true,
                clickable: true,
                tickColor: "#d5d5d5",
                borderWidth: 1,
                color: '#fff'
            },
            colors: ["rgba(38, 185, 154, 0.38)", "rgba(3, 88, 106, 0.38)"],
            xaxis: {
                tickColor: "rgba(51, 51, 51, 0.06)",
                mode: "time",
                tickSize: [1, "day"],
                //tickLength: 10,
                axisLabel: "Date",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial',
                axisLabelPadding: 10
            },
            yaxis: {
                ticks: 8,
                tickColor: "rgba(51, 51, 51, 0.06)",
            },
            tooltip: false
        });

        function gd(year, month, day) {
            return new Date(year, month - 1, day).getTime();
        }
    });
</script>
<!-- /Flot -->

<!-- JQVMap -->
<script>
    $(document).ready(function () {
        $('#world-map-gdp').vectorMap({
            map: 'world_en',
            backgroundColor: null,
            color: '#ffffff',
            hoverOpacity: 0.7,
            selectedColor: '#666666',
            enableZoom: true,
            showTooltip: true,
            values: sample_data,
            scaleColors: ['#E6F2F0', '#149B7E'],
            normalizeFunction: 'polynomial'
        });
    });
</script>
<!-- /JQVMap -->

<!-- Skycons -->
<script>
    $(document).ready(function () {
        var icons = new Skycons({
                "color": "#73879C"
            }),
            list = [
                "clear-day", "clear-night", "partly-cloudy-day",
                "partly-cloudy-night", "cloudy", "rain", "sleet", "snow", "wind",
                "fog"
            ],
            i;

        for (i = list.length; i--;)
            icons.set(list[i], list[i]);

        icons.play();
    });
</script>
<!-- /Skycons -->

<!-- Doughnut Chart -->
<script>
    $(document).ready(function () {
        var options = {
            legend: false,
            responsive: false
        };

        new Chart(document.getElementById("canvas1"), {
            type: 'doughnut',
            tooltipFillColor: "rgba(51, 51, 51, 0.55)",
            data: {
                labels: [
                    "Symbian",
                    "Blackberry",
                    "Other",
                    "Android",
                    "IOS"
                ],
                datasets: [{
                    data: [15, 20, 30, 10, 30],
                    backgroundColor: [
                        "#BDC3C7",
                        "#9B59B6",
                        "#E74C3C",
                        "#26B99A",
                        "#3498DB"
                    ],
                    hoverBackgroundColor: [
                        "#CFD4D8",
                        "#B370CF",
                        "#E95E4F",
                        "#36CAAB",
                        "#49A9EA"
                    ]
                }]
            },
            options: options
        });
    });
</script>
<!-- /Doughnut Chart -->


<!-- bootstrap-daterangepicker -->
<script>
    $(document).ready(function () {

        var cb = function (start, end, label) {
            console.log(start.toISOString(), end.toISOString(), label);
            $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        };

        var optionSet1 = {
            startDate: moment().subtract(29, 'days'),
            endDate: moment(),
            minDate: '01/01/2012',
            maxDate: '12/31/2015',
            dateLimit: {
                days: 60
            },
            showDropdowns: true,
            showWeekNumbers: true,
            timePicker: false,
            timePickerIncrement: 1,
            timePicker12Hour: true,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            opens: 'left',
            buttonClasses: ['btn btn-default'],
            applyClass: 'btn-small btn-primary',
            cancelClass: 'btn-small',
            format: 'MM/DD/YYYY',
            separator: ' to ',
            locale: {
                applyLabel: 'Submit',
                cancelLabel: 'Clear',
                fromLabel: 'From',
                toLabel: 'To',
                customRangeLabel: 'Custom',
                daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
                monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                firstDay: 1
            }
        };
        $('#reportrange span').html(moment().subtract(29, 'days').format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));
        $('#reportrange').daterangepicker(optionSet1, cb);
        $('#reportrange').on('show.daterangepicker', function () {
            console.log("show event fired");
        });
        $('#reportrange').on('hide.daterangepicker', function () {
            console.log("hide event fired");
        });
        $('#reportrange').on('apply.daterangepicker', function (ev, picker) {
            console.log("apply event fired, start/end dates are " + picker.startDate.format('MMMM D, YYYY') + " to " + picker.endDate.format('MMMM D, YYYY'));
        });
        $('#reportrange').on('cancel.daterangepicker', function (ev, picker) {
            console.log("cancel event fired");
        });
        $('#options1').click(function () {
            $('#reportrange').data('daterangepicker').setOptions(optionSet1, cb);
        });
        $('#options2').click(function () {
            $('#reportrange').data('daterangepicker').setOptions(optionSet2, cb);
        });
        $('#destroy').click(function () {
            $('#reportrange').data('daterangepicker').remove();
        });
    });
</script>
<!-- /bootstrap-daterangepicker -->

<!-- gauge.js -->
<script>
    var opts = {
        lines: 12,
        angle: 0,
        lineWidth: 0.4,
        pointer: {
            length: 0.75,
            strokeWidth: 0.042,
            color: '#1D212A'
        },
        limitMax: 'false',
        colorStart: '#1ABC9C',
        colorStop: '#1ABC9C',
        strokeColor: '#F0F3F3',
        generateGradient: true
    };
    var target = document.getElementById('foo'),
        gauge = new Gauge(target).setOptions(opts);

    gauge.maxValue = 6000;
    gauge.animationSpeed = 32;
    gauge.set(3200);
    gauge.setTextField(document.getElementById("gauge-text"));
</script>
<!-- /gauge.js -->


<!-- bootstrap-wysiwyg -->
<script>
    $(document).ready(function () {
        function initToolbarBootstrapBindings() {
            var fonts = ['Serif', 'Sans', 'Arial', 'Arial Black', 'Courier',
                    'Courier New', 'Comic Sans MS', 'Helvetica', 'Impact', 'Lucida Grande', 'Lucida Sans', 'Tahoma', 'Times',
                    'Times New Roman', 'Verdana'
                ],
                fontTarget = $('[title=Font]').siblings('.dropdown-menu');
            $.each(fonts, function (idx, fontName) {
                fontTarget.append($('<li><a data-edit="fontName ' + fontName + '" style="font-family:\'' + fontName + '\'">' + fontName + '</a></li>'));
            });
            $('a[title]').tooltip({
                container: 'body'
            });
            $('.dropdown-menu input').click(function () {
                return false;
            })
                .change(function () {
                    $(this).parent('.dropdown-menu').siblings('.dropdown-toggle').dropdown('toggle');
                })
                .keydown('esc', function () {
                    this.value = '';
                    $(this).change();
                });

            $('[data-role=magic-overlay]').each(function () {
                var overlay = $(this),
                    target = $(overlay.data('target'));
                overlay.css('opacity', 0).css('position', 'absolute').offset(target.offset()).width(target.outerWidth()).height(target.outerHeight());
            });

            if ("onwebkitspeechchange" in document.createElement("input")) {
                var editorOffset = $('#editor').offset();

                $('.voiceBtn').css('position', 'absolute').offset({
                    top: editorOffset.top,
                    left: editorOffset.left + $('#editor').innerWidth() - 35
                });
            } else {
                $('.voiceBtn').hide();
            }
        }

        function showErrorAlert(reason, detail) {
            var msg = '';
            if (reason === 'unsupported-file-type') {
                msg = "Unsupported format " + detail;
            } else {
                console.log("error uploading file", reason, detail);
            }
            $('<div class="alert"> <button type="button" class="close" data-dismiss="alert">&times;</button>' +
                '<strong>File upload error</strong> ' + msg + ' </div>').prependTo('#alerts');
        }

        initToolbarBootstrapBindings();

        $('#editor').wysiwyg({
            fileUploadError: showErrorAlert
        });

        window.prettyPrint;
        prettyPrint();
    });
</script>
<!-- /bootstrap-wysiwyg -->

<!-- Select2 -->
{{--<script>--}}
{{--$(document).ready(function () {--}}
{{--$(".select2_single").select2({--}}
{{--placeholder: "Select a state",--}}
{{--allowClear: true--}}
{{--});--}}
{{--$(".select2_group").select2({});--}}
{{--$(".select2_multiple").select2({--}}
{{--maximumSelectionLength: 4,--}}
{{--placeholder: "With Max Selection limit 4",--}}
{{--allowClear: true--}}
{{--});--}}
{{--});--}}
{{--</script>--}}
<!-- /Select2 -->

<!-- jQuery Tags Input -->
<script>
    function onAddTag(tag) {
        alert("Added a tag: " + tag);
    }

    function onRemoveTag(tag) {
        alert("Removed a tag: " + tag);
    }

    function onChangeTag(input, tag) {
        alert("Changed a tag: " + tag);
    }

    $(document).ready(function () {
        $('#tags_1').tagsInput({
            width: 'auto'
        });
    });
</script>
<!-- /jQuery Tags Input -->

<!-- Parsley -->
<script>
    $(document).ready(function () {
        $.listen('parsley:field:validate', function () {
            validateFront();
        });
        $('#demo-form .btn').on('click', function () {
            $('#demo-form').parsley().validate();
            validateFront();
        });
        var validateFront = function () {
            if (true === $('#demo-form').parsley().isValid()) {
                $('.bs-callout-info').removeClass('hidden');
                $('.bs-callout-warning').addClass('hidden');
            } else {
                $('.bs-callout-info').addClass('hidden');
                $('.bs-callout-warning').removeClass('hidden');
            }
        };
    });

    $(document).ready(function () {
        $.listen('parsley:field:validate', function () {
            validateFront();
        });
        $('#demo-form2 .btn').on('click', function () {
            $('#demo-form2').parsley().validate();
            validateFront();
        });
        var validateFront = function () {
            if (true === $('#demo-form2').parsley().isValid()) {
                $('.bs-callout-info').removeClass('hidden');
                $('.bs-callout-warning').addClass('hidden');
            } else {
                $('.bs-callout-info').addClass('hidden');
                $('.bs-callout-warning').removeClass('hidden');
            }
        };
    });
    try {
        hljs.initHighlightingOnLoad();
    } catch (err) {
    }
</script>
<!-- /Parsley -->

<!-- Autosize -->
<script>
    $(document).ready(function () {
        autosize($('.resizable_textarea'));
    });
</script>
<!-- /Autosize -->

<!-- Starrr -->
<script>
    $(document).ready(function () {
        $(".stars").starrr();

        $('.stars-existing').starrr({
            rating: 4
        });

        $('.stars').on('starrr:change', function (e, value) {
            $('.stars-count').html(value);
        });

        $('.stars-existing').on('starrr:change', function (e, value) {
            $('.stars-count-existing').html(value);
        });
    });
</script>
<!-- /Starrr -->

<!-- Print Multiple Div -->
<script>
    function printDiv(divName) {
        var printContents = document.getElementById(divName).innerHTML;
        var originalContents = document.body.innerHTML;

        document.body.innerHTML = printContents;

        window.print();


        document.body.innerHTML = originalContents;

        location.reload();
    }
</script>


<script type="text/javascript">

    $(".give_att").on("click", function () {


        var url = {!! json_encode(url('/giv_atten')) !!};


        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        });


        $.ajax({
            type: 'GET',
            url: url,
            success: function (data) {
                console.log(data);

                if (data == 'check_in') {
                    $("#att_btn").html('Check Out');
                    $("#att_btn").removeClass("btn-success").addClass("btn-danger");
                }
                else if (data == 'check_out') {
                    $("#att_btn").html('Check In');
                    $("#att_btn").removeClass("btn-danger").addClass("btn-success");
                }
                else if (data == 'Not_Same_IP') {
                    new PNotify({
                        title: "Oops!",
                        type: "error",
                        delay: 2000,
                        text: "You are outside from Office!",
                        nonblock: {
                            nonblock: true
                        },
                        styling: 'bootstrap3',
                        hide: true,
                        before_close: function (PNotify) {
                            PNotify.update({
                                title: PNotify.options.title + " - Enjoy your Stay",
                                before_close: null
                            });

                            PNotify.queueRemove();

                            return false;
                        }

                    });
                }

            },
            error: function (data) {
                console.log('Error:', data);
            }
        });


    });

</script>


@yield('script')

</body>
</html>
