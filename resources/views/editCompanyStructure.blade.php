@extends('layouts.master')
@section('title', 'Edit User')
@section('content')

    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    {{--<h3>Company Structure</h3>--}}
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Company Structure User
                                <!--<small>Edit different form elements</small>-->
                            </h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <br/>


                            <form action="{{url('company_structure/'.$structureList->id)}}" method="POST" class="form-horizontal" role="form"
                                  enctype="multipart/form-data">
                                {{ method_field('PUT') }}
                                {{ csrf_field() }}
                                <div class="form-body">
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Name</label>
                                        <div class="col-md-7">
                                            <input type="text" class="form-control col-md-7 col-xs-12"
                                                   placeholder="Name" name="name"
                                                   value="{{$structureList->name}}">
                                            @if ($errors->has('name'))
                                                <span class="help-block">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Details</label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <textarea class="form-control col-md-7 col-xs-12"
                                                      placeholder="Details" name="details">{{$structureList->details}}</textarea>
                                            @if ($errors->has('details'))
                                                <span class="help-block">
                                                <strong>{{ $errors->first('details') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Address</label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-envelope-o"></i>
                                    </span>
                                                <textarea name="address" class="form-control col-md-7 col-xs-12"
                                                          placeholder="Address">{{$structureList->address}}</textarea>
                                            </div>
                                            @if ($errors->has('address'))
                                                <span class="help-block">
                                                <strong>{{ $errors->first('address') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Department Type</label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            {!! Form::select('department_type', array('Company' => 'Company', 'Department' => 'Department','Head Office' => 'Head Office','Regional Office' => 'Regional Office','Sub Unit Office' => 'Sub Unit Office','Unit' => 'Unit','Other' => 'Other'), $structureList->department_type, ['class'=>'form-control col-md-7 col-xs-12','placeholder' => 'Choose a Department Type']) !!}
                                            @if ($errors->has('department_type'))
                                                <span class="help-block">
                                                <strong>{{ $errors->first('department_type') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Country</label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="text" class="form-control col-md-7 col-xs-12"
                                                       placeholder="Enter Country Name" name="country" value="{{$structureList->country}}">
                                                @if ($errors->has('country'))
                                                    <span class="help-block">
                                                <strong>{{ $errors->first('country') }}</strong>
                                            </span>
                                                @endif
                                            </div>
                                        </div>

                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">IP Address</label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="text" class="form-control col-md-7 col-xs-12"
                                                       placeholder="Enter IP Address" name="ip_address" value="{{$structureList->ip_address}}">
                                                @if ($errors->has('ip_address'))
                                                    <span class="help-block">
                                                <strong>{{ $errors->first('ip_address') }}</strong>
                                            </span>
                                                @endif
                                            </div>
                                        </div>

                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Office Start Time</label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type='text' name="office_start_time" class="form-control datetimepicker3" id=""
                                                       placeholder="Start Time"  value="{{$structureList->office_start}}" />
                                                @if ($errors->has('office_start_time'))
                                                    <span class="help-block">
                                                <strong>{{ $errors->first('office_start_time') }}</strong>
                                            </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Last Entry Time</label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type='text' name="last_entry_time" class="form-control datetimepicker3" id=""
                                                       placeholder="Last Entry Time"  value="{{$structureList->last_entry_time}}" />
                                                @if ($errors->has('office_start_time'))
                                                    <span class="help-block">
                                                <strong>{{ $errors->first('office_start_time') }}</strong>
                                            </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Office End Time</label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type='text' name="office_end_time" class="form-control datetimepicker3" id=""
                                                       placeholder="End Time" value="{{$structureList->office_end}}" />
                                                @if ($errors->has('office_end_time'))
                                                    <span class="help-block">
                                                <strong>{{ $errors->first('office_end_time') }}</strong>
                                            </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>



                                    <div class="ln_solid"></div>
                                    <div class="form-actions right">
                                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                            <a type="cancel" href="{{ url('company_structure') }}" class="btn btn-danger">Cancel</a>
                                            <button type="submit" class="btn btn-primary">Submit</button>
                                        </div>
                                    </div>
                                </div>
                            </form>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection