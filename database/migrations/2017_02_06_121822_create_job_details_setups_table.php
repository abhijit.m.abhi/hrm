<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobDetailsSetupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_details_setups', function (Blueprint $table) {
            $table->increments('id');
            $table->string('job_code');
            $table->string('job_title');
            $table->text('description');
            $table->string('specification')->nullable();
            $table->double('min_salary', 15, 2);
            $table->double('max_salary', 15, 2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('job_details_setups');
    }
}
