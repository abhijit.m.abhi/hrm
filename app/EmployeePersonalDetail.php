<?php

namespace App;
use App\User;

use Illuminate\Database\Eloquent\Model;

class EmployeePersonalDetail extends Model
{
    protected $fillable = [
        'empCode',
        'firstName',
        'middleName',
        'lastName',
        'nationality',
        'nid',
        'dateOfBirth',
        'birthId',
        'passportNo',
        'gender',
        'maritalStatus',
        'address',
        'city',
        'postalCode',
        'country',
        'phone',
        'officePhone',
        'email',
    ];

    public function emp_job_details()
    {
        return $this->hasOne('App\EmployeeJobDetail', 'employee_id');
    }

    public function emp_qualifications()
    {
        return $this->hasOne('App\EmployeeQualification', 'employee_id');
    }

    public function emp_emergency_contact()
    {
        return $this->hasOne('App\EmployeeEmergencyContact', 'employee_id');
    }

    public function emp_experience()
    {
        return $this->hasMany('App\EmployeeExperience', 'employee_id');
    }

    public function emp_attachment()
    {
        return $this->hasMany('App\EmployeeAttachment', 'employee_id');
    }

    public function empTraining()
    {
        return $this->hasOne('App\EmployeeTraningSession', 'employee_id');
    }

    public  function emp_attendance()
    {
        return $this->hasMany('App\Attendance', 'employee_id');
    }

    public function emp_sal()
    {
        return $this->belongsTo('App\EmployeeSalarySetup');
    }



}
