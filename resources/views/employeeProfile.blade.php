@extends('layouts.master')
@section('title', 'Employee Profile')
@section('content')

    <!-- page content -->
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    {{--<h3>User Profile</h3>--}}
                </div>

            </div>

            <div class="clearfix"></div>

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Employee Profile</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <div class="col-md-3 col-sm-3 col-xs-12 profile_left">
                                <div class="profile_img">
                                    <div id="crop-avatar">
                                        <!-- Current avatar -->

                                        <img class="img-responsive avatar-view"
                                             src="{{ asset('/images/users/'.$employee->photo) }}" alt="Avatar"
                                             title="Change the avatar">
                                    </div>
                                </div>
                                <h3>{{$employee->fname.' '.$employee->lname}}</h3>

                                <ul class="list-unstyled user_data">
                                    <li><i class="fa fa-map-marker user-profile-icon"></i> {{$employee->address}}
                                    </li>

                                    <li>
                                        <i class="fa fa-briefcase user-profile-icon"></i> {{$employee->emp_official_info->emp_job_details->job_title}}
                                    </li>

                                    <li class="m-top-xs">
                                        <i class="fa fa-phone user-profile-icon"></i> {{$employee->emp_official_info->phone}}
                                        {{--<a href="http://www.kimlabs.com/profile/" target="_blank">www.kimlabs.com</a>--}}
                                    </li>
                                </ul>

                                <a href="{{route('employeeSetup.edit', $employee->id)}}" class="btn btn-success"><i
                                            class="fa fa-edit m-right-xs"></i>Edit Profile</a>

                                <br/>


                            </div>
                            <div class="col-md-9 col-sm-9 col-xs-12">

                                <div class="" role="tabpanel" data-example-id="togglable-tabs">
                                    <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                                        <li role="presentation" class="active"><a href="#tab_content1" id="home-tab"
                                                                                  role="tab" data-toggle="tab"
                                                                                  aria-expanded="true">Personal Info</a>
                                        </li>
                                        <li role="presentation" class=""><a href="#tab_content2" role="tab"
                                                                            id="profile-tab" data-toggle="tab"
                                                                            aria-expanded="false">Job Info</a></li>
                                        <li role="presentation" class=""><a href="#tab_content3" role="tab"
                                                                            id="profile-tab2" data-toggle="tab"
                                                                            aria-expanded="false">Qualification</a>
                                        <li role="presentation" class=""><a href="#tab_content4" role="tab"
                                                                            id="profile-tab2" data-toggle="tab"
                                                                            aria-expanded="false">Experience</a></li>
                                        <li role="presentation" class=""><a href="#tab_content5" role="tab"
                                                                            id="profile-tab2" data-toggle="tab"
                                                                            aria-expanded="false">Emergency Contact</a>
                                        </li>
                                        <li role="presentation" class=""><a href="#tab_content6" role="tab"
                                                                            id="profile-tab2" data-toggle="tab"
                                                                            aria-expanded="false">Attachment</a></li>
                                    </ul>
                                    <div id="myTabContent" class="tab-content">
                                        <div role="tabpanel" class="tab-pane fade active in" id="tab_content1"
                                             aria-labelledby="home-tab">

                                            <table class="data table table-striped no-margin">
                                                <tr>
                                                    <th>First Name:</th>
                                                    <td>{{$employee->fname}}</td>
                                                </tr>
                                                <tr>
                                                    <th>Last Name:</th>
                                                    <td>{{$employee->lname}}</td>
                                                </tr>
                                                <tr>
                                                    <th>Email:</th>
                                                    <td>{{$employee->email}}</td>
                                                </tr>
                                                <tr>
                                                    <th>Address:</th>
                                                    <td>{{$employee->address}}</td>
                                                </tr>
                                                <tr>
                                                    <th>Type:</th>
                                                    <td>{{$employee->type}}</td>
                                                </tr>
                                                <tr>
                                                    <th>Nationality:</th>
                                                    <td>{{$employee->emp_official_info->nationality}}</td>
                                                </tr>
                                                <tr>
                                                    <th>NID:</th>
                                                    <td>{{$employee->emp_official_info->nid}}</td>
                                                </tr>
                                                <tr>
                                                    <th>Date of Birth:</th>
                                                    <td>{{$employee->emp_official_info->date_of_birth}}</td>
                                                </tr>
                                                <tr>
                                                    <th>Birth Certificate ID:</th>
                                                    <td>{{$employee->emp_official_info->birth_certificate_id}}</td>
                                                </tr>
                                                <tr>
                                                    <th>Passport No:</th>
                                                    <td>{{$employee->emp_official_info->passport_no}}</td>
                                                </tr>
                                                <tr>
                                                    <th>Gender:</th>
                                                    <td>{{$employee->emp_official_info->gender}}</td>
                                                </tr>
                                                <tr>
                                                    <th>Martital Status:</th>
                                                    <td>{{$employee->emp_official_info->marital_status}}</td>
                                                </tr>
                                                <tr>
                                                    <th>City:</th>
                                                    <td>{{$employee->emp_official_info->city}}</td>
                                                </tr>
                                                <tr>
                                                    <th>Postal or Zip Code:</th>
                                                    <td>{{$employee->emp_official_info->postal_or_zip_code}}</td>
                                                </tr>
                                                <tr>
                                                    <th>Phone:</th>
                                                    <td>{{$employee->emp_official_info->phone}}</td>
                                                </tr>
                                                <tr>
                                                    <th>Work Phone:</th>
                                                    <td>{{$employee->emp_official_info->work_phone}}</td>
                                                </tr>

                                                <tr>
                                                    <th>Country:</th>
                                                    <td>{{$employee->emp_official_info->country}}</td>
                                                </tr>


                                            </table>

                                        </div>
                                        <div role="tabpanel" class="tab-pane fade" id="tab_content2"
                                             aria-labelledby="profile-tab">

                                            <table class="data table table-striped no-margin">
                                                <tr>
                                                    <th>Employee Code:</th>
                                                    <td>{{$employee->emp_official_info->employee_code}}</td>
                                                </tr>
                                                <tr>
                                                    <th>Employment Status:</th>
                                                    <td>{{$employee->emp_official_info->emp_job_details->employment_status}}</td>
                                                </tr>
                                                <tr>
                                                    <th>Job Title:</th>
                                                    <td>{{$employee->emp_official_info->emp_job_details->job_title}}</td>
                                                </tr>
                                                <tr>
                                                    <th>Joined Date:</th>
                                                    <td>{{$employee->emp_official_info->emp_job_details->joined_date}}</td>
                                                </tr>
                                                <tr>
                                                    <th>Confirmation:</th>
                                                    <td>{{$employee->emp_official_info->emp_job_details->confirmation_date}}</td>
                                                </tr>
                                                {{--<tr>--}}
                                                {{--<th>Termination:</th>--}}
                                                {{--<td>{{$employee->emp_official_info->emp_job_details->termination_date}}</td>--}}
                                                {{--</tr>--}}
                                                <tr>
                                                    <th>Department:</th>
                                                    <td>{{$employee->emp_official_info->emp_job_details->dep_info->name}}</td>
                                                </tr>


                                            </table>

                                        </div>
                                        <div role="tabpanel" class="tab-pane fade" id="tab_content3"
                                             aria-labelledby="profile-tab">
                                            <table class="data table table-striped no-margin">
                                                <caption><h4>Skills</h4></caption>

                                                @php
                                                    $skills =$employee->emp_official_info->emp_qualifications->skill;
                                                    $skill = json_decode($skills );
                                                @endphp

                                                @foreach($skill as $sk)
                                                    <tr>
                                                        <td>{{$sk}}</td>
                                                    </tr>

                                                @endforeach


                                            </table>

                                            <table class="data table table-striped no-margin">
                                                <caption><h4>Education</h4></caption>

                                                @php
                                                    $qualifications =$employee->emp_official_info->emp_qualifications->qualification;
                                                    $qualification = json_decode($qualifications);
                                                @endphp

                                                @foreach($qualification as $qua)
                                                    <tr>
                                                        <td>{{$qua}}</td>
                                                    </tr>

                                                @endforeach


                                            </table>


                                            <table class="data table table-striped no-margin">
                                                <caption><h4>Certification</h4></caption>

                                                @php
                                                    $certifications =$employee->emp_official_info->emp_qualifications->certification;
                                                    $certification = json_decode($certifications);
                                                @endphp

                                                @foreach($certification as $cer)
                                                    <tr>
                                                        <td>{{$cer}}</td>
                                                    </tr>

                                                @endforeach


                                            </table>


                                        </div>

                                        <div role="tabpanel" class="tab-pane fade" id="tab_content4"
                                             aria-labelledby="profile-tab">
                                            <table class="data table table-striped no-margin">

                                                <tr>
                                                    <th>Organization Name</th>
                                                    <th>Address</th>
                                                    <th>Contact No.</th>
                                                    <th>Duration</th>
                                                </tr>

                                                @foreach($employee->emp_official_info->emp_experience as $emp_exp)

                                                    <tr>
                                                        <td>{{$emp_exp->organization_name}}</td>
                                                        <td>{{$emp_exp->address}}</td>
                                                        <td>{{$emp_exp->contact_no}}</td>
                                                        <td>{{$emp_exp->duration}}</td>
                                                    </tr>

                                                @endforeach

                                            </table>


                                        </div>

                                        <div role="tabpanel" class="tab-pane fade" id="tab_content5"
                                             aria-labelledby="profile-tab">
                                            <table class="data table table-striped no-margin">
                                                {{--<caption><h4>Experience</h4></caption>--}}

                                                <tr>
                                                    <th>Name</th>
                                                    <th>Relation</th>
                                                    <th>Address</th>
                                                    <th>Mobile</th>
                                                </tr>

                                                <tr>
                                                    <td>{{$employee->emp_official_info->emp_emergency_contact->name}}</td>
                                                    <td>{{$employee->emp_official_info->emp_emergency_contact->relation}}</td>
                                                    <td>{{$employee->emp_official_info->emp_emergency_contact->address}}</td>
                                                    <td>{{$employee->emp_official_info->emp_emergency_contact->mobile}}</td>
                                                </tr>

                                            </table>


                                        </div>

                                        <div role="tabpanel" class="tab-pane fade" id="tab_content6"
                                             aria-labelledby="profile-tab">
                                            <table class="data table table-striped no-margin">
                                                {{--<caption><h4>Experience</h4></caption>--}}

                                                <tr>
                                                    <th>Title</th>
                                                    <th>Attachment</th>
                                                    <th>Upload Date</th>
                                                    <th>Download</th>
                                                </tr>

                                                @foreach($employee->emp_official_info->emp_attachment as $emp_attach)

                                                    <tr>
                                                        <td>{{$emp_attach->title}}</td>
                                                        <td><img src="{{ asset($emp_attach->location) }}"
                                                                 alt="User Photo" height="80" width="80"></td>
                                                        <td>{{$emp_attach->updated_at}}</td>
                                                        <td><a href="{{ asset($emp_attach->location) }}" download>
                                                                <button type="button" class="btn btn-primary"><i
                                                                            class="glyphicon glyphicon-download"></i>
                                                                </button>
                                                            </a></td>
                                                    </tr>

                                                @endforeach

                                            </table>


                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /page content -->

@endsection