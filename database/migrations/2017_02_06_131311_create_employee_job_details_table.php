<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeeJobDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_job_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('employee_id')->unsigned();
            $table->string('employment_status');
            $table->string('job_title');
            $table->date('joined_date');
            $table->date('confirmation_date')->nullable();
            $table->date('termination_date')->nullable();
            $table->integer('department')->unsigned();
            $table->timestamps();

            /*
             * Foreign Key
             */
            $table->foreign('employee_id')->references('id')->on('employee_personal_details')->onDelete('cascade');
            $table->foreign('department')->references('id')->on('company_structures')->onDelete('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee_job_details');

    }
}
