@extends('layouts.master')
@section('title', 'Employee List')
@section('content')

    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3></h3>
                </div>


            </div>

            <div class="clearfix"></div>

            <div class="row">

                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Employee List <!--<small>Users</small>--></h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        @if(Session::has('message'))

                            <div id="successMessage" class="alert alert-success alert-dismissible fade in" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                            aria-hidden="true">×</span>
                                </button>
                                <strong>{{ Session::get('message') }}</strong>
                            </div>
                        @endif
                        <div class="x_content">
                            <div class="table-responsive">
                                <table id="datatable-buttons"
                                       class="table table-striped table-bordered table-hover display">
                                    <thead>
                                    <tr class="alert-info">
                                        <th>First Name</th>
                                        <th>Last Name</th>
                                        <th>E-Mail</th>
                                        <th>Address</th>
                                        <th>Phone</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>


                                    <tbody>
                                    @foreach($allEmployeeList as $employeeList)

                                        <tr id="employee_id{{$employeeList->id}}">
                                            <td>{{$employeeList->emp_official_info->first_name}}</td>
                                            <td>{{$employeeList->emp_official_info->last_name}}</td>
                                            <td>{{$employeeList->emp_official_info->email}}</td>
                                            <td>{{$employeeList->emp_official_info->address}}</td>
                                            <td>{{$employeeList->emp_official_info->phone}}</td>
                                            <td>@if($employeeList->flag == 1)
                                                    {{ "Active" }}
                                                    @elseif($employeeList->flag == 2)
                                                        {{"Inactive"}}
                                                    @else
                                                        {!!"<span style='color:red'>Deleted</span>"!!}
                                                @endif
                                            </td>
                                            <td align="center">
                                                {{--<a href="{{route('employeeSetup.profile', $employeeList->id)}}"--}}
                                                   {{--class="btn btn-success btn-sm"><span class="glyphicon glyphicon-eye-open"></span></a>--}}
                                                <a href="{{ url('empProfile/'.$employeeList->id) }}" class="btn btn-success btn-sm"><span class="glyphicon glyphicon-eye-open"></span></a>
                                                <a href="{{route('employeeSetup.edit', $employeeList->id)}}"
                                                   class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-edit"></span></a>

                                                {{--<button class="btn confirm btn-danger btn-sm delete_p" data-toggle="modal" data-id="{{$employeeList->id}}"><span class="glyphicon glyphicon-trash"></span></button>--}}
                                            </td>
                                        </tr>

                                        {{--Modal--}}
                                        <div class="modal fade" id="myModal" aria-labelledby="myModalLabel" role="dialog">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header alert-danger">
                                                        <button type="button" class="close" data-dismiss="modal"
                                                                aria-label="Close"><span aria-hidden="true">&times;</span>
                                                        </button>
                                                        <h4 class="modal-title"><span class="glyphicon glyphicon-trash"></span>
                                                            Confirmation Message</h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        <p>Are you sure that you want to permanently delete the selected
                                                            element?</p>
                                                        <input type="hidden" id="d_id">
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-warning" data-dismiss="modal">No
                                                        </button>
                                                        <button type="button" class="btn btn-danger delete_employee"
                                                                data-dismiss="modal" value="{{$employeeList->id}}"
                                                                data-token="{{ csrf_token() }}" data-dismiss="modal">Delete
                                                        </button>
                                                    </div>
                                                </div><!-- /.modal-content -->
                                            </div><!-- /.modal-dialog -->
                                        </div><!-- /.modal -->

                                    @endforeach

                                    </tbody>
                                </table>


                            </div>
                        </div>

                        <meta name="_token" content="{!! csrf_token() !!}"/>
                        {{--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>--}}
                        {{--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>--}}
                    </div>
                </div>

            </div>
        </div>
    </div>


@endsection
@section('script')

    <script>

        setTimeout(function () {
            $("#successMessage").fadeOut('slow');
        }, 3000);

    </script>

    <script type="text/javascript">
        //        $(document).ready(function (){
        //

        $(document).on("click", ".delete_p", function () {
            var did = $(this).data('id');
            $("#d_id").val(did);
            $('#myModal').modal('show');
        });

        var url = "employee";

        $('.delete_employee').click(function () {
            var _id = $("#d_id").val();
            //alert(_id);
            console.log("ok");
            var id = _id;
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            })
            $.ajax({
                type: 'DELETE',
                url: url + '/' + id,
                success: function (data) {
                    console.log(id);
                    $("#employee_id" + id).remove();
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        });

        //        });
    </script>

    <script type="text/javascript">

        var handleDataTableButtons = function () {
            if ($("#datatable-buttons").length) {
                $("#datatable-buttons").DataTable({
                    dom: "Bfrtip",
                    buttons: [
                        {
                            extend: "copy",
                            className: "btn-sm"
                        },
                        {
                            extend: "csv",
                            className: "btn-sm"
                        },
                        {
                            extend: "excel",
                            className: "btn-sm"
                        },
                        {
                            extend: "pdfHtml5",
                            className: "btn-sm"
                        },
                        {
                            extend: "print",
                            className: "btn-sm"
                        },
                        {
                            extend: "colvis",
                            className: "btn-sm"
                        }
                    ],
                    responsive: false
                });
            }
        };

        TableManageButtons = function () {
            "use strict";
            return {
                init: function () {
                    handleDataTableButtons();
                }
            };
        }();

        $('#datatable').dataTable();

        $('#datatable-keytable').DataTable({
            keys: true
        });

        $('#datatable-responsive').DataTable();

        $('#datatable-scroller').DataTable({
            ajax: "js/datatables/json/scroller-demo.json",
            deferRender: true,
            scrollY: 380,
            scrollCollapse: true,
            scroller: true
        });

        $('#datatable-fixed-header').DataTable({
            fixedHeader: true
        });

        var $datatable = $('#datatable-checkbox');

        $datatable.dataTable({
            'order': [[1, 'asc']],
            'columnDefs': [
                {orderable: false, targets: [0]}
            ]
        });
        $datatable.on('draw.dt', function () {
            $('input').iCheck({
                checkboxClass: 'icheckbox_flat-green'
            });
        });

        TableManageButtons.init();

    </script>
@endsection