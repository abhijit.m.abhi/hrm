<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmployeeJobDetail extends Model
{
    protected $fillable = [
        'employmentStatus',
        'jobTitle',
        'joinedDate',
        'confirmationDate',
        'terminationDate',
        'department',
    ];

    public function dep_info()
    {
        return $this->belongsTo('App\CompanyStructure', 'department');

    }
    public function emp_jobDetails()
    {
        return $this->belongsTo('App\EmployeeSalarySetup');
    }

}
