@extends('layouts.master')
@section('title', 'Edit Employee')
@section('content')


    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3></h3>
                </div>


            </div>
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            {{--<h2>Add Employee<!--<small>Input different form elements</small>--></h2>--}}
                            <button type="button" class="btn btn-round btn-info">Edit Employee</button>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>

                        @if(Session::has('message'))

                            <div id="successMessage" class="alert alert-danger alert-dismissible fade in" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                            aria-hidden="true">×</span>
                                </button>
                                <strong>{{ Session::get('password_message') }}</strong>
                            </div>
                        @endif

                        <div class="x_content">
                            <br/>

                            <div id="wizard" class="form_wizard wizard_horizontal">
                                <ul class="wizard_steps">
                                    <li>
                                        <a href="#step-1">
                                            <span class="step_no">1</span>
                                            <span class="step_descr">
                                              Step 1<br/>
                                              <small>Personal Info</small>
                                          </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#step-2">
                                            <span class="step_no">2</span>
                                            <span class="step_descr">
                                              Step 2<br/>
                                              <small>Personal Address</small>
                                          </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#step-3">
                                            <span class="step_no">3</span>
                                            <span class="step_descr">
                                              Step 3<br/>
                                              <small>Job Info</small>
                                          </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#step-4">
                                            <span class="step_no">4</span>
                                            <span class="step_descr">
                                              Step 4<br/>
                                              <small>Qualification Info</small>
                                          </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#step-5">
                                            <span class="step_no">5</span>
                                            <span class="step_descr">
                                              Step 5<br/>
                                              <small>Emergencey Contact Info</small>
                                          </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#step-6">
                                            <span class="step_no">6</span>
                                            <span class="step_descr">
                                              Step 6<br/>
                                              <small>Experience Info</small>
                                          </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#step-7">
                                            <span class="step_no">7</span>
                                            <span class="step_descr">
                                              Step 7<br/>
                                              <small>Attachments</small>
                                          </span>
                                        </a>
                                    </li>
                                </ul>

                                <form action="{{url('employeeSetup/'.$employee->id)}}" method="POST"
                                      class="form-horizontal form-label-left" role="form"
                                      files="true" enctype="multipart/form-data">
                                    {{ method_field('PUT') }}
                                    {{ csrf_field() }}
                                    <div class="divider-dashed"></div>
                                    <div id="step-1">

                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="empCode">Employee
                                                Code <span class="required" style="color: red;">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="text" id="empCode" name="empCode" value="{{$employee->emp_official_info->employee_code}}"
                                                       placeholder="Employee Code" required="required"
                                                       class="form-control has-feedback-left col-md-7 col-xs-12">
                                                <span class="fa fa-credit-card form-control-feedback left"
                                                      aria-hidden="true"></span>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="firstName">First
                                                Name <span class="required" style="color: red;">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="text" id="firstName" name="firstName" value="{{$employee->fname}}"
                                                       placeholder="First Name" required="required"
                                                       class="form-control has-feedback-left col-md-7 col-xs-12">
                                                <span class="fa fa-user form-control-feedback left"
                                                      aria-hidden="true"></span>
                                            </div>
                                        </div>
                                        {{--<div class="form-group">--}}
                                            {{--<label for="middleName" class="control-label col-md-3 col-sm-3 col-xs-12">Middle--}}
                                                {{--Name </label>--}}
                                            {{--<div class="col-md-6 col-sm-6 col-xs-12">--}}
                                                {{--<input id="middleName" name="middleName" placeholder="Middle Name"--}}
                                                       {{--class="form-control has-feedback-left col-md-7 col-xs-12"--}}
                                                       {{--type="text">--}}
                                                {{--<span class="fa fa-user form-control-feedback left"--}}
                                                      {{--aria-hidden="true"></span>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="lastName">Last
                                                Name <span class="required" style="color: red;">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="text" id="lastName" name="lastName" value="{{$employee->lname}}" placeholder="Last Name"
                                                       required="required"
                                                       class="form-control has-feedback-left col-md-7 col-xs-12">
                                                <span class="fa fa-user form-control-feedback left"
                                                      aria-hidden="true"></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nationality">Nationality
                                                <span class="required" style="color: red;">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="text" id="nationality" name="nationality" value="{{$employee->emp_official_info->nationality}}"
                                                       placeholder="Nationality" required="required"
                                                       class="form-control has-feedback-left col-md-7 col-xs-12">
                                                <span class="fa fa-flag form-control-feedback left"
                                                      aria-hidden="true"></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nid">NID No.
                                                <span class="required" style="color: red;">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="text" id="nid" name="nid" value="{{$employee->emp_official_info->nid}}" placeholder="NID No."
                                                       required="required"
                                                       class="form-control has-feedback-left col-md-7 col-xs-12">
                                                <span class="fa fa-credit-card form-control-feedback left"
                                                      aria-hidden="true"></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <?php
                                            $dob_date_formatted = date('m-d-Y',strtotime($employee->emp_official_info->date_of_birth));
                                            ?>
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="dateOfBirth">Date
                                                of Birth <span class="required" style="color: red;">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="text" name="dateOfBirth"
                                                       class="form-control has-feedback-left single_cal3" id=""
                                                       value="{{$dob_date_formatted}}" aria-describedby="inputSuccess2Status3"
                                                       required>
                                                <span class="fa fa-birthday-cake form-control-feedback left"
                                                      aria-hidden="true"></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="birthId">Birth
                                                ID No.</label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="text" id="birthId" name="birthId" value="{{$employee->emp_official_info->birth_certificate_id}}"
                                                       placeholder="Birth ID No."
                                                       class="form-control has-feedback-left col-md-7 col-xs-12">
                                                <span class="fa fa-credit-card form-control-feedback left"
                                                      aria-hidden="true"></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="passportNo">Passport
                                                No.</label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="text" id="passportNo" name="passportNo" value="{{$employee->emp_official_info->passport_no}}"
                                                       placeholder="Passport No."
                                                       class="form-control has-feedback-left col-md-7 col-xs-12">
                                                <span class="fa fa-credit-card form-control-feedback left"
                                                      aria-hidden="true"></span>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Gender <span
                                                        class="required" style="color: red;">*</span></label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <p>
                                                    <input type="radio" class="flat" name="gender" id="genderM" value="Male" <?php echo ($employee->emp_official_info->gender=='Male')?'checked':'' ?>  required /> Male
                                                    <input type="radio" class="flat" name="gender" id="genderF" value="Female" <?php echo ($employee->emp_official_info->gender=='Female')?'checked':'' ?>  /> Female
                                                </p>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Marital Status
                                                <span class="required" style="color: red;">*</span></label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <select name="maritalStatus"
                                                        class="form-control col-md-7 col-xs-12 has-feedback-left"
                                                        required>
                                                    <option value="{{$employee->emp_official_info->marital_status}}">{{$employee->emp_official_info->marital_status}}</option>
                                                    <option value="" disabled >Choose your Martital Status
                                                    </option>
                                                    <option value="Married" <?php echo ($employee->emp_official_info->marital_status=='Married') ? 'style="display:none;"' : '' ?>>Married</option>
                                                    <option value="Widowed" <?php echo ($employee->emp_official_info->marital_status=='Widowed') ? 'style="display:none;"' : '' ?>>Widowed</option>
                                                    <option value="Separated" <?php echo ($employee->emp_official_info->marital_status=='Separated') ? 'style="display:none;"' : '' ?>>Separated</option>
                                                    <option value="Divorced" <?php echo ($employee->emp_official_info->marital_status=='Divorced') ? 'style="display:none;"' : '' ?>>Divorced</option>
                                                    <option value="Single"<?php echo ($employee->emp_official_info->marital_status=='Single') ? 'style="display:none;"' : '' ?> >Single</option>
                                                </select>
                                                <span class="fa fa-caret-square-o-down form-control-feedback left"
                                                      aria-hidden="true"></span>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="exampleInputFile" class="control-label col-md-3 col-sm-3 col-xs-12">Photo</label>
                                            <div class="col-md-3">
                                                <img src="{{ asset('/images/users/'.$employee->photo) }}" alt="User Photo"
                                                     height="120"
                                                     width="120">
                                            </div>
                                            <div class="col-md-6">
                                                {!! Form::file('photo') !!}
                                                @if ($errors->has('photo'))
                                                    <span class="help-block">
                                                <strong>{{ $errors->first('photo') }}</strong>
                                            </span>
                                                @endif
                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12"></label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                            <span class="text-muted">
                                                <em>
                                                    <span style="color: red;">*</span>
                                                     Indicates required field
                                                </em>
                                            </span>
                                            </div>
                                        </div>

                                    </div>

                                    <div id="step-2">

                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="address">Address
                                                <span class="required" style="color: red;">*</span> </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <textarea rows="3" cols="20" class="form-control col-md-7 col-xs-12"
                                                          placeholder="Address" name="address" required>{{$employee->emp_official_info->address}}</textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="city">City
                                                <span class="required" style="color: red;">*</span></label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="text" id="city" name="city" value="{{$employee->emp_official_info->city}}" placeholder="City"
                                                       class="form-control has-feedback-left col-md-7 col-xs-12"
                                                       required>
                                                <span class="fa fa-building form-control-feedback left"
                                                      aria-hidden="true"></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="city">Postal
                                                or ZIP Code <span class="required" style="color: red;">*</span></label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="text" id="postalCode" name="postalCode" value="{{$employee->emp_official_info->postal_or_zip_code}}"
                                                       placeholder="Postal or ZIP Code"
                                                       class="form-control has-feedback-left col-md-7 col-xs-12"
                                                       required>
                                                <span class="fa fa-paper-plane form-control-feedback left"
                                                      aria-hidden="true"></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Country <span
                                                        class="required" style="color: red;">*</span></label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <select name="country" class="select2_single form-control" tabindex="-1"
                                                        required>
                                                    <option value="{{$employee->emp_official_info->country}}">{{$employee->emp_official_info->country}}</option>
                                                    <option value="Afghanistan">Afghanistan</option>
                                                    <option value="Albania">Albania</option>
                                                    <option value="Algeria">Algeria</option>
                                                    <option value="American Samoa">American Samoa</option>
                                                    <option value="Andorra">Andorra</option>
                                                    <option value="Angola">Angola</option>
                                                    <option value="Anguilla">Anguilla</option>
                                                    <option value="Antartica">Antarctica</option>
                                                    <option value="Antigua and Barbuda">Antigua and Barbuda</option>
                                                    <option value="Argentina">Argentina</option>
                                                    <option value="Armenia">Armenia</option>
                                                    <option value="Aruba">Aruba</option>
                                                    <option value="Australia">Australia</option>
                                                    <option value="Austria">Austria</option>
                                                    <option value="Azerbaijan">Azerbaijan</option>
                                                    <option value="Bahamas">Bahamas</option>
                                                    <option value="Bahrain">Bahrain</option>
                                                    <option value="Bangladesh">Bangladesh</option>
                                                    <option value="Barbados">Barbados</option>
                                                    <option value="Belarus">Belarus</option>
                                                    <option value="Belgium">Belgium</option>
                                                    <option value="Belize">Belize</option>
                                                    <option value="Benin">Benin</option>
                                                    <option value="Bermuda">Bermuda</option>
                                                    <option value="Bhutan">Bhutan</option>
                                                    <option value="Bolivia">Bolivia</option>
                                                    <option value="Bosnia and Herzegowina">Bosnia and Herzegowina
                                                    </option>
                                                    <option value="Botswana">Botswana</option>
                                                    <option value="Bouvet Island">Bouvet Island</option>
                                                    <option value="Brazil">Brazil</option>
                                                    <option value="British Indian Ocean Territory">British Indian Ocean
                                                        Territory
                                                    </option>
                                                    <option value="Brunei Darussalam">Brunei Darussalam</option>
                                                    <option value="Bulgaria">Bulgaria</option>
                                                    <option value="Burkina Faso">Burkina Faso</option>
                                                    <option value="Burundi">Burundi</option>
                                                    <option value="Cambodia">Cambodia</option>
                                                    <option value="Cameroon">Cameroon</option>
                                                    <option value="Canada">Canada</option>
                                                    <option value="Cape Verde">Cape Verde</option>
                                                    <option value="Cayman Islands">Cayman Islands</option>
                                                    <option value="Central African Republic">Central African Republic
                                                    </option>
                                                    <option value="Chad">Chad</option>
                                                    <option value="Chile">Chile</option>
                                                    <option value="China">China</option>
                                                    <option value="Christmas Island">Christmas Island</option>
                                                    <option value="Cocos Islands">Cocos (Keeling) Islands</option>
                                                    <option value="Colombia">Colombia</option>
                                                    <option value="Comoros">Comoros</option>
                                                    <option value="Congo">Congo</option>
                                                    <option value="Congo">Congo, the Democratic Republic of the</option>
                                                    <option value="Cook Islands">Cook Islands</option>
                                                    <option value="Costa Rica">Costa Rica</option>
                                                    <option value="Cota D'Ivoire">Cote d'Ivoire</option>
                                                    <option value="Croatia">Croatia (Hrvatska)</option>
                                                    <option value="Cuba">Cuba</option>
                                                    <option value="Cyprus">Cyprus</option>
                                                    <option value="Czech Republic">Czech Republic</option>
                                                    <option value="Denmark">Denmark</option>
                                                    <option value="Djibouti">Djibouti</option>
                                                    <option value="Dominica">Dominica</option>
                                                    <option value="Dominican Republic">Dominican Republic</option>
                                                    <option value="East Timor">East Timor</option>
                                                    <option value="Ecuador">Ecuador</option>
                                                    <option value="Egypt">Egypt</option>
                                                    <option value="El Salvador">El Salvador</option>
                                                    <option value="Equatorial Guinea">Equatorial Guinea</option>
                                                    <option value="Eritrea">Eritrea</option>
                                                    <option value="Estonia">Estonia</option>
                                                    <option value="Ethiopia">Ethiopia</option>
                                                    <option value="Falkland Islands">Falkland Islands (Malvinas)
                                                    </option>
                                                    <option value="Faroe Islands">Faroe Islands</option>
                                                    <option value="Fiji">Fiji</option>
                                                    <option value="Finland">Finland</option>
                                                    <option value="France">France</option>
                                                    <option value="France Metropolitan">France, Metropolitan</option>
                                                    <option value="French Guiana">French Guiana</option>
                                                    <option value="French Polynesia">French Polynesia</option>
                                                    <option value="French Southern Territories">French Southern
                                                        Territories
                                                    </option>
                                                    <option value="Gabon">Gabon</option>
                                                    <option value="Gambia">Gambia</option>
                                                    <option value="Georgia">Georgia</option>
                                                    <option value="Germany">Germany</option>
                                                    <option value="Ghana">Ghana</option>
                                                    <option value="Gibraltar">Gibraltar</option>
                                                    <option value="Greece">Greece</option>
                                                    <option value="Greenland">Greenland</option>
                                                    <option value="Grenada">Grenada</option>
                                                    <option value="Guadeloupe">Guadeloupe</option>
                                                    <option value="Guam">Guam</option>
                                                    <option value="Guatemala">Guatemala</option>
                                                    <option value="Guinea">Guinea</option>
                                                    <option value="Guinea-Bissau">Guinea-Bissau</option>
                                                    <option value="Guyana">Guyana</option>
                                                    <option value="Haiti">Haiti</option>
                                                    <option value="Heard and McDonald Islands">Heard and Mc Donald
                                                        Islands
                                                    </option>
                                                    <option value="Holy See">Holy See (Vatican City State)</option>
                                                    <option value="Honduras">Honduras</option>
                                                    <option value="Hong Kong">Hong Kong</option>
                                                    <option value="Hungary">Hungary</option>
                                                    <option value="Iceland">Iceland</option>
                                                    <option value="India">India</option>
                                                    <option value="Indonesia">Indonesia</option>
                                                    <option value="Iran">Iran (Islamic Republic of)</option>
                                                    <option value="Iraq">Iraq</option>
                                                    <option value="Ireland">Ireland</option>
                                                    <option value="Israel">Israel</option>
                                                    <option value="Italy">Italy</option>
                                                    <option value="Jamaica">Jamaica</option>
                                                    <option value="Japan">Japan</option>
                                                    <option value="Jordan">Jordan</option>
                                                    <option value="Kazakhstan">Kazakhstan</option>
                                                    <option value="Kenya">Kenya</option>
                                                    <option value="Kiribati">Kiribati</option>
                                                    <option value="Democratic People's Republic of Korea">Korea,
                                                        Democratic People's Republic of
                                                    </option>
                                                    <option value="Korea">Korea, Republic of</option>
                                                    <option value="Kuwait">Kuwait</option>
                                                    <option value="Kyrgyzstan">Kyrgyzstan</option>
                                                    <option value="Lao">Lao People's Democratic Republic</option>
                                                    <option value="Latvia">Latvia</option>
                                                    <option value="Lebanon">Lebanon</option>
                                                    <option value="Lesotho">Lesotho</option>
                                                    <option value="Liberia">Liberia</option>
                                                    <option value="Libyan Arab Jamahiriya">Libyan Arab Jamahiriya
                                                    </option>
                                                    <option value="Liechtenstein">Liechtenstein</option>
                                                    <option value="Lithuania">Lithuania</option>
                                                    <option value="Luxembourg">Luxembourg</option>
                                                    <option value="Macau">Macau</option>
                                                    <option value="Macedonia">Macedonia, The Former Yugoslav Republic
                                                        of
                                                    </option>
                                                    <option value="Madagascar">Madagascar</option>
                                                    <option value="Malawi">Malawi</option>
                                                    <option value="Malaysia">Malaysia</option>
                                                    <option value="Maldives">Maldives</option>
                                                    <option value="Mali">Mali</option>
                                                    <option value="Malta">Malta</option>
                                                    <option value="Marshall Islands">Marshall Islands</option>
                                                    <option value="Martinique">Martinique</option>
                                                    <option value="Mauritania">Mauritania</option>
                                                    <option value="Mauritius">Mauritius</option>
                                                    <option value="Mayotte">Mayotte</option>
                                                    <option value="Mexico">Mexico</option>
                                                    <option value="Micronesia">Micronesia, Federated States of</option>
                                                    <option value="Moldova">Moldova, Republic of</option>
                                                    <option value="Monaco">Monaco</option>
                                                    <option value="Mongolia">Mongolia</option>
                                                    <option value="Montserrat">Montserrat</option>
                                                    <option value="Morocco">Morocco</option>
                                                    <option value="Mozambique">Mozambique</option>
                                                    <option value="Myanmar">Myanmar</option>
                                                    <option value="Namibia">Namibia</option>
                                                    <option value="Nauru">Nauru</option>
                                                    <option value="Nepal">Nepal</option>
                                                    <option value="Netherlands">Netherlands</option>
                                                    <option value="Netherlands Antilles">Netherlands Antilles</option>
                                                    <option value="New Caledonia">New Caledonia</option>
                                                    <option value="New Zealand">New Zealand</option>
                                                    <option value="Nicaragua">Nicaragua</option>
                                                    <option value="Niger">Niger</option>
                                                    <option value="Nigeria">Nigeria</option>
                                                    <option value="Niue">Niue</option>
                                                    <option value="Norfolk Island">Norfolk Island</option>
                                                    <option value="Northern Mariana Islands">Northern Mariana Islands
                                                    </option>
                                                    <option value="Norway">Norway</option>
                                                    <option value="Oman">Oman</option>
                                                    <option value="Pakistan">Pakistan</option>
                                                    <option value="Palau">Palau</option>
                                                    <option value="Panama">Panama</option>
                                                    <option value="Papua New Guinea">Papua New Guinea</option>
                                                    <option value="Paraguay">Paraguay</option>
                                                    <option value="Peru">Peru</option>
                                                    <option value="Philippines">Philippines</option>
                                                    <option value="Pitcairn">Pitcairn</option>
                                                    <option value="Poland">Poland</option>
                                                    <option value="Portugal">Portugal</option>
                                                    <option value="Puerto Rico">Puerto Rico</option>
                                                    <option value="Qatar">Qatar</option>
                                                    <option value="Reunion">Reunion</option>
                                                    <option value="Romania">Romania</option>
                                                    <option value="Russia">Russian Federation</option>
                                                    <option value="Rwanda">Rwanda</option>
                                                    <option value="Saint Kitts and Nevis">Saint Kitts and Nevis</option>
                                                    <option value="Saint LUCIA">Saint LUCIA</option>
                                                    <option value="Saint Vincent">Saint Vincent and the Grenadines
                                                    </option>
                                                    <option value="Samoa">Samoa</option>
                                                    <option value="San Marino">San Marino</option>
                                                    <option value="Sao Tome and Principe">Sao Tome and Principe</option>
                                                    <option value="Saudi Arabia">Saudi Arabia</option>
                                                    <option value="Senegal">Senegal</option>
                                                    <option value="Seychelles">Seychelles</option>
                                                    <option value="Sierra">Sierra Leone</option>
                                                    <option value="Singapore">Singapore</option>
                                                    <option value="Slovakia">Slovakia (Slovak Republic)</option>
                                                    <option value="Slovenia">Slovenia</option>
                                                    <option value="Solomon Islands">Solomon Islands</option>
                                                    <option value="Somalia">Somalia</option>
                                                    <option value="South Africa">South Africa</option>
                                                    <option value="South Georgia">South Georgia and the South Sandwich
                                                        Islands
                                                    </option>
                                                    <option value="Span">Spain</option>
                                                    <option value="SriLanka">Sri Lanka</option>
                                                    <option value="St. Helena">St. Helena</option>
                                                    <option value="St. Pierre and Miguelon">St. Pierre and Miquelon
                                                    </option>
                                                    <option value="Sudan">Sudan</option>
                                                    <option value="Suriname">Suriname</option>
                                                    <option value="Svalbard">Svalbard and Jan Mayen Islands</option>
                                                    <option value="Swaziland">Swaziland</option>
                                                    <option value="Sweden">Sweden</option>
                                                    <option value="Switzerland">Switzerland</option>
                                                    <option value="Syria">Syrian Arab Republic</option>
                                                    <option value="Taiwan">Taiwan, Province of China</option>
                                                    <option value="Tajikistan">Tajikistan</option>
                                                    <option value="Tanzania">Tanzania, United Republic of</option>
                                                    <option value="Thailand">Thailand</option>
                                                    <option value="Togo">Togo</option>
                                                    <option value="Tokelau">Tokelau</option>
                                                    <option value="Tonga">Tonga</option>
                                                    <option value="Trinidad and Tobago">Trinidad and Tobago</option>
                                                    <option value="Tunisia">Tunisia</option>
                                                    <option value="Turkey">Turkey</option>
                                                    <option value="Turkmenistan">Turkmenistan</option>
                                                    <option value="Turks and Caicos">Turks and Caicos Islands</option>
                                                    <option value="Tuvalu">Tuvalu</option>
                                                    <option value="Uganda">Uganda</option>
                                                    <option value="Ukraine">Ukraine</option>
                                                    <option value="United Arab Emirates">United Arab Emirates</option>
                                                    <option value="United Kingdom">United Kingdom</option>
                                                    <option value="United States">United States</option>
                                                    <option value="United States Minor Outlying Islands">United States
                                                        Minor Outlying Islands
                                                    </option>
                                                    <option value="Uruguay">Uruguay</option>
                                                    <option value="Uzbekistan">Uzbekistan</option>
                                                    <option value="Vanuatu">Vanuatu</option>
                                                    <option value="Venezuela">Venezuela</option>
                                                    <option value="Vietnam">Viet Nam</option>
                                                    <option value="Virgin Islands (British)">Virgin Islands (British)
                                                    </option>
                                                    <option value="Virgin Islands (U.S)">Virgin Islands (U.S.)</option>
                                                    <option value="Wallis and Futana Islands">Wallis and Futuna
                                                        Islands
                                                    </option>
                                                    <option value="Western Sahara">Western Sahara</option>
                                                    <option value="Yemen">Yemen</option>
                                                    <option value="Yugoslavia">Yugoslavia</option>
                                                    <option value="Zambia">Zambia</option>
                                                    <option value="Zimbabwe">Zimbabwe</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12"
                                                   for="email">Email</label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="email" id="email" name="email" value="{{$employee->emp_official_info->email}}" placeholder="Email"
                                                       class="form-control has-feedback-left col-md-7 col-xs-12" readonly>
                                                <span class="fa fa-envelope form-control-feedback left"
                                                      aria-hidden="true"></span>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="phone">Phone
                                                <span class="required" style="color: red;">*</span></label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="text" id="phone" name="phone" value="{{$employee->emp_official_info->phone}}"  placeholder="Phone"
                                                       class="form-control has-feedback-left col-md-7 col-xs-12"
                                                       required>
                                                <span class="fa fa-mobile-phone form-control-feedback left"
                                                      aria-hidden="true"></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="phone">Office
                                                Phone</label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="text" id="officePhone" name="officePhone" value="{{$employee->emp_official_info->work_phone}}"
                                                       placeholder="Office Phone"
                                                       class="form-control has-feedback-left col-md-7 col-xs-12">
                                                <span class="fa fa-phone form-control-feedback left"
                                                      aria-hidden="true"></span>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12"></label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                            <span class="text-muted">
                                                <em>
                                                    <span style="color: red;">*</span>
                                                     Indicates required field
                                                </em>
                                            </span>
                                            </div>
                                        </div>

                                    </div>

                                    <div id="step-3">

                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Employment Status
                                                <span class="required" style="color: red;">*</span></label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <select name="employmentStatus"
                                                        class="form-control col-md-7 col-xs-12 has-feedback-left"
                                                        required>
                                                    <option value="{{$employee->emp_official_info->emp_job_details->employment_status}}">{{$employee->emp_official_info->emp_job_details->employment_status}}</option>
                                                    <option value="" disabled>Choose a Status</option>
                                                    <option value="Full Time">Full Time</option>
                                                    <option value="Part Time">Part Time</option>
                                                    <option value="Conditional">Conditional</option>
                                                    <option value="Daily">Daily</option>
                                                </select>
                                                <span class="fa fa-caret-square-o-down form-control-feedback left"
                                                      aria-hidden="true"></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Job Title <span
                                                        class="required" style="color: red;">*</span></label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">

                                                <select name="jobTitle"
                                                        class="form-control col-md-7 col-xs-12 has-feedback-left"
                                                        required>
                                                    <option value="{{$employee->emp_official_info->emp_job_details->job_title}}">{{$employee->emp_official_info->emp_job_details->job_title}}</option>
                                                    <option value="" disabled>Choose a Title</option>
                                                    @foreach($jobTitleList as $jobTitle)
                                                        <option value="{{$jobTitle}}"> {{$jobTitle}}  </option>
                                                    @endforeach
                                                </select>

                                                <span class="fa fa-caret-square-o-down form-control-feedback left"
                                                      aria-hidden="true"></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <?php
                                            $joined_date_formatted = date('m-d-Y',strtotime($employee->emp_official_info->emp_job_details->joined_date));
                                            ?>
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="joinedDate">Joined
                                                Date <span class="required" style="color: red;">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="text" name="joinedDate" value="{{$joined_date_formatted}}"
                                                       class="form-control has-feedback-left single_cal3" id=""
                                                       aria-describedby="inputSuccess2Status3" required>
                                                <span class="fa fa-calendar form-control-feedback left"
                                                      aria-hidden="true"></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <?php
                                            $confirmation_date_formatted = date('m-d-Y',strtotime($employee->emp_official_info->emp_job_details->confirmation_date));
                                            ?>
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12"
                                                   for="confirmationDate">Confirmation Date
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="text" name="confirmationDate" value="{{$confirmation_date_formatted}}"
                                                       class="form-control has-feedback-left single_cal3" id=""
                                                       value="12/31/9999" aria-describedby="inputSuccess2Status3">
                                                <span class="fa fa-calendar form-control-feedback left"
                                                      aria-hidden="true"></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <?php
                                            $termination_date_formatted = date('m-d-Y',strtotime($employee->emp_official_info->emp_job_details->termination_date));
                                            ?>
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12"
                                                   for="terminationDate">Termination Date
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="text" name="terminationDate" value="{{$termination_date_formatted}}"
                                                       class="form-control has-feedback-left single_cal3" id=""
                                                       value="12/31/9999" aria-describedby="inputSuccess2Status3">
                                                <span class="fa fa-calendar form-control-feedback left"
                                                      aria-hidden="true"></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Department <span
                                                        class="required" style="color: red;">*</span></label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <select name="department"
                                                        class="form-control col-md-7 col-xs-12 has-feedback-left"
                                                        required>
                                                    <option value="{{$employee->emp_official_info->emp_job_details->department}}">{{$employee->emp_official_info->emp_job_details->department}}</option>
                                                    <option value="" disabled>Choose a Department</option>
                                                    @foreach($companyDeptList as $companyDept)
                                                        <option value="{{$companyDept}}">{{$companyDept}}</option>
                                                    @endforeach
                                                </select>
                                                <span class="fa fa-caret-square-o-down form-control-feedback left"
                                                      aria-hidden="true"></span>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12"></label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                            <span class="text-muted">
                                                <em>
                                                    <span style="color: red;">*</span>
                                                     Indicates required field
                                                </em>
                                            </span>
                                            </div>
                                        </div>

                                    </div>
                                    <div id="step-4">

                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Skill <span
                                                        class="required" style="color: red;">*</span></label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <select name="skill[][name]"
                                                        class="select2_multiple_skill  form-control col-md-7 col-xs-12"
                                                        multiple="multiple" required>
                                                    <?php
                                                        $skills =$employee->emp_official_info->emp_qualifications->skill;
                                                        $skill = json_decode($skills );
                                                        foreach ($skill as $sk)
                                                        {
                                                            echo "<option selected='selected' >".$sk."</option>";
                                                        }
                                                    ?>
                                                    @foreach($skillList as $skill)
                                                        <option value="{{$skill}}">{{$skill}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Education <span
                                                        class="required" style="color: red;">*</span></label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <select name="qualification[][name]"
                                                        class="select2_multiple_education form-control col-md-7 col-xs-12"
                                                        multiple="multiple" required>
                                                    <?php
                                                    $qualifications =$employee->emp_official_info->emp_qualifications->qualification;
                                                    $qualification = json_decode($qualifications);
                                                    foreach ($qualification as $qua)
                                                    {
                                                        echo "<option selected='selected' >".$qua."</option>";
                                                    }
                                                    ?>


                                                    @foreach($qualificationList as $qualification)
                                                        <option value="{{$qualification}}">{{$qualification}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Certification <span
                                                        class="required" style="color: red;">*</span></label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <select name="certification[][name]"
                                                        class="select2_multiple_certification form-control col-md-7 col-xs-12"
                                                        multiple="multiple" required>
                                                    <?php
                                                    $certifications =$employee->emp_official_info->emp_qualifications->certification;
                                                    $certification = json_decode($certifications);
                                                    foreach ($certification as $cer)
                                                    {
                                                        echo "<option selected='selected' >".$cer."</option>";
                                                    }
                                                    ?>

                                                    @foreach($certificationList as $certification)
                                                        <option value="{{$certification}}">{{$certification}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12"></label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                            <span class="text-muted">
                                                <em>
                                                    <span style="color: red;">*</span>
                                                     Indicates required field
                                                </em>
                                            </span>
                                            </div>
                                        </div>
                                    </div>

                                    <div id="step-5">

                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="emrName">Emergency
                                                Contact Person <span class="required" style="color: red;">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">

                                                <input type="text" id="emrName" name="emrName" value="{{$employee->emp_official_info->emp_emergency_contact->name}}"
                                                       placeholder="Emergency Contact Person" required="required"
                                                       class="form-control has-feedback-left col-md-7 col-xs-12">
                                                <span class="fa fa-user form-control-feedback left"
                                                      aria-hidden="true"></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="relation">Relation<span
                                                        class="required" style="color: red;">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="text" id="relation" name="relation" value="{{$employee->emp_official_info->emp_emergency_contact->relation}}" placeholder="Relation"
                                                       required="required"
                                                       class="form-control has-feedback-left col-md-7 col-xs-12">
                                                <span class="fa fa-users form-control-feedback left"
                                                      aria-hidden="true"></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="emrAddress">Address
                                                <span class="required" style="color: red;">*</span> </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <textarea rows="3" cols="20" class="form-control col-md-7 col-xs-12"
                                                          placeholder="Address" name="emrAddress" required>{{$employee->emp_official_info->emp_emergency_contact->address}}</textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="emrPhone">Phone
                                                <span class="required" style="color: red;">*</span></label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="text" id="emrPhone" name="emrPhone" value="{{$employee->emp_official_info->emp_emergency_contact->mobile}}"
                                                       placeholder="Emergency Phone"
                                                       class="form-control has-feedback-left col-md-7 col-xs-12"
                                                       required>
                                                <span class="fa fa-mobile-phone form-control-feedback left"
                                                      aria-hidden="true"></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12"></label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                            <span class="text-muted">
                                                <em>
                                                    <span style="color: red;">*</span>
                                                     Indicates required field
                                                </em>
                                            </span>
                                            </div>
                                        </div>
                                    </div>

                                    <div id="step-6">

                                        @foreach($employee->emp_official_info->emp_experience as $emp_exp)

                                        <div id="">

                                            <div class="form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12"
                                                       for="organization">Organization Name <span class="required"
                                                                                                  style="color: red;">*</span>
                                                </label>
                                                <div class="col-md-6 col-sm-6 col-xs-12">


                                                    <input type="text" id="organization" name="organization[]" value="{{$emp_exp ->organization_name}}"
                                                           placeholder="Organization Name" required="required"
                                                           class="form-control has-feedback-left col-md-7 col-xs-12">
                                                    <span class="fa fa-suitcase form-control-feedback left"
                                                          aria-hidden="true"></span>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12"
                                                       for="orgaAddress">Organization Address <span class="required"
                                                                                                    style="color: red;">*</span>
                                                </label>
                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                    <textarea rows="3" cols="20" class="form-control col-md-7 col-xs-12"
                                                              placeholder="Organization Address" name="orgaAddress[]"
                                                              required>{{$emp_exp ->address}}</textarea>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="emrPhone">Contact
                                                    No. <span class="required" style="color: red;">*</span></label>
                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                    <input type="text" id="contactNo" name="contactNo[]" value="{{$emp_exp ->contact_no}}"
                                                           placeholder="Contact No."
                                                           class="form-control has-feedback-left col-md-7 col-xs-12"
                                                           required>
                                                    <span class="fa fa-mobile-phone form-control-feedback left"
                                                          aria-hidden="true"></span>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="duration">Duration
                                                    <span class="required" style="color: red;">*</span></label>
                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                    <input type="text" id="duration" name="duration[]" value="{{$emp_exp ->duration}}"
                                                           placeholder="Duration"
                                                           class="form-control has-feedback-left col-md-7 col-xs-12"
                                                           required>
                                                    <span class="fa fa-mobile-phone form-control-feedback left"
                                                          aria-hidden="true"></span>
                                                </div>
                                            </div>
                                        </div>

                                        <hr><br>




                                        @endforeach

                                            <div id="form_experience">
                                            </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12"></label>
                                            <div class="col-md-3 col-sm-3 col-xs-12">
                                                <button type="button" class="btn btn-primary" id="add_more_experience">
                                                    Add More Experience
                                                </button>
                                            </div>

                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12"></label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                        <span class="text-muted">
                                            <em>
                                                <span style="color: red;">*</span>
                                                 Indicates required field
                                            </em>
                                        </span>
                                            </div>
                                        </div>
                                    </div>


                                    <div id="step-7">

                                        @foreach($employee->emp_official_info->emp_attachment as $emp_attach )

                                        <div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Title</label>
                                                <div class="col-md-2 col-sm-3 col-xs-12">
                                                    <input type="text" class="form-control"
                                                           placeholder="Title of Attachment" name="title[]" value="{{$emp_attach->title}}" required>
                                                </div>
                                                <div class="col-md-2">
                                                    <img src="{{ asset($emp_attach->location) }}" alt="User Photo"
                                                         height="80"
                                                         width="80">
                                                </div>
                                                <div class="col-md-3">
                                                    <input type="file" name="file[]" value="{{$emp_attach->file_name}}" >
                                                </div>
                                                <div class="col-md-1">
                                                    <button type="button"
                                                            class="btn btn-sm btn-danger btn-circle removeFile"><i
                                                                class="glyphicon glyphicon-remove"></i></button>
                                                </div>
                                            </div>
                                        </div>

                                        @endforeach

                                        <div id="form-pi"></div>


                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12"></label>
                                            <div class="col-md-3 col-sm-3 col-xs-12">
                                                <button type="button" class="btn btn-primary" id="add_more_attach">Add
                                                    More File
                                                </button>
                                            </div>

                                        </div>

                                        <div class="ln_solid"></div>
                                        <div class="form-group">
                                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                                <a type="cancel" href="{{ url('') }}" class="btn btn-danger">Cancel</a>
                                                <button type="submit" class="btn btn-primary">Add</button>
                                            </div>
                                        </div>

                                    </div>


                                </form>

                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection

@section('script')


    {{-- Start Seccuess Message Script--}}
    {{--<script>--}}

    {{--setTimeout(function () {--}}
    {{--$("#successMessage").fadeOut('slow');--}}
    {{--}, 3000);--}}

    {{--</script>--}}
    {{-- End Seccuess Message Script--}}

    {{--Start Form Valiation Script--}}
    <script>
        // validate a field on "blur" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':
        $('form')
            .on('blur', 'input[required], input.optional, select.required', validator.checkField)
            .on('change', 'select.required', validator.checkField)
            .on('keypress', 'input[required][pattern]', validator.keypress);

        $('.multi.required').on('keyup blur', 'input', function () {
            validator.checkField.apply($(this).siblings().last()[0]);
        });

        $('form').submit(function (e) {
            e.preventDefault();
            var submit = true;

            // evaluate the form using generic validaing
            if (!validator.checkAll($(this))) {
                submit = false;
            }

            if (submit)
                this.submit();

            return false;
        });

    </script>
    {{--End Form Valiation Script--}}


    {{--DatePicker--}}
    <script>
        $('.single_cal3').daterangepicker({
            singleDatePicker: true,
            singleClasses: "picker_3"
        }, function (start, end, label) {
            console.log(start.toISOString(), end.toISOString(), label);
        });

    </script>

    <script>
        $(document).on('click', '#add_more_experience', function () {
            $('#form_experience').append('<div><br/><hr/><div class="form-group"> ' +
                '<label class="control-label col-md-3 col-sm-3 col-xs-12" for="organization">Organization Name <span class="required" style="color: red;">*</span> ' +
                '</label> ' +
                '<div class="col-md-6 col-sm-6 col-xs-12"> ' +
                '<input type="text" id="organization" name="organization[]" placeholder="Organization Name" required="required" class="form-control has-feedback-left col-md-7 col-xs-12"> ' +
                '<span class="fa fa-suitcase form-control-feedback left" aria-hidden="true"></span> ' +
                '</div> ' +
                '</div> ' +
                '<div class="form-group"> ' +
                '<label class="control-label col-md-3 col-sm-3 col-xs-12" for="orgaAddress">Organization Address <span class="required" style="color: red;">*</span> </label> ' +
                '<div class="col-md-6 col-sm-6 col-xs-12"> ' +
                '<textarea rows="3" cols="20" class="form-control col-md-7 col-xs-12" placeholder="Organization Address" name="orgaAddress[]" required></textarea> ' +
                '</div> ' +
                '</div> ' +
                '<div class="form-group"> ' +
                '<label class="control-label col-md-3 col-sm-3 col-xs-12" for="emrPhone">Contact No. <span class="required" style="color: red;">*</span></label> ' +
                '<div class="col-md-6 col-sm-6 col-xs-12"> ' +
                '<input type="text" id="contactNo" name="contactNo[]" placeholder="Contact No."  class="form-control has-feedback-left col-md-7 col-xs-12" required> ' +
                '<span class="fa fa-mobile-phone form-control-feedback left" aria-hidden="true"></span> ' +
                '</div> ' +
                '</div> ' +
                '<div class="form-group"> ' +
                '<label class="control-label col-md-3 col-sm-3 col-xs-12" for="duration">Duration <span class="required" style="color: red;">*</span></label> ' +
                '<div class="col-md-6 col-sm-6 col-xs-12"> ' +
                '<input type="text" id="duration" name="duration[]" placeholder="Duration"  class="form-control has-feedback-left col-md-7 col-xs-12" required> ' +
                '<span class="fa fa-mobile-phone form-control-feedback left" aria-hidden="true"></span> ' +
                '</div></div><div class="col-md-9"></div><div class="col-md-3"> <button type="button"  class="btn btn-sm btn-danger btn-circle removeExp"><i class="glyphicon glyphicon-remove"></i></button> </div>');
        });
        $(document).on('click', '.removeExp', function () {
            $(this).parent().parent().remove();
        });
    </script>

    <script>
        $(document).on('click', '#add_more_attach', function () {
            $('#form-pi').append('<div class="form-group">' +
                ' <label class="control-label col-md-3 col-sm-3 col-xs-12">Title</label> ' +
                '<div class="col-md-2"> ' +
                '<input type="text" class="form-control" placeholder="Title of Attachment" name="title[]" required> </div> ' +
                '<div class="col-md-3">' +
                ' <input type="file" name="file[]" required> </div>' +
                ' <div class="col-md-1">' +
                ' <button type="button" class="btn btn-sm btn-danger btn-circle removeFile">' +
                '<i class="glyphicon glyphicon-remove"></i></button> ' +
                '</div>');
        });
        $(document).on('click', '.removeFile', function () {
            $(this).parent().parent().remove();
        });
    </script>


    <!-- Select2 -->
    <script>

        $(".select2_single").select2({
            placeholder: "Select a Country",
            allowClear: true
        });
        $(".select2_group").select2({});

        $(".select2_multiple_skill").select2({
            maximumSelectionLength: 4,
            placeholder: "Choose Skills",
            allowClear: true
        });
        $(".select2_multiple_education").select2({
            maximumSelectionLength: 4,
            placeholder: "Choose Educations",
            allowClear: true
        });
        $(".select2_multiple_certification").select2({
            maximumSelectionLength: 4,
            placeholder: "Choose Certifications",
            allowClear: true
        });
    </script>
    <!-- /Select2 -->



@endsection
