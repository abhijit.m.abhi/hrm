<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeePersonalDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_personal_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('employee_code');
            $table->string('first_name');
            $table->string('middle_name')->nullable();
            $table->string('last_name');
            $table->string('nationality');
            $table->string('nid');
            $table->date('date_of_birth');
            $table->string('birth_certificate_id')->nullable();
            $table->string('passport_no')->nullable();
            $table->string('gender');
            $table->string('marital_status');
            $table->text('address');
            $table->string('city');
            $table->string('postal_or_zip_code');
            $table->string('country');
            $table->string('phone')->nullable();
            $table->string('work_phone')->nullable();
            $table->string('email');
            $table->timestamps();

            /*
             * Foreign Key
             */
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee_personal_details');
    }
}
