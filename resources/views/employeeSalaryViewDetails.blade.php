@extends('layouts.master')
@section('title', 'Employee Salary Details')
@section('content')
    <body onload="myFunction()">
    <!-- page content -->
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    {{--<h3>User Profile</h3>--}}
                </div>

            </div>

            <div class="clearfix"></div>

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Employee Salary Details</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <div class="col-md-12 col-sm-12 col-xs-12 profile_left">


                                <div id="myTabContent" class="tab-content col-md-6">
                                    <div role="tabpanel" style="align-content: center" class="tab-pane fade active in" id="tab_content1"
                                         aria-labelledby="home-tab">

                                        <table class="data table table-striped no-margin" align="center">

                                            <h4>Basic Information</h4><br>
                                            <tr>
                                                <th>Employee Name:</th>
                                                <td>{{$viewInfo->emp_salary->first_name}} {{$viewInfo->emp_salary->middle_name}} {{$viewInfo->emp_salary->last_name}}</td>
                                            </tr>

                                            <tr>
                                                <th>Employment Code:</th>
                                                <td>{{$viewInfo->emp_salary->employee_code}}</td>
                                            </tr>

                                            <tr>
                                                <th>Email:</th>
                                                <td>{{$viewInfo->emp_salary->email}}</td>
                                            </tr>
                                            <tr>
                                                <th>Address:</th>
                                                <td>{{$viewInfo->emp_salary->address}}</td>
                                            </tr>

                                            <tr>
                                                <th>NID No:</th>
                                                <td>{{$viewInfo->emp_salary->nid}}</td>
                                            </tr>





                                        </table>
                                        </div>
                                    </div>
                                    <div class="tab-content col-md-6">

                                            <table class="data table table-striped no-margin" align="center">

                                                <h4>Salary Information</h4><br>
                                                <tr>
                                                    <th>Employment Status:</th>
                                                   <td>{{$viewInfo->emp_salary->emp_job_details->employment_status}}</td>
                                                </tr>
                                                <tr>
                                                    <th>Job Title:</th>
                                                    <td>{{$viewInfo->emp_salary->emp_job_details->job_title}}</td>
                                                </tr>
                                                <tr>
                                                    <th>Salary Grade:</th>
                                                    <td>{{$viewInfo->salary_grade}}</td>
                                                </tr>
                                                <tr id="basic_sal" style="display: none;">
                                                    <th>Basic Salary:</th>
                                                    <td>{{$viewInfo->basic_salary}}</td>
                                                </tr>
                                                <tr id="over_time" style="display: none;">
                                                    <th>Over Time (Per Hour):</th>
                                                    <td>{{$viewInfo->overtime}}</td>
                                                </tr>
                                                <tr id="hour" style="display: none;">
                                                    <th>Hourly Rate :</th>
                                                    <td>{{$viewInfo->hourly_rate}}</td>
                                                </tr>
                                                <tr id="daily" style="display: none;">
                                                    <th>Daily Rate :</th>
                                                    <td>{{$viewInfo->daily_rate}}</td>
                                                </tr>

                                            </table>



                                        <input type="hidden" class="form-control col-md-7 col-xs-12 valid hour_valid daily_valid"
                                               placeholder="Enter Salary Grade" id="salary_type" value="{{$viewInfo->salary_type}}" >







                                    </div>

                                </div>







                            </div>


                        </div>
                    <div id="monthly_salary" style="display: none;">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6 panel-heading" id="allowance"><b><p style="font-size: medium">Allowances</p></b><hr style="width: 100%; height: 1px; background-color: green; color: green;" />
                                    <div style="opacity: 1.0;">
                                        <div class="form-group" style="margin-top: 55px;">


                                            <div class="col-md-12 col-sm-12 col-xs-12">

                                                <?php
                                                $all = $viewInfo->allowances;
                                                $basic_salary = $viewInfo->basic_salary;
                                                $allowance = json_decode($all,true);
                                                $sum_allowances = 0;
                                                echo '<table class="data table table-striped no-margin" align="center">';
                                                foreach ($allowance as $key=>$a)
                                                {
                                                    //echo $key."</br>";
                                                    foreach ($a as $t=>$b)
                                                    {
                                                        // echo $t."=".$b."</br>";
                                                        $sum_allowances =  $sum_allowances+$b;
                                               /* <tr>
                                                <th>Email:</th>
                                                <td>{{$viewInfo->emp_salary->email}}</td>
                                                </tr>*/
                                                        echo "<tr><th>".$t.'<td>'.$b.'</td>
                                                </tr>';

                                                        //echo '<input type="text" id="title" name="title[]" value="'.$t.'" class="form-control has-feedback-left col-md-7 col-xs-12"><input type="number" id="amount_deduction" name="amount_deduction[]" value="'.$b.'"  class="form-control has-feedback-left col-md-7 col-xs-12" > ' ;

                                                    }
                                                }
                                                echo "<tr style='font-weight: bold;font-size: medium'><th>"."Total".'</th><td>'.$sum_allowances.'</td></tr>';
                                                echo '</table>';

                                                ?>
                                            </div>
                                        </div>


                                    </div>
                                </div>
                                <div class="col-md-6 panel-heading" id="deductions"><b><p style="font-size: medium">Deductions</p></b><hr style="width: 100%; height: 1px; background-color: green; color: green;" />
                                    <div style="opacity: 1.0;">
                                        <div class="form-group" style="margin-top: 55px;"></div>
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <?php
                                            $all_deduction = $viewInfo->deductions;
                                            $sum_deduction = 0;
                                            $deduction = json_decode($all_deduction,true);
                                            echo '<table class="data table table-striped no-margin" align="center">';
                                            foreach ($deduction as $key=>$a)
                                            {
                                                //echo $key."</br>";
                                                foreach ($a as $title=>$amount)
                                                {
                                                    // echo $t."=".$b."</br>";
                                                    $sum_deduction =$sum_deduction+$amount ;

                                                    //echo "<p style='font-size: medium'>Title :".$title.'<br>'."Amount :".$amount.'</p><br>';
                                                    echo "<tr><th>".$title.'</th><td>'.$amount.'</td></tr>';
                                                    //echo '<input type="text" id="title" name="title[]" value="'.$t.'" class="form-control has-feedback-left col-md-7 col-xs-12"><input type="number" id="amount_deduction" name="amount_deduction[]" value="'.$b.'"  class="form-control has-feedback-left col-md-7 col-xs-12" > ' ;

                                                }
                                            }
                                            echo "<tr style='font-weight: bold;font-size: medium'><th>"."Total".'<td>'.$sum_deduction.'</td></tr>';
                                            echo '</table>';
                                            ?>
                                        </div>



                                    </div>
                                </div>
                            </div>
                        </div>
                </div>

                        <div align="center" style="font-weight: bold;font-size: large;display: none;" id="net_sal">
                          Net Salary :  {{$viewInfo->net_salary}}
                        </div>


                    </div>

                </div>
            </div>
        </div>
    </div>
    </body>
    <!-- /page content -->

    <script>

        function myFunction() {
            var type = $('#salary_type').val();



            if(type=='Monthly')
            {
                $('#basic_sal').show();
                $('#over_time').show();
                $('#net_sal').show();
                $('#monthly_salary').show();

            }
            else if(type=='Hourly')
            {

                $('#hour').show();
            }
            else if(type=='Daily'){

                $('#daily').show();
            }

        }


    </script>

@endsection