<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Resign extends Model
{
    //

    protected $fillable = ['employee_id', 'comments', 'resign_type' ,'flag'];

    public function emp_resign()
    {

        return $this->belongsTo('App\EmployeePersonalDetail','employee_id');
    }

}
