<?php

namespace App\Http\Controllers;

use App\JobDetailsSetup;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class JobDetailsSetupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $allJobDetailsList = JobDetailsSetup::latest()->get();
        return view('jobDetailsList', ['allJobDetailsList' => $allJobDetailsList]);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('addJobDetails');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $jobDetails = new JobDetailsSetup();

        $jobDetails->job_code = $request->input('jobCode');
        $jobDetails->job_title = $request->input('jobTitle');
        $jobDetails->description = $request->input('description');
        $jobDetails->specification = $request->input('specification');
        $jobDetails->min_salary = $request->input('minSal');
        $jobDetails->max_salary = $request->input('maxSal');

        if ($jobDetails->save()) {
            Session::flash('message', 'Job Details Added Successfully !');
        }

        return redirect('/jobDetails/create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $jobDetailsData = JobDetailsSetup::findOrFail($id);
        return view('editJobDetails', ['jobDetailsData' => $jobDetailsData]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $jobDetails = JobDetailsSetup::destroy($id);
    }
}
