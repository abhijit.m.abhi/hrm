<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmployeeEmergencyContact extends Model
{
    protected $fillable = [
        'emrName',
        'relation',
        'emrAddress',
        'emrPhone',
    ];
}
