<?php

namespace App\Http\Controllers;

use App\EmployeeLeave;
use App\EmployeePersonalDetail;
use App\Holiday;
use DateInterval;
use DatePeriod;
use DateTimeZone;
use Illuminate\Http\Request;
use DateTime;

class LeaveController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $weeklyHoliday = Holiday::all()->where('holiday_type','=','Weekly Holiday');
        return view('weeklyHoliday', ['weeklyHoliday' => $weeklyHoliday]);
    }
    public function addWeeklyHoliday(Request $req)
    {

        $w_date = $req->date;
        $yr = $req->year;
        function getDays($year, $day, $format, $timezone='UTC')
        {
            $fridays = array();
            $startDate = new DateTime("{$year}-01-01 {$day}", new DateTimezone($timezone));
            $year++;
            $endDate = new DateTime("{$year}-01-01", new DateTimezone($timezone));
            $int = new DateInterval('P7D');
            foreach(new DatePeriod($startDate, $int, $endDate) as $d) {
                $fridays[] = $d->format($format);
            }
            return $fridays;
        }
        $days = getDays($yr, $w_date, 'Y-m-d');
        foreach ($days as $dd)
        {
            $addWeeklyHoliday = new Holiday();
            $addWeeklyHoliday->date = $dd;
            $addWeeklyHoliday->title = 'Weekly Holiday';
            $addWeeklyHoliday->holiday_type = 'Weekly Holiday';
            $addWeeklyHoliday->save();
        }
       return response()->json($addWeeklyHoliday);
    }

    public function deleteWeeklyHoliday(Request $req)
    {
        $w_date = $req->dDate;
        $w_year = $req->dYear;
        function getDays($year, $day, $format, $timezone='UTC')
        {
            $fridays = array();
            $startDate = new DateTime("{$year}-01-01 {$day}", new DateTimezone($timezone));
            $year++;
            $endDate = new DateTime("{$year}-01-01", new DateTimezone($timezone));
            $int = new DateInterval('P7D');
            foreach(new DatePeriod($startDate, $int, $endDate) as $d) {
                $fridays[] = $d->format($format);
            }
            return $fridays;
        }
        $days = getDays($w_year, $w_date, 'Y-m-d');
        foreach ($days as $dd)
        {
            $deletedRows = Holiday::where('date', $dd)->delete();
        }
        return response()->json($deletedRows);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function view(){
        $publicHoliday = Holiday::all()->where('holiday_type','=','Public Holiday');
        return view('publicHoliday', ['publicHoliday' => $publicHoliday]);
    }

    public function addPublicHoliday(Request $req)
    {
        $addPublicHoliday = new Holiday();
        $p_date = $req->date;
        $addPublicHoliday->date = date("Y-m-d", strtotime($p_date));
        $addPublicHoliday->title = $req->title;
        $addPublicHoliday->holiday_type = 'Public Holiday';

        $addPublicHoliday->save();
        return response()->json($addPublicHoliday);
    }
    public function editPublicHoliday(Request $req){
        $editPublicHoliday = Holiday::find ($req->id);
        $p_date = $req->date;
        $editPublicHoliday->date = date("Y-m-d", strtotime($p_date));
        $editPublicHoliday->title = $req->title;
        $editPublicHoliday->update();
        return response()->json($editPublicHoliday);
    }


    public function viewEmpLeave(){
        $employees = EmployeePersonalDetail::all();
        $empLeaves = EmployeeLeave::with("emp_leave")->get();
        return view('employeeLeave', ['employees' => $employees,'empLeaves' => $empLeaves]);
    }
    public function addEmployeeLeave(Request $req)
    {
        $addEmployeeLeave = new EmployeeLeave();
        $addEmployeeLeave->employee_id = $req->emp_name;
        $addEmployeeLeave->leave_type = $req->leave_type;
        $addEmployeeLeave->leave_days = $req->leave_days;

        $addEmployeeLeave->save();
        $id= $addEmployeeLeave->id;
        $addEmployeeLeave = EmployeeLeave::with("emp_leave")->where('id', '=' ,$id)->get();

        return response()->json($addEmployeeLeave);
    }
    public function editEmployeeLeave(Request $req){
        $editEmployeeLeave = EmployeeLeave::find ($req->id);
        $editEmployeeLeave->employee_id = $req->emp_name;
        $editEmployeeLeave->leave_type = $req->leave_type;
        $editEmployeeLeave->leave_days = $req->leave_days;
        $editEmployeeLeave->update();
        $id= $editEmployeeLeave->id;
        $editEmployeeLeave = EmployeeLeave::with("emp_leave")->where('id', '=' ,$id)->get();
        return response()->json($editEmployeeLeave);
    }

}
