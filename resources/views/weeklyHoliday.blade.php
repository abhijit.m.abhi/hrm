@extends('layouts.master')
@section('title', 'Weekly Holidays ')
@section('style')
    <style>
        .calendar-table
        {

        }
        .select2{
            width: 100% !important;
        }

    </style>
@endsection
@section('content')

    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>Weekly Holidays</h3>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Weelly Holidays<!--<small>Input different form elements</small>--></h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                            @if(Session::has('message'))

                                <div id="successMessage" class="alert alert-success alert-dismissible fade in" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                                aria-hidden="true">×</span>
                                    </button>
                                    <strong>{{ Session::get('message') }}</strong>
                                </div>
                            @endif
                        </div>
                        {{--tab view--}}
                        <div class="bs-example">
                            <div class="tabbable">
                                <ul class="nav nav-tabs">
                                    {{--<li class="active"> <input type="button" data-toggle="tab" href="#tab1" data-loading-text="Loading..." class="btn btn-info" value="Loan"></li>--}}
                                    {{--<li> <input type="button" data-toggle="tab" href="#tab2" data-loading-text="Loading..." class="btn btn-info" value="  Employee Loan    "></li>--}}

                                </ul>
                                <div class="tab-content">
                                    <div id="tab1" class="tab-pane active fade in">
                                        {{--fade view for add skill--}}
                                        <button onclick="myFunction()" data-backdrop="ture" id="hello" class="btn btn-primary btn-md" data-type="Skills"  data-token="{{ csrf_token() }}"  data-toggle="modal" data-target="#myModalHorizontalkk"><span class="glyphicon glyphicon-plus">
                                                    Add Weekly Holiday </span>
                                        </button>
                                        <button onclick="deleteFunction()" data-backdrop="ture" id="hello" class="btn btn-danger btn-md" data-type="delete"  data-token="{{ csrf_token() }}"  data-toggle="modal" data-target="#myModalHorizontalkk"><span class="glyphicon glyphicon-minus">
                                                    Delete Holiday </span>
                                        </button>


                                        <div class="x_content">
                                            <div class="table-responsive">
                                                <table id="datatable-buttons" class="Holidays table table-striped table-bordered table-hover display">
                                                    <thead>
                                                    <tr class="alert-info">
                                                        <th>Date</th>
                                                        {{--<th>Action</th>--}}
                                                    </tr>

                                                    </thead>

                                                    <tbody>
                                                    @foreach($weeklyHoliday as $wHoliday)
                                                        <tr class='item{{$wHoliday->id}}' id="row_id{{$wHoliday->id}}">
                                                            <td>{{$wHoliday->date}}</td>

                                                            {{--<td align="center">--}}
                                                                {{--<button class="btn btn-primary btn-sm edit-modal-weekly_holiday" data-id="{{$wHoliday->id}}" data-date="{{$wHoliday->date}}"><span class="glyphicon glyphicon-edit"></span></button>--}}
                                                                {{--<button class="btn confirm btn-danger btn-sm delete_holiday"  data-id="{{$wHoliday->id}}" data-date="{{$wHoliday->date}}"><span class="glyphicon glyphicon-trash"></span></button>--}}
                                                            {{--</td>--}}
                                                        </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            {{--Modal--}}
                                            <!-- /.modal -->
                                            </div>
                                        </div>
                                        <meta name="_token" content="{!! csrf_token() !!}"/>
                                        {{--end table view--}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    {{--delete modal--}}
    <div class="modal fade" id="myModal" aria-labelledby="myModalLabel" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header alert-danger">
                    <button type="button" class="close" data-dismiss="modal"
                            aria-label="Close"><span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title"><span class="glyphicon glyphicon-trash"></span>
                        Confirmation Message</h4>
                </div>
                <div class="modal-body">
                    <p>Are you sure that you want to permanently delete the selected
                        element?</p>
                    <input type="hidden" id="d_year">
                    <input type="hidden" id="d_date">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">No
                    </button>
                    <button type="button" class="btn btn-danger delete_holiday_btn delete_empLoan_btn"
                            data-dismiss="modal" value=""
                            data-token="{{ csrf_token() }}" data-dismiss="modal">Delete
                    </button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    {{--end delete modal--}}


    {{--add form modal --}}
    <div class="modal fade open_modal" id="myModalHorizontal" tabindex="-1" role="dialog"
         aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header">
                    <button type="button" class="close closeBtn"
                            data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                        <span class="sr-only">Close</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">
                        Add A Holiday
                    </h4>
                </div>

                <!-- Modal Body -->
                <div class="modal-body">
                    <div class="modal-body">

                        <form id="form" class="form-horizontal" role="form" data-parsley-validate>
                            <div class="form-group">
                                <label class="control-label col-md-4 col-sm-4 col-xs-12">Year</label>
                                <div class="col-md-6 col-sm-10 col-xs-12">
                                    <select class="loanType form-control required"  id="year" name="year">
                                        <option value="<?php echo date("Y"); ?>"><?php echo date("Y"); ?></option>
                                        <option value="<?php echo date("Y")+1; ?>"><?php echo date("Y")+1; ?></option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-4 col-sm-4 col-xs-12">Day</label>
                                <div class="col-md-6 col-sm-10 col-xs-12">
                                    <select class="loanType form-control required" tabindex="-1" id="date" name="date">
                                        <option value="">Select a day</option>
                                        <option value="Saturday">Saturday</option>
                                        <option value="Sunday">Sunday</option>
                                        <option value="Monday">Monday</option>
                                        <option value="Tuesday">Tuesday</option>
                                        <option value="Wednesday">Wednesday</option>
                                        <option value="Thursday">Thursday</option>
                                        <option value="Friday">Friday</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-2 col-sm-10">
                                    <button type="button" id="save-btn-weeklyHoliday"   data-token="{{ csrf_token() }}"  class="btn btn-primary">Add</button>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-10">
                                    <input type="hidden" class="form-control"
                                           id="id" name="id" placeholder="Enter Certification"/>
                                </div>
                            </div>

                            <div style="display:none" class="modal-footer" id="footer">
                                <button type="button" class="btn btn-default closeBtn"
                                        data-dismiss="modal">
                                    Close
                                </button>
                                <button type="button" class="btn btn-primary edit" id="edit-weeklyHoliday">
                                    Update
                                </button>
                            </div>

                        </form>
                    </div>

                    <meta name="_token" content="{!! csrf_token() !!}" />
                </div>
                <!-- Modal Footer -->
            </div>
        </div>
    </div>

    {{--Delete Holiday Modal--}}
    <div class="modal fade open_modal" id="deleteAholiday" tabindex="-1" role="dialog"
         aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header">
                    <button type="button" class="close closeT"
                            data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                        <span class="sr-only">Close</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">
                        Delete A Weekly Holiday
                    </h4>
                </div>

                <!-- Modal Body -->
                <div class="modal-body">
                    <div class="modal-body">

                        <form id="form" class="form-horizontal" role="form">
                            <div class="form-group">
                                <label class="control-label col-md-4 col-sm-4 col-xs-12">Year</label>
                                <div class="col-md-6 col-sm-10 col-xs-12">
                                    <select class="loanType form-control required"  id="yearD" name="yearD">
                                        <option value="<?php echo date("Y"); ?>"><?php echo date("Y"); ?></option>
                                        <option value="<?php echo date("Y")+1; ?>"><?php echo date("Y")+1; ?></option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-4 col-sm-4 col-xs-12">Day</label>
                                <div class="col-md-6 col-sm-10 col-xs-12">
                                    <select class="loanType form-control required" tabindex="-1" id="dateD" name="dateD">
                                        <option value="">Select a day</option>
                                        <option value="Saturday">Saturday</option>
                                        <option value="Sunday">Sunday</option>
                                        <option value="Monday">Monday</option>
                                        <option value="Tuesday">Tuesday</option>
                                        <option value="Wednesday">Wednesday</option>
                                        <option value="Thursday">Thursday</option>
                                        <option value="Friday">Friday</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-2 col-sm-10">
                                    <button type="button" id="delete-btn-holiday"  data-token="{{ csrf_token() }}"  class="delete-btn-holiday btn btn-danger">Delete</button>
                                </div>
                            </div>

                            <div class="form-group">

                                <div class="col-sm-10">
                                    <input type="hidden" class="form-control"
                                           id="id" name="id" placeholder="Enter Certification"/>
                                </div>
                            </div>


                        </form>
                    </div>

                    <meta name="_token" content="{!! csrf_token() !!}" />
                </div>

                <!-- Modal Footer -->

            </div>

        </div>
    </div>


@endsection
@section('script')

    <script>
        // validate a field on "blur" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':
        $('form')
            .on('blur', 'input[required], input.optional, select.required', validator.checkField)
            .on('change', 'select.required', validator.checkField)
            .on('keypress', 'input[required][pattern]', validator.keypress);

        $('.multi.required').on('keyup blur', 'input', function() {
            validator.checkField.apply($(this).siblings().last()[0]);
        });


        $('form').submit(function(e) {
            e.preventDefault();
            var submit = true;
            // evaluate the form using generic validaing
            if (!validator.checkAll($(this))) {
                submit = false;
            }

            if (submit)
                this.submit();
            return false;
        });
    </script>
    <script type="text/javascript">

        var handleDataTableButtons = function () {
            if ($("#datatable-buttons").length) {
                $("#datatable-buttons").DataTable({
                    dom: "Bfrtip",
                    buttons: [
                        {
                            extend: "copy",
                            className: "btn-sm"
                        },
                        {
                            extend: "csv",
                            className: "btn-sm"
                        },
                        {
                            extend: "excel",
                            className: "btn-sm"
                        },
                        {
                            extend: "pdfHtml5",
                            className: "btn-sm"
                        },
                        {
                            extend: "print",
                            className: "btn-sm"
                        },
                        {
                            extend: "colvis",
                            className: "btn-sm"
                        }
                    ],
                    responsive: false
                });
            }
        };

        TableManageButtons = function () {
            "use strict";
            return {
                init: function () {
                    handleDataTableButtons();
                }
            };
        }();

        $('#datatable').dataTable();

        $('#datatable-keytable').DataTable({
            keys: true
        });

        $('#datatable-responsive').DataTable();

        $('#datatable-scroller').DataTable({
            ajax: "js/datatables/json/scroller-demo.json",
            deferRender: true,
            scrollY: 380,
            scrollCollapse: true,
            scroller: true
        });

        $('#datatable-fixed-header').DataTable({
            fixedHeader: true
        });

        var $datatable = $('#datatable-checkbox');

        $datatable.dataTable({
            'order': [[1, 'asc']],
            'columnDefs': [
                {orderable: false, targets: [0]}
            ]
        });
        $datatable.on('draw.dt', function () {
            $('input').iCheck({
                checkboxClass: 'icheckbox_flat-green'
            });
        });

        TableManageButtons.init();

    </script>

    <script>
        $(document).ready(function(){
            $(".nav-tabs a").click(function(){
                $(this).button('loading').delay(500).queue(function(){
                    $(this).button('reset');
                    $(this).dequeue();
                });
            });
        });
    </script>

    <script>

        function  myFunction() {
            $("#myModalHorizontal").modal('show');
            //$('#footer').hide();
        }
    </script>
    <script>
        function deleteFunction() {
            $('#deleteAholiday').modal('show');
        }
    </script>

    <script>
        $('.closeBtn').click(function(){
            $('#date').val('');
            $('#year').val('');
            $('#footer').hide('');
            $('#save-btn-weeklyHoliday').show();
            $('input[type="text"],select.required').each(function () {
                if ($.trim($(this).val()) == '') {
                    $(this).css({
                        "border": "",
                        "background": ""
                    });
                }
            });
        });
    </script>

    <script>
        $("#save-btn-weeklyHoliday").click(function() {
            //  alert("hello");
            $.ajaxSetup ({
                // Disable caching of AJAX responses
                cache: false
            });
            $.ajax({
                type: 'get',
                url: 'add_weekly_holiday',
                data: {
                    '_token': $('input[name=_token]').val(),
                    'year' : $('#year').val(),
                    'date' : $('#date').val(),
                },
                success: function(data) {
                    console.log(data);
                    $(".Holidays").load(location.href + " .Holidays");
                    if ((data.errors)){
                        $('.error').removeClass('hidden');
                        $('.error').text(data.errors.name);
                    }
                    else {
                        $('.error').addClass('hidden');
                        $('.Holidays').append("<tr class='item" + data.id + "'><td>" + data.date + "</td></tr>");
                        $("#myModalHorizontal").modal('hide');
                        $('#year').val('');
                        $('#date').val('');
                    }
                },
            });

        });

    </script>


    <script>
        $(document).on("click", ".delete-btn-holiday", function () {
            var dYear = $("#yearD").val();
            var dDate = $("#dateD").val();
            $("#d_year").val(dYear);
            $("#d_date").val(dDate);
            $('#myModal').modal('show');
            $('#deleteAholiday').modal('hide');
        });
        $('.delete_holiday_btn').click(function () {
            var dYear = $("#d_year").val();
            var dDate = $("#d_date").val();
            var url = "delete_weekly_holiday";
            $.ajaxSetup({
                cache: false
//                headers: {
//                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
//                }
            });
            $.ajax({
                type: 'get',
                url: url,
                data: {
                    '_token': $('input[name=_token]').val(),
                    'dYear' : $('#d_year').val(),
                    'dDate' : $('#d_date').val(),
                },
                success: function (data) {
                    console.log(id);
                    $(".item" + id).remove();
                    $(".Holidays").load(location.href + " .Holidays");
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        })
    </script>

    <script>
        $("#save-btn-weeklyHoliday").click(function (e) {
            var isValid = true;
            $('select.required').each(function () {
                if ($.trim($(this).val()) == '') {
                    isValid = false; /* Required class style */
                    $(this).css({
                        "border": "1px solid red",
                        "background": "#FFCECE"
                    });
                }
                else { /* Required class style removed */
                    $(this).css({
                        "border": "",
                        "background": ""
                    });
                }
            });
            if (isValid == false)
                e.preventDefault();
            else{
            }
        });
    </script>


    <script>
        $(".dateTime").datepicker( {
            format: "dd MM yyyy",
            autoclose: true
        });
    </script>
    <script type="text/javascript">

        $(".newTable").on("click", function () {
            var type = $(this).children('.fl').val();
            var id = $(this).children('.id').val();
            var url = "status_change";

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            });

            $.ajax({
                type: 'GET',
                url: url + '/' + id,
                success: function (data) {
                    console.log(data);

                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        });
    </script>



@endsection