<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QualificationSetup extends Model
{
    //
    protected $table = 'qualification_setups';
    protected $fillable = [
        'name',
        'description',
        'type',


    ];
}
