<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeeSalarySetupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_salary_setups', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('employee_id')->unsigned();
            $table->string('salary_grade');
            $table->string('salary_type');
            $table->string('daily_rate')->nullable();
            $table->double('basic_salary')->nullable();
            $table->double('hourly_rate')->nullable();
            $table->double('overtime')->nullable();
            $table->string('allowances')->nullable();
            $table->string('deductions')->nullable();
            $table->double('net_salary')->nullable();
            $table->integer('flag')->default(0);
            $table->integer('job_id')->unsigned();

            $table->timestamps();


            $table->foreign('employee_id')->references('id')->on('employee_personal_details')->onDelete('cascade');
            $table->foreign('job_id')->references('id')->on('job_details_setups')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee_salary_setups');
    }
}
