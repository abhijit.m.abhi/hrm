<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddEmployeeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

            'empCode' => 'required|string',
            'firstName' => 'required|string',
            'lastName' => 'required|string',
            'nationality' => 'required|string',
            'nid' => 'required|string',
            'dateOfBirth' => 'required',
            'gender' => 'required',
            'maritalStatus' => 'required',
            'photo' => 'required',
            'address' => 'required',
            'city' => 'required',
            'postalCode' => 'required',
            'country' => 'required',
            'email' => 'email',
            'phone' => 'required',
            'employmentStatus' => 'required',
            'jobTitle' => 'required',
            'joinedDate' => 'required',
            'department' => 'required',
            'skill' => 'required',
            'qualification' => 'required',
            'certification' => 'required',
            'emrName' => 'required',
            'relation' => 'required',
            'emrAddress' => 'required',
            'emrPhone' => 'required',
            'organization' => 'required',

        ];
    }
}
