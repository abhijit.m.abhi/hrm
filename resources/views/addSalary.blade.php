@extends('layouts.master')
@section('title', 'Add User')
@section('content')

    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>Employee Salary</h3>
                </div>


            </div>
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Add Employee Salary<!--<small>Input different form elements</small>--></h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <br/>

                            <form {{--action="{{route('company_structure.store')}}"--}} data-parsley-validate method="POST" class="form-horizontal" role="form"
                                  files="true" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div class="form-body">

                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Basic Salary</label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="number" class="form-control col-md-7 col-xs-12"
                                                   placeholder="Enter Name" name="basic_salary" id="basic_salary" value="{{old('basic_salary')}}" required>
                                            @if ($errors->has('basic_salary'))
                                                <span class="help-block">
                                                <strong>{{ $errors->first('basic_salary') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">House Rent</label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="number" class="form-control col-md-7 col-xs-12"
                                                   placeholder="Enter Name" name="house_rent" id="house_rent" value="{{old('house_rent')}}" required>
                                            @if ($errors->has('house_rent'))
                                                <span class="help-block">
                                                <strong>{{ $errors->first('house_rent') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Medical Expense</label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="number" class="form-control col-md-7 col-xs-12"
                                                   placeholder="Enter Name" name="medical_expense" id="medical_expense" value="{{old('medical_expense')}}" required>
                                            @if ($errors->has('medical_expense'))
                                                <span class="help-block">
                                                <strong>{{ $errors->first('medical_expense') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group">

                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Medical Expense</label>
                                        <div class="col-xs-3">

                                            <input class="form-control input-group-textfield col-md-7 col-xs-12" id="ex1" type="text" placeholder="Enter Title">

                                            <input class="form-control" id="ex2" type="text" placeholder="Enter Amount">

                                            <span class="input-group-btn">
                                            <button class="btn btn-success btn-add" type="button">
                                                <span class="glyphicon glyphicon-plus"></span>
                                            </button>
                                        </span>

                                        </div>
                                        {{--<div class="container">
                                            <div class="row">
                                                <div class="control-group" id="fields">
                                                    <label class="control-label" for="field1">Nice Multiple Form Fields</label>
                                                    <div class="controls">
                                                        <form role="form" autocomplete="off">
                                                            <div class="entry input-group col-xs-3">
                                                                <input class="form-control" name="fields[]" type="text" placeholder="Type something" />
                                                                <span class="input-group-btn">
                                                            <button class="btn btn-success btn-add" type="button">
                                                                <span class="glyphicon glyphicon-plus"></span>
                                                            </button>
                                                        </span>
                                                            </div>
                                                        </form>
                                                        <br>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>--}}
                                    </div>

                                    <div class="ln_solid"></div>
                                    <div class="form-group">
                                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                            <a type="cancel" href="{{ url('dashboard') }}" class="btn btn-danger">Cancel</a>
                                            <button type="button" class="btn btn-success">Add</button>
                                        </div>
                                    </div>

                                </div>
                            </form>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        // validate a field on "blur" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':
        $('form')
            .on('blur', 'input[required], input.optional, select.required', validator.checkField)
            .on('change', 'select.required', validator.checkField)
            .on('keypress', 'input[required][pattern]', validator.keypress);

        $('.multi.required').on('keyup blur', 'input', function() {
            validator.checkField.apply($(this).siblings().last()[0]);
        });

        $('form').submit(function(e) {
            e.preventDefault();
            var submit = true;

            // evaluate the form using generic validaing
            if (!validator.checkAll($(this))) {
                submit = false;
            }

            if (submit)
                this.submit();

            return false;
        });

    </script>



@endsection