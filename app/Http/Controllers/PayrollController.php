<?php

namespace App\Http\Controllers;

use App\Http\Requests\SetSalary;
use App\SalarySetup;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;

class PayrollController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function view(){

        return view('setSalary');
    }


    public function view_setup(){

        $salary = SalarySetup::all();

        $monthly  = SalarySetup::where('salary_type','=','Monthly')->where('flag', '=', '0')->get();
        $daily  = SalarySetup::where('salary_type','=','Daily')->where('flag', '=', '0')->get();
        $hourly  = SalarySetup::where('salary_type','=','Hourly')->where('flag', '=', '0')->get();

        return view('viewSalary',['salaryList'=> $salary, 'monthly'=>$monthly ,'daily'=>$daily ,'hourly'=>$hourly]);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        

        $this->validate($request,[
            'salary_type' => 'required',

        ]);


        $setSalary = new SalarySetup();

        $title_data = Input::get('title');
        $amount_data = Input::get('amount');

        foreach( $title_data as $key => $n ) {

            $data = array(
                $n => $amount_data[$key]
            );
            $file_data_array[] = $data;
        }
        $allowance = json_encode($file_data_array);



        $title_deduction = Input::get('title_deduction');
        $amount_deduction = Input::get('amount_deduction');

        foreach( $title_deduction as $key => $n ) {

            $data = array(
                $n => $amount_deduction[$key]
            );
            $file_data_deductions_array[] = $data;
        }
        $deduction = json_encode($file_data_deductions_array);

        $setSalary->salary_type=$request->input('salary_type');
        $setSalary->salary_grade=$request->input('salary_grade');
        $setSalary->basic_salary=$request->input('basic_salary');
        $setSalary->overtime=$request->input('over_time');
        //$setSalary->hourly_grade=$request->input('hourly_grade');
        $setSalary->hourly_rate=$request->input('hourly_rate');
        // $setSalary->daily_grade=$request->input('daily_grade');
        $setSalary->daily_rate=$request->input('daily_rate');
        $setSalary->flag=0;
        $setSalary->allowances=$allowance;
        $setSalary->deductions=$deduction;
        $setSalary->net_salary=$request->input('net_salary');

        /*var_dump($setSalary);
        exit;*/
        if($setSalary->save()){
            return redirect('/view_salary_setup');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $salaryEdit = SalarySetup::findOrFail($id);
        return view('editSalarySetup', ['salaryEdit' => $salaryEdit]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $title_data = Input::get('title');
        $amount_data = Input::get('amount');


        foreach( $title_data as $key => $n ) {

            $data = array(
                $n => $amount_data[$key]
            );
            $file_data_array[] = $data;
        }
        $allowance = json_encode($file_data_array);

        $title_deduction = Input::get('title_deduction');
        $amount_deduction = Input::get('amount_deduction');

        foreach( $title_deduction as $key => $n ) {

            $data = array(
                $n => $amount_deduction[$key]
            );
            $file_data_deductions_array[] = $data;
        }
        $deduction = json_encode($file_data_deductions_array);


        $salaryGradeEdit = SalarySetup::findOrFail($id);

        $salaryGradeEdit->salary_type=$request->input('salary_type');
        $salaryGradeEdit->salary_grade=$request->input('salary_grade');
        $salaryGradeEdit->basic_salary=$request->input('basic_salary');
        $salaryGradeEdit->overtime=$request->input('over_time');
        $salaryGradeEdit->hourly_rate=$request->input('hourly_rate');
        $salaryGradeEdit->daily_rate=$request->input('daily_rate');
        $salaryGradeEdit->flag=0;
        $salaryGradeEdit->allowances=$allowance;
        $salaryGradeEdit->deductions=$deduction;
        $salaryGradeEdit->net_salary=$request->input('net_salary');

        if ($salaryGradeEdit->update()) {

            Session::flash('message', 'Information Updated Successfully !');
            return redirect('/view_salary_setup');
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
