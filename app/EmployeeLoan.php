<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmployeeLoan extends Model
{
    public function emp_loan()
    {

        return $this->belongsTo('App\EmployeePersonalDetail', 'employee_id');
    }
}
