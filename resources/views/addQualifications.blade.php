@extends('layouts.master')
@section('title', 'Qualifications Setup')
@section('content')

    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>Qualifications Setup</h3>
                </div>


            </div>
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Add Qualification<!--<small>Input different form elements</small>--></h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                            @if(Session::has('message'))

                                <div id="successMessage" class="alert alert-success alert-dismissible fade in" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                                aria-hidden="true">×</span>
                                    </button>
                                    <strong>{{ Session::get('message') }}</strong>
                                </div>
                            @endif
                        </div>
                        {{--tab view--}}
                        <div class="bs-example">
                            <div class="tabbable">
                                <ul class="nav nav-tabs">
                                    <li class="active"> <input type="button" data-toggle="tab" href="#tab1" data-loading-text="Loading..." class="btn btn-info" value="Skills" id="Skills"></li>
                                    <li> <input type="button" data-toggle="tab" href="#tab2" data-loading-text="Loading..." class="btn btn-info" value="  Educations  "></li>
                                    <li> <input type="button" data-toggle="tab" href="#tab3" data-loading-text="Loading..." class="btn btn-info" value="  Certification  "></li>

                                </ul>
                                <div class="tab-content">
                                    <div id="tab1" class="tab-pane active fade in">
                                      {{--fade view for add skill--}}
                                        <button onclick="myFunction('Skills')" data-backdrop="ture" id="hello" class="btn btn-primary btn-md" data-type="Skills"  data-token="{{ csrf_token() }}"  data-toggle="modal" data-target="#myModalHorizontalkk"><span class="glyphicon glyphicon-plus">
                                                    Add Skill
                                                </span></button>


                                        <div class="x_content">
                                            <div class="table-responsive">
                                                <table id="datatable-buttons" class="example Skills table table-striped table-bordered table-hover display">
                                                    <thead>
                                                    <tr class="alert-info">
                                                        <th>Name</th>
                                                        <th>Details</th>
                                                        <th>Action</th>
                                                    </tr>

                                                    </thead>


                                                    <tbody>
                                                    @foreach($skill as $sk)
                                                        <tr class='item{{$sk->id}}' id="row_id{{$sk->id}}">
                                                            <td>{{$sk->name}}</td>
                                                            <td>{{$sk->description}}</td>


                                                            <td align="center">
                                                                <button class="btn btn-primary btn-sm edit-modal"   data-id="{{$sk->id}}" data-name="{{$sk->name}}" data-description="{{$sk->description}}" data-type="{{$sk->type}}"><span class="glyphicon glyphicon-edit"></span></button>

                                                                <button class="btn confirm btn-danger btn-sm delete_p"  data-id="{{$sk->id}}" data-title="{{$sk->name}}" data-description="{{$sk->description}}" data-type="{{$sk->type}}" ><span class="glyphicon glyphicon-trash"></span></button>
                                                            </td>
                                                        </tr>
                                                    @endforeach

                                                    </tbody>
                                                </table>


                                            {{--Modal--}}


                                                <!-- /.modal -->

                                            </div>
                                        </div>

                                        <meta name="_token" content="{!! csrf_token() !!}"/>


                                        {{--end table view--}}
                                    </div>
                                    <div id="tab2" class="tab-pane fade">
                                        <button onclick="myFunction('Education')"  class="btn btn-primary btn-md" data-toggle="modal"  data-token="{{ csrf_token() }}"  data-type="Education" data-target="#myModalHorizontalkk"><span class="glyphicon glyphicon-plus">
                                                    Add Education
                                                </span></button>


                                        <div class="x_content">
                                            <div class="table-responsive">
                                                <table id="datatable-buttons" class="Education table table-striped table-bordered table-hover display">
                                                    <thead>
                                                    <tr class="alert-info">
                                                        <th>Name</th>
                                                        <th>Details</th>
                                                        <th>Action</th>
                                                    </tr>
                                                    </thead>


                                                    <tbody>

                                                    @foreach($edu as $education)
                                                        <tr class='item{{$education->id}}' id="row_id{{$education->id}}">
                                                            <td>{{$education->name}}</td>
                                                            <td>{{$education->description}}</td>


                                                            <td align="center">
                                                                <button class="btn btn-primary btn-sm edit-modal"   data-id="{{$education->id}}" data-name="{{$sk->name}}" data-description="{{$education->description}}" data-type="{{$education->type}}"><span class="glyphicon glyphicon-edit"></span></button>

                                                                <button class="btn confirm btn-danger btn-sm delete_p"  data-id="{{$education->id}}" data-title="{{$sk->name}}" data-description="{{$education->description}}" data-type="{{$education->type}}" ><span class="glyphicon glyphicon-trash"></span></button>
                                                            </td>
                                                        </tr>
                                                    @endforeach


                                                    </tbody>
                                                </table>


                                            </div>
                                        </div>

                                        <meta name="_token" content="{!! csrf_token() !!}"/>



                                    </div>
                                    <div id="tab3" class="tab-pane fade">
                                        <button onclick="myFunction('Certification')" class="btn btn-primary btn-md" data-toggle="modal"  data-token="{{ csrf_token() }}"  data-type="Certification" data-target="#myModalHorizontalkk"><span class="glyphicon glyphicon-plus">
                                                    Add Certification
                                                </span></button>


                                        {{--table--}}
                                        <div class="x_content">
                                            <div class="table-responsive">
                                                <table id="datatable-buttons" class="Certification table table-striped table-bordered table-hover display">
                                                    <thead>
                                                    <tr class="alert-info">
                                                        <th>Name</th>
                                                        <th>Details</th>
                                                        <th>Action</th>
                                                    </tr>
                                                    </thead>


                                                    <tbody>

                                                    @foreach($cer as $certification)
                                                        <tr class='item{{$certification->id}}' id="row_id{{$certification->id}}">
                                                            <td>{{$certification->name}}</td>
                                                            <td>{{$certification->description}}</td>


                                                            <td align="center">
                                                                <button class="btn btn-primary btn-sm edit-modal"   data-id="{{$certification->id}}" data-name="{{$certification->name}}" data-description="{{$certification->description}}" data-type="{{$certification->type}}"><span class="glyphicon glyphicon-edit"></span></button>
                                                                <button class="btn confirm btn-danger btn-sm delete_p"  data-id="{{$certification->id}}" data-title="{{$certification->name}}" data-description="{{$certification->description}}" data-type="{{$certification->type}}" ><span class="glyphicon glyphicon-trash"></span></button>
                                                            </td>
                                                        </tr>
                                                    @endforeach

                                                    </tbody>
                                                </table>


                                            </div>
                                        </div>





                                        <meta name="_token" content="{!! csrf_token() !!}"/>


                                        {{--end table--}}
                                    </div>
                                </div>
                            </div>
                        </div>



                    </div>
                </div>
            </div>
        </div>
    </div>



    {{--delete modal--}}

    <div class="modal fade" id="myModal" aria-labelledby="myModalLabel" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header alert-danger">
                    <button type="button" class="close" data-dismiss="modal"
                            aria-label="Close"><span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title"><span class="glyphicon glyphicon-trash"></span>
                        Confirmation Message</h4>
                </div>
                <div class="modal-body">
                    <p>Are you sure that you want to permanently delete the selected
                        element?</p>
                    <input type="hidden" id="d_id">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">No
                    </button>
                    <button type="button" class="btn btn-danger delete_product"
                            data-dismiss="modal" value=""
                            data-token="{{ csrf_token() }}" data-dismiss="modal">Delete
                    </button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    {{--end delete modal--}}


{{--add form modal --}}
    <div class="modal fade open_modal" id="myModalHorizontal" tabindex="-1" role="dialog"
         aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header">
                    <button type="button" class="close closeBtn"
                            data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                        <span class="sr-only">Close</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">
                        Add <span id="submitted_type"></span>
                    </h4>
                </div>

                <!-- Modal Body -->
                <div class="modal-body">
                    <div class="modal-body">

                        <form id="form" class="form-horizontal" role="form">

                            <div class="form-group">

                                <label  class="col-sm-2 control-label"
                                        for="inputEmail3">Name</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control required"
                                           id="name" name="name" placeholder="Enter Certification">
                                </div>
                            </div>
                            <div class="form-group">

                                <div class="col-sm-10">
                                    <input type="hidden" class="form-control"
                                           id="id" name="id" placeholder="Enter Certification"/>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label"
                                       for="inputPassword3" >Description</label>
                                <div class="col-sm-10">
                                    <textarea class="form-control" rows="5" id="description" name="description" required></textarea>

                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-2 col-sm-10">
                                    <button type="button" id="save-btn"   data-token="{{ csrf_token() }}"  class="btn btn-primary">Add</button>
                                </div>
                            </div>

                            <div style="display:none" class="modal-footer" id="footer">
                                <button type="button" class="btn btn-default closeBtn"
                                        data-dismiss="modal">
                                    Close
                                </button>
                                <button type="button" class="btn btn-primary edit" id="edit">
                                    Save changes
                                </button>
                            </div>

                        </form>
                    </div>
                    <input type="hidden" class="form-control"
                           id="type" name="" placeholder="Enter Certification"/>
                    <meta name="_token" content="{!! csrf_token() !!}" />
                </div>

                <!-- Modal Footer -->

            </div>

        </div>
    </div>


    <script>
        // validate a field on "blur" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':
        $('form')
            .on('blur', 'input[required], input.optional, select.required', validator.checkField)
            .on('change', 'select.required', validator.checkField)
            .on('keypress', 'input[required][pattern]', validator.keypress);

        $('.multi.required').on('keyup blur', 'input', function() {
            validator.checkField.apply($(this).siblings().last()[0]);
        });

        $('form').submit(function(e) {
            e.preventDefault();
            var submit = true;

            // evaluate the form using generic validaing
            if (!validator.checkAll($(this))) {
                submit = false;
            }

            if (submit)
                this.submit();

            return false;
        });

    </script>

    <script>
        $(document).ready(function(){
            $(".nav-tabs a").click(function(){
                $(this).button('loading').delay(500).queue(function(){
                    $(this).button('reset');
                    $(this).dequeue();
                });
            });
        });

    </script>


    <script>

        function  myFunction(id) {

            var type = id;
            $('#type').val(type);
            var y = $('#type').val();
            $("#submitted_type").text(y);

            $("#myModalHorizontal").modal('show');
            $("#name").attr("placeholder", y);

            $("#name").css({
                "border": "",
                "background": ""
            });
            //$('#footer').hide();

        }

    </script>

    <script>
        $('.closeBtn').click(function(){

            $('#name').val('');
            $('#description').val('');
            $('#type').val('');
            $('#footer').hide();
            $('#save-btn').show();


            $('input[type="text"].required').each(function () {
                if ($.trim($(this).val()) == '') {

                    $(this).css({
                        "border": "",
                        "background": ""
                    });
                }

            });



        });



    </script>

    <script>

            $("#save-btn").click(function() {
                var tableView= $('#type').val();
                  //  alert("hello");
                $.ajaxSetup ({
                    // Disable caching of AJAX responses
                    cache: false
                });
                $.ajax({
                    type: 'get',
                    url: 'addItem',
                    data: {
                        '_token': $('input[name=_token]').val(),
                        'name': $('#name').val(),
                        'description' : $('#description').val(),
                        'type' : $('#type').val()
                    },
                    success: function(data) {
                        console.log(data);

                        if ((data.errors)){
                            $('.error').removeClass('hidden');
                            $('.error').text(data.errors.name);
                        }
                        else {
                            $('.error').addClass('hidden');
                            $('.'+tableView).append("<tr class='item" + data.id + "'><td>" + data.name + "</td><td>" + data.description + "</td><td align='center'><button class='edit-modal btn btn-info' data-id='" + data.id + "' data-name='" + data.name + "' data-description='" + data.description + "'  data-type='" + data.type+ "' ><span class='glyphicon glyphicon-edit'></span></button> <button class='delete-modal btn btn-danger delete_p'data-id='" + data.id + "' data-name='" + data.name + "' data-description='" + data.description + "'  data-type='" + data.type+ "'><span class='glyphicon glyphicon-trash'></span></button></td></tr>");
                           $("#myModalHorizontal").modal('hide');


                        }
                    },
                });


                $('#name').val('');
                $('#description').val('');
                $('#type').val('');


            });

    </script>

    <script>

        $(document).on('click', '.edit-modal', function() {

            $('#id').val($(this).data('id'));
            $('#name').val($(this).data('name'));
            $('#description').val($(this).data('description'));
            $('#type').val($(this).data('type'));
            $('#myModalHorizontal').modal('show');
            $('#save-btn').hide();
            $('#footer').css('display','block');
            $('#name').css({
                "border": "",
                "background": ""
            });

            $('#edit').click(function(){


                $.ajax({
                    type: 'get',
                    url: './editItem',
                    data: {
                        '_token': $('input[name=_token]').val(),
                        'id': $("#id").val(),
                        'name': $('#name').val(),
                        'description': $("#description").val(),
                        'type':  $('#type').val()
                    },
                    success: function(data) {
                        $('.item' + data.id).replaceWith("<tr class='item" + data.id + "'><td>" + data.name + "</td><td>" + data.description + "</td><td align='center'><button class='edit-modal btn btn-info' data-id='" + data.id + "' data-name='" + data.name + "' data-description='" + data.description + "'  data-type='" + data.type+ "' ><span class='glyphicon glyphicon-edit'></span></button> <button class='delete-modal btn btn-danger delete_p'data-id='" + data.id + "' data-name='" + data.name + "' data-description='" + data.description + "'  data-type='" + data.type+ "'><span class='glyphicon glyphicon-trash'></span></button></td></tr>");
                        $('#myModalHorizontal').modal('hide');
                        $('#id').val($(this).data(''));
                        $('#name').val($(this).data(''));
                        $('#description').val($(this).data(''));
                        $('#type').val($(this).data(''));
                        $('#save-btn').show();
                        $('#footer').css('display','none');
                        $("#myModalHorizontal").modal('hide');

                    }
                });

            });




        });
    </script>
    <script>
        $(document).on("click", ".delete_p", function () {
            var did = $(this).data('id');
            $("#d_id").val(did);
            $('#myModal').modal('show');
        });

        $('.delete_product').click(function () {

            var _id = $("#d_id").val();
            var id = _id;
            var url = "delete";

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            })
            $.ajax({
                type: 'DELETE',
                url: url + '/' + id,
                success: function (data) {
                    console.log(id);
                    $(".item" + id).remove();
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });

        })
    </script>

    <script>
        $("#save-btn").click(function (e) {
            var isValid = true;
            $('input[type="text"].required').each(function () {
                if ($.trim($(this).val()) == '') {
                    isValid = false; /* Required class style */
                    $(this).css({
                        "border": "1px solid red",
                        "background": "#FFCECE"
                    });
                }
                else { /* Required class style removed */
                    $(this).css({
                        "border": "",
                        "background": ""
                    });
                }
            });
            if (isValid == false)
                e.preventDefault();
            else{

            }

        });
    </script>

@endsection