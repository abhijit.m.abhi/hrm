@extends('layouts.master')
@section('title', 'Public Holidays ')
@section('style')
    <style>
        .calendar-table
        {

        }
        .select2{
            width: 100% !important;
        }

    </style>
@endsection
@section('content')

    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>Public Holidays</h3>
                </div>


            </div>
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Public Holidays<!--<small>Input different form elements</small>--></h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                            @if(Session::has('message'))

                                <div id="successMessage" class="alert alert-success alert-dismissible fade in" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                                aria-hidden="true">×</span>
                                    </button>
                                    <strong>{{ Session::get('message') }}</strong>
                                </div>
                            @endif
                        </div>
                        {{--tab view--}}
                        <div class="bs-example">
                            <div class="tabbable">
                                <ul class="nav nav-tabs">
                                    {{--<li class="active"> <input type="button" data-toggle="tab" href="#tab1" data-loading-text="Loading..." class="btn btn-info" value="Loan"></li>--}}
                                    {{--<li> <input type="button" data-toggle="tab" href="#tab2" data-loading-text="Loading..." class="btn btn-info" value="  Employee Loan    "></li>--}}

                                </ul>
                                <div class="tab-content">
                                    <div id="tab1" class="tab-pane active fade in">
                                        {{--fade view for add skill--}}
                                        <button onclick="myFunction()" data-backdrop="ture" id="hello" class="btn btn-primary btn-md" data-type="Skills"  data-token="{{ csrf_token() }}"  data-toggle="modal" data-target="#myModalHorizontalkk"><span class="glyphicon glyphicon-plus">
                                                    Add A Public Holiday
                                                </span></button>


                                        <div class="x_content">
                                            <div class="table-responsive">
                                                <table id="datatable-buttons" class="Holidays table table-striped table-bordered table-hover display">
                                                    <thead>
                                                    <tr class="alert-info">
                                                        <th>Date</th>
                                                        <th>Title</th>
                                                        <th>Action</th>
                                                    </tr>

                                                    </thead>


                                                    <tbody>
                                                    @foreach($publicHoliday as $Pholiday)
                                                        <tr class='item{{$Pholiday->id}}' id="row_id{{$Pholiday->id}}">
                                                            <td>{{$Pholiday->date}}</td>
                                                            <td>{{$Pholiday->title}}</td>

                                                            <td align="center">
                                                                <button class="btn btn-primary btn-sm edit-modal-public_holiday" data-id="{{$Pholiday->id}}" data-date="{{$Pholiday->date}}" data-title="{{$Pholiday->title}}" ><span class="glyphicon glyphicon-edit"></span></button>
                                                                <button class="btn confirm btn-danger btn-sm delete_course"  data-id="{{$Pholiday->id}}" data-date="{{$Pholiday->date}}" data-title="{{$Pholiday->title}}" ><span class="glyphicon glyphicon-trash"></span></button>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            {{--Modal--}}
                                            <!-- /.modal -->
                                            </div>
                                        </div>
                                        <meta name="_token" content="{!! csrf_token() !!}"/>
                                        {{--end table view--}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



    {{--delete modal--}}
    <div class="modal fade" id="myModal" aria-labelledby="myModalLabel" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header alert-danger">
                    <button type="button" class="close" data-dismiss="modal"
                            aria-label="Close"><span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title"><span class="glyphicon glyphicon-trash"></span>
                        Confirmation Message</h4>
                </div>
                <div class="modal-body">
                    <p>Are you sure that you want to permanently delete the selected
                        element?</p>
                    <input type="hidden" id="d_id">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">No
                    </button>
                    <button type="button" class="btn btn-danger delete_holiday_btn delete_empLoan_btn"
                            data-dismiss="modal" value=""
                            data-token="{{ csrf_token() }}" data-dismiss="modal">Delete
                    </button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    {{--end delete modal--}}


    {{--add form modal --}}
    <div class="modal fade open_modal" id="myModalHorizontal" tabindex="-1" role="dialog"
         aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header">
                    <button type="button" class="close closeBtn"
                            data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                        <span class="sr-only">Close</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">
                        Add A Holiday
                    </h4>
                </div>

                <!-- Modal Body -->
                <div class="modal-body">
                    <div class="modal-body">

                        <form id="form" class="form-horizontal" role="form" data-parsley-validate>
                            <div class="form-group">
                                <label class="control-label col-md-4 col-sm-4 col-xs-12">Date</label>
                                <div class="col-md-6 col-sm-10 col-xs-12">
                                    <input type="text" class="form-control has-feedback-left dateTime" name="date" id="date" placeholder="Select Month" aria-describedby="inputSuccess2Status2" required>
                                    <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                                    <span id="inputSuccess2Status2" class="sr-only">(success)</span>
                                </div>
                            </div>

                            <div class="form-group">
                                <label  class="col-sm-2 control-label">Title</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control required"
                                           id="title" name="title" placeholder="Enter Title" required>
                                </div>
                            </div>


                            <div class="form-group">
                                <div class="col-sm-2 col-sm-10">
                                    <button type="button" id="save-btn-publicHoliday"   data-token="{{ csrf_token() }}"  class="btn btn-primary">Add</button>
                                </div>
                            </div>

                            <div class="form-group">

                                <div class="col-sm-10">
                                    <input type="hidden" class="form-control"
                                           id="id" name="id" placeholder="Enter Certification"/>
                                </div>
                            </div>

                            <div style="display:none" class="modal-footer" id="footer">
                                <button type="button" class="btn btn-default closeBtn"
                                        data-dismiss="modal">
                                    Close
                                </button>
                                <button type="button" class="btn btn-primary edit" id="edit-publicHoliday">
                                    Update
                                </button>
                            </div>

                        </form>
                    </div>

                    <meta name="_token" content="{!! csrf_token() !!}" />
                </div>
                <!-- Modal Footer -->
            </div>
        </div>
    </div>


@endsection
@section('script')

    <script>
        // validate a field on "blur" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':
        $('form')
            .on('blur', 'input[required], input.optional, select.required', validator.checkField)
            .on('change', 'select.required', validator.checkField)
            .on('keypress', 'input[required][pattern]', validator.keypress);

        $('.multi.required').on('keyup blur', 'input', function() {
            validator.checkField.apply($(this).siblings().last()[0]);
        });


        $('form').submit(function(e) {
            e.preventDefault();
            var submit = true;
            // evaluate the form using generic validaing
            if (!validator.checkAll($(this))) {
                submit = false;
            }

            if (submit)
                this.submit();
            return false;
        });
    </script>
    <script type="text/javascript">

        var handleDataTableButtons = function () {
            if ($("#datatable-buttons").length) {
                $("#datatable-buttons").DataTable({
                    dom: "Bfrtip",
                    buttons: [
                        {
                            extend: "copy",
                            className: "btn-sm"
                        },
                        {
                            extend: "csv",
                            className: "btn-sm"
                        },
                        {
                            extend: "excel",
                            className: "btn-sm"
                        },
                        {
                            extend: "pdfHtml5",
                            className: "btn-sm"
                        },
                        {
                            extend: "print",
                            className: "btn-sm"
                        },
                        {
                            extend: "colvis",
                            className: "btn-sm"
                        }
                    ],
                    responsive: false
                });
            }
        };

        TableManageButtons = function () {
            "use strict";
            return {
                init: function () {
                    handleDataTableButtons();
                }
            };
        }();

        $('#datatable').dataTable();

        $('#datatable-keytable').DataTable({
            keys: true
        });

        $('#datatable-responsive').DataTable();

        $('#datatable-scroller').DataTable({
            ajax: "js/datatables/json/scroller-demo.json",
            deferRender: true,
            scrollY: 380,
            scrollCollapse: true,
            scroller: true
        });

        $('#datatable-fixed-header').DataTable({
            fixedHeader: true
        });

        var $datatable = $('#datatable-checkbox');

        $datatable.dataTable({
            'order': [[1, 'asc']],
            'columnDefs': [
                {orderable: false, targets: [0]}
            ]
        });
        $datatable.on('draw.dt', function () {
            $('input').iCheck({
                checkboxClass: 'icheckbox_flat-green'
            });
        });

        TableManageButtons.init();

    </script>

    <script>
        $(document).ready(function(){
            $(".nav-tabs a").click(function(){
                $(this).button('loading').delay(500).queue(function(){
                    $(this).button('reset');
                    $(this).dequeue();
                });
            });
        });
    </script>

    <script>

        function  myFunction() {
            $("#myModalHorizontal").modal('show');
            //$('#footer').hide();
        }
    </script>

    <script>
        $('.closeBtn').click(function(){
            $('#date').val('');
            $('#title').val('');
            $('#footer').hide('');
            $('#save-btn-publicHoliday').show();
            $('input[type="text"],select.required').each(function () {
                if ($.trim($(this).val()) == '') {
                    $(this).css({
                        "border": "",
                        "background": ""
                    });
                }
            });
        });
    </script>

    <script>
        $("#save-btn-publicHoliday").click(function() {
            //  alert("hello");
            $.ajaxSetup ({
                // Disable caching of AJAX responses
                cache: false
            });
            $.ajax({
                type: 'get',
                url: 'add_public_holiday',
                data: {
                    '_token': $('input[name=_token]').val(),
                    'date' : $('#date').val(),
                    'title': $('#title').val()
                },
                success: function(data) {
                    console.log(data);

                    if ((data.errors)){
                        $('.error').removeClass('hidden');
                        $('.error').text(data.errors.name);
                    }
                    else {
                        $('.error').addClass('hidden');
                        $('.Holidays').append("<tr class='item" + data.id + "'><td>" + data.date + "</td><td>" + data.title+ "</td><td align='center'><button class='edit-modal-public_holiday btn btn-info' data-id='" + data.id + "' data-date='" + data.date + "'  data-title='" + data.title+ "' ><span class='glyphicon glyphicon-edit'></span></button> <button class='delete-modal btn btn-danger delete_course' data-id='" + data.id + "' data-date='" + data.date + "'  data-title='" + data.title+ "' ><span class='glyphicon glyphicon-trash'></span></button></td></tr>");
                        $("#myModalHorizontal").modal('hide');
                        $('#date').val('');
                        $('#title').val('');
                    }
                },
            });

        });

    </script>

    <script>
        $(document).on('click', '.edit-modal-public_holiday', function() {
            $('#id').val($(this).data('id'));
            $('#date').val($(this).data('date'));
            $('#title').val($(this).data('title'));
            $('#myModalHorizontal').modal('show');
            $('#save-btn-publicHoliday').hide();
            $('#footer').css('display','block');
            $('#name').css({
                "border": "",
                "background": ""
            });

            $('#edit-publicHoliday').click(function(){
                $.ajax({
                    type: 'get',
                    url: './edit_public_holiday',
                    data: {
                        '_token': $('input[name=_token]').val(),
                        'id': $("#id").val(),
                        'date' : $('#date').val(),
                        'title': $('#title').val()
                    },
                    success: function(data) {
                        $('.item' + data.id).replaceWith("<tr class='item" + data.id + "'><td>" + data.date+ "</td><td>" + data.title+ "</td><td align='center'><button class='edit-modal-public_holiday btn btn-info' data-id='" + data.id + "' data-date='" + data.date+ "'  data-title='" + data.title+ "' ><span class='glyphicon glyphicon-edit'></span></button> <button class='delete-modal btn btn-danger delete_course'data-id='" + data.id + "' data-date='" + data.date + "' data-title='" + data.title + "' ><span class='glyphicon glyphicon-trash'></span></button></td></tr>");
                        $('#myModalHorizontal').modal('hide');
                        $('#id').val($(this).data(''));
                        $('#date').val($(this).data(''));
                        $('#title').val($(this).data(''));

                        $('#save-btn-publicHoliday').show();
                        $('#footer').css('display','none');
                        //  $("#myModalHorizontal").modal('hide');
                    }
                });
            });
        });
    </script>
    <script>
        $(document).on("click", ".delete_course", function () {
            var did = $(this).data('id');
            $("#d_id").val(did);
            $('#myModal').modal('show');
        });
        $('.delete_holiday_btn').click(function () {
            var _id = $("#d_id").val();
            var id = _id;
            var url = "delete_public_holiday";
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            })
            $.ajax({
                type: 'DELETE',
                url: url + '/' + id,
                success: function (data) {
                    console.log(id);
                    $(".item" + id).remove();
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        })
    </script>

    <script>
        $("#save-btn-publicHoliday").click(function (e) {
            var isValid = true;
            $('input[type="text"].required').each(function () {
                if ($.trim($(this).val()) == '') {
                    isValid = false; /* Required class style */
                    $(this).css({
                        "border": "1px solid red",
                        "background": "#FFCECE"
                    });
                }
                else { /* Required class style removed */
                    $(this).css({
                        "border": "",
                        "background": ""
                    });
                }
            });
            if (isValid == false)
                e.preventDefault();
            else{
            }
        });
    </script>


    <script>
        $(".dateTime").datepicker( {
            format: "dd MM yyyy",
            autoclose: true
        });
    </script>
    <script type="text/javascript">

        $(".newTable").on("click", function () {
            var type = $(this).children('.fl').val();
            var id = $(this).children('.id').val();
            var url = "status_change";

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            });

            $.ajax({
                type: 'GET',
                url: url + '/' + id,
                success: function (data) {
                    console.log(data);

                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        });
    </script>



@endsection