<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class EmployeeTraningSession extends Model
{
    //
    public function emp_Training()
    {

       // return $this->belongsTo('App\EmployeePersonalDetails');
        return $this->belongsTo('App\EmployeePersonalDetail', 'employee_id');
    }
}
