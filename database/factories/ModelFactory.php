<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'flag' => '1',
        'fname' => $faker->firstName,
        'lname' => $faker->lastName,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'address' => 'Dhaka, Bangladesh',
//        'type' => $faker->randomElement($array = array('Admin','User')),
        'type' => 'Admin',
        'saved_by' => 0,
        'remember_token' => str_random(10),
        'api_token' => str_random(10),
    ];
});

$factory->define(App\JobDetailsSetup::class, function (Faker\Generator $faker) {

    return [
        'job_code' => $faker->numberBetween(10000,99000),
        'job_title' => $faker->jobTitle,
        'description' => $faker->sentence(3),
        'min_salary' => $faker->numberBetween(25000,30000),
        'max_salary' => $faker->numberBetween(85000,99000),
    ];
});

$factory->define(App\CompanyStructure::class, function (Faker\Generator $faker) {

    return [
        'name' => $faker->company,
        'details' => $faker->sentence(3),
        'address' => $faker->address,
        'department_type' => $faker->randomElement($array = array('Company','Department', 'Head Office', 'Regional Office', 'Sub Unit Office', 'Unit', 'Other')),
        'country' => $faker->country,
        'ip_address' => $faker->ipv4,
        'office_start' => '09:00:00',
        'office_end' => '17:00:00',
        'last_entry_time' => '09:30:00'
    ];
});

$factory->define(App\QualificationSetup::class, function (Faker\Generator $faker) {

    return [
        'name' => $faker->colorName,
        'description' => $faker->sentence(3),
        'type' => $faker->randomElement($array = array('Skills','Education', 'Certification')),
    ];
});
