@extends('layouts.master')
@section('title', 'Add Installment')
@section('content')
    <style>
        .header_center{
            text-align: center;
        }
    </style>

    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>Installment</h3>
                </div>


            </div>
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">


                            <h2>Set Employee Installment<!--<small>Input different form elements</small>--></h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <br/>
                            <form id="form" data-parsley-validate method="POST" class="form-horizontal" role="form"
                                  files="true">
                                {{ csrf_field() }}
                                <div class="form-body">
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Employee Name</label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <select class="select2_single form-control employeeName required" tabindex="-1" id="employeeId" name="employeeId" onchange="employeeData();return false;">
                                                <option value="">Select Employee</option>

                                                @foreach($employees as $emp)
                                                    <option value="{{$emp->id}}">{{$emp->first_name}} {{$emp->middle_name}} {{$emp->last_name}}</option>
                                                @endforeach

                                            </select>
                                        </div>
                                    </div>

                                    <div class="x_content" id="loan_table" style="display: none;">
                                        <div class="table-responsive">
                                            <table id="datatable-buttons" class="table table-striped table-bordered table-hover display">
                                                <thead>
                                                <tr class="alert-info">
                                                    <th class="header_center">Loan Type</th>
                                                    <th class="header_center">Loan Amount</th>
                                                    <th class="header_center">Paid Amount</th>
                                                    <th class="header_center">Loan Period</th>
                                                    {{--<th class="header_center">Status</th>--}}
                                                    <th class="header_center">Add Installment</th>
                                                </tr>
                                                </thead>
                                                <tbody class="loanData">
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>

                        {{--Installment--}}
                        <div class="x_content" id="instalment_table" style="display: none;">
                            <br/>
                            <form id="form" class="form-horizontal" role="form" data-parsley-validate>

                                <div class="x_content" >
                                    <div class="table-responsive">
                                        <table id="datatable-buttons" class="Installments table table-striped table-bordered table-hover display">
                                            <thead>
                                            <tr class="alert-info">
                                                <th class="header_center">SI. No.</th>
                                                <th class="header_center">Date</th>
                                                <th class="header_center">Installment Amount</th>
                                                <th class="header_center">Action</th>
                                            </tr>
                                            </thead>
                                            <tbody class="installmentData">
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div id="installmentAdd">
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Date</label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="text" class="form-control has-feedback-left dateTime" name="date" id="date" value="<?php echo date("d F Y");?>" placeholder="Select Month" aria-describedby="inputSuccess2Status2">
                                            <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                                            <span id="inputSuccess2Status2" class="sr-only">(success)</span>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Installment Amount</label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="number" class="form-control required"
                                                   id="amount" name="amount" placeholder="Enter Amount" readonly>
                                        </div>
                                    </div>

                                    <input type="hidden" name="i_id" id="i_id">
                                    <input type="hidden" name="period_count" id="period_count">

                                    <div class="ln_solid"></div>
                                    <div class="form-group">
                                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                            <a type="cancel" href="{{ url('dashboard') }}" class="btn btn-danger">Cancel</a>
                                            <button type="button" id="save-btn-installment"   data-token="{{ csrf_token() }}"  class="btn btn-primary">Add Installment</button>
                                        </div>
                                    </div>
                                </div>


                            </form>


                        </div>

                    {{--delete modal--}}

                    <div class="modal fade" id="myModal" aria-labelledby="myModalLabel" role="dialog">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header alert-danger">
                                    <button type="button" class="close" data-dismiss="modal"
                                            aria-label="Close"><span aria-hidden="true">&times;</span>
                                    </button>
                                    <h4 class="modal-title"><span class="glyphicon glyphicon-trash"></span>
                                        Confirmation Message</h4>
                                </div>
                                <div class="modal-body">
                                    <p>Are you sure that you want to permanently delete the selected
                                        element?</p>
                                    <input type="hidden" name="mid" id="mid">
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-warning" data-dismiss="modal">No
                                    </button>
                                    <button type="button" class="btn btn-danger delete_course_btn delete_LoanInstallment_btn"
                                            data-dismiss="modal" value="" data-token="{{ csrf_token() }}" data-dismiss="modal">Delete
                                    </button>
                                </div>
                            </div><!-- /.modal-content -->
                        </div><!-- /.modal-dialog -->
                    </div><!-- /.modal -->

                    {{--end delete modal--}}


                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        // validate a field on "blur" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':
        $('form')
            .on('blur', 'input[required], input.optional, select.required', validator.checkField)
            .on('change', 'select.required', validator.checkField)
            .on('keypress', 'input[required][pattern]', validator.keypress);

        $('.multi.required').on('keyup blur', 'input', function() {
            validator.checkField.apply($(this).siblings().last()[0]);
        });

        $('form').submit(function(e) {
            e.preventDefault();
            var submit = true;

            // evaluate the form using generic validaing
            if (!validator.checkAll($(this))) {
                submit = false;
            }

            if (submit)
                this.submit();

            return false;
        });

    </script>

    <script>
        $(document).ready(function () {
            $(".dateTime").datepicker( {
                format: "dd MM yyyy",
                autoclose: true
            });
        });

    </script>

    <script>
        $(document).ready(function() {
            $(".select2_single").select2({
                placeholder: "Select a state",
                allowClear: true
            });

        });
    </script>

    <script>

        function employeeData() {
            var employeeId = $('#employeeId').val();
           // alert(employeeId);
            $('.loanData').html("");
            $('#instalment_table').hide();
            $('#loan_table').show();

            var url = "../employee-loan";
//            alert(id);

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            })
            $.ajax({
                type: 'GET',
                url: url + '/' + employeeId,
                success: function (data) {
                    var i;
                    for (i = 0; i < data.length; ++i) {
                        $('.loanData').append("<tr class='item'>" +
                            "<td style='display: none;'>"+data[i].id+"</td>" +
                            "<td>"+data[i].loan_type+"</td>" +
                            "<td align='right'>"+data[i].loan_amount+"</td>" +
                            "<td align='right'>"+data[i].paid_amount+"</td>" +
                            "<td align='center'>"+data[i].loan_period+"</td>" +
//                            "<td align='center'>"+data[i].status+"</td>" +
                            "<td align='center'>" +
                            "<button type='button' class='btn btn-primary' id='loanId' onclick='installmentTbl(" + data[i].id + "," + data[i].loan_period + "," + data[i].monthly_installment + ")' data-id='" + data[i].id + "' data-monthly_installment='" + data[i].monthly_installment + "'>" +
                            "<span class='glyphicon glyphicon-edit'></span>" +
                            "</button>" +
                            "</td>" +
                            "</tr>");
                    }
//                    $('.error').addClass('hidden');
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        }
    </script>

    <script>
        function installmentTbl(id,loan_period,monthly_installment) {
            var loanId = id;
            var count = loan_period;
            $("#i_id").val(loanId);
            $('.installmentData').html("");
            $('#amount').val(monthly_installment);
            $('#period_count').val(count);
            $('#instalment_table').show();

            var url = "../loan-installment";
//            alert(loanId);

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            })
            $.ajax({
                type: 'GET',
                url: url + '/' + loanId,
                success: function (data) {
                    var i;
                    for (i = 0; i < data.length; ++i) {
                        $('.installmentData').append("<tr id='item11" + data[i].id + "'>" +
                            "<td style='display: none;'>"+data[i].id+"</td>" +
                            "<td align='center'>"+data[i].count_installment+"</td>" +
                            "<td align='center'>"+data[i].date+"</td>" +
                            "<td align='center'>"+data[i].amount+"</td>" +
                            "<td align='center' class='action_type'>"+
//                            "<button type='button' class='btn confirm btn-danger btn-sm delete_loanInstallment'  " +
//                            "data-id='"+data[i].id+"' data-date='"+data[i].date+"' data-amount='"+data[i].amount+"'  ><span class='glyphicon glyphicon-trash'></span></button>"+
                            "</td>" +
                            "</tr>");
                            if(data[i].count_installment== count){
                                $('#installmentAdd').hide();
                                $('.action_type').html("Installment Complete");
                            }
                            else{
                                $('#installmentAdd').show();
                                $('.action_type').html("");
                            }
                    }

//                    $('.error').addClass('hidden');
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        }
    </script>


    <script>
        $("#save-btn-installment").click(function() {
            var count =  $('#period_count').val();
//              alert("hello");
            $.ajaxSetup ({
                // Disable caching of AJAX responses
                cache: false
            });
            $.ajax({
                type: 'get',
                url: '../addInstallment',
                data: {
                    '_token': $('input[name=_token]').val(),
                    'id' : $('#i_id').val(),
                    'date' : $('#date').val(),
                    'amount': $('#amount').val()
                },
                success: function(data) {
                    console.log(data);

                    if ((data.errors)){
                        $('.error').removeClass('hidden');
                        $('.error').text(data.errors.name);
                    }
                    else {
                        $('.error').addClass('hidden');
                        $('.Installments').append("<tr id='item11" + data.id + "'>" +
                            "<td align='center'>" + data.count_installment + "</td>" +
                            "<td align='center'>" + data.date+ "</td>" +
                            "<td align='center'>" + data.amount+ "</td>" +
                            "<td align='center'>"+
                            "<button type='button' class='btn confirm btn-danger btn-sm delete_loanInstallment'  " +
                            "data-id='"+data.id+"' data-count_installment='"+data.count_installment+"' data-date='"+data.date+"' data-amount='"+data.amount+"'  ><span class='glyphicon glyphicon-trash'></span></button>"+
                            "</td>" +
                            "</tr>");
                        if(data.count_installment== count){
                            $('#installmentAdd').hide();
                            $('.action_type').html("Installment Complete");
                        }
                        else{
                            $('#installmentAdd').show();
                            $('.action_type').html("");
                        }
//                        $('#date').val('');

                    }
                },
            });

        });

    </script>

    <script>
        $(document).on("click", ".delete_loanInstallment", function () {
            var did = $(this).data('id');
            //alert(did);
            $("#mid").val(did);
            $('#myModal').modal('show');
        });

        $('.delete_LoanInstallment_btn').click(function () {
            var _id = $('#mid').val();
           // alert(_id);
          var id = _id;
            var url = "../delete_LoanInstallment";
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            });
            $.ajax({
                type: 'DELETE',
                url: url + '/' + id,
                data: {
                    '_token': $('input[name=_token]').val()
                },
                success: function (data) {
                    console.log("#item11" + id);
                    $("#item11" + id).remove();
                    $('#installmentAdd').show();
                    $('.action_type').html("");
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });

        })
    </script>



@endsection