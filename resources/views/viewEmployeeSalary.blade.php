@extends('layouts.master')
@section('title', 'Employee Salary')
@section('content')

    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>Employee Salary</h3>
                </div>


            </div>
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Set Employee Salary<!--<small>Input different form elements</small>--></h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">

                            <div class="table-responsive">
                                <table id="Hourly"  class="empSalary table table-striped table-bordered table-hover display" >
                                    <thead>
                                    <tr class="alert-info">
                                        <th>Salary Grade</th>
                                        <th>Employee Name</th>
                                        <th>Salary Type</th>
                                        <th>Salary</th>

                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tfoot>
                                    <tr>

                                        <th>Total:</th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>


                                    </tr>
                                    </tfoot>

                                    <tbody>
                                    @foreach($salary as $salaryInfo)

                                        <tr id="EmpSalary{{$salaryInfo->id}}">
                                            <td>{{$salaryInfo->salary_grade}}</td>
                                            <td>{{$salaryInfo->emp_salary->first_name}} {{$salaryInfo->emp_salary->middle_name}} {{$salaryInfo->emp_salary->last_name}}</td>
                                            <td>{{$salaryInfo->salary_type}}</td>

                                            <?php
                                                $salary = 0;
                                            $var = $salaryInfo->salary_type;
                                            if($var === 'Monthly'){
                                                $salary = $salaryInfo->net_salary;
                                            }
                                            else if($var === 'Hourly'){
                                                $salary = $salaryInfo->hourly_rate;
                                            }
                                            else if($var === 'Daily'){
                                                $salary = $salaryInfo->daily_rate;
                                            }

                                            ?>
                                            <td><?php echo $salary;?></td>





                                            <td align="center">
                                                <a href="{{url('view_details/'. $salaryInfo->id)}}" class="btn btn-primary"><span class="glyphicon glyphicon-eye-open"></span></a>

                                                <a href="{{route('set_employee_salary.edit', $salaryInfo->id)}}" class="btn btn-primary"><span class="glyphicon glyphicon-edit"></span></a>



                                                <button class="btn confirm btn-danger delete_salary"  data-toggle="modal" data-id="{{$salaryInfo->id}}" ><span class="glyphicon glyphicon-trash"></span></button>

                                            </td>
                                        </tr>


                                    @endforeach
                                    <meta name="_token" content="{!! csrf_token() !!}" />

                                    </tbody>
                                </table>
                            </div>

                            <div class="modal fade" tabindex="-1" id="myModal" aria-labelledby="myModalLabel" role="dialog">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            <h3 class="modal-title ">Delete Confirmation </h3>
                                        </div>
                                        <div class="modal-body ">
                                            <p><b>Do you want to delete?&hellip;</b></p>
                                            <input type="hidden" id="d_id">
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default warning" data-dismiss="modal">Close</button>
                                            <button type="button " class="btn btn-primary delete_confirm btn-danger" data-dismiss="modal" value="" data-token="{{ csrf_token() }}"data-dismiss="modal">Delete</button>
                                        </div>
                                    </div><!-- /.modal-content -->
                                </div><!-- /.modal-dialog -->
                            </div>




                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <script type="text/javascript">

        $(document).on("click", ".delete_salary", function() {
            var did = $(this).data('id');
            $("#d_id").val(did);
            $('#myModal').modal('show');
        });
        var url = "deleteEmployeeSalary";

        $('.delete_confirm').click(function(){
            var _id =  $("#d_id").val();

            var id = _id;

            $.ajaxSetup({
                headers : {
                    'X-CSRF-TOKEN' : $('meta[name="_token"]').attr('content')
                }

            })
            $.ajax({
                type : 'GET',
                url : url + '/' + id,
                success: function(data){
                    console.log(id);

                    $("#EmpSalary" + id).remove();
                },
                error:function(data){
                    console.log('Error:',data);
                }

            });
        });

    </script>


@endsection