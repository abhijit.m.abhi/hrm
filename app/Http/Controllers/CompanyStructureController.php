<?php

namespace App\Http\Controllers;

use App\CompanyStructure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class CompanyStructureController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $structureList = CompanyStructure::all();
        return view('companyStructure', ['structureList' => $structureList]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('addCompanyStructure');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $structure = new CompanyStructure();

        $structure->name=$request->input('name');
        $structure->details=$request->input('details');
        $structure->address=$request->input('address');
        $structure->department_type=$request->input('department_type');
        $structure->country=$request->input('country');
        $structure->ip_address=$request->input('ip_address');
        $structure->office_start = $request->input('office_start_time');
        $structure->last_entry_time = $request->input('last_entry_time');
        $structure->office_end = $request->input('office_end_time');
        if ($structure->save()) {

            Session::flash('message', 'Company Structure Created Successfully!');

        };
        return redirect('/company_structure');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
       /* $userData = User::findOrFail($id);
        return view('editUser', ['user' => $userData]);*/


        $structureList = CompanyStructure::findOrFail($id);
        return view('editCompanyStructure', ['structureList' => $structureList]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        $structureList = CompanyStructure::findOrFail($id);
        $structureList->name = $request->input('name');
        $structureList->details = $request->input('details');
        $structureList->address = $request->input('address');
        $structureList->department_type = $request->input('department_type');
        $structureList->country = $request->input('country');
        $structureList->ip_address=$request->input('ip_address');
        $structureList->office_start = $request->input('office_start_time');
        $structureList->last_entry_time = $request->input('last_entry_time');
        $structureList->office_end = $request->input('office_end_time');

        if ($structureList->update()) {

            Session::flash('message', 'Information Updated Successfully !');
            return redirect('company_structure');
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $list = CompanyStructure::destroy($id);
    }
}
