@extends('layouts.master')
@section('title', 'Employee Resign List')
@section('content')

    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>View Employee Resign List</h3>
                </div>


            </div>
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Employee Resign List<!--<small>Input different form elements</small>--></h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">


                            <div class="table-responsive">
                                <table id="Hourly"  class="empSalary table table-striped table-bordered table-hover display" >
                                    <thead>
                                    <tr class="alert-info">
                                        <th>Name</th>
                                        <th>Title</th>
                                        <th>Date</th>
                                        <th>Total</th>
                                        <th>Attachment</th>


                                        <th align="center">Action</th>
                                    </tr>
                                    </thead>

                                    <tbody>
                                    @foreach($bill as $bill_info)

                                        <tr id="EmpBill{{$bill_info->id}}">
                                            <td>{{$bill_info->emp_bill->first_name}} {{$bill_info->emp_bill->last_name}}</td>
                                            <td>{{$bill_info->title}}</td>
                                            <td>{{$bill_info->date}}</td>
                                            <td>{{$bill_info->total_bill}}</td>

                                            <td> <a href="{{asset('/files/'.$bill_info->attachment)}}">
                                                    {{$bill_info->attachment}}</a></td>



                                            <td align="center">
                                                <div class="btn-group c_button">
                                                    <button type="button" data-value="{{$bill_info->id}}"
                                                            class="btn btn-primary t_button{{$bill_info->id}}">
                                                        <?php

                                                        $user_id =$bill_info->id;
                                                        if($bill_info->flag == 0)
                                                        {
                                                            echo "Pending";
                                                        }
                                                        else if($bill_info->flag == 1)
                                                        {
                                                            echo "Accepted";
                                                        }
                                                        else if($bill_info->flag == 2)
                                                        {
                                                            echo "Rejected";
                                                        }


                                                        ?>
                                                    </button>
                                                    <button type="button" class="btn btn-primary dropdown-toggle"
                                                            data-toggle="dropdown" aria-haspopup="true"
                                                            aria-expanded="false">
                                                        <span class="caret"></span>
                                                        <span class="sr-only">Toggle Dropdown</span>
                                                    </button>
                                                    <ul class="dropdown-menu change_d">
                                                        <li><a href="#" class="ad"
                                                               data-value="Accepted~{{$bill_info->id}}~{{$bill_info->title}}~{{$bill_info->total_bill}}~{{$bill_info->employee_id}}">Accepted</a></li>
                                                        <li><a href="#" class="ad"
                                                               data-value="Rejected~{{$bill_info->id}}~{{$bill_info->title}}~{{$bill_info->total_bill}}~{{$bill_info->employee_id}}">Rejected</a></li>
                                                    </ul>

                                                    <button class="btn confirm btn-danger delete_p"  data-toggle="modal" data-id="{{$bill_info->id}}" data-attachment="{{$bill_info->attachment}}"><span class="glyphicon glyphicon-trash"></span></button>
                                                </div>
                                            </td>
                                        </tr>


                                    @endforeach
                                    <meta name="_token" content="{!! csrf_token() !!}" />

                                    </tbody>
                                </table>
                            </div>

                            <div class="modal fade" tabindex="-1" id="myModal" aria-labelledby="myModalLabel" role="dialog">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            <h3 class="modal-title ">Delete Confirmation </h3>
                                        </div>
                                        <div class="modal-body ">
                                            <p><b>Do you want to delete?&hellip;</b></p>
                                            <input type="text" id="d_id">
                                            <input type="text" id="attach_file">
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default warning" data-dismiss="modal">Close</button>
                                            <button type="button " class="btn btn-primary delete_confirm btn-danger" data-dismiss="modal" value="" data-token="{{ csrf_token() }}"data-dismiss="modal">Delete</button>
                                        </div>
                                    </div><!-- /.modal-content -->
                                </div><!-- /.modal-dialog -->
                            </div>



                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <script>
        jQuery(".ad").click(function (e) {
            e.preventDefault();


            var bill_decision = $(this).data('value');
            var fields = bill_decision.split(/~/);
            var decision = fields[0];
            var id_value = fields[1];
            var title_name = fields[2];
            var total_bill = fields[3];
            var emp_id = fields[4];

            //alert(emp_id);

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            });

            $.ajax({
                type: 'get',
                url: 'bill_decision',
                data: {
                    '_token': $('input[name=_token]').val(),
                    'id': id_value,
                    'title_name':title_name,
                    'decision':decision,
                    'total_bill':total_bill,
                    'emp_id':emp_id


                },
                success: function(data) {
                    console.log(data);

                    var flag = data.flag;
                    if(flag == 1){
                        $(".t_button" + id_value).html('Accepted');
                    }
                    else if(flag == 2){
                        $(".t_button" + id_value).html('Rejected');
                    }
                    // $(".t_button" + userId).html(data);

                },
            });


        })

    </script>


    <script type="text/javascript">

        $(document).on("click", ".delete_p", function() {
            var did = $(this).data('id');
            var attachment = $(this).data('attachment');

            $("#d_id").val(did);
            $("#attach_file").val(attachment);
            $('#myModal').modal('show');
        });
        var url = "adminDeleteBill";

        $('.delete_confirm').click(function(){
            var _id =  $("#d_id").val();
            var id = _id;
            var file = $("#attach_file").val();

            $.ajaxSetup({
                headers : {
                    'X-CSRF-TOKEN' : $('meta[name="_token"]').attr('content')
                }

            })
            $.ajax({
                type : 'GET',
                url : url,
                data: {
                    '_token': $('input[name=_token]').val(),
                    'id': id,
                    'file': file,

                },

                success: function(data){
                    console.log(id);

                    $("#EmpBill" + id).remove();
                },
                error:function(data){
                    console.log('Error:',data);
                }

            });
        });

    </script>





@endsection